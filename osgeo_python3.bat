@echo off
SETLOCAL
REM This file is intended to identify likely locations of osgeo4w
REM at runtime

if exist "C:\Program Files\QGIS 3.8\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.8\"
) else if exist "C:\Program Files\QGIS 3.6\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.6\"
) else if exist "C:\Program Files\QGIS 3.4\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.4\"
) else if exist "C:\Program Files\QGIS 3.2\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.2\"
) else if exist "C:\Program Files (x86)\QGIS 3.2\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files (x86)\QGIS 3.2\"
) else if exist "C:\Program Files\QGIS 3.0\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 3.0\"
) else if exist "C:\Program Files (x86)\QGIS 3.0\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files (x86)\QGIS 3.0\"
) else if exist "D:\OSGeo4w\OSGeo4W.bat" (
set OSGEO4W_ENV="D:\OSGeo4w\"
) else if exist "C:\OSGeo4W\osgeo4w.bat" (
set OSGEO4W_ENV="C:\OSGeo4W"
) else if exist "C:\OSGeo4W64\osgeo4w.bat" (
set OSGEO4W_ENV="C:\OSGeo4W64"
) else if exist "C:\Program Files\QGIS 2.18\osgeo4w.bat" (
set OSGEO4W_ENV="C:\Program Files\QGIS 2.18"
)

::
echo Found OSGeo Instance %OSGEO4W_ENV%
pushd %OSGEO4W_ENV%
::call "%OSGEO4W_ENV%bin\o4w_env.bat"
REM Make parent of this script location our current directory,
REM converting UNC path to drive letter if needed
::pushd %~dp0
::cd ..

::REM set OSGEO4W_ROOT to short path version
for %%i in ( %OSGEO4W_ENV% ) do set OSGEO4W_ROOT=%%~fsi

REM start with clean path
set path=%OSGEO4W_ROOT%\bin;%WINDIR%\system32;%WINDIR%;%WINDIR%\system32\WBem

for %%f in ("%OSGEO4W_ROOT%\etc\ini\*.bat") do call "%%f"


::call "%OSGEO4W_ENV%bin\py3_env.bat"
::
SET PYTHONPATH=%OSGEO4W_ROOT%\apps\qgis\python
SET PYTHONHOME=%OSGEO4W_ROOT%\apps\Python37

PATH %OSGEO4W_ROOT%\bin;%OSGEO4W_ROOT%\apps\qgis\bin;%OSGEO4W_ROOT%\apps\Python37;%OSGEO4W_ROOT%\apps\Python37\Scripts;%PATH%

::::::::::::::::::::::::::::::::::::::::::::

popd

rem List available o4w programs
rem but only if osgeo4w called without parameters
@echo off
echo %PATH%
@if [%1]==[] (
	echo "OSGeo (%%OSGEO4W_ENV%%) is %OSGEO4W_ENV%" & cmd.exe /k
) else (
	cmd /c %OSGEO4W_ROOT%\bin\python3.exe %*
)


ENDLOCAL
TIMEOUT /T 90
EXIT /B 0
