#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  ar_consolidator.py
#
#  Copyright 2019 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
from tape_handler import tape_handler
import os
import sys
import datetime

from osgeo import gdal
from osgeo import ogr
from osgeo import osr

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import mm_processor.build_empty_dataset
import stack.choose_a_tech_spec

class ar_consolidator(tape_handler):
	""" Consolidates the ARs from a particular unit """
	eType = 'fi_portal_imported'
	successEvent = "ar_consolidated"
	channel = eType # event Type is used for the channel name... :-)
	jobType = 'fi_portal_ar_consolidator'
	exclusive_job = False

	#staletime = '60 minutes'
	staletime = '2 minutes'
	#backlog_frequency = 60
	backlog_frequency = 3
	#timeout = 60 # seconds
	timeout = 1 # seconds
	#This makes the default to be run a backlog every hour, and handle
	# interstitial events every minute.

	year_field = 'Submission_Year'
	jsonfilename=stack.choose_a_tech_spec.choose_a_tech_spec('AR',2018)

	def __init__(self,d):
		#print "__init__"
		tape_handler.__init__(self,d)
		#return self.self.d['subid']
		try:
			#print self.d
			self.go( self.getEvent_FromSubmission(self.d['auto_subid']) )
		except KeyError as e:
			pass


	def go(self, event=None):
		"""
		Run the AR Consolidator
		"""

		#print ( event )

		payload = event['payload']
		subid = payload['fi_portal_id']
		sink_dir = os.path.dirname(payload['sink_dir'])

		#print ("Running the AR Consolidator\n\tfi_portal_id from Payload: {0}".format(subid))
		d = self.getforestproductyear(subid)
		if d['product'] != 'AR': return self.status("Success") # This is only for AR's....

		d['date'] = datetime.date.today()
		src_gpkg = payload['sink_gpkg']

		if not os.path.exists(src_gpkg):
			return self.status('Cannot Find Input Files')
		#Create the sink dir...
		if not os.path.exists(os.path.join(sink_dir,'CONSOLIDATED') ):
			os.makedirs(os.path.join(sink_dir,'CONSOLIDATED') )
		sink_gpkg= os.path.join(os.path.join(sink_dir,'CONSOLIDATED'),'Consolidated_AR.gpkg')

		year = d['year']
		planyear = d['planyear']

		# Get the SRS from the source layers
		src_ds = ogr.Open(src_gpkg)
		for l in src_ds:
			src_srs = l.GetSpatialRef()
			if src_srs is not None:
				break

		# Does sink_dir\AR.GPKG exist? If so, then use it...
		if os.path.exists(sink_gpkg):
			print "Sink Exists - using it... {0}".format(sink_gpkg)
		else:
			print "Sink NOT Exists - Creating it and adding a metadata table"
			e = mm_processor.build_empty_dataset.empty_dataset_builder(jsonfilename = self.jsonfilename, outputfilename = sink_gpkg,
				use_fim_compliant_names=False,
				srs=src_srs , year=year, planyear=planyear, use_multi=True
			)
			for t in e.getTableList():
				e.addfield(t,self.year_field,'integer')
		try:
			sink_ds = ogr.Open(sink_gpkg,update=1)
		except RuntimeError as e:
			print (e)
			if 'database is locked' in str(e) :
				return self.status("Error Opening sink_ds for update- database is locked")
			else :
				return self.status("Error Opening sink_ds for update")


		# Remove this year's AR from the consolidated files...
		for sink_layer in sink_ds:
			sink_layer.SetAttributeFilter("{year_field} = {year}".format(year_field=self.year_field,year=year))
			if sink_layer.GetFeatureCount() > 0:
				print "Removing {year} data from {layer}".format(year=year,layer=sink_layer.GetName())
				for feat in sink_layer:
					sink_layer.DeleteFeature(feat.GetFID())
			sink_layer.SyncToDisk()
			sink_layer.SetAttributeFilter(None)

		for sink_layer in sink_ds :
			src_layer = src_ds.GetLayerByName( sink_layer.GetName() )
			if src_layer is None: continue

			if src_layer.GetFeatureCount() == 0:
				#print ( "\tAborting... No Features" )
				continue

			print "Copying {year} data in {layer} -> {count}".format(year=year,layer=sink_layer.GetName(), count=src_layer.GetFeatureCount())

			src_layerDefinition  = src_layer.GetLayerDefn()
			sink_layerDefinition = sink_layer.GetLayerDefn()
			dst_geom = sink_layerDefinition.GetGeomType()

			dst_fields = [ sink_layerDefinition.GetFieldDefn(i).GetName() for i in range(sink_layerDefinition.GetFieldCount()) ]

			#
			# Append the datafile rows
			#
			i=0
			for infeature in src_layer:
				#if i > 10: 	break
				i=i+1
				#Build Row

				row = dict([ (src_layerDefinition.GetFieldDefn(i).GetName(),infeature.GetField(i) ) for i in range(src_layerDefinition.GetFieldCount()) ])
				geom = infeature.GetGeometryRef()

				outFeature = ogr.Feature(sink_layerDefinition)
				outFeature.SetField(self.year_field, year )

				for fname,fvalue in row.iteritems():
					if fname not in dst_fields: continue
					try:
						outFeature.SetField(fname, fvalue )
					except NotImplementedError:
						# This happens with the ArcIDs field... it's a list of id's...
						if isinstance(fvalue,list):
							outFeature.SetField(fname, str(fvalue) )

				if geom:
					# For consolidation, we are coercing to Multi always
					outFeature.SetGeometry(ogr.ForceTo(geom.GetLinearGeometry(), dst_geom ))

				# Add new feature to output Layer
				try:
					sink_layer.CreateFeature(outFeature)
				except RuntimeError as e:
					print e
					print ( "Failed row is {0}".format(row))
					print ( "src_geom is {0}".format(str(geom)))
					return self.status("Row Failure")

				outFeature = None

			if sink_layer:
				sink_layer.SyncToDisk()

			src_layer = None
		src_ds = None




		# If we got here, we must be doing something right...
		new_event_payload ={ 'fi_portal_id': subid, 'src_gpkg':src_gpkg, 'filename':sink_gpkg, 'status':0 }
		#print new_event_payload
		self.generateEvent(new_event_payload)
		return self.status("Success")















#=======================


	def copy_layers(self,src_gpkg, dst_gpkg,year_field='submission_year',year=999, ):
		results = dict()
		try:
			src_ds = ogr.Open(filename)
		except RuntimeError as e:
			print e
			return ("Could not open Deferred Layer files {0}".format(filename))
		if not src_ds:
			print ("Could not open Deferred Layer files {0}".format(filename))
			return ("Could not open Deferred Layer files {0}".format(filename))
		for src_layer in src_ds:
			#src_layer = src_ds.GetLayer(layername)
			self.get_srs_from_file(src_layer)
			#print self.layerdescription(src_layer)

			layerDefinition = src_layer.GetLayerDefn()
			print ("Processing {0} Layer".format(layerDefinition.GetName()[8:11]))

			t = None
			t= self.findTable(layerDefinition.GetName())
			if t is None:
				try:
					t = self.findTable(self.getTableListMatchingString(layerDefinition.GetName())[0])
					dst_fieldNames = [f['name'].lower() for f in  t['fields']]
				except IndexError:
					t = { 'name':layerDefinition.GetName() , 'fields':[] }
					dst_fieldNames = [ ]


			geom_type = src_layer.GetGeomType()
				#geom_type_LUT[t['geometry_type']  ]

			#Report on concordance of file geometry type with table geometry type
			#print ("Src_Layer geom type {0}".format(src_layer.GetGeomType()))
			#print ("DST_Layer geom type {0}".format(geom_type_LUT[t['geometry_type']  ]))


			# if the data file is open (self.ds) then we have a geopackage
			if self.ds is not None:
				# Create Layer (Fields from mindmap spec)
				dst_geom = src_layer.GetGeomType()
				#Promote to multi types for Shapefiles...
				promote_to_multi = False
				#print ("src_driver is of type {0}".format(src_ds.GetDriver().GetName()))
				if src_ds.GetDriver().GetName() == "ESRI Shapefile":
					if dst_geom in [ 1,2,3 ]:   # wkbPoint = 1,wkbLineString = 2,wkbPolygon = 3
						dst_geom = dst_geom + 3 # wkbMultiPoint = 4,wkbMultiLineString = 5,wkbMultiPolygon = 6,
						promote_to_multi = True
				#print ("dst_geom is of type {0}".format(dst_geom))
				dst_layer = self.create_layer(layerdesc=t, geom_type=dst_geom)

				# Add Fields from datafile
				for i in range(layerDefinition.GetFieldCount()):
					if layerDefinition.GetFieldDefn(i).GetName().lower() not in dst_fieldNames:
						# Add this field to the gpkg
						try:
							dst_layer.CreateField(layerDefinition.GetFieldDefn(i))
						except RuntimeError:
							pass # This happens when the field already exists

				dst_LayerDefn = dst_layer.GetLayerDefn()
				results['srcLayer_'+layerDefinition.GetName()] = [ 'GPKG_' + dst_LayerDefn.GetName() ]


			#
			# Append the datafile rows
			#
			i=0
			for infeature in src_layer:
				#if i > 10:
				#	return 0
				#else :
				#	i=i+1
				i=i+1
				#Build Row
				row = dict([ (layerDefinition.GetFieldDefn(i).GetName(),infeature.GetField(i) ) for i in range(layerDefinition.GetFieldCount()) ])
				geom = infeature.GetGeometryRef()

				#If we are doing the geopackage
				if self.ds is not None:
					outFeature = ogr.Feature(dst_LayerDefn)

					for fname,fvalue in row.iteritems():
						try:
							outFeature.SetField(fname, fvalue )
						except NotImplementedError:
							# This happens with the ArcIDs field... it's a list of id's...
							if isinstance(fvalue,list):
								outFeature.SetField(fname, str(fvalue) )
					if geom:
						if promote_to_multi:
							outFeature.SetGeometry(ogr.ForceTo(geom.GetLinearGeometry(), dst_geom ))
						else :
							outFeature.SetGeometry(geom.GetLinearGeometry())

					# Add new feature to output Layer
					try:
						dst_layer.CreateFeature(outFeature)
					except RuntimeError as e:
						print e
						print ( "Failed row is {0}".format(row))
						print ( "src_geom is {0}".format(str(geom)))

					outFeature = None

				results['srcLayer_'+layerDefinition.GetName()+'_rowcount'] = i

		src_layer = None
		src_ds = None

		if dst_layer:
			dst_layer.SyncToDisk()

		return results

if __name__ == '__main__':
	t = ar_consolidator({})
	t.interpret_command_line(sys.argv)
