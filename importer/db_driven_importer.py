#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  db_driven_importer.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import sys
from osgeo import gdal
from osgeo import ogr
from osgeo import osr

import zipfile
from itertools import chain

import shutil
import fnmatch
import tempfile

import subprocess

import json

import socket

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from importer import importer_conductor
from stack.pg_db import pg_db


qry_for_all_downloaded_but_unimported_elements = """WITH
	latest_only as (
	SELECT * FROM (
		SELECT
		s.submissionid as submissionid,
		max(s.submissionid) OVER (PARTITION BY s.managementunitdistrict, s.product,s.fiscalyear) as latest_submissionid
		FROM fi_portal_scraper.fiportal_scrape s ) i
	WHERE latest_submissionid = submissionid
	),
	q as (
		SELECT
		s.submissionid as submissionid,
		s.managementunitdistrict,
		CASE WHEN position('Forest' in managementunitdistrict) = 0 THEN
			replace( trim( trailing '0123456789 -' FROM s.managementunitdistrict ),' ','_' )
		ELSE
			replace(left( trim( trailing '0123456789 -' FROM s.managementunitdistrict ), -7 ),' ','_' )
		END as forest ,
		trim(right(s.managementunitdistrict,4)) as fmuid,
		s.planperiod as planperiod, s.planterm as planterm,
		CASE
			WHEN s.product = 'Annual Report' THEN 'AR'
			WHEN s.product = 'Annual Work Schedule' THEN 'AWS'
			WHEN s.product = 'Final Plan' THEN 'FMP'
			WHEN s.product = 'Draft Plan' THEN 'FMP'
			ELSE upper(s.product)
		END AS product,
		s.fiscalyear ,
		CASE WHEN left(s.fiscalyear,4) = '' THEN left(planperiod,4)
		ELSE left(s.fiscalyear , 4 )
		END as year,
		left(s.planperiod,4) as planyear,
		f.filenamepath as filename,
		specfile
		FROM fi_portal_scraper.fiportal_scrape s
		JOIN latest_only ON s.submissionid = latest_only.submissionid
		LEFT JOIN fi_portal_scraper.importlog i on i.submissionid = s.submissionid
		JOIN fi_portal_scraper.fiportal_scrape_downloaded f ON s.submissionid = f.fk_downloadsubid
		LEFT JOIN fi_portal_scraper.import_custom_specs c ON c.submissionid = s.submissionid
		WHERE
			i.result is NULL
			AND f.filenamepath not in ( 'Records has been removed' )
		ORDER BY s.submissionid DESC
		)
		SELECT *
		FROM q
	"""

qry_to_ack_import = """INSERT INTO fi_portal_scraper.importlog ( submissionid, result )
	VALUES ( {0} , '{1}' )
	RETURNING id
	"""

where_clause_joiner = '\r\n\t\tAND\r\n\t\t'

class db_driven_importer( ):
	"""
	This is the main task that orchestrates using the database for running
	the importer.



	"""
	def __init__(self,data_filter=None , limit=None, sink_dir=None, order_descending=True ):
		self.db = pg_db(port=5433,dbname='FI_Portal_Scrape')
		if sink_dir:
			self.sink_dir=sink_dir
		elif socket.gethostname().upper() == 'lrcgikdcwhiis06'.upper():
			self.sink_dir='d:\FMP'
		else:
			self.sink_dir=r'\\lrcgikdcwhiis06\FMP'

		self.src_code_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

		if data_filter:
			#Build the list of data elements to pull off the table.
			self.qry_to_get_action_items=qry_for_all_downloaded_but_unimported_elements+\
			'\r\n\tWHERE ' +\
			where_clause_joiner.join([ k+' LIKE \''+v+'\'' for k,v in data_filter.iteritems() ])
		else:
			self.qry_to_get_action_items=qry_for_all_downloaded_but_unimported_elements

		if order_descending :
			self.qry_to_get_action_items += "\r\n\tORDER BY submissionid DESC "
		else :
			self.qry_to_get_action_items += "\r\n\tORDER BY submissionid ASC "

		if limit is not None:
			self.qry_to_get_action_items= self.qry_to_get_action_items + "\r\n\tLIMIT " + str(limit)
		#print self.qry_to_get_action_items

		print self.db.run_select_get_value_or_rowlist(self.qry_to_get_action_items)

	def go(self):
		results = list()
		for d in self.db.run_select_get_value_or_rowlist(self.qry_to_get_action_items):
			#d = dict([n])
			jsonfilename,filemetadata = self.get_metadata_from_database(d)
			datafilename = d['filename']
			print ("\n\n\nTrying to do {forest} {product} {year}".format(**d) )
			if not os.path.exists(datafilename):
				print ("\t\t\tFailed... {filename} does not exist...".format(**d) )
			else:
				result = importer_conductor(jsonfilename=jsonfilename, datafilename=datafilename, filemetadata=filemetadata, overwrite=True, sink_dir=self.sink_dir).go()
				#print json.dumps(result)
				if result is not None:
					#print ( qry_to_ack_import.format(filemetadata['id'],json.dumps(result)) )
					r = self.db.run_query_with_return(qry_to_ack_import.format(filemetadata['id'],json.dumps(result)))

					print ("ImportLog Query returned {0}".format(str(r)))

				print d['filename']
			results.append((d['forest'],d['product'],d['year'],d['submissionid'],d['planyear'],d['fmuid']))
		return results


	def get_metadata_from_database(self,submission_row):
			print submission_row['product'] + '_'+ str(submission_row['year'])
			if submission_row['specfile'] is not None:
				jsonfilename=submission_row['specfile']
			elif submission_row['product'] == 'AR' and submission_row['year'] >= 2017:
				jsonfilename=os.path.join(self.src_code_dir,'tech_spec/2018/AR.json')
			elif submission_row['product'] == 'AR' and submission_row['year'] >= 2009:
				jsonfilename=os.path.join(self.src_code_dir,'tech_spec/2009/AR.json')
			elif submission_row['product'] == 'AWS' and submission_row['year'] >= 2017:
				jsonfilename=os.path.join(self.src_code_dir,'tech_spec/2018/AWS.json')
			elif submission_row['product'] == 'AWS' and submission_row['year'] >= 2009:
				jsonfilename=os.path.join(self.src_code_dir,'tech_spec/2009/AWS.json')
			elif submission_row['product'] == 'FMP' and submission_row['year'] >= 2017:
				jsonfilename=os.path.join(self.src_code_dir,'tech_spec/2018/FMP.json')
			elif submission_row['product'] == 'FMP' and submission_row['year'] >= 2009:
				jsonfilename=os.path.join(self.src_code_dir,'tech_spec/2009/FMP.json')
			else:
				jsonfilename=r'../NONEXISTENT_SPEC_FILE.json'

			datafilename = submission_row['filename']

			filemetadata=dict()
			filemetadata['fmu']=submission_row['forest']  #--.split('-')[0].replace('Forest','').strip().replace(' ','_')
			filemetadata['fmu_id']=submission_row['fmuid'] #.split('-')[1].strip()[:3]
			filemetadata['product']=submission_row['product']
			filemetadata['year']=submission_row['year']
			filemetadata['planyear'] = submission_row['planperiod'][:4]
			filemetadata['id'] = submission_row['submissionid']
			filemetadata['phase'] = submission_row['planterm']

			#print filemetadata
			return (jsonfilename,filemetadata)

	#def import_file
#	def

def test(args):
	print "Testing"
	print "Start by initializing the database stuff..."
	#data_filter = {"forest":"Gordon Cosens%", "product":"AR", "year":"2017" }
	# "forest":"Red_Lake%",
	data_filter = { "product":"AR", "year":"2017%" }
	dbdi = db_driven_importer(data_filter,limit=1,sink_dir=r'd:\fmp_test')

	dbdi.go(   )


	#dbdi.findfile()
	pass

def main(args):
	"""
	if main has no parameters, then run the test. If not, the parameters
	should be a filter of some sort...
	"""
	print args[1:]
	if not args[1:] :
		test(args[1:])
	else :
		# Loop endlessly to read from the database to run stuff...

		# "forest":"Red_Lake%",
		data_filter = dict()#;  ; { "product":"AR", "year":"2017%" }

		try:
			data_filter['forest']=args[1]
		except IndexError:
			pass

		try:
			data_filter['product']=args[2]
		except IndexError:
			pass

		try:
			data_filter['year']=args[3]
		except IndexError:
			pass

		try:
			sink_dir=args[4]
		except IndexError:
			sink_dir=None

		try:
			data_filter['submissionid::text']=args[5]
		except IndexError:
			pass



		dbdi = db_driven_importer(data_filter,limit=1,sink_dir=sink_dir)
		dbdi.go(  )

	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
