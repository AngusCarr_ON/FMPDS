#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  run_full_process_for_a_fisubmission.py
#
#  Copyright 2019 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import gc

import tape_handler
import download_initiated_handler
import download_complete_handler
import run_nw_checker
import run_gdb_lyr
import run_rod_checker
import ar_consolidator
import run_delivery
import run_load_features

def drop_claims(subid, th):
	print ("Dropping event claims for {0} of job type {1}".format(subid, th.jobType))
	continue_loop = True
	while continue_loop :
		event = th.getEvent_FromSubmission(subid)
		if event is None:
			continue_loop = False
			continue
		#print ("\tProcessing Event {0}".format(event['event_id']))

		continue_loop = th.drop_event_claims(event=event)
		#print ( "." if continue_loop else "\n"),
	del th

def drop_events(subid, th):
	print ("Dropping events of type {1} for success {0}".format(subid, th.successEvent ))
	continue_loop = True
	while continue_loop :
		event = th.getEvent_FromSubmission(subid,useChildeType=False)
		if event is None:
			continue_loop = False
			continue
		print ("\tProcessing Event {0}".format(event['event_id']))

		continue_loop = th.drop_event(event=event, dropParent=True, dropChild=False)
		print ( "." if continue_loop else "\n"),
	del th

def delete_sink_dir(subid):
	"""
	Removes the sink directory, and dependant events and claims
	"""
	th = run_nw_checker.run_nw_checker({})
	continue_loop = True
	while continue_loop :
		event = th.getEvent_FromSubmission(subid)
		if event is None:
			continue_loop = False
			continue
		else:
			payload = event['payload']
			sink_dir= payload['sink_dir']
			print ("Removing sink directory {0}".format(sink_dir))

			th.remove_sink_directory(event, ignore_errors=True)
			drop_claims(subid, download_initiated_handler.download_initiated_handler({}))
			drop_claims(subid, download_complete_handler.download_complete_handler({}))
			drop_claims(subid, run_nw_checker.run_nw_checker({}))
			drop_claims(subid, ar_consolidator.ar_consolidator({}))
			drop_claims(subid, run_gdb_lyr.mkrellyr({}))
			drop_claims(subid, run_rod_checker.run_rod_checker({}))

			drop_events(subid, run_nw_checker.run_nw_checker({}))
			drop_events(subid, ar_consolidator.ar_consolidator({}))
			drop_events(subid, run_gdb_lyr.mkrellyr({}))
			drop_events(subid, run_rod_checker.run_rod_checker({}))
			drop_events(subid, download_complete_handler.download_complete_handler({}))
	del th



def clean(subid,args):
	#print args
	if subid < 1:
		return 0

	#print args
	# Identify specific actions from combination flags
	if '--trigger' in args: args.extend( [ '--complete', '--clean_only'] )

	if '--complete' in args: args.extend( [ '--drop_all_events', '--drop_all_claims', '--add_event_download_initiated' , '--delete_sink_dir'] )

	if '--drop_all_events' in args: args.extend( [ \
		'--drop_events_download_initiated_handler',
		'--drop_events_download_complete_handler',
		'--drop_events_nw_checker',
		'--drop_events_gdblyr',
		'--drop_events_rod_checker',
		'--drop_events_ar_consolidator',
		'--drop_events_load_features' ] )
	if '--drop_all_claims' in args: args.extend( [ \
		'--drop_claims_download_initiated_handler',
		'--drop_claims_download_complete_handler',
		'--drop_claims_nw_checker',
		'--drop_claims_gdblyr',
		'--drop_claims_rod_checker',
		'--drop_claims_ar_consolidator',
		'--drop_claims_load_features' ] )

	if '--arcpy' in args: args.extend( [ \
		'--drop_claims_gdblyr',
		'--drop_claims_rod_checker'] )


	# Now that we have the actions identified... Act on them
	if '--delete_sink_dir'                        in args: delete_sink_dir(subid)

	# Drop claims for events associated with a submission
	if '--drop_claims_load_features'              in args: drop_claims(subid, run_load_features.copy_to_pg({}))
	if '--drop_claims_nw_checker'                 in args: drop_claims(subid, run_nw_checker.run_nw_checker({}))
	if '--drop_claims_rod_checker'                in args: drop_claims(subid, run_rod_checker.run_rod_checker({}))
	if '--drop_claims_gdblyr'                     in args: drop_claims(subid, run_gdb_lyr.mkrellyr({}))
	if '--drop_claims_ar_consolidator'            in args: drop_claims(subid, ar_consolidator.ar_consolidator({}))
	if '--drop_claims_download_complete_handler'  in args: drop_claims(subid, download_complete_handler.download_complete_handler({}))
	if '--drop_claims_download_initiated_handler' in args: drop_claims(subid, download_initiated_handler.download_initiated_handler({}))

	# Don't clear off the dl init event... <sigh>
	if '--drop_events_load_features'              in args: drop_events(subid, run_load_features.copy_to_pg({}))
	if '--drop_events_nw_checker'                 in args: drop_events(subid,run_nw_checker.run_nw_checker({}))
	if '--drop_events_rod_checker'                in args: drop_events(subid,run_rod_checker.run_rod_checker({}))
	if '--drop_events_gdblyr'                     in args: drop_events(subid,run_gdb_lyr.mkrellyr({}))
	if '--drop_events_ar_consolidator'            in args: drop_events(subid,ar_consolidator.ar_consolidator({}))
	if '--drop_events_download_complete_handler'  in args: drop_events(subid,download_complete_handler.download_complete_handler({}))
	if '--drop_events_download_initiated_handler' in args: drop_events(subid,download_initiated_handler.download_initiated_handler({}))


	if '--add_event_download_initiated' in args:
		sql = """SELECT filenamepath FROM fi_portal_scraper.fiportal_scrape_downloaded WHERE fk_downloadsubid = """ + str(subid) + """ ;"""
		res = tape_handler.tape_handler({}).run_query_with_return(sql)
		for r in res:
			fn = r['filenamepath']
			#print fn
			new_event_payload = {"eType":"fi_portal_download_initiated", "fi_portal_id": subid, "filename":fn}
			#print new_event_payload
			tape_handler.tape_handler({}).generateEvent( new_event_payload , override_type=False)


def main(*args):
	#print args
	arglist = [ ]
	sublist = [ ]
	for a in args[0][1:] :
		#print a
		if a[0:2] == '--':
			#print ("Argument {0}".format(a))
			arglist.append(a)
		elif a.isdigit():
			#print ("Submission {0}".format(a))
			sublist.append(int(a))

	if '--range' in arglist and len(sublist) == 2 :
		if sublist[0] < sublist[1]:
			sublist.extend(range(sublist[0] + 1, sublist[1]))
		elif sublist[0] > sublist[1]:
			sublist.extend(range(sublist[1] + 1, sublist[0]))

	print ("Operating on subs: \n\t{0}".format("\n\t".join([str(s) for s in sublist])))
	print ("Cleaning with {0}".format("\n\t".join(arglist)))

	for subid in sublist:
		print ("Cleaning {0}".format(subid))
		clean(subid,arglist)

		if '--clean_only' not in arglist:

			print "running required tasks...\n\t",
			print "download_initiated_handler\n\t",
			download_initiated_handler.download_initiated_handler({}).claim_go_release(subid)
			gc.collect()

			print "download_complete_handler\n\t",
			download_complete_handler.download_complete_handler({}).claim_go_release(subid)
			gc.collect()

			print "run_nw_checker\n\t",
			run_nw_checker.run_nw_checker({}).claim_go_release(subid)
			gc.collect()

			print "ar_consolidator\n\t",
			ar_consolidator.ar_consolidator({}).claim_go_release(subid)
			gc.collect()

			print "run_rod_checker\n\t",
			run_rod_checker.run_rod_checker({}).claim_go_release(subid)
			gc.collect()

			print "run_gdb_lyr\n\t",
			run_gdb_lyr.mkrellyr({}).claim_go_release(subid)
			gc.collect()

			print "run_load_features\n\t",
			run_load_features.copy_to_pg({}).claim_go_release(subid)
			gc.collect()

			if '--run_delivery' in arglist:
				print "run_delivery\n\t",
				run_delivery.run_delivery({}).claim_go_release(subid)
				gc.collect()

			print "\t... Completed Tasks\n"


def usage():
	print ("""
	Usage Message for run_full_process_for_a_fisubmission.py

	Runs in OSGeo4W Python - requires postgresql
	Will call ArcGIS Python as needed.

	python run_full_process_for_a_fisubmission.py <fi_submission_id> [ options ]
		Where <fi_submission_id> is an fi portal submission that has been
			downloaded by the scraper

		Optional flags:

		--complete
			Adds these flags:
				--drop_all_events
				--drop_all_claims
				--add_event_download_initiated
				'--delete_sink_dir'

		--drop_all_events
			Adds these flags:
				--drop_events_download_initiated_handler
				--drop_events_download_complete_handler
				--drop_events_nw_checker
				--drop_events_gdblyr
				--drop_events_rod_checker
				--drop_events_ar_consolidator

		--drop_all_claims
			Adds these flags:
				--drop_claims_download_initiated_handler
				--drop_claims_download_complete_handler
				--drop_claims_nw_checker
				--drop_claims_gdblyr
				--drop_claims_rod_checker
				--drop_claims_ar_consolidator

		--delete_sink_dir
			Removes the sink directory. Yes... This is dangerous...

		--add_event_download_initiated
			Gets the FI Submission Zip filename from the database and
				files an event in the database to start the chain of events
			This is required when the original event has been removed
				for whatever reason

		--clean_only
			Prevents the new processing of events associated with
				the fi submission in this run of the program

		--drop_events_download_initiated_handler
		--drop_events_download_complete_handler
		--drop_events_nw_checker
		--drop_events_gdblyr
		--drop_events_rod_checker
		--drop_events_ar_consolidator
			The drop_events_* flags drop the event that is handled by
				the named handler
			This will reset the run of a particular event, so it
				can be processed again.
			The preceding event must be run to file the event upon
				which the processing will be based.

		--drop_claims_download_initiated_handler
		--drop_claims_download_complete_handler
		--drop_claims_nw_checker
		--drop_claims_gdblyr
		--drop_claims_rod_checker
		--drop_claims_ar_consolidator
			Drops claims associated with specific events
			This will allow the re-run of an existing event

		--run_delivery
			Runs the delivery process to the final file system

	""")

if __name__ == '__main__':
	import sys
	#sys.argv.extend([-1])
	sys.exit(main(sys.argv))
