#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       untitled.py
#
#       Copyright 2018  <>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from tape_handler import tape_handler
import os,sys

class mkrellyr( tape_handler ):
	"""
	Intended as the entry point to create the relative layer files...
	This is actually a TAPE Handler

	It receives a dict containing:
		d= dict(zip(['forest','product','year'],["Lac_Seaul",'AR','2017']))
	From the db run or event or whatnot...

	It generates the relative layer file and associated GDB file for the
	importer products
	"""
#	def __init__(self,d,debug=False):
#		tape_handler.__init__(self,d,debug)

	eType = 'fi_portal_imported'
	successEvent = "fi_portal_mkgdb_completed"
	channel = eType # event Type is used for the channel name... :-)
	jobType = 'fi_portal_mkGdbLyr'
	exclusive_job = False

	staletime = '60 minutes'
	#backlog_frequency = 60
	backlog_frequency = 3
	#timeout = 60 # seconds
	timeout = 5 # seconds

	def go(self, event=None):
		"""
		Run the GDB / LYR Creator
		"""
		print ("MkRelLyr Event: {0}".format(str(event)))
		#event['payload']['filename']
		#gdb_call= r"c:\Python27\ArcGIS10.3\python.exe U:\src\FMPDS\arcpy_support\make_rel_lyr.py {forest} {product} {year} f:\ {submissionid}".format(**d)
		mkRelLyr = os.path.join(os.path.join(self.src_code_dir,'arcpy_support'),'make_rel_lyr.py')

		d=self.getforestproductyear(event['payload']['fi_portal_id'])

		gdb_call= [self.arcpy_environment,mkRelLyr,d['forest'], d['product'], d['year'], '%','--overwrite']

		#if self.debug:
		print gdb_call

		try:
			gpkgname=event['payload']['sink_gpkg'].replace('\\\\','\\').replace('\\','/')
		except KeyError as e:
			print e
			return self.status('Cannot Find Input Files')
		gdb_call.append('--input')
		gdb_call.append(gpkgname)

		self.call(gdb_call)

		if os.path.exists(gpkgname.replace('gpkg','lyr')) and os.path.isdir(gpkgname.replace('gpkg','gdb')) :
			self.generateEvent({ 'sink_dir':os.path.dirname(gpkgname),'src':gpkgname,'filename':gpkgname.replace('gpkg','lyr'), 'lyrname':gpkgname.replace('gpkg','lyr'),'fi_portal_id':event['payload']['fi_portal_id'] })

			return self.status("Success")

		return self.status("Failure")


if __name__ == '__main__':
	t = mkrellyr({})
	t.interpret_command_line(sys.argv)

