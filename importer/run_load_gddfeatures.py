#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  run_load_gddfeatures.py
#
#  Copyright 2019 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import run_load_features

class gdd_loader(run_load_features.copy_to_pg):
	"""
	This class is a pure subclass of the copy_to_pg handler
	It only deals with a subset of layers, and they are always the
	same ones.

	There is no go() function, because it is in the copy_to_pg anyway...
	"""
	#use the same thing as we used for fi_portal, but replace the table
	#where gdd_id matches, instead of using fi_portal_id
	# We could load subsets of the gdd data that way...
	# This one is for the ownership composite layers
	parm_model = { 'sub_id':'gdd_grp'}
	replace_parms = parm_model.keys()
	starter_parameters = {'gdd_grp':'ownership_composite_layers'} # generic parameters used for all molecules
	allowed_layers= [ '...' ] # use all layers if empty, otherwise only certain ones...

	def build_set_parameters(self,event):
		"""
		This function should be overloaded to provide submission information
		for each submission type
		"""
		parm = self.starter_parameters
		fpy  = self.getforestproductyear(event['payload']['fi_portal_id'])
		parm['fi_portal_id'] = event['payload']['fi_portal_id']
		parm['forest'] = fpy['forest']
		parm['product'] = fpy['product']
		parm['year'] = fpy['year']
		parm['planyear'] = fpy['planyear']
		return parm

def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
