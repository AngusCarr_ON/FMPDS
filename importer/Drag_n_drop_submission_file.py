# Read input file dragged on and identify it's product submission,
#Copy file to \\lrcgikdcwhiis06\FMP\forest\product\year\fi_submission.zip
# Register file in database
import os
import sys
import glob
import subprocess
import time

from importer import importer_conductor
from stack.pg_db import pg_db
import shutil

from psycopg2 import IntegrityError

class dd_filer( importer_conductor ):
	def __init__(self,filename):
		self.datafilename = filename
		sub_id = self.get_fi_portal_id_from_zipfilename()

		if sub_id is None:
			for f in glob.glob(os.path.join(os.path.dirname(filename),"fi_submission_*.txt")):
				print ("Found {0} as a subname text file".format(f))
				sub_text = os.path.basename(f).lower().replace("fi_submission_","").replace(".txt","")
				print sub_text
				sub_id = int(sub_text)

		jsonfilename,filemetadata = self.get_metadata_from_database(sub_id)
		#print "\t"+ "\r\n\t".join([ str(k) +":"+str(v) for k,v in filemetadata.iteritems() ])

		new_fn = r'f:\{fmu}\zips\{id}.zip'.format(**filemetadata)

		filemetadata['new_fn'] = new_fn
		filemetadata['filename'] = filename


		filemetadata['downloaddate'] = time.ctime(os.path.getmtime(filename))


		#print filename
		#print new_fn

		#print "shutil.copy({filename},{new_fn})".format(**filemetadata)

		if not os.path.exists(new_fn):
			shutil.copy(filename,new_fn)

		sql = """
		INSERT INTO fi_portal_scraper.fiportal_scrape_downloaded(
			filenamepath, downloaddate, currentuser, fk_downloadsubid)
			VALUES ('{new_fn}', '{downloaddate}'::timestamp, current_user , {id});
		""".format(**filemetadata)
		#print sql
		try:
			pg_db(port=5433, dbname='FI_Portal_Scrape').run_sql_onevalue(sql)
		except IntegrityError :
			print ("Submission {0} already logged...".format(sub_id))


def main(args):
	#print "Args\r\n\t",
	#print "\r\n\t".join(args)
	d = dd_filer(args[1])
	return 0

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
		sys.exit(main([__file__,r'\\cihs.ad.gov.on.ca\mnrf\Groups\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Kenora\AWS\2012\MU644_2012_AWS_11046.zip']))
		# r'\\cihs.ad.gov.on.ca\mnrf\Groups\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Kenora\AR\2008\download_cart_2016-04-28_AR_2008_Kenora_6214.zip']))
    sys.exit(main(sys.argv))
