#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  run_load_features.py
#
#  Copyright 2019 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import sys
import datetime
import glob

from osgeo import gdal
from osgeo import ogr
from osgeo import osr

import json

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
#from importer import importer_conductor
from stack.choose_a_tech_spec  import choose_a_tech_spec
from tape_handler import tape_handler


class copy_to_pg(tape_handler):
	"""
	Copies specified files to the database, potentially
	replacing existing records

	T: On demand
	A: Copy records from an OGR data source to the GDBBITS format in
		another ogr data source
	P: GDBBITS format records, with matching metadata
	E: Do we need to file an event?

	So... This should be subclassed to do some other things, like handling gdd
	"""
	eType = 'fi_portal_imported'
	successEvent = "fi_molecule_loaded"
	jobType = 'fi_molecule_loader'
	channel = eType # event Type is used for the channel name... :-)
	exclusive_job = True

	staletime = '60 minutes'
	#staletime = '2 minutes'
	#backlog_frequency = 60
	backlog_frequency = 2
	#timeout = 60 # seconds
	timeout = 1 # seconds
	#This makes the default to be run a backlog every hour, and handle
	# interstitial events every minute.


	# Parameters for the file copy stuff...
	parm_model = { 'sub_id':'fi_portal_id'} # Parameters for the import...
	# Replacement parms are the set of parameters that have to match to
	# do a replacement. This can be used to replace FI Portal submissions
	# for the same FPY

	replace_parms = parm_model.keys() #FIXME - should thins be values?

	# or ['sub_id'] # match this set of parameters to do the replacement

	starter_parameters = { 'Program':'Forest Management Planning' } # generic parameters used for all molecules
	allowed_layers= [ ] # use all layers if empty, otherwise only certain ones...

	debug = True

	quark    = None
	atom     = None
	molecule = None

	srs = None
	srscode = -1

	destination_srscode = 3161
	destination_srs = None
	coordTrans = None

	def __init__(self, kwargs):
		tape_handler.__init__(self, kwargs)
		db_connect_string = "PG: "+self.db_connection_string() + ' SCHEMAS=rain TABLES=quark,atom,molecule '
		#print db_connect_string
		self.sink_ds = ogr.Open( db_connect_string, 1 )
			#"PG: " + self.db_connection_string() + ' SCHEMAS=gbdbits TABLES=quark,atom,molecule  ')
			#SCHEMAS=string: Restricted sets of schemas to explore (comma separated).
			#TABLES=string: Restricted set of tables to list (comma separated).
			#LIST_ALL_TABLES=YES/NO: This may be "YES" to force all tables, including non-spatial ones, to be listed.
		self.quark    = self.sink_ds['quark'] # lyr = self.sink_ds.GetLayer( lyr_name )
		self.atom     = self.sink_ds['atom']
		self.molecule = self.sink_ds['molecule']

		self.destination_srs = osr.SpatialReference()
		self.destination_srs.ImportFromEPSG(self.destination_srscode)

		self.debug = False

	def go(self, event):
		"""
		Generic loader of things... default to fi portal entities
		"""
		#Hmm...
		parm=self.build_set_parameters(event)


		# get the filename from the event
		payload = event['payload']
		src_file = payload['sink_gpkg']
		if not os.path.exists(src_file):
			print "Source file not found. Trying alternate file(s)"
			for test_name in glob.glob(os.path.join(payload['sink_dir'],"mu*.gpkg")):
				src_file = test_name
				print("\t{0}".format(src_file))


		#Load all the payload keys into the parameter model
		for k,v in self.parm_model.iteritems():
			if v in payload.keys():
				parm[k] = v+"."+str(payload[v])


		print "Starting with payload {0}".format(parm)

		#Send the whole thing to load_molecule...
		try:
			self.load_molecule(src_file, parm)
		except RuntimeError as e:
			print e
			if 'Cannot find src gpkg file' in str(e):
				return self.status('Cannot Find Input Files')
			return self.status('Failure')

		# We should file an event... eventually
		new_payload = dict()
		for k in ['forest','product','year','fi_portal_id']:
			try:
				new_payload[k] = payload[k]
			except KeyError:
				pass
		self.generateEvent(new_payload)

		return self.status('Success')

	def build_set_parameters(self,event):
		"""
		This function should be overloaded to provide submission information
		for each submission type
		"""
		parm = self.starter_parameters
		fpy  = self.getforestproductyear(event['payload']['fi_portal_id'])
		parm['fi_portal_id'] = event['payload']['fi_portal_id']
		parm['forest'] = fpy['forest']
		parm['product'] = fpy['product']
		parm['year'] = fpy['year']
		parm['planyear'] = fpy['planyear']
		return parm

	def load_molecule(self,src_file,parm):
		# Open the file
		if os.path.exists(src_file):
			ogr_ds = ogr.Open(src_file)
		else:
			raise RuntimeError("Cannot find src gpkg file {0}".format(src_file))
		# create the molecule
		# Filter the parm model to be only the class-derived parameter set for the match param
		molecule_id = self.create_molecule( match_parm = [ (k,parm[k]) for k in self.replace_parms ], set_parm=parm )
		self.ogr_ds = ogr_ds

		for  layer in self.ogr_ds:
			if layer.GetName() in self.allowed_layers or not self.allowed_layers :
				atom_id = self.create_atom( layer=layer, molecule_id=molecule_id )
				for feature in layer:
					self.create_quark(atom_id,feature)

	def create_molecule(self,match_parm,set_parm):
		"""
		#-- Check if the "thing" already exists with the same id
		#"SELECT id, datasrc FROM rain.molecule WHERE {0}".format( ' AND '.join( [ "datasrc->>'{0}' = '{1}'" for k,v in parm ]
		## if it does, remove it...
		#"DELETE FROM rain.molecule WHERE id = {0} ; ".format( old_molecule_id )

		# Create the new one...
		INSERT INTO rain.molecule ( datasrc ) VALUES ( '{0}'::jsonb ) RETURNING id;".format( json.dumps(set_parm) )

		match_parm is the set of parameters used to match the row.
			Only a subset of the parameters for the thingummy.
		set_parm is the full set of parameters in the event
		"""
		# self.molecule # This is the table containing the molecules...
		sql = """SELECT id FROM rain.molecule WHERE {0}""".format( ' AND '.join( [ "datasrc->>'{0}' = '{1}'".format(k,v) for k,v in match_parm ] ) )
		if self.debug: print sql

		r = self.run_query_with_return(sql)
		if r:
			# Remove the existing row...
			old_molecule_id = r[0]['id']
			if self.debug: print("Removing a molecule... {0}".format( old_molecule_id))
			sql = """DELETE FROM rain.molecule WHERE id = {0} ; """.format( old_molecule_id )
			if self.debug: print sql
			r = self.run_query_with_return(sql)
			# No need to check return.... we will discard...

		# CREATE THE molecule
		sql = """INSERT INTO rain.molecule ( datasrc ) VALUES ( '{0}'::jsonb ) RETURNING id;""".format( json.dumps(set_parm) )
		if self.debug: print sql
		r = self.run_query_with_return(sql)
		if r:
			return r[0]['id']
		else:
			return None

	def create_atom(self,molecule_id,layer):
		"""
		Create the atom - a table feature definition

		Build the rowtype json from the table
		"""
		rowtype = { 'name':layer.GetName() }
		self.find_spatial_reference(layer=layer)
		layerDefinition = layer.GetLayerDefn()
		rowtype['fields'] = self.remove_common_fields(\
			dict([\
				(	layerDefinition.GetFieldDefn(i).GetName().lower(),
					layerDefinition.GetFieldDefn(i).GetFieldTypeName( layerDefinition.GetFieldDefn(i).GetType() ) \
				) for i in range(layerDefinition.GetFieldCount()) ])
			)
		#for i in range(layerDefinition.GetFieldCount()):
		#	fieldName =  layerDefinition.GetFieldDefn(i).GetName()
		#	fieldTypeCode = layerDefinition.GetFieldDefn(i).GetType()
		#	fieldType = layerDefinition.GetFieldDefn(i).GetFieldTypeName(fieldTypeCode)
		#	rowtype['fields'][fieldName]=fieldType
		rowtype['spatialreference'] = self.srscode


		# CREATE THE atom
		sql = """INSERT INTO rain.atom ( molecule_id, rowtype ) VALUES ( {1}, '{0}'::jsonb ) RETURNING id;""".format( json.dumps(rowtype), molecule_id )
		if self.debug: print sql
		r = self.run_query_with_return(sql)
		if r:
			return r[0]['id']
		else:
			return None

	def remove_common_fields(self,row):
		#eliminate some common issues in the layer being loaded
		#generate a list of things to ignore
		removal_items = [ 'fid','ogc_fid','OBJECTID', 'Shape_Length', 'shape_leng', 'Shape_Area', 'Perimeter' , 'arcids', 'area' ]
		for k,v in row.iteritems():
			if v is None:
				removal_items.append(k)
			if k[-1]=='#' and k.replace('#','-id') in row.keys() :
				removal_items.append(k)
				removal_items.append(k.replace('#','-id'))

		#now actually remove them
		for n in removal_items :
			try:
				del row[n.lower()]
			except KeyError:
				pass
		return row


	def create_quark(self,atom_id,feature):
		"""
		Generate the json and geometry needed to store in the table...
		Carry the atom id so it can be removed...
		"""
		# get the dict of the row
		featureDefn = feature.GetDefnRef()
		row = self.remove_common_fields(dict([ (featureDefn.GetFieldDefn(i).GetName().lower(), feature.GetField(i) ) for i in range(featureDefn.GetFieldCount()) ]))
		#print str(row)


		#geom = feature.GetGeometryRef()

		# Create the feature and set values
		outfeature = ogr.Feature(self.quark.GetLayerDefn())

		if not feature.GetGeometryRef().IsEmpty():
			# If feature has no srs, assign the atom version
			geom = feature.GetGeometryRef()
			# No need to assign the feature srs by feature - our data is coming from GIS Layers, with layer-level projection support
			#s = self.find_spatial_reference(geom=geom)
			geom.Transform( self.coordTrans )
			outfeature.SetGeometry(geom)

		outfeature.SetField("atom_id", atom_id )
		outfeature.SetField("rowdata", json.dumps( row ))

		self.quark.CreateFeature(outfeature)
		feature = None

	def find_spatial_reference(self, layer=None, geom=None, srs=None ):
		"""
		Get a spatial reference... Somehow
		If a layer is provided, use that one.
		If a layer in a prior call gave us an SRS, use that one
		If not, then go through all of the layers in the src_ds
		If the source event identifies a forest, then we could use a default UTM with zone chosen by FMU...

		layer could be a layer or a geometry...

		Returns spatial reference and an srscode

		"""
		def if_valid_EPSG_code(srs):
			#if self.debug: print ("SRS CODE is {0} for projection \n{1}".format(srs.GetAttrValue("AUTHORITY", 1), srs.ExportToPrettyWkt(False)))

			self.srs = srs
			self.coordTrans = osr.CoordinateTransformation(self.srs, self.destination_srs )

			srscode = -1
			try:
				for s,c in srs.FindMatches() :
					if s.GetAuthorityName(None) == 'EPSG':
						#print ("Using EPSG Code... {0}".format(s.GetAuthorityCode(None)))
						srscode = s.GetAuthorityCode(None)
						#Another method to get the code... s.GetAttrValue("AUTHORITY", 1)

				if srscode > 0 :
					self.srscode = srscode
			except AttributeError as e:
				pass

			return (self.srs,self.srscode)


		# First... does the layer have a srs?
		if srs is not None:
			s = if_valid_EPSG_code( srs )
			if s is not None:  return s
		elif geom is not None:
			s = if_valid_EPSG_code( geom.GetSpatialReference() )
			if s is not None:  return s
		elif layer is not None:
			s = if_valid_EPSG_code( layer.GetSpatialRef() )
			if s is not None:  return s

		# Second... Did we already find one?
		elif self.srs is not None:
			return (self.srs,self.srscode)

		# Third... is there a layer in the submission that has an srs?
		else:
			for layer in self.ogr_ds:
				s = if_valid_EPSG_code(layer.GetSpatialRef())
				if s is not None:
					return s

		# Last chance...
		return None

if __name__ == '__main__':
	t = copy_to_pg({})
	t.interpret_command_line(sys.argv)

