#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  robocopy.py
#
#  Copyright 2019 CARRAN <CARRAN@LRCGIKDCWHIIS06>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


from tape_handler import tape_handler
import os
import sys
import datetime

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

class run_delivery( tape_handler ):
	eType = 'fi_portal_mkgdb_completed'
	successEvent = "fi_portal_delivery"
	channel = eType # event Type is used for the channel name... :-)
	jobType = 'fi_portal_delivery'
	exclusive_job = False

	def go(self, event=None):
		"""
		Copies files from one to another... Whatever that means...

		"""

		print ("Running robocopy\n\tfi_portal_id from Payload: {0}".format(event['payload']['fi_portal_id']))
		d = self.getforestproductyear(event['payload']['fi_portal_id'])
		d['date'] = datetime.date.today()
		try:
			src_dir = event['payload']['sink_dir']  # \\lrcgikdcwhiis06.cihs.ad.gov.on.ca\fmp\Dog_River-Matawin\AR\2017
		except KeyError:
			try:
				print event['payload'].keys()
				src_dir = os.path.dirname(event['payload']['src'])
			except KeyError:
				src_dir = os.path.dirname(event['payload']['filename'])
				if os.path.exists(src_dir) and not os.path.isdir(src_dir):
					src_dir = os.path.dirname(event['payload']['filename'])

		src_grandparent = os.path.dirname( os.path.dirname ( src_dir ) )

		forest_sinks_lut = { "NWR":r'\\lrcpthbafp00002\gis-data\FMP', 'SR':'\\lrcpptboshfs001\MNR_SR_External\FOREST_MGMT\FMPDS'}

		for r in self.forest_src_lut.keys():
			for u in self.forest_src_lut[r]:
				if d['forest'] == u:
					sink_dir = r'{sink_dir}\{forest}'.format(sink_dir=forest_sinks_lut[r], forest=u)
					cmd = "robocopy {src_grandparent} {sink_dir} /MIR /Z /XA:H /W:5 /xf *.lock ".format(src_grandparent=src_grandparent, sink_dir=sink_dir)

					print cmd
					status = self.call(cmd)

					####################################
					# Status Report and event creation #
					####################################
					# if we get here, there is no problem...
					#url = outputfile.replace('f:\\\\','/fmp_import/').replace('\\','/')
					new_event_payload = { 'cmd':cmd, 'src':src_dir,'filename':sink_dir, 'status':status }
					print new_event_payload
					self.generateEvent(new_event_payload)
		return self.status("Success")

if __name__ == '__main__':
	t = run_delivery({})
	t.interpret_command_line(sys.argv)

