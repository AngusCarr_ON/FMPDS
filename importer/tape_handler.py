#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       tape_handler.py
#
#       Copyright 2018  <>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
import os
import sys

import subprocess
import select
import socket

import psycopg2
import psycopg2.extras

try:
  import pwd
  current_user = pwd.getpwuid(os.geteuid()).pw_name.lower()
except ImportError:
  import getpass
  current_user = getpass.getuser().lower()
  pwd = None

import json

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import stack.choose_a_tech_spec
from stack.status_codes import status_code, status_msg, StatusError

debug = False

class tape_handler():
	"""
	Tape Handler is how we handle individual TAPEs

	This is a class that gets subclassed
	"""
	src_code_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	arcpy_environment = os.path.join(os.path.join(src_code_dir,'arcpy_support'),'env_arcpy.bat')

	conn = None
	eType = 'Test Event'
	successEvent = 'Test_Success_Event'
	channel = eType # event Type is used for the channel name... :-)
	jobType = 'Test Event Handler'
	exclusive_job = True
	staletime = '60 minutes'
	backlog_frequency = 60
	timeout = 60 # seconds
	#This makes the default to be run a backlog every hour, and handle
	# interstitial events every minute.

	only_process_latest_submissions = True

	failure_count = 5
	current_claimed_event = None

	status_callback = None # Used to provide feedback to the GUI

	sql_to_get_metadata = 'SELECT * FROM fi_portal_scraper.importer_metadata'

	def __init__(self,d):
		global debug
		self.debug = True if debug else False
		if self.debug : print "Debug is ON"
		#if not self.debug : print "Debug is OFF"
		self.status_callback = self.msg_print
		self.d = d
		self.db_string = self.db_connection_string()


	def go(self,event=None):
		"""
		Do the actual thing...
		"""
		print ("Tape Handler Go does Nothing!!!!")
		print str(event)
		return self.status("Success")
		#pass

	def msg_print(self,message):
		print(message)

	def call(self,*args):
		cmd = [ arg for count, arg in enumerate(args) ][0]
		if self.debug : print ' '.join(cmd)
		return subprocess.call(cmd , shell=True)


	def db_connection_string(self,db_string = None):
		"""
		Initiates contact with the db...

		If the string is passed in, use it.

		Use a dbstring from the registry.

		If that doesn't work, then use the db_config from the db_config
		file in the directory.

		If that doesn't work, use the generic version... which will work
		if your password is saved in the pgpass file
		( %appdata%/postgresql/pgpass )
		"""

		if db_string is not None:
			return db_string

		try:
			import _winreg
			with  _winreg.OpenKey(_winreg.HKEY_CURRENT_USER, "Software\NWR_Importer") as key:
				return str(_winreg.QueryValueEx(key, 'db_string')[0])
		except WindowsError:
			pass

		#try:
		if True:
			from db_config import default_db_string
			db_string = default_db_string
			#look for a site-specific file that conatains a db string
			# Create it with:
			# echo "default_db_string = \"dbname='fmp' user='bob' host='localhost' \" " > db_config.py
		#except ImportError:
		#	db_string = "dbname='FI_Portal_Scrape' user='%s' host='lrcgikdcwhiis06' port=5433 "%(current_user)

		return db_string

	def db_conn(self):
		if self.conn is None:
			#print "Connecting with {0}".format(self.db_string)
			self.conn = psycopg2.connect(self.db_string)
		return self.conn

	def run_query_with_return(self,sql):
		#print sql
		self.db_conn().set_isolation_level(0) #Autocommit so that Listen works
		try:
			cur = self.db_conn().cursor(cursor_factory=psycopg2.extras.DictCursor)
			cur.execute(sql)
			r = cur.fetchall()
			self.db_conn().commit()
			return r
		except psycopg2.ProgrammingError as e:
			self.db_conn().commit()
			if str(e) == 'no results to fetch': return None
			print e
			print sql
			return None
		except psycopg2.IntegrityError as e:
			self.db_conn().commit()
			return None
		except Exception as e:
			print e
			print sql
			raise e

	def getEvent(self,event_id):
		sql = """SELECT * FROM fi_portal_scraper.events where event_id = {0}""".format(event_id)
		r = self.run_query_with_return(sql)
		try:
			if self.debug : print ("Returned Event {0}".format( r[0] ) )
			return r[0]
		except IndexError:
			if self.debug : print ("No Event Returned" )
			return None

	def getEvent_FromSubmission(self,sub_id,useChildeType=False):
		"""
		Get latest event of an event type referring to a submission
		"""
		eType = self.successEvent if useChildeType else self.eType

		sql = """with i as ( SELECT max(event_id) OVER ( PARTITION BY payload->>'fi_portal_id',payload->>'eType' ) as max_event_id,* FROM fi_portal_scraper.events where payload->>'fi_portal_id' = {0}::text and payload->>'eType' = '{1}' ) SELECT * FROM i WHERE max_event_id = event_id """.format(sub_id,eType)
		r = self.run_query_with_return(sql)
		try:
			returnable_event = r[0]
		except IndexError:
			if self.debug : print ("No Event Returned" )
			return None
		try:
			parameters = returnable_event['payload'].keys()
		except AttributeError as e:
			returnable_event['payload'] = json.loads(returnable_event['payload'])

		if self.debug : print ("Returned Event {0}".format( returnable_event ) )
		return returnable_event


	def get_event_completed(self,event):
		if event is None:
			return False
		sql = """SELECT * FROM fi_portal_scraper.event_claim WHERE event_id = {0} and job_type = '{1}' and completion_status = 0 ORDER BY id DESC LIMIT 1 """.format(event['event_id'], self.jobType )
		res = self.run_query_with_return(sql)
		if res :
			if self.debug : print ("Event {event_id} Claim {id} status {completion_status}".format( **(res[0]) ))
			return True
		else :
			if self.debug : print ("No Event claim found")
			return False
		return None

	def generateEvent(self,payload, override_type=True ):
		standard_elements = dict()
		if self.current_claimed_event is not None:
			standard_elements['parent_event'] = self.current_claimed_event
		if override_type:
			standard_elements['eType'] = self.successEvent

		p = json.dumps(dict( payload, **standard_elements ))

		sql = """INSERT INTO fi_portal_scraper.events (payload) VALUES ('{0}') returning event_id""".format(p)
		res = self.run_query_with_return(sql)
		if res :
			if self.debug : print ("Inserted Event {0}".format( res ))
		else :
			if self.debug : print ("No Event Generated")
		return None

	def listener(self, timeout=5, function=None, channel=None):
		if function is None:
			function = self.go
		if channel is None:
			channel = self.channel
		else:
			self.channel = channel

		self.db_conn().set_isolation_level(0) #Autocommit so that this works
		cur = self.db_conn().cursor()
		cur.execute("LISTEN \"{0}\";".format(self.channel))
		print("Waiting for notifications on channel '{0}'".format(self.channel))

		i = self.backlog_frequency -1 #41

		while 42:
			if select.select([self.db_conn()],[],[], self.timeout) == ([], [], []):
				print('TIMEOUT')
				i = i+1
				if i>self.backlog_frequency:
					self.get_backlog(limit=1)
					i=0
			else:
				self.db_conn().poll()
				while self.db_conn().notifies:
					notify = self.db_conn().notifies.pop()
					print("Got NOTIFY:", notify.pid, notify.channel, notify.payload)
					event_id = notify.payload
					event=self.getEvent(event_id)
					self.claim(event)
					self.release(self.claim_id, event, status=function(event) )

	def drop_event_claims(self,event):
		if event is None:
			return False
		sql = """DELETE FROM fi_portal_scraper.event_claim WHERE event_id = {0} and job_type = '{1}'""".format(event['event_id'], self.jobType )
		res = self.run_query_with_return(sql)
		if res :
			if self.debug : print ("Event {event_id} Claim {id} deleted".format( **(res[0]) ))
			return True
		#else :
		#	print ("No Event claim found")
		#	return False
		return False

	def drop_event(self, event, dropParent=True, dropChild=True):
		# Do Child Event
		## Get subid from event
		### possible ways to look:
		### fi_portal and successEvent
		### parent_event and successEvent
		sqllist = []
		try:
			sqllist.append( """DELETE FROM fi_portal_scraper.events
				WHERE
					payload->>'eType' = '{successEvent}'
					AND
					payload->>'fi_portal_id' = '{fi_portal_id}'
				RETURNING event_id
			""".format(successEvent=self.successEvent, fi_portal_id=event['fi_portal_id']) )
		except KeyError:
			print "No FI Portal..."
			pass

		try:
			sqllist.append( """DELETE FROM fi_portal_scraper.events
				WHERE
					payload->>'eType' = '{successEvent}'
					AND
					payload->>'parent_event' = '{parent_event}'
				RETURNING event_id
			""".format(successEvent=self.successEvent, parent_event=event['parent_event']) )
		except KeyError:
			print "No Parent Event..."
			pass

		## DELETE FROM fi_portal_scraper.events WHERE payload->>'eType' = {self.successEvent} and payload->>'fi_portal_id' = {fi_portal_id}
		for sql in sqllist:
			print ("Removed Child Event(s) {0}".format(str(self.run_query_with_return(sql))))

		# Do Parent Event
		sql = """DELETE FROM fi_portal_scraper.events WHERE event_id = {0}""".format(event['event_id'] )
		res = self.run_query_with_return(sql)
		#if self.debug :
		print ("Event {event_id} deleted".format( **event )),
		print (" with sql {0}".format(sql))
		return True
		#else :
		#	print ("No Event claim found")
		#	return False
		#return False

	def update_status(self,status_msg):
		"""
		This should update the module status message for the user
		"""
		#self.status=status
		if self.status_callback:
			self.status_callback(status_msg)

	def parmdict(self, rdict={} ):
		session_id = socket.gethostname()+'_'+current_user
		pdict = { 'eType':self.eType,
			'jobType':self.jobType,
			'exclude':self.exclusive_job,
			'session_id':session_id
			}
		res = dict( rdict, **pdict )
		return res

	def getforestproductyear(self,submissionid):
		""" Finds out the important metadata for a submissionid
			Includes a "latest_submission" boolean to help identify whether
				the submission is the latest of it's ilk
		"""
		sql = self.sql_to_get_metadata +""" WHERE submissionid = {0}""".format(submissionid)
		r = self.run_query_with_return(sql)
		#print sql
		if self.debug : print r
		try:
			d = dict(r[0])
			d['year_2'] = str(d['year'])[-2:]
			if self.debug : print ("Returned FPY {0}".format( d ) )
			return dict(d)
		except IndexError:
			if self.debug : print ("No FPY Returned" )
			return None

	def claim(self, r ):
		#print "Claimed"
		#print r
		#print  self.parmdict(r)
		sql = """INSERT INTO fi_portal_scraper.event_claim
			(event_id, session_id, exclude, job_type, completion_status) VALUES
			( {event_id},'{session_id}',{exclude},'{jobType}',-1 )
			returning id,event_id,completion_status""".format(**self.parmdict(r))
		#print sql
		for c in self.run_query_with_return(sql):
			self.claim_id = c['id']
			#print ("Claimed claim_id {id} with status {completion_status}".format(**c))
			self.update_status("Claimed claim_id {id} with status {completion_status}".format(**c))

			self.current_claimed_event = c['event_id']
		return self.claim_id

	def release(self, claim_id, r, status=1):
		print ("Released with status {0}: {1}".format(self.status(status), self.status_msg(status)))
		#print  self.parmdict(r)
		sql = """UPDATE fi_portal_scraper.event_claim
			set completion_status = {status},completion_time=now()
			WHERE id = {id}
			returning id,completion_status""".format(id=claim_id,status=status)
		#print sql
		for c in self.run_query_with_return(sql):
			#print ("Released claim_id {id} with status {completion_status}".format(**c))
			self.update_status("Released claim_id {id} with status {completion_status}".format(**c))
			self.current_claimed_event = None
		self.claim_id = None

	def claim_go_release(self, subid=None, event=None):
		if event is None:
			event = self.getEvent_FromSubmission(subid)
		#print event
		if event is None:
			return "No Event Found"
		if not self.get_event_completed(event):
			claim_id = self.claim(event)
			#print ("claim_id: {0}".format(claim_id))
			status = self.go(event)
			#print ("Status: {0}".format(status))
			result = self.release(claim_id, event, status=status )
			#print ("Release: {0}".format(result))
			return "Event {0} completed with status {1}".format(event['event_id'],status)
		return "Event {0} already completed".format(event['event_id'])

	def claim_release(self, subid=None, event=None, status=0):
		"""
		Claim and release an event with a status of 0 without running GO

		This is to allow us to claim a
		"""
		if event is None:
			event = self.getEvent_FromSubmission(subid)
		#print event
		if event is None:
			return "No Event Found"
		if not self.get_event_completed(event):
			claim_id = self.claim(event)
			result = self.release(claim_id, event, status=status )
			return "Event {0} completed with status {1}".format(event['event_id'],status)
		return "Event {0} already completed".format(event['event_id'])


	def get_backlog( self, eType=None, sql=None , limit=None, event_id=None ):
		if eType is None:
			eType = self.eType

		if sql is None:
			# First sql looks for unclaimed events
			sql = """WITH ec as (SELECT e.*, c.id as claim_id, payload->>'eType' as eType
			FROM fi_portal_scraper.events e
			LEFT JOIN fi_portal_scraper.event_claim c ON (e.event_id = c.event_id and c.job_type = '{job_type}' )
			)
			SELECT *
			FROM ec
			WHERE claim_id is null
			and eType = '{eType}'
			limit {limit};""".format(eType=eType, limit=limit, job_type=self.jobType )
			if self.debug : print(sql)

		for r in self.run_query_with_return(sql):
			limit-=1
			if self.debug : print "Backlog Fresh {0}".format(r)
			if self.only_process_latest_submissions:
				# Decline to process a submission that is not the latest of it's type
				if self.getforestproductyear(r['payload']['fi_portal_id'] )['latest_submission'] :
					self.claim_go_release(event=r)
				else:
					print ("{0} is not latest submission :{1}".format(r['payload']['fi_portal_id'], r))
					self.claim_release(event=r)
			else :
				self.claim_go_release(event=r)

		if limit < 1: return None

		# Second sql looks for failed events that haven't been tried too often...
		sql = """
			WITH ec as (
				SELECT e.*,
					c.claim_time, c.job_type, session_id,
					c.id as claim_id,
					payload->>'eType' as eType,
					payload->>'fi_portal_id' as fi_portal_id,
					completion_status, completion_time,
					bool_or( completion_status = 0  ) OVER w as run_to_completion,
					bool_or(
						( completion_status > 0 and completion_time < now() - '{staletime}'::interval  ) or
						( completion_status < 0 and claim_time < now() - '{staletime}'::interval )
					) OVER w as redo,
					count( * ) OVER w as attempts

					FROM fi_portal_scraper.events e
					JOIN fi_portal_scraper.event_claim c ON (e.event_id = c.event_id and c.job_type = '{job_type}' )
					WINDOW w AS ( PARTITION BY payload->>'fi_portal_id', payload->>'eType',c.job_type )
				)
				SELECT event_id,
					-- eType,fi_portal_id,
					-- completion_status,
					-- run_to_completion,
					-- redo,
					-- attempts,
					job_type,
					payload
				FROM ec
				WHERE not run_to_completion and redo and attempts < {deadcount}
					and eType = '{eType}'
					and job_type = '{job_type}'
				ORDER BY random()
				limit {limit}
		""".format(limit=limit,staletime=self.staletime,eType=self.eType,deadcount=self.failure_count,job_type=self.jobType )
		if self.debug : print(sql)
		for r in self.run_query_with_return(sql):
			if self.debug : print "Backlog Old {0}".format(r)
			if not self.getforestproductyear(r['payload']['fi_portal_id'] ):
				print ("{0} has a wierd payload...".format(r['payload']))
				self.claim_release(event=r)
				return None
			if self.only_process_latest_submissions:
				# Decline to process a submission that is not the latest of it's type
				if not self.getforestproductyear(r['payload']['fi_portal_id'] ):
					print ("{0} has a wierd payload...".format(r['payload']))
					self.claim_release(event=r)
					return None
				if not self.getforestproductyear(r['payload']['fi_portal_id'] )['latest_submission'] :
					print ("{0} is not latest submission :{1}".format(r['payload']['fi_portal_id'], r))
					self.claim_release(event=r)
					return None
			self.claim_go_release(event=r)

	def clean_event_claims(self):
		"""
		Find claims that do not have the resultant file present and delete the claim
		"""
		pass

	def remove_sink_directory(self, event, ignore_errors=False): # dirname=None, ignore_errors = False):
		dirname = event['payload']['sink_dir']

		if os.path.exists(dirname) :
			print "Deleting {0}.".format(dirname)
			try:
				os.removedirs(dirname)
			except WindowsError as e:
				print ("\tCannot remove {1}:\n\t\t{0}".format(e,dirname))
				# YIKES!
				if ignore_errors :
					print ("\tTrying again with shutil.rmtree")
					import shutil
					shutil.rmtree(dirname,ignore_errors=True)

	def choose_a_tech_spec(self, product, year):
		return stack.choose_a_tech_spec.choose_a_tech_spec(product,year)

	def status(self,code_or_msg):
		"""
		Checks to make sure the status code is properly being maintained
		"""
		return status_code(self.jobType,code_or_msg)

	def status_msg(self,code_or_msg):
		"""
		Checks to make sure the status code is properly being maintained
		"""
		return status_msg(self.jobType,code_or_msg)

	def interpret_command_line(self,args):
		"""
		a standard way to approach the command line...
		if the run function includes a single number, then run
		claim_go_release() on that number.

		If there are two numbers, then run the range from the first to
		the second...

		"""
		if len(args) == 3:
			print ("running the range from {0} to {1}".format(*args[1:]))
			for arg in range(int(args[1]),int(args[2] + 1)):
				self.claim_go_release(subid=arg)
			return self.status("Success")
		elif len(args) == 2:
			self.drop_event_claims(self.getEvent_FromSubmission(int(args[1])))
			self.claim_go_release(subid=args[1])
			return self.status("Success")
		elif len(args) == 1:
			self.get_backlog(limit=1)
			self.listener()
			return self.status("Success")
		else :
			print ("running submissions: {0}".format(str(args[1:])))
			for arg in args[1:] :
				self.claim_go_release(subid=arg)
			return self.status("Success")

	forest_src_lut   = { "NWR": ['Kenora', 'Whitefeather', 'Red_Lake',
		'Whiskey_Jack', 'Crossroute','Trout_Lake','Lac_Seul',
		'Wabigoon', 'Dryden','English_River', 'Caribou', 'Black_Spruce',
		'Dog_River-Matawin', 'Sapawe', 'Lakehead',
		'Lake_Nipigon', 'Armstrong', 'Ogoki', 'Kenogami', 'Big_Pic',
		'Pic_River' ],
		"SR": ['Ottawa_Valley','French_Severn','Mazinaw-Lanark','Algonquin_Park','Bancroft-Minden']  }


if __name__ == '__main__':
	t = tape_handler({})
	t.interpret_command_line(sys.argv)
