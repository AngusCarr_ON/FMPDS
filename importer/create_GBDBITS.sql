﻿-- DROP SCHEMA rain CASCADE ;

CREATE SCHEMA rain;
COMMENT ON SCHEMA rain IS
'Regional Activity Information Nfrastructure
	This is the data structure that embodies the
	GDD and FI Portal data consolidation.
' ;


-- A Molecule is a collection of indivisible elements
-- Each molecule is built from a collection of OGR-able tables (layers)
-- A Molecule is a set of sets of spatial records. A Submission.
CREATE TABLE rain.molecule
(
id serial PRIMARY KEY,
datasrc jsonb --Data Source - as in {"Program":"Forestry","Specification":"AR_2009","Forest":"Dryden","year":2016 }
);
CREATE INDEX
   ON rain.molecule USING GIN ( datasrc );

COMMENT ON TABLE rain.molecule IS
'
-- A Molecule is a collection of indivisible elements
-- Each molecule is built from a collection of OGR-able tables (layers)
-- A Molecule is a set of sets of spatial records. A Submission.

-- ADD a trigger to remove any atoms on insert when datasrc already exists
' ;


-- An atom contains a set of spatial records. Think of it as the table/layer
CREATE TABLE rain.atom
(
id serial PRIMARY KEY,
molecule_id integer,
	FOREIGN KEY ( molecule_id) REFERENCES rain.molecule ( id ) ON DELETE CASCADE ON UPDATE CASCADE ,
rowtype jsonb -- {"table":"HRV"}
);
CREATE INDEX
   ON rain.atom USING GIN ( rowtype );
CREATE INDEX
   ON rain.atom ( molecule_id );
COMMENT ON TABLE rain.atom IS
'
-- An atom contains a set of spatial records. Think of it as the table/layer

' ;


CREATE TABLE rain.quark
(
id serial PRIMARY KEY,
atom_id integer,
	FOREIGN KEY ( atom_id) REFERENCES rain.atom ( id ) ON DELETE CASCADE ON UPDATE CASCADE ,
geom geometry(GEOMETRY), -- Generic Geometry
rowdata jsonb
);

CREATE INDEX
   ON rain.quark ( atom_id );
CREATE INDEX
   ON rain.quark USING GIN ( rowdata );
CREATE INDEX sidx_quark_geom
  ON rain.quark  USING gist  (geom);

CREATE OR REPLACE FUNCTION rain.convert_to_lambert()
  RETURNS trigger AS
$BODY$
BEGIN
	NEW.geom = ST_SetSRID(NEW.geom, 3161);
	RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
COMMENT ON FUNCTION rain.convert_to_lambert() IS 'Converts all shapes on load to Lambert Conformal Conic, EPSG:3161.';

CREATE TRIGGER lambert_ify
  BEFORE INSERT OR UPDATE OF geom
  ON rain.quark
  FOR EACH ROW
  EXECUTE PROCEDURE rain.convert_to_lambert();



COMMENT ON TABLE rain.quark IS
'
-- A quark is the actual spatial record. It has the geometry and the attributes
' ;

