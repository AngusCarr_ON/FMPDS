#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  find_lost_claims.py
#  
#  Copyright 2019 CARRAN <CARRAN@LRCGIKDCWHIIS06>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from tape_handler import tape_handler
import os
import sys
import datetime

import glob

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

class lost_claim_finder ( tape_handler ):
	""" 
	Finds claims that contain filenames that are invalid 
	and does something with them (remove) 
	"""
	min = 0
	max = 30000
	
	def __init__(self, **kwargs):
		#for k,v in kwargs.items():
		try:
			self.min = kwargs['minimum']
		except KeyError:
			pass
		try:
			self.max = kwargs['maximum']
		except KeyError:
			pass
			
		if self.max < self.min:
			tmp = self.min
			self.min = self.max
			self.max = tmp
		#print self.min
		#print minimum
		tape_handler.__init__(self,{})
		self.debug = False
		
	def go(self):
		events_for_deletion = list()
		claims_for_deletion = list()
		for k,v in {'fi_portal_nw_checker':{'filename':'filename','jobtype':'fi_portal_nw_checker','parent_event':'fi_portal_imported'}}.iteritems():
			for i in range (self.min,self.max+1):
				sql = """ 
					SELECT event_id, 
						payload->>'eType' as eType, 
						payload->>'{filename}' as filename, 
						payload->>'fi_portal_id' as fi_portal_id
					FROM fi_portal_scraper.events
					WHERE 
						payload->>'eType' = '{eType}'
						and (payload->>'fi_portal_id')::int >= {min}::int
						and (payload->>'fi_portal_id')::int <= {max}::int
				""".format(eType=k,filename=v['filename'],min=self.min, max=self.max)
				for r in self.run_query_with_return(sql):
					# Test if product file exists
					if not os.path.exists(r['filename']):
						# Product does not exist
						# add event to heap for deletion
						events_for_deletion.append(r['event_id'])
						# Event claims that created the event should be deleted
						claims_for_deletion.append((v['jobtype'],r['fi_portal_id'],v['parent_event']))
				
				for e in events_for_deletion:
					self.drop_event({'event_id':e})
					
				for c in claims_for_deletion:
					sql = """
							DELETE FROM fi_portal_scraper.event_claim AS c 
							USING fi_portal_scraper.events e 
							WHERE 
								e.event_id = c.event_id and 
								job_type = '{0}' and 
								e.payload->>'eType' = '{2}' and 
								e.payload->>'fi_portal_id' = '{1}'
						""".format(*c)
					 
					self.run_query_with_return(sql)
						 
	
			
	
def main(args):
	lcf = lost_claim_finder( maximum=24000, minimum=23000 )
	lcf.go()
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
