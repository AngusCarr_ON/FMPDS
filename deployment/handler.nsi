; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory,
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

;--------------------------------

; Set the version code
!define VERSION "0.0.3"

; The name of the installer
Name "Handler"

; The file to write
OutFile "..\..\FMPDS_pages\content\releases\Handler_${VERSION}.exe"
;OutFile "Handler_${VERSION}.exe"

; The default installation directory
;InstallDir "$PROGRAMFILES\MNRF_NWR_RIAU_Checker"
InstallDir "$PROFILE\MNRF_NWR_RIAU_Handler"

; Registry key to check for directory (so if you install again, it will
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\MNRF_NWR_RIAU_Handler" "Install_Dir"


; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

Var ARC_PYTHON
Var INDEPENDENT_PYTHON
Var OSGEO4W_PYTHON
Var OSGEO4W_PYTHON3
Var PYTHON
Var OSGEO4W_ENV


;--------------------------------
; Find a copy of

;--------------------------------

; The stuff to install

Section "Handler Installer"

  SectionIn RO

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; Put file there
  File /r   "..\arcpy_support"

  ; Stack Support files for GDBLYR
  CreateDirectory "$INSTDIR\stack"
  FILE "/oname=$INSTDIR\stack\fs_context.py" "..\stack\fs_context.py"
  FILE "/oname=$INSTDIR\stack\static_db.py" "..\stack\static_db.py"


  File /r   "..\handler"
  ; Handler.ico: <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
  ; File /r  "..\stack"
  ; "$INSTDIR\osgeo_python.bat"
  File "/oname=$INSTDIR\osgeo_python.bat" "..\osgeo_python.bat"
  File "/oname=$INSTDIR\osgeo_python3.bat" "..\osgeo_python3.bat"
  File "/oname=$INSTDIR\osgeo_env.bat" "..\osgeo_env.bat"
  FILE "/oname=$INSTDIR\handler\ht_equations_and_LUT.py" "..\stack\mist\ht_equations_and_LUT.py"
  FILE "/oname=$INSTDIR\handler\si_equations_and_LUT.py" "..\stack\mist\si_equations_and_LUT.py"
  FILE "/oname=$INSTDIR\handler\si_parameters.py" "..\stack\mist\si_parameters.py"

  ; Write the installation path into the registry
  WriteRegStr HKLM "Software\MNRF_NWR_RIAU_Handler" "Install_Dir" "$INSTDIR"
  WriteRegStr HKLM "Software\MNRF_NWR_RIAU_Handler" "Version" "${VERSION}"

  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Handler" "DisplayName" "Handler"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Handler" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Handler" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Handler" "NoRepair" 1
  WriteUninstaller "uninstall.exe"

SectionEnd

SectionGroup "Python Default Choice"
Section /o "Default to Arc Python" arc
	StrCpy $PYTHON $ARC_PYTHON
SectionEnd

Section /o "Default to Independent Python" independent
	StrCpy $PYTHON $INDEPENDENT_PYTHON
SectionEnd

Section "Default to OSGeo4w / QGIS Python" osgeo4w
	StrCpy $PYTHON $OSGEO4W_PYTHON
SectionEnd
SectionGroupEnd

# defines for newer versions
!include Sections.nsh
# SECTION_OFF is defined in Sections.nsh as 0xFFFFFFFE
!define SECTION_ON ${SF_SELECTED} # 0x1

Section "User Context Menu Entry Interface"
	; GeoPackage
	WriteRegStr HKCU "Software\Classes\.gpkg" "" "GeoPackage"
	WriteRegStr HKCU "Software\Classes\.gpkg" "Content Type" "application/octet-stream"

	;Generate the context entries for geopackages
	;First the menu
	;menu-name,submenu,icon
	WriteRegStr HKCU "Software\Classes\GeoPackage\Shell\Handler" "MUIVerb" "Handler"
	WriteRegStr HKCU "Software\Classes\GeoPackage\Shell\Handler" "ExtendedSubCommandsKey" "GeoPackage\HandlerMenu"
	WriteRegStr HKCU "Software\Classes\GeoPackage\Shell\Handler" "Position" "Top"
	WriteRegStr HKCU "Software\Classes\GeoPackage\Shell\Handler" "Icon" "$INSTDIR\handler\handler.ico"

	#Second, menu entries
	WriteRegStr	HKCU "Software\Classes\GeoPackage\HandlerMenu\shell\ToGDBLyr" "MUIVerb" "Convert to GDB (Arc Only)"
	WriteRegStr	HKCU "Software\Classes\GeoPackage\HandlerMenu\shell\ToGDBLyr\command" "" '"$ARC_PYTHON" "$INSTDIR\handler\toGDBLYR.py" -i "%1" '
	WriteRegStr	HKCU "Software\Classes\GeoPackage\HandlerMenu\shell\ToShpDir" "MUIVerb" "Convert to SHP Dir (OSGeo4W)"
	WriteRegStr	HKCU "Software\Classes\GeoPackage\HandlerMenu\shell\ToShpDir\command" "" '"$OSGEO4W_PYTHON" "$INSTDIR\handler\toSHPDIR.py" "%1" '
	WriteRegStr	HKCU "Software\Classes\GeoPackage\HandlerMenu\shell\Add_PCI_Classification" "MUIVerb" "Add PCI Classifications (OSGeo4W)"
	WriteRegStr	HKCU "Software\Classes\GeoPackage\HandlerMenu\shell\Add_PCI_Classification\command" "" '"$OSGEO4W_PYTHON3" "$INSTDIR\handler\classifications.py" "%1" '

SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

	CreateDirectory "$SMPROGRAMS\Handler"
	CreateShortcut "$SMPROGRAMS\Handler\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0

	; Generate a Show Installed Folder Shortcut
	CreateShortcut "$SMPROGRAMS\Handler\Installation Folder.lnk" "$INSTDIR" "" "$INSTDIR" 0

SectionEnd


;--------------------------------

; Uninstaller

Section "Uninstall"
  SetRegView 32

  ; Delete ArcToolbox Version
  ; if RegKEY HKLM ...
  ; Delete tbx and directory
  ; delete reg key

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MNRF_NWR_RIAU_Handler"
  DeleteRegKey HKLM "Software\MNRF_NWR_RIAU_Handler"

  ; Remove Context Entry Registry Keys
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2009\command"
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2009"
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2017\command"
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Menu\shell\CheckFMP2017"
  ;DeleteRegKey HKCU "Software\Classes\Shapefile\Shell\Checker"
  DeleteRegKey HKCU "Software\Classes\GeoPackage\HandlerMenu"
  ;DeleteRegKey HKCU "Software\Classes\.gpkg"


  ; Remove files and uninstaller
  ;Delete $INSTDIR\checker.nsi
  RMDIR /r $INSTDIR\handler
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Handler\*.*"

  ; Remove directories used
  ;RMDir "$SMPROGRAMS\handler"
  ;RMDir "$SMPROGRAMS\arcpy_support"
  RMDir "$SMPROGRAMS"
  RMDir /r "$INSTDIR"

SectionEnd


Function .onInit
	SetRegView 32

	; Set a default ARC_PYTHON value
	SectionGetFlags ${arc} $0
	SectionGetFlags ${osgeo4w} $1
	SectionGetFlags ${independent} $0

	StrCpy $ARC_PYTHON "$INSTDIR\arcpy_support\env_arcpy.bat"
	StrCpy $INDEPENDENT_PYTHON "C:\Python27\python.exe"
	StrCpy $OSGEO4W_PYTHON "$INSTDIR\osgeo_python.bat"
	StrCpy $OSGEO4W_PYTHON3 "$INSTDIR\osgeo_python3.bat"
	StrCpy $OSGEO4W_ENV "$INSTDIR\osgeo_env.bat"

FunctionEnd

