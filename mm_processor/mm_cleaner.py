#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  mm_cleaner.py
#
#  Copyright 2017 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# Remove the elements from mindmaps that are unhelpful... Folded=, and all the dates

import os
import sys
import getopt

#Local Imports:
from freemind import fm_map
from freemind import fm_node
from freemind import fm_icon
from freemind import parse,parseLiteral,parseEtree,parsexml_,get_root_tag

from conductor import conductor
c = conductor()

def main(argv):

	# Load the mindmap
	#print "Hello, World!"

	#print argv

	cond = conductor()

	#for arg in argv:
	#	print arg

	try:
		opts, args = getopt.getopt(argv, "vo:i:wmjhtfr",
								   ["help", "verbose","outputfile=",
									"inputfile=","overwrite","mindmap","json","html","test","folding","trim","remove_id","json_tests_only","process_includes"
									])
	except getopt.GetoptError:
		pass
		usage()
		sys.exit(2)

	#Set defaults for conductor:
	cond.debug = False
	cond.inputfile = r'..\tech_spec\2009\FMP.mm'
	cond.outputfile = 'clean.mm'
	cond.overwrite = False
	cond.verbose = True

	# Local options for convenience
	do_mindmap = False
	global do_folding
	global do_remove_id
	do_folding = False
	do_remove_id = False
	do_json = False
	do_html = False
	do_tester = False
	do_data_generation_trimming	= False
	json_tests_only = False
	process_includes = False

	for opt, arg in opts:
		if opt in ["--help"]:
			usage()
			sys.exit()
		elif opt in ["-v", "--verbose"]:
			cond.verbose = True
			cond.debug = True
		elif opt in ["-o", "--outputfile"]:
			cond.outputfile = arg
		elif opt in ["-i", "--inputfile"]:
			print 'Using -i %s'%(arg)
			cond.inputfile = arg
		elif opt in ["-w", "--overwrite"]:
			cond.overwrite = True
		elif opt in ["-m", "--mindmap" ]:
			do_mindmap = True
		elif opt in ["-j", "--json"]:
			do_json = True
		elif opt in ["-h", "--html"]:
			do_html = True
		elif opt in ["-t", "--test"]:
			do_tester = True
		elif opt in ["-f", "--folding"]:
			do_folding = True
		elif opt in ["--trim"]:
			do_data_generation_trimming = True
		elif opt in ["-r", "--remove_id"]:
			do_remove_id = True
		elif opt in [ "--json_tests_only"]:
			json_tests_only = True
		elif opt in [ "--process_includes"]:
			process_includes = True


	if  cond.inputfile == cond.outputfile and not cond.overwrite  :
		print 'Name collision. Backing away from rewriting input as output...'
		raise ValueError('Name collision. Backing away from rewriting input as output...')


	#print cond.inputfile[-4:]
	if cond.inputfile[-2:] == 'mm':
		cond.getmm(process_includes=process_includes)
	elif cond.inputfile[-4:] == 'json':
		cond.getjson(process_includes=process_include)
		cond.convert_json2mm()

	# Iterate the nodes
	cond.iteratethroughnodes(callback=callback)

	#Unfold the Root
	if do_folding: cond.mm.get_node().set_FOLDED(None)

	if do_tester :
		#cond.run_tester(table="ScheduledHarvest", field="SILVSYS")
		cond.run_tester()

	# Remove anything not needed for data generation
	if do_data_generation_trimming :
		cond.iteratethroughnodes(callback=callback_to_remove_parts)

	# Save the mindmap - use default or parameter
	if do_mindmap :
		cond.putmm()

	if do_json :
		cond.convert_mm2json()

		if json_tests_only:
			#Save json file
			cond.putjson_tests_only( cond.outputfile.replace(".mm",".json") )
		else:
			#Save json file
			cond.putjson( cond.outputfile.replace(".mm",".json") )

	if do_html :
		cond.convert_mm2json()

		#Save json file
		#cond.debug = True
		cond.puthtml( cond.outputfile.replace(".mm",".html") )

	return 0



def callback(n):
	global do_folding
	global do_remove_id
	#print "Node TEXT: %s"%(n.TEXT)
	## Remove "Folded" attribute or Set to None
	## Remove other extraneous attributes in the same way...
	n.set_TEXT(c.ununicode(n.TEXT))
	if do_folding: n.set_FOLDED('true')
	n.set_CREATED(None)
	n.set_MODIFIED(None)
	if do_remove_id: n.set_ID(None)

def callback_to_remove_parts(n):
	if n.node :   #implies it has subnodes
		subnodes = [ str(nn.TEXT) for nn in n.node ]
		# If this is a table node, it has fields as a subnode...
		if "fields" in subnodes:
			#remove any subnodes that are not in [ 'name' , shortname, spatial_reference, fields, geometry_type, alias, path ]
			#print subnodes
			goodnodes = [ nn for nn in n.node if str(nn.TEXT).lower() in ['name' , 'shortname', 'spatial_reference', 'fields', 'geometry_type', 'alias', 'path' ] ]
			n.node = goodnodes
			subnodes = [ str(nn.TEXT) for nn in n.node ]
			#print subnodes
			#print "-------------------------------"
		elif "type" in subnodes :  #That is most likely a field object
			subnodes = [ str(nn.TEXT) for nn in n.node ]
			#print subnodes
			goodnodes = [ nn for nn in n.node if str(nn.TEXT).lower() in ['name' , 'shortname', 'length', 'type', 'alias', 'domain' ] ]
			n.node = goodnodes
			subnodes = [ str(nn.TEXT) for nn in n.node ]
			#print subnodes
			#print "-------------------------------"


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv[1:]))
