@echo on

call "c:\OSGeo4W\bin\o4w_env.bat"

:: C:\OSGeo4W\OSGeo4W.bat python c:\src\FMPDS\checker\cmd_checker.py -i c:\GIS\FMP\Crossroute\FMP\2020\CRSA_20PCI_APR20.gdb -l MUCRSA_20PCI00_APR20  --forest=Crossroute --product=FMP --year=2020 --planyear=2020  -t PCI -r -r -o clean.html
:: FOR %A IN (list) DO command [ parameters ]
::c:\src\FMPDS\mm_processor\
FOR %%f IN ( %~dp0\..\tech_spec\2017\fmp.mm %~dp0\..\tech_spec\2017\ar.mm %~dp0\..\tech_spec\2017\aws.mm ) DO python  %~dp0\mm_cleaner.py -i %%f -o %%f --overwrite --mindmap --folding --remove_id --test

FOR %%f IN ( %~dp0\..\tech_spec\2018\*.mm  ) DO python  %~dp0\mm_cleaner.py -i %%f -o %%f --overwrite --mindmap --folding --remove_id --test

FOR %%f IN ( %~dp0\..\tech_spec\2009\*.mm ) DO python  %~dp0\mm_cleaner.py -i "%%f" -o "%%f" --overwrite --mindmap --folding --remove_id --test

::"help", "verbose","outputfile=","inputfile=","overwrite","mindmap","json","html","test","folding","trim","remove_id","json_tests_only"--forest=Crossroute --product=FMP --year=2020 --planyear=2020  -r -r -o clean.html -t PCI  -l MUCRSA_20PCI00_APR20



timeout /T 900
