#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  build_empty_dataset.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import sys
from osgeo import gdal
from osgeo import ogr
from osgeo import osr


sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from checker.filechecker import jsonloader
from validator import validator
import stack.choose_a_tech_spec

#  empty_dataset
# The idea here is to read information from a mindmap to build a geopackage
# containing an empty dataset.
#
# Stretch goal: foreign keys... :-)

fieldTypeLUT = {'integer':ogr.OFTInteger, 'string':ogr.OFTString, 'text':ogr.OFTString, 'character':ogr.OFTString ,'float':ogr.OFTReal, 'double':ogr.OFTReal}
geom_type_LUT = { 'Polygon':ogr.wkbPolygon, 'Polyline':ogr.wkbLineString, 'Point': ogr.wkbPoint }

# This is based on the sample from geopackage.org ( https://www.geopackage.org/#sampledata )

class empty_dataset_builder( jsonloader ):
	srs = osr.SpatialReference() # SRS used for data. Assuming we are always using 1 SRS in this system
	v = None
	layers = dict()

	def __init__(self,jsonfilename=None,outputfilename="mu702_2020_FMPDP.gpkg", srs=26916 , year=2020, planyear=2020, lower_case=True, tables_to_create=None , use_fim_compliant_names= True, use_multi=False):
		if jsonfilename is None:
			jsonfilename = stack.choose_a_tech_spec.choose_a_tech_spec('FMP',2020)

		jsonloader.__init__(self, jsonfilename, ignore_non_tables=True)

		outputfilename=outputfilename.lower() if lower_case else outputfilename.upper()

		print ("Generating Output File at {0}".format(outputfilename))

		if isinstance(srs,int):  # e.g. 26916
			self.srs.ImportFromEPSG(srs)
		elif isinstance(srs, str):    # e.g. "WGS84"
			self.srs.SetFromUserInput(srs)
		elif srs is not None :
			self.srs = srs

		if os.path.exists(outputfilename):
			self.ds = ogr.Open(outputfilename,update=1)
		else:
			self.ds = ogr.GetDriverByName('GPKG').CreateDataSource(outputfilename)

		self.year=year
		self.planyear = planyear

		if tables_to_create is None:
			tables_to_create = [ t['name'] for t in self.json ]

		for t in self.json:
			try:
				if t['name'] in tables_to_create:
					self.build_layer(layerdesc=t,lower_case=lower_case, use_fim_compliant_names=use_fim_compliant_names,use_multi=use_multi)
			except RuntimeError as e:
				print e
				print "moving on..."

		#print "Ready to Close..."


	def build_layer(self,layerdesc,lower_case=True,use_fim_compliant_names=True,use_multi=False):
		# Create Data Layer
		try:
			geom_type = geom_type_LUT[layerdesc['geometry_type']  ]
			if use_multi and geom_type in [ 1,2,3 ]:   # wkbPoint = 1,wkbLineString = 2,wkbPolygon = 3
				geom_type = geom_type + 3 # wkbMultiPoint = 4,wkbMultiLineString = 5,wkbMultiPolygon = 6,
		except KeyError as e:
			print e
			raise(e)

		#print "Doing layer"
		#CreateLayer(DataSource self, char const * name, SpatialReference srs=None, OGRwkbGeometryType geom_type, char ** options=None)

		if use_fim_compliant_names:
			layername = ('mu702_'+str(self.year)[2:]+layerdesc['shortname']+'00').lower() if lower_case else ('mu702_20'+layerdesc['shortname']+'00').upper()
		else:
			layername = layerdesc['name']

		lyr = self.ds.CreateLayer(layername , srs=self.srs, geom_type = geom_type, options=['OVERWRITE=YES'])

		#print "Doing Fields"
		for f in layerdesc['fields'] :
			try:
				field_type = fieldTypeLUT[f['type'].lower() ]
			except KeyError as e:
				print e
				raise(e)

			lyr.CreateField( ogr.FieldDefn(f['name'], field_type ) )
		#print "Fields Done"
		self.layers[layerdesc['name'] ] = lyr

	def addfield(self,tablename,name,field_type=None):
		if field_type in fieldTypeLUT.values():
			pass
		elif field_type in fieldTypeLUT.keys():
			field_type = fieldTypeLUT[field_type.lower() ]
		else :
			field_type = ogr.OFTString

		try:
			lyr=self.layers[tablename]
		except KeyError :
			lyr = self.ds.GetLayerByName(tablename)

		#Only create if not already present...
		#print "Field {n} Index {i}".format(n=name,i=lyr.GetLayerDefn().GetFieldIndex(name))
		if lyr.GetLayerDefn().GetFieldIndex(name) == -1:
			lyr.CreateField( ogr.FieldDefn(name, field_type ) )

	def get_sample_feature(self,tablename):
		t = self.findTable(tablename)
		if t is None: return {}

		retdict = dict()
		for f in t['fields']:
			if 'default' in f.keys():
				retdict[ f['name'] ] = f['default']
			else:
				retdict[ f['name'] ] = f['values'][0]['name'] if 'values' in f.keys() else None


			if retdict[ f['name'] ] == 'None':
				retdict[ f['name'] ] = None
			elif fieldTypeLUT[f['type'].lower() ] == ogr.OFTString and retdict[ f['name'] ] is not None :
				retdict[ f['name'] ] = str(retdict[ f['name'] ] )
		return retdict

	def get_sample_features(self):
		return dict([ ( r, self.get_sample_feature(r) ) for r in self.getTableList() ])

	def get_perturbations(self, tablename):
		t = self.findTable(tablename)
		#if t is None: return {}

		v = validator(json=self.json, tabletype=tablename, year=self.year, planyear=self.planyear )

		retdict = dict()


		if isinstance(t['fields'], str) or isinstance(t['fields'], unicode):
			print "=======> Found a single Field Table %s"%tablename
		for f in  t['fields'] :
			if t['name'] in [ 'PlannedAggregateExtractionAreas', 'TreeImprovement' ]:
				print v.getTests(f['name'])

			retdict[ f['name'] ] = v.getTests(f['name'])

		return retdict

	def build_dummy_feature(self, row, tablename, fid_name='OBJECTID'):
		"""
		Builds a feature using dummy wkt based on the
		POLYID and the feature type
		"""
		t = self.findTable(tablename)
		geom_type = geom_type_LUT[t['geometry_type']  ]

		try:
			lyr=self.layers[tablename]
		except KeyError :
			lyr = self.ds.GetLayerByName(tablename)

#		print "Building Dummy Feature"
#		print fid_name
#		print row
#		print row[fid_name]


		# Generate WKT Geometry from polyid only
		step = 1000 # metres
		x_min = 400000 +    ( int (row[ fid_name ]) % 50 ) * step
		y_min = 5900000+ int(int  (row[ fid_name ]) / 50 ) * step

		if geom_type == ogr.wkbPolygon :
			wkt = 'POLYGON(( {x_min} {y_min} , {x_min} {y_max}, {x_max} {y_max}, {x_max} {y_min}, {x_min} {y_min} ))'.format(x_min=x_min,x_max=x_min + step, y_min=y_min,y_max=y_min + step)
		if geom_type == ogr.wkbLineString:
			wkt = 'LINESTRING( {x_min} {y_min} , {x_max} {y_max} ))'.format(x_min=x_min,x_max=x_min + step, y_min=y_min,y_max=y_min + step)
		if geom_type == ogr.wkbPoint  :
			wkt = 'POINT( {x} {y} )'.format(x=x_min+step/2,y=y_min + step/2)

		feat = ogr.Feature(lyr.GetLayerDefn())
		for k,v in row.iteritems():
			try:
				#print "Trying feature {k}\t".format(k=k),
				#if lyr.FindFieldIndex( k, True ) >= 0: #Test for presence of field before pushing data in...
				feat[k] = v
				#print "...success"
			except ValueError as e :
				#feat[k] is either a string, and we are coercing, or an integer...
				#print "Trying feature {k}\t".format(k=k),
				#print lyr.FindFieldIndex( k, True )
				#print feat.GetFieldDefnRef(lyr.FindFieldIndex(k)).getType()
				#GetFieldDefnRef OGR_Fld_GetType( lyr.GetLayerDefn().GetField(lyr.GetLayerDefn().GetFieldIndex(k))
				#print '{v}<-{t}'.format(v=v,t=type(v))
				#print e
				#raise e
				pass
			except KeyError as e :
				# KeyError: 'Illegal field requested in SetField()'
				# There is no such field in the feature
				#
				# If the field cannot be found, there's no point in trying...
				#print "Trying feature {k}<-{v}\t".format(k=k,v=v),
				#if lyr.FindFieldIndex( k, True ) > 0:
				#	print feat.GetFieldDefnRef(lyr.FindFieldIndex(k,True)).getType()
				#GetFieldDefnRef OGR_Fld_GetType( lyr.GetLayerDefn().GetField(lyr.GetLayerDefn().GetFieldIndex(k))
				#print '{v}<-{t}'.format(v=v,t=type(v))
				#print e
				#raise e
				pass

		feat.SetGeometry(ogr.CreateGeometryFromWkt(wkt))
		lyr.CreateFeature(feat)


def main(args):
	m = empty_dataset_builder()
	print m.get_sample_features()
	print m.get_perturbations("ForecastDepletions")
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
