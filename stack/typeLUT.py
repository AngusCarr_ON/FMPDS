#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  typeLUT.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


def inverse_mapping(f):
	return f.__class__(map(reversed, f.items()))

def type_match(t):
	try:
		return fieldTypeLUT_reverse[fieldTypeLUT[t.lower()]]
	except KeyError:
		return t

try:
	from osgeo import gdal
	from osgeo import ogr
	from osgeo import osr


	###############################
	## Some general osgeo and mindmap stuff
	## This is to take the mindmap type and generate a file-format type
	fieldTypeLUT  = {'integer':ogr.OFTInteger,	#0
		'integerList':ogr.OFTIntegerList,		#1
	#	'integer':ogr.OFTInteger,				#12
	#	'integerList':ogr.OFTInteger64List,		#13
		'real':ogr.OFTReal,						#2
		'float':ogr.OFTReal,
		'double':ogr.OFTReal,
		'realList':ogr.OFTRealList,				#3
		'string':ogr.OFTString,					#4
		'text':ogr.OFTString,
		'character':ogr.OFTString ,
		'stringList':ogr.OFTStringList,			#5
		'widestring':ogr.OFTWideString,			#6
		'widestringList':ogr.OFTWideStringList,	#7
		'binary':ogr.OFTBinary,					#8
		'date':ogr.OFTDate,						#9
		'time':ogr.OFTTime,						#10
		'datetime':ogr.OFTDateTime,				#11
		'fimdate':ogr.OFTString,
		}
	#  OFTInteger = 0, OFTIntegerList = 1, OFTReal = 2, OFTRealList = 3,
	#  OFTString = 4, OFTStringList = 5, OFTWideString = 6, OFTWideStringList = 7,
	#  OFTBinary = 8, OFTDate = 9, OFTTime = 10, OFTDateTime = 11,
	#  OFTInteger64 = 12, OFTInteger64List = 13, OFTMaxType = 13

	geom_type_LUT = { 'Polygon':ogr.wkbPolygon, 'Polyline':ogr.wkbLineString, 'Point': ogr.wkbPoint }
	e00_type_LUT  = { 'Polygon':'PAL', 'Polyline':'ARC', 'Point': 'LAB' }
	################################

	fieldTypeLUT_reverse = inverse_mapping(fieldTypeLUT)
	fieldTypeLUT_reverse  = {ogr.OFTInteger:'integer',	#0
		ogr.OFTIntegerList:'integerList',		#1
		ogr.OFTInteger64:'integer',				#12
		ogr.OFTInteger64List:'integerList',		#13
		ogr.OFTReal:'real',						#2
		ogr.OFTRealList:'realList',				#3
		ogr.OFTString:'string',					#4
		ogr.OFTStringList:'stringList',			#5
		ogr.OFTWideString:'string',			#6
		ogr.OFTWideStringList:'stringList',	#7
		ogr.OFTBinary:'binary',					#8
		ogr.OFTDate:'date',						#9
		ogr.OFTTime:'time',						#10
		ogr.OFTDateTime:'datetime'				#11
		}

	geom_type_LUT_reverse = inverse_mapping(geom_type_LUT)
	e00_type_LUT_reverse = inverse_mapping(e00_type_LUT)
except ImportError:
	#FIXME! This should be based on the actual list....
	arcpy_fieldTypeLUT_reverse ={'Blob':'Blob',
	'Date':'date',
	'Single':'float',
	'Float':'float',
	'Double':'float',
	'Real':'float',
	'Guid':'GlobalID',
	'Integer':'integer',
	'SmallInteger':'integer',
	'OID':'oid',
	'raster':'raster',
	'Character':'string',
	'String':'string',
	'Text':'string',
	'Geometry':'geometry'
		}
#	arcpy_fieldTypeLUT = inverse_mapping(arcpy_fieldTypeLUT_reverse)
	arcpy_fieldTypeLUT = {'Blob':'Blob',
	'date':'Date',
	'float':'Float',
	'GlobalID':'Guid',
	'integer':'integer',
	'oid':'OID',
	'raster':'raster',
	'string':'string',
	'fimdate':'string',
	'geometry':'Geometry'
		}
	fieldTypeLUT_reverse = arcpy_fieldTypeLUT_reverse
	fieldTypeLUT = arcpy_fieldTypeLUT

def main(args):
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
