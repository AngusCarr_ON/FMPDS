#pg_db.py

import os

try:
  import pwd
except ImportError:
  import getpass
  pwd = None

try:
	import psycopg2
	import psycopg2.extras

	#Test to see if we can get into the DB, and if not, raise an ImportError or something...
	try:
		from db_config import default_db_string
		db_string = default_db_string
		#look for a site-specific file that conatains a db string
		# Create it with:
		# echo "default_db_string = \"dbname='fmp' user='bob' host='localhost' \" " > db_config.py
	except ImportError:
		if pwd:
			current_user = pwd.getpwuid(os.geteuid()).pw_name.lower()
		else:
			current_user = getpass.getuser().lower()
		db_string = "dbname='fmp' user='%s' host='lrcgikdcwhiis06' "%(current_user)

	try:
		conn = psycopg2.connect(db_string)
	except psycopg2.OperationalError as e:
		print "Can't connect to the PG database... Using static data"
		print str(e)
		raise ImportError
	cur = conn.cursor()
	cur.execute("SELECT current_user " )
	r = cur.fetchone()
	print "Testing database connectivity... User name comes back as %s"%(r[0])

	from  static_db import static_db
except ImportError:
	from  static_db import pg_db,static_db
else:
	class pg_db():
		db_string = None
		db_type = "pg_db"

		def __init__(self,db_string=None,port=None,dbname=None,host=None,user=None,password=None):
			if db_string is not None:
				self.db_string = db_string
			else:
				self.get_db_string()
			parmdict= dict([ p.split('=') for p in self.db_string.split(' ') if '=' in p ] )
			#print parmdict
			if dbname is not None:
				parmdict['dbname'] = "'"+dbname+"'"
			if port is not None:
				parmdict['port'] = ""+str(port)+" "
			if host is not None:
				parmdict['host'] = "'"+host+"'"
			if user is not None:
				parmdict['user'] = "'"+user+"'"
			if password is not None:
				parmdict['password'] = "'"+password+"'"

			self.db_string = ' '.join(['='.join(p) for p in parmdict.iteritems() ])
			print self.db_string



		def current_user(self):
		  if pwd:
			return pwd.getpwuid(os.geteuid()).pw_name.lower()
		  else:
			return getpass.getuser().lower()

		def default_db_string(self):
			return "dbname='fmp' user='%s' host='lrcgikdcwhiis06' "%(self.current_user())

		def get_db_string(self):
			try:
				from db_config import default_db_string
				self.db_string = default_db_string
				#look for a site-specific file that conatains a db string
				# Create it with:
				# echo "default_db_string = \"dbname='fmp' user='bob' host='localhost' \" " > db_config.py
			except ImportError:
				self.db_string = self.default_db_string()
			return self.db_string

		def run_update(self,sql):
			#print sql
			try:
				conn = psycopg2.connect(self.db_string)
				cur = conn.cursor()
				cur.execute(sql)
				conn.commit()
			except psycopg2.ProgrammingError as e:
				conn.commit()
				print e
				print sql
				return None
			except Exception as e:
				print e
				print sql
				raise e

		def run_query_with_return(self,sql):
			#print sql
			try:
				conn = psycopg2.connect(self.db_string)
				cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
				cur.execute(sql)
				r = cur.fetchall()
				conn.commit()
				return r
			except psycopg2.ProgrammingError as e:
				conn.commit()
				print e
				print sql
				return None
			except Exception as e:
				print e
				print sql
				raise e

		def run_select(self,sql):
			""" Return first value from the first row of the return from SQL"""
			#print sql
			try:
				conn = psycopg2.connect(self.db_string)
				cur = conn.cursor()
				cur.execute(sql)
				r = cur.fetchone()
				return r[0]
			except TypeError as e:
				print e
				print sql
				return None
			except psycopg2.ProgrammingError as e:
				print e
				print sql
				return None
			except Exception as e:
				print e
				print sql
				raise e

		def run_select_get_dict(self,sql):
			""" Return values from the all rows of the return from SQL as a dictionary"""
			#print sql
			try:
				conn = psycopg2.connect(self.db_string)
				cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
				cur.execute(sql)
				r = cur.fetchall()
				return r
			except TypeError as e:
				print e
				print sql
				return None
			except psycopg2.ProgrammingError as e:
				print e
				print sql
				return None
			except Exception as e:
				print e
				print sql
				raise e

		def run_sql_onevalue(self,sql):
			""" Return first value from the first row of the return from SQL"""
			if sql.lower().startswith('select'):
				return self.run_select(sql)
			else:
				return self.run_update(sql)

		def get_list_of_fmus(self):
			sql = "SELECT fmu from fmp.fmu_lut ORDER BY id"
			return [ n[0] for n in self.run_select_get_dict(sql) ]

		def get_plan_submission_id(self,fmu,product,year):
			#sql = "SELECT id FROM fmp.submission WHERE upper(fmu) = upper(%s) and upper(product) == upper(%s) AND year <= %s "%(fmu,'plan',str(year))
			sql = """SELECT fi_portal_id FROM fmp.submissions s JOIN ( 	SELECT fmu, min(year) as min_year FROM fmp.submissions WHERE product = 'fmp' GROUP BY fmu 	) p ON p.fmu = s.fmu AND p.min_year <= s.year
						WHERE s.fmu = '%s' and s.year = %s ORDER BY s.fmu, product, year"""%(fmu,str(year))
			return [ n[0] for n in self.run_select_get_dict(sql) ][0]

		def get_plan_year(self,fmu,year):
			#sql = "SELECT year FROM fmp.submissions WHERE upper(fmu) = upper('%s') AND upper(product) = upper('%s') AND year <= %s AND year > %s - 10 ORDER BY year ASC LIMIT 1 "%(fmu,'fmp',str(year),str(year))
			sql = """SELECT p.min_year as planyear FROM fmp.submissions s JOIN ( 	SELECT fmu, min(year) as min_year FROM fmp.submissions WHERE product = 'fmp' GROUP BY fmu 	) p ON p.fmu = s.fmu AND p.min_year <= s.year
						WHERE s.fmu = '%s' and s.year = %s ORDER BY s.fmu, product, year"""%(fmu,str(year))
			try: #Edits by James to account for instances where submission hasn't been loaded into FMPDS at this point.
				return [ n[0] for n in self.run_select_get_dict(sql) ][0]
			except IndexError as e:
				try:
					print '- Submission has not been loaded into FMPDS.  Using static_db values for submission check.'
					# plan start year has to be less than or equal to the product submission year
					query = [planyr for planyr in static_db[fmu][1] if planyr <= int(year) ]
					# if there are more than one plan year that is less than or equal to current submission year, choose the most recent one
					query.sort()
					if len(query)>0:
						return query[-1]
					# if there's no plan year that fits the above criteria, return 0
					else:
						return 0
				except KeyError:
					return 0  # No plan name that fits...

		def run_select_get_value_or_rowlist(self, sql):
			conn = psycopg2.connect(self.db_string)
			cur = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
			try:
				cur.execute(sql)
			except psycopg2.ProgrammingError as e:
				print e
				raise e
			rows = cur.fetchall()

			#set up a list of row_dictionaries
			rowlist = [ r._asdict() for r in rows ]

			# return a text string if the value is a singleton
			if (len(rowlist) == 1) :
				if len(rowlist[0].values()) == 1 :
					return (rowlist[0]).values()[0]

			#Otherwise return the list of dictionaries
			return rowlist

		def FMUCodeConverter(self, x):
			# if the input x is fmu name, the output is the code and vice versa.
			# note that the input should be String format.
			sql = "SELECT fmu, fmu_num FROM fmp.fmu_lut"
			#sql = "SELECT fmu from fmp.fmu_lut ORDER BY id"
			r =  self.run_select_get_dict(sql)
			for (f,c) in r:
				if f == x:
					return c
				elif c == x:
					return f
				elif str(c).zfill(3) == str(x).zfill(3):
					return f
			print "ERROR: " + x + " is neither fmu name nor the code."
			return None

if __name__ == "__main__":
	p= pg_db()
	print p.current_user()
	#print run_sql_onevalue("UPDATE fmp.submissions SET consolidation_success = '%d format: a number is required, not str' WHERE fi_portal_id = 17227")
	print p.run_sql_onevalue("SELECT date_part('epoch',consolidation_time) FROM fmp.submissions WHERE fi_portal_id=%s"%( str(17227) ))
	print p.run_select_get_dict("SELECT * FROM fmp.submissions WHERE fi_portal_id>%s"%( str(17800) ))
	sql = """
		SELECT id,
			import_path || '\\' || fmu || '\\' ||  product || '\\' || year || '\\fi_submission_' || fi_portal_id || '.txt '  as filename ,
			fi_portal_status
		FROM fmp.consolidation_failures
		WHERE failure_type = 'import'
			AND fmu = 'Trout_Lake'
		LIMIT 1
		"""
	print p.run_select_get_dict(sql)
	print p.get_list_of_fmus()
	print p.FMUCodeConverter('Whitefeather')
	print p.FMUCodeConverter('405')
	print p.FMUCodeConverter(405)

	print "Testing db connection with parameters"
	p2 = pg_db(port=5433,dbname='FI_Portal_Scrape')
	print p2.db_string
	p2.current_user()
	sql = """WITH q as (
                SELECT
                s.submissionid as submissionid,
                s.managementunitdistrict as forest ,
                s.planperiod as planperiod, s.planterm as planterm,
                CASE
                        WHEN s.product = 'Annual Report' THEN 'AR'
                        WHEN s.product = 'Annual Work Schedule' THEN 'AWS'
                        WHEN s.product = 'Final Plan' THEN 'FMP'
                        WHEN s.product = 'Draft Plan' THEN 'FMP'
                        ELSE upper(s.product)
                END AS product,
                s.fiscalyear ,
                left(s.fiscalyear , 4 ) as year,
                f.filenamepath as filename
                FROM fi_portal_scraper.fiportal_scrape s
                LEFT JOIN fi_portal_scraper.importlog i on s.submissionid = s.submissionid
                JOIN fi_portal_scraper.fiportal_scrape_downloaded f ON s.submissionid = f.fk_downloadsubid
                WHERE
                        i.result is NULL
                ORDER BY s.submissionid DESC
                )
                SELECT *
                FROM q
         WHERE product LIKE 'AR'
                AND
                forest LIKE 'Gordon Cosens%'
                AND
                year LIKE '2017';"""
	print p2.run_select_get_dict(sql)
