#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  paths.py
#
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

basepaths = [r'\\cihs.ad.gov.on.ca\mnrf\Groups\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP',
             r"\\lrcpptboshfs001\MNR_SR_External\FOREST_MGMT\FMPDS",
             r'\\lrcpsoprfp00001\GIS-DATA\WORK-DATA\FMPDS'
            ]
if __name__ == '__main__':
	print basepaths
