#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  file.py
#  
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#######################################################################
# Generic File components
#######################################################################

import os
import sys

def fileExists(infilePath='', inFileName='', perms_check=os.R_OK):
	#checks for the existence of file and if user has permissions to read/access the file
	if infilePath=='' and inFileName=='':
		return False
	
	try:
		if os.path.isfile(os.path.join(infilePath,inFileName)) and os.access(os.path.join(infilePath,inFileName),perms_check):
			return True
		else:
			return False
	except:
		return False

def main(args):
	testdir = r"\\lrcpsoprfp00001\GIS-DATA\WORK-DATA\FMPDS\Abitibi_River\AWS\2017"
	print(fileExists(testdir, "fi_submission_21401.txt"))
	
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
