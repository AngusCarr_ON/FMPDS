#
# Equations and concepts derived from MIST and (hopefully) refined.
#
# The code at the bottom will produce a lookup table to operationalize this.
#
# This is the "Calculate SI" Code. The search in the case of the iterating
# species is a linear search, because it is not time-critical in this case.
# The search could be more refined. :-)
#
#
#
import sys
import math

#sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))
from ht_equations_and_LUT  import ht_equations
from si_parameters import siid_tbl

def  Iteration(SP,AGE,HT,AGE2BH,BH_INDX_AGE,min_si=1.5,max_si=30):
	"""Estimates SI by progressive approximation using the height equation..."""
	ht_equn = ht_equations[SP]

	minht = 1000.0 #Search fit value

	divisor = 10 # 5 means .0, .2, .4, .6, .8
	si_index = 0 #Search value
	for si_int in range(int(min_si*divisor)-divisor,int(max_si*divisor)+divisor):
		si=float(si_int) / float(divisor)
		try:
			calcd_ht = ht_equn(float(AGE),float(si),float(AGE2BH),float(BH_INDX_AGE))
		except ValueError :
			#Probably too small an si to get a useful height...
			calcd_ht = -1.0
		except ZeroDivisionError as e:
			#print (str(e) +str(v))
			calcd_ht = -1.0

		try:
			if calcd_ht > 0.0 and abs(calcd_ht - float(HT)) < minht :
				minht = abs(calcd_ht - float(HT))
				si_index = si
		except 	TypeError as e:
			# Error caused by returned complex number
			# Python3 Error
			# TypeError: '>' not supported between instances of 'complex' and 'float'
			#print (str(e) +str(v))
			continue
		if calcd_ht - HT > 0.5:
			break
	if si_index <= max_si and si_index >= min_si :
		return si_index # FIXME.. WHY DID THEY ADD 0.5?  +0.5
	# Assumption on the +0.5 is that that represents the granularity of the
	# Calculations originally, and that moved
	return None

si_equations = {
"AB": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.2388*(HT*3.28-4.5)**1.1583*(1.0-math.exp(-0.0102*(AGE-AGE2BH)))**(-1.8455*(HT*3.28 - 4.5)**(-0.1883)))/3.28 ,
"AW": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.1728*(HT*3.28-4.5)**1.2560*(1.0-math.exp(-0.0110*(AGE-AGE2BH)))**(-3.3605*(HT*3.28 - 4.5)**(-0.3452)))/3.28 ,
"BD": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.1921*(HT*3.28-4.5)**1.2010*(1.0-math.exp(-0.0100*(AGE-AGE2BH)))**(-2.3009*(HT*3.28 - 4.5)**(-0.2331)))/3.28 ,
"BF": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5 + 0.0061*(HT*3.28)**1.3539*(1.0-math.exp(-0.00019*(AGE-AGE2BH)))** (-1.0286*(HT*3.28)**(-0.0723)))/3.28 ,
"BW": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.5119*(HT*3.28-4.5)**1.0229*(1.0-math.exp(-0.0167*(AGE-AGE2BH)))** (-1.0284*(HT*3.28 - 4.5)**(-0.0049)))/3.28 ,
"BY": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.1817*(HT*3.28-4.5)**1.2430*(1.0-math.exp(-0.0110*(AGE-AGE2BH)))**(-3.0184*(HT*3.28 - 4.5)**(-0.3180)))/3.28 ,
"CE": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.6528*(HT*3.28-4.5)**1.000*(1.0-math.exp(-0.0213*(AGE-AGE2BH)))**(-1.0243*(HT*3.28)**(-0.0046)))/3.28 ,
"CH": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.1073*(HT*3.28-4.5)**1.3455*(1.0-math.exp(-0.0070*(AGE-AGE2BH)))**(-3.3034*(HT*3.28 - 4.5)**(-0.3899)))/3.28 ,
"EW": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.1898*(HT*3.28-4.5)**1.2186*(1.0-math.exp(-0.0110*(AGE-AGE2BH)))**(-2.6865*(HT*3.28 - 4.5)**(-0.2717)))/3.28 ,
"HE": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.2172*(HT*3.28-4.5)**1.1309*(1.0-math.exp(-0.0105*(AGE-AGE2BH)))** (-1.9120*(HT*3.28 - 4.5)**(-0.1327)))/3.28 ,
"LA": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.6464*(HT*3.28-4.5)*(1.0-math.exp(-0.0225*(AGE-AGE2BH)))** (-1.1129))/3.28 ,
"MH": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.1984*(HT*3.28-4.5)**1.2089*(1.0-math.exp(-0.0110*(AGE-AGE2BH)))**(-2.4917*(HT*3.28 - 4.5)**(-0.2542)))/3.28 ,
"OR": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: (4.5+0.1692*(HT*3.28-4.5)**1.2648*(1.0-math.exp(-0.0110*(AGE-AGE2BH)))**(-3.4334*(HT*3.28 - 4.5)**(-0.3557)))/3.28 ,
"SW": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: 1.3 + (1.0/(40.6506*(HT-1.3)**5.6605*(1.0-(1.0-(1.0/(40.6506*(HT-1.3)**6.6605))**(1.0/(1.2544*(HT-1.3)**(-0.1567))))**((AGE - AGE2BH)/BH_INDX_AGE))**(1.2544*(HT-1.3)**(-0.1567)))),
"PJ": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: Iteration("PJ",AGE,HT,AGE2BH,BH_INDX_AGE),
"PO": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: Iteration("PO",AGE,HT,AGE2BH,BH_INDX_AGE),
"PR": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: Iteration("PR",AGE,HT,AGE2BH,BH_INDX_AGE),
"PW": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: Iteration("PW",AGE,HT,AGE2BH,BH_INDX_AGE),
"SB": lambda AGE,HT,AGE2BH=5.0,BH_INDX_AGE=50.0: Iteration("SB",AGE,HT,AGE2BH,BH_INDX_AGE,max_si=25)
}


def test():
	# Test 1
	print("Test 1- SB")
	sp = "SB"
	HT = 10.1549283275 # Should calculate a SI of 11, if this goes around...
	AGE=float(50)
	run_the_test(sp,HT,AGE)


	# Test 2
	print("Test 2- BD")
	sp = "BD"
	HT = 10.1549283275
	run_the_test(sp,HT,AGE)

def run_the_test(sp,HT,AGE):
	AGE2BH = float(siid_tbl[sp]['age2bh'])
	BH_INDX_AGE=float(siid_tbl[sp]['bh_indx_age'])

	print ("\tsi_equations[{sp}]({age},{ht},{age2bh},{bh_indx_age})={answer}".format(sp=sp,age=AGE,ht=float(HT),age2bh=AGE2BH, bh_indx_age=BH_INDX_AGE,answer=si_equations[sp](AGE,float(HT),AGE2BH,BH_INDX_AGE)))
	print ( "At {HT}: \tFunction" )
	for HT in range(10,25):
		si = si_equations[sp](AGE,float(HT),AGE2BH,BH_INDX_AGE)
		print ( "At  {HT} : \t{Function}".format(Function=si, HT=HT ) )

sp_max_ages = dict(zip(si_equations.keys(),[ 150 for sp in si_equations.keys() ]))
sp_max_ages['PJ'] = 120
sp_max_ages['PO'] = 110
sp_max_ages['BW'] = 110
#TOL HDWDS 210
#sp_max_ages[''] = 210
sp_max_ages['PW'] = 210
sp_max_ages['PR'] = 180
divisor = 10

def do_1_species_table(sp, insert_statement="INSERT INTO si_lut ( sp, age, ht, si ) VALUES ( '{sp}', {age}, {ht}, {si}) ;", ages=None, hts=None):
	#print ('-- {0}'.format(sys.argv))
	rows = list()
	max_age = sp_max_ages[sp]

	if ages is None:
		ages=range(20,max_age)

	if hts is None:
		hts = [ float(ht_times_divisor) / float(divisor) for ht_times_divisor in range(0,40*divisor+1) ]

	for age in ages:
		for ht in hts :
			try:
				v=dict(zip(['sp','age','ht','si'],[sp,age,ht,si_equations[sp](float(age),float(ht))]))
			except ValueError as e:
				# Can't always calculate a value...
				# This is the error in Python 2 when we are out of range.
				#print (str(e) + str(dict(zip(['sp','age','ht','si'],[sp,age,ht,'err']))))
				continue
			except ZeroDivisionError as e:
				#print (str(e) +str(v))
				continue

			try:
				#print (v)
				if v['si'] > 0.0 and v['si'] < 30:  # a reasonable valid range...
					rows.append(insert_statement.format(**v))
			except TypeError as e:
				# Error caused by returned complex number
				# Python3 Error
				# TypeError: '>' not supported between instances of 'complex' and 'float'
				#print (str(e) +str(v))
				pass

	return rows

def main(*args):
	msg=list()
	table_create_sql = "CREATE TABLE {schema}si_lut ( id serial PRIMARY KEY, sp character varying(2), age numeric, ht numeric, si numeric, CONSTRAINT si_lut_sp_age_ht_key UNIQUE (sp, age, ht)) ; "
	#insert_template =  "INSERT INTO si_lut ( sp, age, ht, si ) VALUES ( '{sp}', {age}, {ht}, {si}) ;"
	if '--sqlite' in args:
		schema = ''
		insert_statement = "INSERT INTO {schema}".format(schema=schema)+"si_lut ( sp, age, ht, si ) VALUES ( '{sp}', {age}, {ht}, {si}) ;"
		msg.append(table_create_sql.format(schema=schema))
		msg.append( "CREATE INDEX si_lut_sp_age_ht_idx ON si_lut (sp, age, ht);")
	else :
		schema = 'code.'
		msg.append(table_create_sql.format(schema=schema))
		insert_statement = "INSERT INTO {schema}".format(schema=schema)+"si_lut ( sp, age, ht, si ) VALUES ( '{sp}', {age}, {ht}, {si}) ;"
		msg.append( "CREATE INDEX si_lut_sp_age_ht_idx ON {schema}si_lut USING btree (sp COLLATE pg_catalog.\"default\", age, ht);".format(schema=schema))

	msg.append( "BEGIN ;")
	for sp in sp_max_ages.keys():
		msg.extend(do_1_species_table(sp,insert_statement) )
	msg.append( "COMMIT ;")
	return msg



if __name__ == '__main__':  #generate the ht lookup table
	#msg = main(sys.argv[:1])
	#for l in msg:
	#	print (l)
	#test()
	#print ("\n\t" + "\n\t".join(do_1_species_table("SB",ages=[80], hts=[15.0,15.1,15.2])))

	#print ("\n\t" + "\n\t".join(do_1_species_table("BD",ages=[80])))
#	print ("\n\t" + "\n\t".join(do_1_species_table("LA")))

	#print("Testing for Complex Numbers:")
	#do_1_species_table("BD",ages=[80])

	#for sp in sp_max_ages.keys():
	#	print ("\n\t".join(do_1_species_table(sp,ages=[80],hts=[0.1,20,39.9])))
	msg = main(sys.argv[:1])
	if msg:
		for l in msg:
			print (l)
