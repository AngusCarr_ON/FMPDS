#
#Table derived from MIST
# (as interpreted into postgresql at si_hi.ycl_species)
#
# Code at the bottom creates a lookup table to convert from missing species
# to known species
#
# It is up to the user to do anything with that...
#
#
import sys
#import os
#sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

siid_fields = [ 'orderid','sppid','stsppid','siid','htid','baid','ycbaid','succid','ycid','cullid','vbarid','scid','vbarselid','wgcode','age2bh','description','yctemplateidea','yctemplateidua','ycproductidea','ycproductidua','bh_indx_age' ]
rows =  [ [0,"Coeffient Table Name","","yccSI","yccHt","yccScBa","yccBa","","yccGMV,yccGTV,yccDen","yccCull","yccVBar","yccSc","yccVBarSel",None,None,"",0,0,0,0,0],
	[1,"AB","AB","AB","AB","MH","AB","MH","AB","AB","AX","MH","AB",20.00,5,"Black Ash",2,3,3,0,50],
	[2,"AW","AW","AW","AW","MH","AW","MH","AW","AW","AX","MH","AW",20.00,5,"White Ash",2,3,3,0,50],
	[3,"AX","AB","AB","AB","MH","AB","MH","AB","AB","AX","MH","AB",None,5,"Ash, any or mixed",2,3,3,0,50],
	[4,"BD","BD","BD","BD","MH","MH","BD","MH","BD","OH","MH","BD",29.00,5,"Basswood",2,3,3,2,50],
	[5,"BE","BE","MH","MH","MH","BE","BE","BE","BE","OH","MH","BE",29.00,5,"Beech",2,3,3,2,50],
	[6,"BF","BF","BF","BF","SB","BF","BF","BF","BF","BF","SB","BF",13.00,5,"Balsam Fir",1,0,1,0,50],
	[7,"BG","BW","BW","BW","BW","BW","BW","BW","BW","BW","BW","BW",None,5,"Grey birch",1,0,1,0,50],
	[8,"BN","OH","MH","MH","MH","MH","MS","MH","OH","OH","MH","OH",None,5,"Butternut",2,3,3,2,50],
	[9,"BW","BW","BW","BW","BW","BW","BW","BW","BW","BW","BW","BW",36.00,5,"White Birch",1,0,1,0,50],
	[10,"BY","YB","BY","BY","MH","BY","BY","BY","BY","BY","MH","BY",26.00,5,"Yellow Birch",2,3,3,2,50],
	[11,"CB","CH","CH","CH","MH","BY","MH","BY","CB","OH","MH","CB",None,5,"Black cherry",2,3,3,2,50],
	[12,"CD","OH","MH","MH","MH","MH","MS","MH","OH","OH","MH","OH",None,5,"American chestnut",2,3,3,2,50],
	[13,"CE","CE","CE","CE","SB","CE","CE","CE","CE","CE","SB","CE",17.00,5,"Cedar, all",2,0,1,0,50],
	[14,"CH","CH","CH","CH","MH","BY","MH","BY","CB","OH","MH","CB",29.00,5,"Cherry",2,3,3,2,50],
	[15,"CR","OC","SB","SB","SB","SB","SB","SB","SB","SB","SB","SB",None,5,"eastern red cedar (or redcedar)",1,0,1,0,50],
	[16,"CW","CE","CE","CE","SB","CE","CE","CE","CE","CE","SB","CE",None,5,"eastern white cedar (or northern white-cedar)",1,0,1,0,50],
	[17,"EW","EW","EW","EW","MH","MH","MH","MH","EW","OH","MH","EW",29.00,5,"White Elm",2,3,2,0,50],
	[18,"EX","EW","EW","EW","MH","MH","MH","MH","EW","OH","MH","EW",None,5,"Elm, any or mixed",2,3,2,0,50],
	[19,"HE","HE","HE","HE","MH","HE","HE","HE","HE","HE","MH","HE",16.00,5,"Hemlock",2,3,3,0,50],
	[20,"HI","OH","MH","MH","MH","MH","MS","MH","OH","OH","MH","OH",None,5,"Hickory, all",2,3,3,2,50],
	[21,"IW","IW","MH","MH","MH","MH","IW","MH","ID","OH","MH","ID",29.00,5,"Iron Wood",2,3,3,0,50],
	[22,"LA","LA","LA","LA","SB","LA","LA","LA","LA","LA","SB","HE",18.00,5,"Tamrack",1,0,1,0,50],
	[23,"LO","OH","MH","MH","MH","MH","MS","MH","OH","OH","MH","OH",None,5,"Locust, black or honey",2,3,3,2,50],
	[24,"MH","MH","MH","MH","MH","MH","MH","MH","MH","MH","MH","MH",21.00,5,"Hard Maple",2,3,3,2,50],
	[25,"MR","MR","MH","MH","MH","MS","MS","MS","MS","MS","MH","MS",None,5,"Soft (red) Maple",2,3,3,2,50],
	[26,"MS","MS","MH","MH","MH","MS","MS","MS","MS","MS","MH","MS",24.00,5,"silver maple",2,3,3,2,50],
	[27,"MX","MH","MH","MH","MH","MH","MH","MH","MH","MH","MH","MH",None,5,"Maple, any or mixed",2,3,3,2,50],
	[28,"OC","OC","SB","SB","SB","SB","SB","SB","SB","SB","SB","SB",19.00,5,"Other conifer",1,0,3,0,50],
	[29,"OH","OH","MH","MH","MH","MH","MS","MH","OH","OH","MH","OH",29.00,5,"Other Hardwood",1,3,3,0,50],
	[30,"OR","QR","OR","OR","MH","OR","OR","OR","OR","OX","MH","OR",28.00,5,"Red Oak",2,3,3,2,50],
	[31,"OW","OW","MH","MH","MH","OR","OR","OR","OR","OX","MH","OH",28.00,5,"White Oak",2,3,3,2,50],
	[32,"OB","OB","MH","MH","MH","OR","OR","OR","OR","OX","MH","OH",28.00,5,"Black Oak",2,3,3,2,50],
	[33,"OX","QR","OR","OR","MH","OR","OR","OR","OR","OX","MH","OR",None,5,"Oak, any or mixed",2,3,3,2,50],
	[34,"PB","PB","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO",33.00,5,"Balsam Poplar",1,0,1,0,50],
	[35,"PD","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO",None,5,"Eastern cottonwood",1,0,1,0,50],
	[36,"PJ","PJ","PJ","PJ","PJ","PJ","PJ","PJ","PJ","PJ","PJ","HE",7.00,5,"Jack Pine",1,0,1,0,50],
	[37,"PL","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO",None,5,"Largetooth aspen",1,0,1,0,50],
	[38,"PO","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO",33.00,5,"Poplar, any or mixed",1,0,1,0,50],
	[39,"PR","PR","PR","PR","PR","PR","PR","PR","PR","PR","PR","PR",4.00,5,"Red Pine",1,0,3,0,50],
	[40,"PS","PS","SB","SB","PJ","PJ","PJ","PJ","PJ","PJ","PJ","SB",8.00,5,"Scots Pine",1,0,3,0,50],
	[41,"PT","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO",None,5,"Trembling aspen",1,0,1,0,50],
	[42,"PW","PW","PW","PW","PW","PW","PW","PW","PW","PW","PW","PW",1.00,5,"White Pine",2,0,3,0,50],
	[43,"PX","PJ","PJ","PJ","PJ","PJ","PJ","PJ","PJ","PJ","PJ","HE",None,5,"Pine, any or mixed",1,0,1,0,50],
	[44,"SB","SB","SB","SB","SB","SB","SB","SB","SB","SB","SB","SB",11.00,5,"Black Spruce",1,0,1,0,50],
	[45,"SW","SW","SW","SW","SB","SW","SW","SW","SW","SW","SB","SW",12.00,5,"White Spruce",1,0,1,0,50],
	[46,"SX","SB","SB","SB","SB","SB","SB","SB","SB","SB","SB","SB",None,5,"Spruce, any or mixed",1,0,1,0,50],
	[47,"WB","OH","MH","MH","MH","MH","MS","MH","OH","OH","MH","OH",None,5,"Black walnut",2,3,3,2,50],
	[48,"WI","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO","PO",None,5,"Willow, any or mixed",2,3,3,2,50],
	[49,"BW1","","","","","BW1","","BW1","","","","",0.00,None,"White Birch FU",1,0,1,0,0],
	[50,"BY1","","","","","BY1","","BY1","","","","",0.00,None,"Yellow Birch FU",2,0,3,0,0],
	[51,"HDSL1","","","","","","","","","","","",0.00,None,"Tolerant hardwood selection FU",1,0,2,0,0],
	[52,"HDSL2","","","","","HD2","","HD2","","","","",0.00,None,"Tolerant hardwood future selection FU",2,0,3,0,0],
	[53,"LC1","","","","","LC1","","LC1","","","","",0.00,None,"Lowland conifer FU",1,0,1,0,0],
	[54,"LWMW","","","","","","","","","","","",0.00,None,"Lowland mixedwood FU",1,0,1,0,0],
	[55,"PJ1","","","","","PJ1","","PJ1","","","","",0.00,None,"Jack Pine FU",1,0,1,0,0],
	[56,"PJ2","","","","","PJ2","","PJ2","","","","",0.00,None,"Jack Pine mixedwood FU",1,0,1,0,0],
	[57,"PO1","","","","","PO1","","PO1","","","","",0.00,None,"Poplar FU",1,0,1,0,0],
	[58,"PR1","","","","","PR1","","PR1","","","","",0.00,None,"Red Pine FU",1,0,3,0,0],
	[59,"PRW","","","","","PRW","","PRW","","","","",0.00,None,"Red Pine mixedwood FU",2,0,3,0,0],
	[60,"PW1","","","","","PW1","","PW1","","","","",0.00,None,"White Pine Mixedwood FU",2,0,3,0,0],
	[61,"SB1","","","","","SB1","","SB1","","","","",0.00,None,"Lowland Black Spruce FU",1,0,1,0,0],
	[62,"SF1","","","","","SF1","","SF1","","","","",0.00,None,"Spruce Mixedwood FU",1,0,1,0,0],
	[63,"SP1","","","","","SP1","","SP1","","","","",0.00,None,"Upland Spruce FU",1,0,0,0,0],
	[64,"TH1","","","","","","","","","","","",0.00,None,"Tolerand Hardwood FU",0,0,0,0,0],
	[65,"MW1","","","","","","","","","","","",0.00,None,"Mixedwood1 FU",0,0,0,0,0],
	[66,"MW2","","","","","","","","","","","",0.00,None,"Mixedwood2 FU",0,0,0,0,0],
	[67,"PWUS4","","","","","PW1","","PW1","","","","",0.00,None,"White Pine 4-cut shelterwood FU",2,0,3,0,0],
	[68,"PWOR","","","","","PW1","","PW1","","","","",0.00,None,"White Pine - Red Oak FU",2,0,3,0,0],
	[69,"PWUSC","","","","","PW1","","PW1","","","","",0.00,None,"White Pine Uniform Shelterwood - Conifer FU",2,0,3,0,0],
	[70,"PWUSH","","","","","PW1","","PW1","","","","",0.00,None,"White Pine Uniform Shelterwood - Hardwood FU",2,0,3,0,0],
	[71,"PWST","","","","","PW1","","PW1","","","","",0.00,None,"White Pine Seed Tree FU",2,0,3,0,0],
	[72,"HE1","","","","","","","","","","","",0.00,None,"Hemlock FU",2,0,0,0,0],
	[73,"CE1","","","","","","","","","","","",0.00,None,"Cedar FU",2,0,0,0,0],
	[74,"SF2","","","","","","","","","","","",0.00,None,"Unknown FU",1,0,0,0,0],
	[75,"OR1","","","","","","","","","","","",0.00,None,"Red Oak FU",0,0,0,0,0],
	[76,"HDUS","","","","","HD2","","HD2","","","","",0.00,None,"Tolerant Hardwood Uniform Shelterwood FU",2,0,3,0,0],
	[77,"MWD","","","","","","","","","","","",0.00,None,"Mixedwood Pine FU",0,0,0,0,0],
	[78,"MWR","","","","","","","","","","","",0.00,None,"Mixedwood Remainder FU",0,0,0,0,0],
	[79,"MWUS","","","","","","","","","","","",0.00,None,"Mixedwood Uniform Shelterwood FU",0,0,0,0,0],
	[80,"OAK","","","","","","","","","","","",0.00,None,"Oak FU",0,0,0,0,0],
	[81,"HDMX1","","","","","","","","","","","",0.00,None,"Hardwood Mixedwood FU",0,0,0,0,0],
	[82,"BfMx1","","","","","BfMx1","","BfMx1","","","","",0.00,None,"Balsam Fir Mixedwood FU",1,0,1,0,0],
	[83,"BfPur","","","","","BfPur","","BfPur","","","","",0.00,None,"Balsam Fir Pure FU",1,0,1,0,0],
	[84,"BwDee","","","","","BwDee","","BwDee","","","","",0.00,None,"White Birch Deep FU",1,0,1,0,0],
	[85,"BwSha","","","","","BwDee","","BwDee","","","","",0.00,None,"White Birch Shallow FU",1,0,1,0,0],
	[86,"ConMx","","","","","ConMx","","ConMx","","","","",0.00,None,"Conifer Mixedwood FU",1,0,1,0,0],
	[87,"HrDom","","","","","HrDom","","HrDom","","","","",0.00,None,"Hardwood FU",1,0,1,0,0],
	[88,"HrdMw","","","","","HrdMw","","HrdMw","","","","",0.00,None,"Hardwood Boreal Mixedwood FU",1,0,1,0,0],
	[89,"OCLow","","","","","OCLow","","OCLow","","","","",0.00,None,"Other Conifer Low FU",1,0,1,0,0],
	[90,"OthHd","","","","","","","","","","","",0.00,None,"Other hardwwood FU",1,0,1,0,0],
	[91,"PjDee","","","","","PjDee","","PjDee","","","","",0.00,None,"Jack Pine Deep FU",1,0,1,0,0],
	[92,"PjMx1","","","","","PjMx1","","PjMx1","","","","",0.00,None,"Jack Pine NW Mixedwood FU",1,0,1,0,0],
	[93,"PjSha","","","","","PjSha","","PjSha","","","","",0.00,None,"Jack Pine Shallow FU",1,0,1,0,0],
	[94,"PoDee","","","","","PoDee","","PoDee","","","","",0.00,None,"Poplar Deep FU",1,0,1,0,0],
	[95,"PoSha","","","","","PoDee","","PoDee","","","","",0.00,None,"Poplar Shallow FU",1,0,1,0,0],
	[96,"PrDom","","","","","PrDom","","PrDom","","","","",0.00,None,"Red Pine FU",1,0,1,0,0],
	[97,"PrwMx","","","","","PrwMx","","PrwMx","","","","",0.00,None,"Red Pine Boreal  mixedwood FU",1,0,1,0,0],
	[98,"PwDom","","","","","PwDom","","PwDom","","","","",0.00,None,"White Pine Boreals FU",1,0,1,0,0],
	[99,"SbDee","","","","","SbDee","","SbDee","","","","",0.00,None,"Black spruce Deep FU",1,0,1,0,0],
	[100,"SbLow","","","","","SbLow","","SbLow","","","","",0.00,None,"Black Spruce Lowland FU",1,0,1,0,0],
	[101,"SbMx1","","","","","SbMx1","","SbMx1","","","","",0.00,None,"Black Spruce Mixedwood FU",1,0,1,0,0],
	[102,"SbSha","","","","","SbSha","","SbSha","","","","",0.00,None,"Black Spruce Shallow FU",1,0,1,0,0],
	[103,"UplCe","","","","","","","","","","","",0.00,None,"Upland Cedar FU",1,0,1,0,0]
]

siid_tbl = dict( [ ( r[1] , dict(zip(siid_fields,r)) ) for r in rows  ] )

#print siid_tbl

def test():
	#This is really to dump out the smallest list version of the same thing
	# So we can have a simpler version of the list for just the SI and HT calculations
	print ("sppid\tsiid\thtid\tage2bh\tbh_indx_age")

	new_siid_tbl = dict()
	for k,v in siid_tbl.items():
		#print v.keys()
		if v['siid'] and v['htid'] and v['age2bh'] and v['bh_indx_age'] :
			if v['sppid'] != v['siid']:
				new_siid_tbl[k] = v['siid'] # { 'sppid':v['sppid'], 'siid':v['siid'] } #,'htid':v['htid'], 'age2bh':v['age2bh'], 'bh_indx_age':v['bh_indx_age'] }
				print( "{sppid}\t{siid}\t{htid}\t{age2bh}\t{bh_indx_age}".format(**v))

	print (new_siid_tbl)
	print (new_siid_tbl.keys())
	print (new_siid_tbl.values())


def main(*args):
	msg=list()
	if '--sqlite' in args:
		schema = ''
		insert_statement = "INSERT INTO si_spp_substitutions ( src,dst ) VALUES ( '{k}', '{v}'); "
		msg.append ("CREATE TABLE si_spp_substitutions (  src character varying(2) NOT NULL,  dst character varying(2) NOT NULL,  CONSTRAINT si_spp_substutions_pkey PRIMARY KEY (src));")
	else :
		insert_statement = "INSERT INTO code.si_spp_substitutions ( src,dst ) VALUES ( '{k}', '{v}'); "
		msg.append ("CREATE TABLE code.si_spp_substitutions (  src character varying(2) NOT NULL,  dst character varying(2) NOT NULL,  CONSTRAINT si_spp_substutions_pkey PRIMARY KEY (src));")
	#print("BEGIN;")

	new_siid_tbl = dict()
	for k,v in siid_tbl.items():
		#print v.keys()
		if v['siid'] and v['htid'] and v['age2bh'] and v['bh_indx_age'] :
			#if v['sppid'] != v['siid']:
			new_siid_tbl[k] = v['siid'] # { 'sppid':v['sppid'], 'siid':v['siid'] } #,'htid':v['htid'], 'age2bh':v['age2bh'], 'bh_indx_age':v['bh_indx_age'] }
				#print( "{sppid}\t{siid}\t{htid}\t{age2bh}\t{bh_indx_age}".format(**v))

	for k,v in new_siid_tbl.items():
		msg.append (insert_statement.format(k=k,v=v) )
	#print("COMMIT;")
	return msg



if __name__ == '__main__':
	msg = main(sys.argv[:1])
	for l in msg:
		print (l)













