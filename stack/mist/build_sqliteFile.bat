@echo off
:: Run this from an OSGeo4W prompt, although it may work elsewhere...

set dbname=%~dp0si_ht_lookup.db

if not %1.==. (
	set dbname=%1
	)

if EXIST %dbname% GOTO end

python %~dp0ht_equations_and_LUT.py --sqlite | sqlite3 %dbname%
python %~dp0si_equations_and_LUT.py --sqlite | sqlite3 %dbname%
python %~dp0si_parameters.py --sqlite | sqlite3 %dbname%


for /L %%a in ( 5,1,20 )  do (
sqlite3  %dbname% "CREATE VIEW age_by_%%a_years AS SELECT  s.sp || ' ' || s.ht || ' ' ||  s.age as sp_ht_age, s.sp, s.ht, s.age, s.si, s.age + 13 as age_plus, h2.ht as ht_plus FROM ht_lut h JOIN si_lut s ON ( s.sp = h.sp ) and ( s.age = h.age ) and s.ht = round ( h.ht,1) JOIN ht_lut h2 ON (s.sp = h2.sp) and (h.age + %%a= h2.age) and (round(s.si,1) = h2.si ) ;"
)


if not %2.==. (
set inventory_product_table=%2
)

if not %3.==. (
set numbyears=%3
GOTO do_age_lut


)

::Go to the end if you weren't sent somewhere first...
GOTO end

:do_age_lut
sqlite3 %dbname% "CREATE VIEW aged_overstory AS SELECT i.*, a.age_plus, a.ht_plus from %inventory_product_table% i JOIN age_by_%numbyears%_years a ON upper(i.OLEADINGSP ) = a.sp AND i.OHT = a.ht and i.OAGE = a.age ;"
sqlite3 %dbname% "INSERT INTO gpkg_geometry_columns SELECT 'aged_overstory' as table_name, column_name, geometry_type_name,srs_id,z,m FROM gpkg_geometry_columns WHERE table_name = '%inventory_product_table%'  ;"
sqlite3 %dbname% "CREATE VIEW aged_understory AS SELECT i.*, a.age_plus, a.ht_plus from %inventory_product_table% i JOIN age_by_%numbyears%_years a ON upper(i.ULEADINGSP ) = a.sp AND i.UHT = a.ht and i.UAGE = a.age ;"
sqlite3 %dbname% "INSERT INTO gpkg_geometry_columns SELECT 'aged_understory' as table_name, column_name, geometry_type_name,srs_id,z,m FROM gpkg_geometry_columns WHERE table_name = '%inventory_product_table%'  ;"
sqlite3 %dbname% "CREATE VIEW aged_mgmtstory AS SELECT i.*, a.age_plus, a.ht_plus from %inventory_product_table% i JOIN age_by_%numbyears%_years a ON upper(i.LEADINGSP ) = a.sp AND i.HT = a.ht and i.AGE = a.age ;"
sqlite3 %dbname% "INSERT INTO gpkg_geometry_columns SELECT 'aged_mgmtstory' as table_name, column_name, geometry_type_name,srs_id,z,m FROM gpkg_geometry_columns WHERE table_name = '%inventory_product_table%'  ;"







:end
