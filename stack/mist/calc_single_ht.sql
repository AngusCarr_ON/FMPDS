﻿CREATE OR REPLACE FUNCTION code.calc_single_ht(
    IN sp character,
    IN age numeric,
    IN si numeric,
    OUT ht_index numeric)
  RETURNS numeric AS
$BODY$

with sp_sub as (
SELECT CASE WHEN dst is NULL THEN sp ELSE dst END as sp
FROM 	(
	SELECT src, dst  FROM code.si_spp_substitutions WHERE src=$1
	) j FULL OUTER JOIN 
	(
	SELECT $1::character varying as sp 
	) k ON k.sp = j.src
) 
SELECT round(ht::numeric,1)::numeric as ht_index
FROM  code.ht_lut , sp_sub
WHERE 
	ht_lut.sp = sp_sub.sp and 
	ht_lut.age= $2 and
	ht_lut.ht=  $3
	

$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;
