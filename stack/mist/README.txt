The files in this folder represent an attempt to extract information from
MIST and include it in a variety of ways.

si_parameters describes which species are equivalent to others. It prepares
a lookup table for the purpose (code.si_spp_substitutions). This LUT could
be in any database. It is a series of insert statements...

si_equations_and_LUT describes calculations for SI (Site Index). SI is calculated
directly for some species and with a linear search of the SI space for a
matching height at a given age.

The lookup table code.si_lut contains a set of pre-calculated SI's for each
combination of (sp,ht,age), which is indexed as a unique index. The values
are calculated for a reasonable range of ages and height for each species.
The ages are integer, but expressed as a numeric. The heights are every 0.1 m,
expressed as a numeric.

ht_equations_and_LUT describes calculations for height, given SI.

The lookup table code.ht_lut contains a set of pre-calculated ht's for each
combination of (sp,SI,age), which is indexed as a unique index. The values
are calculated for a reasonable range of ages and si for each species.
The ages are integer, but expressed as a numeric. The SI are every 0.1,
expressed as a numeric.

To calculate a future height :

SELECT i.leadingsp, i.ht as height,
	i.photo_interpreted_age as age,
	2019 - i.yrorg as plan_start_age,
	si_lut.si as si,
	ht_lut.ht as plan_start_ht
FROM inventory i JOIN
	code.si_lut ON si_lut.sp=leadingsp
		and si_lut.ht = round(i.height,1)
		and si_lut.age = i.age
	JOIN
	code.ht_lut ON si_lut.sp = ht_lut.sp
		and round(si_lut.si,1) = ht_lut.si
		and 2019 - i.yrorg = ht_lut.age
;


In order to generate the lookup tables, run the files and pipe it to psql.
It's not fast, as it is just a set of insert files... :-)

For example:
python ht_equations_and_LUT.py | psql -U carran -h lrcgikdcwhiis06 -d fmp

Two functions for SQL are included to improve understanding and make the
switch easier. calc_single_ht and calc_single_si calculate the si and ht
with reference to the si_spp_substitutions table (created from si_parameters).


Or, for SQLITE:
python ht_equations_and_LUT.py --sqlite | sqlite3 test_si.db
python si_equations_and_LUT.py --sqlite | sqlite3 test_si.db
python si_parameters.py --sqlite | sqlite3 test_si.db
