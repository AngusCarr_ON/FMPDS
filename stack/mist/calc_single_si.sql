﻿CREATE OR REPLACE FUNCTION code.calc_single_si(
    IN sp character,
    IN age numeric,
    IN ht numeric,
    OUT si_index numeric)
  RETURNS numeric AS
$BODY$

with sp_sub as (
SELECT CASE WHEN dst is NULL THEN sp ELSE dst END as sp
FROM 	(
	SELECT src, dst  FROM code.si_spp_substitutions WHERE src=$1
	) j FULL OUTER JOIN 
	(
	SELECT $1::character varying as sp 
	) k ON k.sp = j.src
)
SELECT round(si::numeric,1)::numeric as si_index
FROM  code.si_lut, sp_sub
WHERE --
	si_lut.sp = sp_sub.sp and 
	si_lut.age= $2 and
	si_lut.ht=  $3
 
$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;
