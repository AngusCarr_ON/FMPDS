#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  testing.py
#  
#  Copyright 2018 CARRAN <CARRAN@ON34C02585177>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from stack_choice import usingArcpy, usingOGR

from interface_output import pyprint

from fi_portal import findSubID

from file_extras import fileExists

from log_functions import write2LogFile

from html_functions import writeHTMLstring, writeHTML, write2File
from html_functions import test as html_functions_test

def main(args):
	if usingArcpy:
		pyprint ("using ArcPy")
	if usingOGR:
		pyprint ("using OGR") #Usually... :-)
	testdir = r"\\lrcpsoprfp00001\GIS-DATA\WORK-DATA\FMPDS\Abitibi_River\AWS\2017"
	pyprint (findSubID(testdir)) # 21401
	pyprint(fileExists(testdir, "fi_submission_21401.txt")) #True
	write2LogFile(r"c:\temp\dummylog.txt","Testing") #check the file...
	
	html_functions_test()
	
	
	
	
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
