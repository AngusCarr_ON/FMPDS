#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  status_codes.py
#
#  Copyright 2019 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# The purpose of this file is to centralize the status codes.
# That way we can do useful things
#
#

status_code_dict = { \
( 'Test Event Handler', 0 ):'Success' ,
( 'Test Event Handler', 1 ):'Failure' ,
( 'Test Event Handler', -1 ):'Underway',

( 'fi_portal_download_initiated Handler', 0 ):'Success',
( 'fi_portal_download_initiated Handler', 1 ):'Failure',
( 'fi_portal_download_initiated Handler', 91 ):'Zip File Error',
( 'fi_portal_download_initiated Handler', 99 ):'IO Error - file not readable',
( 'fi_portal_download_initiated Handler', -1 ):'Underway',

# fi_portal_importer
( 'fi_portal_importer', 0 ):'Success',
( 'fi_portal_importer', 1 ):'Failure',
( 'fi_portal_importer', 2 ):'No Tech Spec',
( 'fi_portal_importer', 91 ):'Zip File Error',
( 'fi_portal_importer', -1 ):'Underway',

# fi_portal_nw_checker
( 'fi_portal_nw_checker', 0):'Success',
( 'fi_portal_nw_checker', 1 ):'Failure',
( 'fi_portal_nw_checker', 2 ):'No Tech Spec',
( 'fi_portal_nw_checker', 101 ):'Cannot find sink_dir in payload',
( 'fi_portal_nw_checker', -1 ):'Underway',

# fi_portal_rod_checker
( 'fi_portal_rod_checker', 0     ):'Success',
( 'fi_portal_rod_checker', 1     ):'Failure',
( 'fi_portal_rod_checker', 2     ):'Arcpy Not Found. Exitting.',
( 'fi_portal_rod_checker', 101   ):'Cannot Find Input Files',
( 'fi_portal_rod_checker', 5     ):'Cannot Find Output Files',
( 'fi_portal_rod_checker', -1    ):'Underway',

# fi_portal_mkGdbLyr
( 'fi_portal_mkGdbLyr', 0   ):'Success',
( 'fi_portal_mkGdbLyr', 1   ):'Failure',
( 'fi_portal_mkGdbLyr', 2   ):'Arcpy Not Found. Exitting.',
( 'fi_portal_mkGdbLyr', 4   ):'Cannot Find Input Files',
( 'fi_portal_mkGdbLyr', -1  ):'Underway',

# ar_consolidator
( 'fi_portal_ar_consolidator', 0):'Success',
( 'fi_portal_ar_consolidator', 92):"Error Opening sink_ds for update- database is locked",
( 'fi_portal_ar_consolidator', 93): "Error Opening sink_ds for update",
( 'fi_portal_ar_consolidator', 94): "Row Failure",
( 'fi_portal_ar_consolidator', 91 ):'Cannot Find Input Files',
( 'fi_portal_ar_consolidator', -1 ):'Underway',

# fi_portal_delivery
( 'fi_portal_delivery', 0):'Success',
( 'fi_portal_delivery', -1 ):'Underway',

#fi_molecule_loader
( 'fi_molecule_loader', 0)  :'Success',
( 'fi_molecule_loader', 1   ):'Failure',
( 'fi_molecule_loader', 91 ):'Cannot Find Input Files',
( 'fi_molecule_loader', -1 ):'Underway',

}

def enum(**enums):
    return type('Enum', (), enums)

class StatusError(Exception):
    e = enum(
		code_not_composed='There no code like that',

        )
    def __init__(self,value,msg):
        self.value = value
        self.msg = msg
    def __str__(self):
        try:
            return self.value + ": " + str(self.msg)
        except IndexError as e:
            pass


sql ="""CREATE OR REPLACE FUNCTION fi_portal_scraper.completion_status(
    status integer,
    job_type text)
  RETURNS text AS
$BODY$
SELECT
	CASE
		{0}
		ELSE job_type || ' Status ' ||  status::text
	END;


$BODY$
  LANGUAGE sql VOLATILE
  COST 100;

SELECT fi_portal_scraper.completion_status(-1,'fruit_picking');

"""

def generate_lookup_table_postgresql_function():
	"""
	Generates a function for postgresql to do reporting well

	"""
	return sql.format( '\n\t\t'.join( [ 'WHEN status = {1} and job_type = \'{0}\' THEN \'{2}\''.format( k[0], k[1], v )  for k,v in status_code_dict.iteritems() ]  ))

def status_code(job_type,status_or_msg):
	"""
	Checks if a job_type has the right message, returns code
	"""
	if str(status_or_msg).isdigit():
		status = int(str(status_or_msg))
		msg = None
	else:
		msg = status_or_msg
		status= None

	if status is None and msg is not None:
		for k,v in  status_code_dict.iteritems():
			if k[0] == job_type and msg == v:
				return k[1]
		# msg not found...
		raise StatusError(StatusError.e.code_not_composed,"Could not find a status_code for {0}".format(str((job_type,status))))
	elif msg is None and status is not None:
		try:
			status_code_dict[(job_type,status)]
			return status
		except KeyError:
			raise StatusError(StatusError.e.code_not_composed,"Could not find a status_code for {0}".format(str((job_type,status))))
	else:
		raise StatusError(StatusError.e.code_not_composed,"Could not find a status_code for {0}".format(str((job_type,status))))

def status_msg(job_type,status_or_msg):
	"""
	Checks if a job_type has the right message, returns msg
	"""
	if str(status_or_msg).isdigit():
		status = int(str(status_or_msg))
		msg = None
	else:
		msg = status_or_msg
		status= None

	if status is None and msg is not None:
		for k,v in  status_code_dict.iteritems():
			if k[0] == job_type and msg == v:
				return v
		# msg not found...
		raise StatusError(StatusError.e.code_not_composed,"Could not find a status_code for {0}".format(str((job_type,status))))
	elif msg is None and status is not None:
		try:
			return status_code_dict[(job_type,status)]
		except KeyError:
			raise StatusError(StatusError.e.code_not_composed,"Could not find a status_code for {0}".format(str((job_type,status))))
	else:
		raise StatusError(StatusError.e.code_not_composed,"Could not find a status_code for {0}".format(str((job_type,status))))



def main(args):
	import pg_db
	#p2 = pg_db.pg_db(port=5433,dbname='FI_Portal_Scrape')
	print "================"
	print "DB Function Change:"
	print pg_db.pg_db(port=5433,dbname='FI_Portal_Scrape').run_select_get_dict(generate_lookup_table_postgresql_function())

	print "================"
	print "Successful Test:"
	print status_code( 'fi_portal_download_initiated Handler', 1 )

	print "================"
	print "Failure Test:   "
	print status_code( 'fi_portal_download_initiated Handler', 99 )

	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
