@echo off

::
:: This script provides an example of how to run the checker script.
:: For obvious reasons, this is specific to my installation of OSGeo4W64\OSGeo4W
:: To use the python that comes with QGIS, replace z:\OSGeo4W64 with the location
::     c:\Program Files\QGIS\
:: Make sure it works...
::

cmd /c "z:\OSGeo4W64\OSGeo4W.bat z:\OSGEO4W64\bin\python %~dp0\checker\ar_checker.py"