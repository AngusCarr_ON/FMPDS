    <map version="1.0.1">
        <node TEXT="AR_2009">
            <node TEXT="NaturalDisturbance:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="NaturalDisturbance" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Natural Disturbance (NDB)" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="NDEPCAT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Natural Depletions Category" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="NDEPCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The natural disturbance category attribute indicates how the area was disturbed." FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="NDEPCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BLOWDOWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLOWDOWN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="wind / blowdown" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="DISEASE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DISEASE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="disease" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="DROUGHT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DROUGHT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="drought" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FIRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fire" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FLOOD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FLOOD" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="flood" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ICE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ICE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ice damage" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="INSECTS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="INSECTS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="insects" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SNOW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SNOW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="snow" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="VOLCON:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Conifer Volume" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="7" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="VOLCON" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The VOLCON attribute contains an estimate of the coniferous volume lost due to  mortality (in cubic metres, rounded off to the nearest cubic metre)." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Licensees may use any method to estimate these volume losses. MNR may request the method by  which volume losses from natural disturbances have been determined as part of the MNR review  and acceptance of annual report information." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct format,  max 9999999" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="If there is no hardwood volume (VOLHWD = 0) then the conifer volume cannot be zero (VOLCON not 0)." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be between 0 and 9999999' ${:N} ( row['${name}'] &gt; 9999999 or row['${name}'] &lt; 0.0 ) ]  " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':10000000}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be between 0 and 9999999" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="VOLHWD:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Volume Hardwood" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="7" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="VOLHWD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The VOLHWD attribute contains an estimate of the hardwood volume lost due to mortality (in cubic metres, rounded off to the nearest cubic metre). " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Licensees may use any method to estimate these volume losses. MNR may request the method by  which volume losses from natural disturbances have been determined as part of the MNR review  and acceptance of annual report information." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="If there is no hardwood volume (VOLCON = 0) then the conifer volume cannot be zero (VOLHWD not 0)." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be between 0 and 9999999' ${:N} ( row['${name}'] &gt; 9999999 or row['${name}'] &lt; 0.0 ) ]  " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':10000000}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be between 0 and 9999999" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="DSTBFU:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Forest Unit at Time of Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DSTBFU" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The forest unit at time of disturbance attribute contains the short form label used to  reference the forest unit for the area at the time of natural disturbance." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="Value is user-defined and must be defined in the current FMP or an associated AWS" FOLDED="true">
                                    <icon BUILTIN="help"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT="Annual information on fire disturbances is currently compiled by MNR Aviation, Forest Fire and  Emergency Services in Sault Ste. Marie. The information is organized spatially into a GIS data layer  based on fire sizes greater than 40 hectares. This information is required by the Licensee in order to  assist in the development of the natural depletion information for the annual report submission." FOLDED="true"/>
                    <node TEXT="The polygons in the geospatial data layer represent the exterior perimeter of the forest fire only (i.e.,  gross fire area) and do not include any large interior green (i.e., unburned) areas. The fire perimeters  do not identify the severity of the burn nor represent any mortality information or other impacts on  forest cover. " FOLDED="true"/>
                    <node TEXT="Perimeters of the individual polygons may be based upon GPS mapping or post-fire digitizing of  paper maps. Since in some cases the fire perimeters were digitized from hand drawn maps at  various scales, the accuracy of the data is considered to be in the range of +/- 100 metres. In cases  where perimeter maps were insufficient or missing, a buffer proportional to the size of the fire was  created. " FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="NDB" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="HarvestDisturbance:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="HarvestDisturbance" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Harvest Disturbance (HRV)" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="HARVCAT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Harvest Category" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The harvest disturbance category attribute indicates the planned type of harvest that  was completed." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Road Right-of-Way" FOLDED="true"/>
                            <node TEXT="The attribute coding scheme for harvest disturbances contains an option for recording spatial  information on road right-of-ways, aggregate pits and landings. The code is ROADROW. The  primary purpose of this code is to identify harvest area for newly constructed road right-of-ways for  primary and branch roads, aggregate pits and landings that are outside of an area of operations,  where the depleted area is not intended for future regeneration purposes and will be maintained for  its intended use. The area associated with road right-of-way, aggregate pit and landing harvests are  not included in the balancing of available harvest area as these areas are usually accounted for in  forest modelling using the estimated roads and landings percentage allowance in SFMM. There  may be exceptions, which should be discussed locally between Districts and Licensees.   " FOLDED="true"/>
                            <node TEXT="The road right-of-way code (ROADROW) can be used for roads (primary and branch), aggregate  pits or landings delineated within an area of operations provided this consideration was included in  the strategic modeling. Normally harvest polygons within area of operations do not include the road  right-of-way area for primary and branch roads, aggregate pits and landings as part of the harvested  area. Primary and branch road areas are identified by delineating the boundary of the harvest area  up to the road edge or by buffering out the approximate road width using the road centre-line to  remove the area of road. The District and licensee may agree on other ways to identify or remove  roads, aggregate pits and landings in harvest areas.   " FOLDED="true"/>
                            <node TEXT="Operational roads within a harvest polygon will be reported as part of the harvest area. Operational  roads outside of a harvest polygon but within an area of operations are not normally reported on the  harvest disturbance layer." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="Bridging (HARVCAT = BRIDGING) is only available when the AR start year is equal to the first year of the 10 year plan period." FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; BRIDGING is only when year is first year of plan' ${:N} row['${name}'] == 'BRIDGING' and ${year} != ${planyear} ]    " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'BRIDGING'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REGULAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGULAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regular harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These harvest areas were categorized as regular under the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BRIDGING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BRIDGING" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="bridging harvest areas" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This category identifies the harvested areas that were approved" FOLDED="true"/>
                                    <node TEXT="as bridging under the FMP. This code is only valid in the first AR" FOLDED="true"/>
                                    <node TEXT="under a new FMP as these areas can only be harvested within" FOLDED="true"/>
                                    <node TEXT="the first AWS." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REDIRECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REDIRECT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="redirected harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These areas are harvested under a pest management plan and count against the available harvest area of the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROADROW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROADROW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="road right-of-way" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of trees in preparation for road construction in which  the depleted area is not intended for future regeneration  purposes and will be maintained for primary or branch road  access as well as aggregate pits and landings" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ACCELER:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ACCELER" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="accelerated harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These areas are harvested under a pest management plan and  are areas in addition to the available harvest area of the FMP. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system,  harvest may be planned in two passes. This is normally when  species within the stand are harvested and utilized by different  logger/contractor/forest resource Licensee in different years (e.g.,  first pass is conifer and second pass is hardwood). The first pass  should be recorded if merchantable tree species remain in the  forest stands which have been allocated for harvest, but not yet  harvested." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SCNDPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCNDPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Second-pass harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system,  harvest may be planned in two passes. This is normally when  species within the stand are harvested and utilized by different  logger/contractor/forest resource licensee in different years (e.g.,  first pass is conifer and second pass is hardwood). The second  pass should be recorded when merchantable tree species have  been harvested from forest stands which have been previously  reported as harvested. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SALVAGE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SALVAGE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="salvage harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Salvage is not considered to be the initial form of disturbance  since most areas which require a salvage operation have been  previously disturbed by natural causes, such as fire, insect,  disease, blowdown, etc. All salvage operations are considered to  be harvesting which is often the first silviculture treatment needed  to bring a forest stand back to a more productive state, more  quickly than if left for natural succession. As such, salvage is also  considered to be a form of protection from further loss of  merchantable volume due to insects or fire (e.g., bark  beetles/borers, or fire after insect damage). Salvage operations  also reduce the risk of further natural disturbances, or volume  loss. " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SILVSYS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural System" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural system attribute indicates the process, following accepted silvicultural  principles, whereby tree species constituting a forest are tended, harvested, and  regenerated, resulting in the production of crops of distinctive form. Systems are  conveniently classified according to the method of harvesting the mature stands with a  view to regeneration and according to the type of tree species and future forest  conditions. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CC" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="clearcut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A silvicultural system of regenerating an even-aged forest stand in which  new seedlings become established in fully exposed micro-environments after  most or all of the existing trees have been removed. Regeneration can  originate naturally or can be applied artificially. Clearcutting may be done in  blocks, strips or patches. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="selection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An uneven-aged silvicultural system where mature and/or undesirable trees  are removed individually or in small groups over the whole area, usually in  the course of a cutting cycle. Regeneration is generally natural. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shelterwood" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged silvicultural system where mature trees are harvested in a  series of two or more cuts (preparatory, seed, first removal, final removal) for  the purpose of obtaining natural regeneration under shelter of the residual  trees, either by cutting uniformly over the entire stand area or in narrow  strips. Regeneration is natural or artificial. The regeneration interval  determines the degree of even-aged uniformity. " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'HARVMTHD':'CONVENTION'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="HARVMTHD:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Harvest Method" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HARVMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="HARVMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The harvest method attribute indicates the process used to harvest the trees within a  particular silviculture system. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="CLAAG is a modification to clearcut harvesting that is designed to protect the advanced growth and regeneration on the given site. CLAAG is not a harvest method but rather a regeneration treatment  that could be found within many harvest methods in the clearcut silviculture system. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The single-tree (HARVMTHD = SINGLETREE) and group selection (HARVMTHD =  GROUPSE) harvest categories are only valid codes when the silviculture system is selection  (SILVSYS = SE) " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="The uniform (HARVMTHD = UNIFORM), strip (HARVMTHD = STRIP) and group  shelterwood (HARVMTHD = GROUPSH) harvest categories are only valid codes when the  silviculture system is shelterwood (SILVSYS = SH). " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="The conventional, block or strip, patch, seed-tree and harvesting with regeneration protection  (HARVMTHD = CONVENTION or BLOCKSTRIP or PATCH or SEEDTREE or HARP)  harvest categories are only valid when the silviculture system is clearcut (SLVSYS = CC) " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="The commercial thinning (HARVMTHD = THINCOM) harvest category is valid for either the  clearcut or the shelterwood silviculture system (SILVSYS = CC or SH) " FOLDED="true">
                                    <icon BUILTIN="button_ok"/>
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; may be SINGLETREE or GROUPSE only when SILVSYS = SE' ${:} row['${name}']  in ['SINGLETREE', 'GROUPSE' ] and row['SILVSYS'] != 'SE']  " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; may be UNIFORM, STRIP, GROUPSH only when SILVSYS = SH' ${:} row['${name}']  in ['UNIFORM', 'STRIP', 'GROUPSH' ] and row['SILVSYS'] != 'SH' ]  " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; may be CONVENTION, BLOCKSTRIP, PATCH, SEEDTREE, HARP only when SILVSYS = CC' ${:} row['${name}']  in ['CONVENTION', 'BLOCKSTRIP', 'PATCH', 'SEEDTREE', 'HARP' ] and row['SILVSYS'] != 'CC' ]  " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; may be THINCOM only when SILVSYS = CC or SH' ${:} row['${name}']  in ['THINCOM' ] and row['SILVSYS'] not in [ 'CC', 'SH' ] ]  " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'GROUPSE', 'SILVSYS':'CC'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; may be SINGLETREE or GROUPSE only when SILVSYS = SE" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'GROUPSH', 'SILVSYS':'CC'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; may be UNIFORM, STRIP, GROUPSH only when SILVSYS = SH" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'CONVENTION', 'SILVSYS':'SE'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; may be CONVENTION, BLOCKSTRIP, PATCH, SEEDTREE, HARP only when SILVSYS = CC" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'THINCOM', 'SILVSYS':'SE'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; may be THINCOM only when SILVSYS = CC or SH" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CONVENTION:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CONVENTION" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut: conventional" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of a stand from a large contiguous area in one  operation. This harvest method is not defined by the cutting cycle  associated with adjacent uncut areas." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BLOCKSTRIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLOCKSTRIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut: block or strip" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the stand in progressive strips or blocks in more than  one operation. Strip and block harvest methods are prescribed to  encourage natural regeneration, provide wildlife habitat, protect  fragile sites, or for aesthetics. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PATCH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PATCH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut: patch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of stands in an irregularly shaped, space and sized cut  area. Patch cuts are well suited to harvesting in broken terrain or in  stands that lack uniformity. Patch configurations are often a reflection  of the mosaic in the original forest and can vary greatly in size. They  are desirable because their physical dimensions can be modified to  accommodate site and stand variability." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut: seed-tree" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of all trees from an area, except for a small number of  seed-bearing trees left singly or in small groups for regeneration  purposes. The objective is to create an even-aged stand. Although in  classical silviculture terms, seed-tree is considered a separate  silviculture system, Ontario's Forest Management Planning Manual  classifies it as a harvest method. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HARP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut: harvesting with regeneration protection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the dominant canopy layer in uneven-aged lowland black spruce ecosystems. HARP protects and retains stems below set diameter limit, leaving a significant component of the overstorey. The resulting stand is uneven-aged and uneven-sized. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINCOM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINCOM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut or Shelterwood: commercial thinning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Free-growing productive forest areas which receive a mid-rotation  partial harvest (reduction in the growing stock) that is designed to  meet various objectives such as improving tree spacing, removing  trees not suited to the site, and promoting the growth of the best  quality trees. The harvested trees are removed from the site and used  for commercial purposes. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="UNIFORM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UNIFORM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Shelterwood: uniform" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is (periodically) opened uniformly throughout the  entire stand to achieve a post-harvest, crown-closure target." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STRIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STRIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Shelterwood: strip" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is opened in progressive stages in narrow  successive strips." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="GROUPSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GROUPSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Shelterwood: group" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is (periodically) opened by harvesting trees in small  groups. The resulting canopy opening usually occupies a fraction of a  hectare. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SINGLETREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SINGLETREE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection: single-tree" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is (periodically) opened uniformly throughout the  entire stand to achieve a post-harvest, basal area target. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="GROUPSE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GROUPSE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection: group" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is (periodically) opened by harvesting trees in small  groups. The resulting canopy opening usually occupies a fraction of a  hectare. " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MGMTSTG:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Stage of Management" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTSTG" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTSTG" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The stage of management attribute indicates the type of harvest that occurred under  the Shelterwood silviculture system." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="This list only applies to the Shelterwood areas and is therefore a subset of what is found as  acceptable codes for stage of management within the Forest Management Planning Technical  Specification." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory where SILVSYS = SH" FOLDED="true"/>
                                <node TEXT="The population of this attribute is mandatory where SILVSYS = SH" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT="ST1: The stage of management will be blank (MGMTSTG = null) when the silviculture system  is either selection or clearcut, or the harvest method is commercial thinning (SILVSYS =  SE or CC) or (HARVMTHD = THINCOM)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when SILVSYS = SH' ${:} ${NIN} and ( row['SILVSYS'] == 'SH'  ) ]  " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be blank or null when ( SILVSYS is SE or CC) or ( HARVMTHD is THINCOM )' ${:N} ( row['SILVSYS'] in [ 'SE', 'CC' ]  or row['HARVMTHD'] in [ 'THINCOM' ] ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'SILVSYS':'SH', 'HARVMTHD':'GROUPSH'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when SILVSYS = SH" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'SILVSYS':'SH', 'HARVMTHD':'GROUPSH' }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'PREPCUT', 'SILVSYS':'CC', 'HARVMTHD':'CONVENTION'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be blank or null when ( SILVSYS is SE or CC) or ( HARVMTHD is THINCOM )" FOLDED="true"/>
                                </node>
                                <node STYLE="fork" TEXT="{'${name}':'PREPCUT', 'SILVSYS':'SH', 'HARVMTHD':'THINCOM'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be blank or null when ( SILVSYS is SE or CC) or ( HARVMTHD is THINCOM )" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PREPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PREPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a preparatory cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management  designed to remove undesirable species of any species from  the stand and to select trees to remain that will provide the best  seed source. The removal of undesirable trees opens the  canopy and enables the crowns of remaining seed bearing  trees to enlarge; to improve conditions for seed production and  natural regeneration. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a seed cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where  trees are removed from a mature stand in order to create  openings in the canopy / create spaces and to prepare sites for  natural regeneration while maintaining the seed bearing trees  and protecting any existing advance regeneration. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRSTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRSTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a first removal harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where  overstorey trees are removed in one or more harvests in order  to release the established seedlings from competition. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LASTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LASTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a last removal harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where  all of the remaining trees in the overstorey are removed. This is  the removal of the seed or shelter trees after the regeneration  has been effective. " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ESTAREA:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="ESTAREA" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ESTAREA" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The estimated area attribute indicates the proportion of the gross disturbance polygon  area that is considered to be disturbed." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="To properly calculate and report area for strip cut polygons, the gross polygon area (AREA attribute)  must be multiplied by this ESTAREA value to determine the net area disturbed.   Example: If an area has been affected by strip cutting, then the ESTAREA attribute is used to  identify the net amount of area affected (i.e., the proportion actually cut) as a decimal percentage  (from 0.01 to 1.0). For example, if 60% of a 200 hectare area was depleted by strip cutting, then the  ESTAREA attribute would be set to 0.60. The net area of the polygon affected by strip cutting would  be the 200 hectare gross area multiplied by the 0.60 estimated area of depletion, which equals 120  hectares. The 120 hectares (calculation result) is what would be recorded on the AR tables and in  lists of areas. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory where HARVMTHD = BLOCKSTRIP" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A zero value is not a valid code - must be a number between 0.01 and 1.00" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;ESTAREA&lt;/mark&gt; must have be greater than 0 when HARVMTHD is BLOCKSTRIP' ${:} ${NIN} and row['HARVMTHD'] == 'BLOCKSTRIP' ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be between 0 and 1.00' ${:N} ( row['${name}'] &gt; 1.00 or row['${name}'] &lt; 0.0 ) ]  " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0, 'HARVMTHD':'BLOCKSTRIP'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;mark&gt;ESTAREA&lt;/mark&gt; must have be greater than 0 when HARVMTHD is BLOCKSTRIP" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':5, 'HARVMTHD':'BLOCKSTRIP'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be between 0 and 1.00" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory where HARVMTHD = BLOCKSTRIP" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SGR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Ground Rule ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SGR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural ground rule identifier attribute contains the SGR code for the selected  silvicultural ground rule when it applies to the area. In cases where a SGR that meets  the requirements of a silviculture guide was not used, the letters AOC will be the  default value which indicates that the prescription can be found in the area of concern  (AOC) prescription as described in the forest management plan. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code only where HARVCAT = ROADROW" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="User defined content- applicable SGR codes must be defined in the current FMP. AOC is an acceptable code." FOLDED="true">
                                    <icon BUILTIN="messagebox_warning"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be null unless HARVCAT is ROADROW' ${:} ${NIN} and row['HARVCAT'] not in [ 'ROADROW'] ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'HARVCAT':'REGULAR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be null unless HARVCAT is ROADROW" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="DSTBFU:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Forest Unit at Time of Harvest" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DSTBFU" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The forest unit at time of disturbance attribute contains the short form label used to  reference the forest unit for the area at the time of harvest disturbance." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="User defined content- must be defined in the current FMP or an associated AWS" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT=" " FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="HRV" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['Field %s not found in table'%(f)  for f in ['HARVCAT','SILVSYS','HARVMTHD','SGR','DSTBFU' ] if f not in fields ] " FOLDED="true">
                            <icon BUILTIN="messagebox_warning"/>
                        </node>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="DomainValidation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="Identify all species that occur in OSPCOMP / USPCOMP" FOLDED="true"/>
                        <node TEXT="Age class distributions by" FOLDED="true"/>
                    </node>
                </node>
            </node>
            <node TEXT="RoadConstructionAndUse:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="RoadConstructionAndUse" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polyline" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Road Construction and Use (RDS)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT=" " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the number, label or name assigned to the road (or road  network) that this arc is a part of." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="It is likely that there will be more than one road segment (more than one record) with the same  ROADID value. A ROADID may not have been assigned to operational roads or tertiary roads that  where planned under the 2004 or 1996 FMPM. The operational roads which were planned under the  2004 FMPM should use the operating areas boundary identifier as the ROADID value. The tertiary  roads which were planned under the 1996 FMPM must now be labelled (i.e., the labels used in the  field, the same label as the primary or secondary road) in order to relate to a use management  strategy. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="for plans prepared under the 2009 Forest Management Planning Manual, this value  must match a ROADID in table FMP-18, Road Construction and Use Management" FOLDED="true"/>
                                <node TEXT="for plans prepared under the 2004 Forest Management Planning Manual, this value  must match a ROADID in table FMP-22, Forecast (10 year) and Planned (5 year)  Road Construction and Use Management, or FMP Appendix 2, Existing Roads  Table " FOLDED="true"/>
                                <node TEXT="for plans prepared under the 1996 Forest Management Planning Manual, this value  must match a ROADID in table FMP-26 for primary and secondary (branch) roads. " FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADCLAS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road class attribute indicates the type (classification) of forest access road  approved in the current FMP. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory where CONSTRCT = Y" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory where CONSTRCT = Y" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="Can be completed for existing road segments if desired by the data provider" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory when CONSTRCT = Y" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory CONSTRCT = Y" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="B:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="B" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="branch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="P:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="P" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Primary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="O:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="O" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Operational" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An operational road is a road within an area of operations, other than a primary  or branch road, that provides short-term access for harvest, renewal and  tending operations. Operational roads are normally not maintained after they  are no longer required for forest management purposes, and are often site  prepared and regenerated. " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONSTRCT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Constructed" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONSTRCT" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road construction attribute indicates whether the section of road was constructed  during the report year or not." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="help"/>
                                </node>
                                <node TEXT="ST1:At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM is not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not null)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not NULL) (${name})' ${:} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR','CONSTRCT'] ]  and row['ACCESS']  in ${list_NULL} and row['DECOM']  in ${list_NULL}   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="help"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="DECOM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioned" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road decommissioning attribute indicates whether the section of road was  decommissioned during the report year or not. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM is not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not null)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not NULL) (${name})' ${:} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR','CONSTRCT'] ]  and row['ACCESS']  in ${list_NULL} and row['DECOM']  in ${list_NULL}   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ACCESS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Access Control Status" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road access control status attribute indicates where there has been a change in  road access. The attribute is to remain blank when there is no change in the control  status from the previous year. This could either indicate that an access control is still  present on the road or it could indicate that there is no access control on the road." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population if this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="When the road access control status is apply or both(ACCESS = APPLY or ACCESS = BOTH) then the control type must be a code other than null (CONTROL1 or CONTROL2  is not null)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not NULL)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control  type should be null (CONTROL1 = null and CONTROL2 = null) " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST2: When ${name} is APPLY or BOTH, CONTROL1 or CONTROL2 should not be null' ${:}  row['${name}'] in [ 'APPLY', 'BOTH' ] and  row['CONTROL1'] in ${list_NULL} and  row['CONTROL2'] in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not NULL) (${name})' ${:} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR','CONSTRCT'] ]  and row['ACCESS']  in ${list_NULL} and row['DECOM']  in ${list_NULL}   ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: When ${name} is REMOVE, CONTROL should be null' ${:}  row['${name}'] == 'REMOVE' and  (     row['CONTROL1'] not in ${list_NULL}  or      row['CONTROL2'] not in ${list_NULL}   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'APPLY', 'CONTROL1':None, 'CONTROL2':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST2: When ${name} is APPLY or BOTH, CONTROL1 or CONTROL2 should not be null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'ACCESS':None, 'CONSTRCT':'N', 'MAINTAIN':'N', 'MONITOR':'N', 'DECOM':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not NULL) (${name})" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'REMOVE', 'CONTROL2':None, 'CONTROL1':'BERM'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST2: When ${name} is REMOVE, CONTROL should be null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="APPLY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="APPLY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="apply" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied  to the road segment. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REMOVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REMOVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="remove" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being  removed from the road segment." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BOTH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BOTH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="both" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied  and removed from the road segment in the same  annual report year. " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MAINTAIN:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Maintenance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MAINTAIN" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road maintenance attribute indicates whether the section of road was maintained  during the report year or not. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="help"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not NULL)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not NULL) (${name})' ${:} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR','CONSTRCT'] ]  and row['ACCESS']  in ${list_NULL} and row['DECOM']  in ${list_NULL}   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="help"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road monitoring attribute indicates whether the section of road was monitored  during the report year or not." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true">
                                    <icon BUILTIN="help"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not NULL)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS is not NULL) (${name})' ${:} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR','CONSTRCT'] ]  and row['ACCESS']  in ${list_NULL} and row['DECOM']  in ${list_NULL}   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="help"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Control or Decom Type 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road access control or decommissioning type attribute indicates the method of  control that was applied to a road segment, if the road was identified as  decommissioned or as having access control applied. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="If there is only one access control type being applied to a road segment, then record it in the  CONTROL1 attribute and leave the CONTROL2 attribute blank.   " FOLDED="true"/>
                            <node TEXT="If two access controls apply to the same road segment, then both access control types must be  recorded in the CONTROL1 and CONTROL2 attributes accordingly.   " FOLDED="true"/>
                            <node TEXT="If there are more than two access control types on the same road segment, then choose two of the  controls and record them in the CONTROL1 and CONTROL2 attributes. When picking which two  controls to report on, choose the ones which are deemed to be the most restrictive (i.e., the most  physically limiting to accessibility). " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of CONTROL1 in the file structure of the layer is mandatory where ACCESS = APPLY" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of CONTROL1 is mandatory where ACCESS = APPLY" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: When the road access control status is apply or both (ACCESS = APPLY OR ACCESS = BOTH) then the control type must be a code other than null (CONTROL1 or CONTROL2 is not null) Tested in ACCESS Attribute" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control type should be null (CONTROL1 = null and CONTROL2 = null) Tested in ACCESS Attribute" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['${name} must not be blank when ACCESS = APPLY' ${:} ${NIN}   and row['ACCESS'] == 'APPLY' ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory where ACCESS=APPLY" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Control or Decom Type 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road access control or decommissioning type attribute indicates the method of  control that was applied to a road segment, if the road was identified as  decommissioned or as having access control applied. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of CONTROL1 in the file structure of the layer is mandatory where ACCESS = APPLY" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of CONTROL1 is mandatory where ACCESS = APPLY" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: When the road access control status is apply or both (ACCESS = APPLY OR ACCESS = BOTH) then the control type must be a code other than null (CONTROL1 or CONTROL2 is not null) Tested in ACCESS Attribute" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control type should be null (CONTROL1 = null and CONTROL2 = null) Tested in ACCESS Attribute" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="RDS" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['Field %s not found in table'%(f)  for f in ['ROADID','ROADCLAS','CONSTRCT','DECOM','ACCESS','MAINTAIN','MONITOR','CONTROL1' ]   if f not in fields ] " FOLDED="true">
                            <icon BUILTIN="messagebox_warning"/>
                        </node>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="WaterCrossings:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="WaterCrossings" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Point" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Water Crossings (WTX)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT="The polygon identifier attribute is a unique identifier / label for the polygon. " FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="If more than one crossing of the same type is built and removed at the same location during a given  year, it is the final status of the crossing that is reported (i.e., is it 'in' or 'out'). For example, consider  the scenario where contractor A installs a culvert in May and removes it in June and contractor B  installs a culvert at the same location in August. For annual reporting purposes, a single point is  recorded on the water crossing layer. The attribution for the point indicates that the crossing type is  culvert and that the crossing was built but not removed as a culvert crossing still exists at the  location (WATXTYPE = CULV, CONSTRCT = Y, REMOVE = N). The intermediate removal of the  crossing is not recorded.   " FOLDED="true"/>
                    <node TEXT="If more than one crossing of different types is built at the same location during a given year, then two  records are required; one for each crossing type. For example, consider the scenario where  contractor A installs a culvert in May and removes it in June and contractor B installs a bridge at the  same location in August. Two points would be recorded on the water crossing layer. The points can  be overlapping. The attribution for one point indicates that the crossing type is culvert and that the  crossing was built and removed during the year (WATXTYPE = CULV, CONSTRCT = Y, REMOVE =  Y). The attribution for the second point will indicate that a bridge was built and still remains at the  location (WATXTYPE = BRID, CONSTRCT = Y, REMOVE = N).   " FOLDED="true"/>
                    <node TEXT="Ice crossings are reported as being built and removed in the same year (WATXTYPE = ICE,  CONSTRCT = Y, REMOVE = Y). " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="WATXID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="12" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WATXID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing identifier attribute is a unique identifier label for the crossing  feature." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The WATXID attribute must contain a unique Value" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="WATXTYPE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WATXTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="WATXTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road water crossing type attribute indicates the type of water crossing, such as  bridge, culvert, or engineered ford." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BRID:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BRID" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="bridge" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Culvert (Span &lt; 3m)" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FORD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FORD" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="engineered ford" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ICE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ICE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ice crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONSTRCT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Constructed" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONSTRCT" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing construction attribute indicates if the associated water crossing  point feature was constructed during the fiscal year being reported. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Construction, Removal, Monitoring or Replacement must occur for each record (CONSTRCT = Y or REMOVE = Y or MONITOR = Y or REPLACE = Y)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: At a minimum, one of Construction, Removal, Monitoring or Replacement must occur for each record (CONSTRCT = Y or REMOVE = Y or MONITOR = Y or REPLACE = Y) (${name})' ${:} 'Y' not in [ row[fff] for fff in ['CONSTRCT','MAINTAIN','MONITOR','CONSTRCT'] ]   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Monitored" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing monitoring attribute indicates if the associated water crossing point  feature was monitored during the fiscal year being reported." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Construction, Removal, Monitoring or Replacement must occur for each record (CONSTRCT = Y or REMOVE = Y or MONITOR = Y or REPLACE = Y)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: At a minimum, one of Construction, Removal, Monitoring or Replacement must occur for each record (CONSTRCT = Y or REMOVE = Y or MONITOR = Y or REPLACE = Y) (${name})' ${:} 'Y' not in [ row[fff] for fff in ['CONSTRCT','MAINTAIN','MONITOR','CONSTRCT'] ]   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="REMOVE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Removed" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="REMOVE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing removal attribute indicates if the associated water crossing point  feature was removed during the fiscal year being reported." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="At a minimum, one of Construction, Removal or Monitoring must occur for each record  (CONSTRCT = Y or REMOVE = Y or MONITOR = Y) " FOLDED="true">
                                    <icon BUILTIN="help"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Construction, Removal, Monitoring or Replacement must occur for each record (CONSTRCT = Y or REMOVE = Y or MONITOR = Y or REPLACE = Y)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: At a minimum, one of Construction, Removal, Monitoring or Replacement must occur for each record (CONSTRCT = Y or REMOVE = Y or MONITOR = Y or REPLACE = Y) (${name})' ${:} 'Y' not in [ row[fff] for fff in ['CONSTRCT','MAINTAIN','MONITOR','CONSTRCT'] ]   ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the number, label or name assigned to the road (or road  network) that the water crossing feature is located on. " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="user defined content" FOLDED="true"/>
                                <node TEXT="for plans prepared under the 2009 Forest Management Planning Manual, this value  must match a ROADID in table FMP-18, Road Construction and Use Management" FOLDED="true"/>
                                <node TEXT="for plans prepared under the 2004 Forest Management Planning Manual, this value  must match ROADID in table FMP-22, Forecast (10 year) and Planned (5 year)  Road Construction and Use Management, or FMP Appendix 2, Existing Roads  Table " FOLDED="true"/>
                                <node TEXT="for plans prepared under the 1996 Forest Management Planning Manual, this value  must match a ROADID in table FMP-26 for primary and secondary (branch) roads." FOLDED="true"/>
                                <node TEXT="if the road segment that this crossing is associated with has been included in the  ROAD layer, then the ROADID values must match" FOLDED="true">
                                    <icon BUILTIN="stop-sign"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="WTX" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                        <node TEXT="The WATXID attribute must contain a unique Value" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['WATXID Field values must be unique' for f in ['WATXID' ] if not self.uniqueValueTest(f) ]" FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="RegenerationTreatments:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="RegenerationTreatments" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Regeneration Treatments (RGN)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT=" " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-REGN-AR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Strip Cutting. Only one polygon is required in the spatial layer. This polygon represents the gross area affected by the strip cut activity that is now being declared as natural regeneration, regardless of how many strips occur within the area. In addition, the ESTAREA attribute must be filled in. The polygon and corresponding gross area represent the outside perimeter of all the strips combined or joined. The ESTAREA attribute indicates the percentage of the total (gross) area affected by the strip cut operation (i.e., the amount of the gross area which is being declared for natural regeneration). So, AREA times ESTAREA equals the net area reported." FOLDED="true"/>
                            <node TEXT="Natural Regeneration for Selection and Shelterwood Managed Areas. Note that the information for reporting on natural regeneration areas associated with selection cuts and with shelterwood seed cuts and strip cuts are derived from the harvest layer using the SILVSYS, HARVMTHD, and MGMTSTG attributes. Only natural regeneration associated with areas managed under the clearcut silvicultural system are recorded in this layer." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="For TRTMTHD1, the presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: One of TRTMTHD1,2,3 must not be null' ${:} ${NIN} and ( row['TRTMTHD2'] if 'TRTMTHD2' in row.keys() else None ) in ${list_NULL} and ( row['TRTMTHD3'] if 'TRTMTHD3' in row.keys() else None ) in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'TRTMTHD2':None,'TRTMTHD3':None }  " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: One of TRTMTHD1,2,3 must not be null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLAAG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLAAG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- careful logging around advance growth / regeneration" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An operational practice that can be applied with any harvest method under the clearcut silvicultural system, where the objective is to remove the overstorey, protect understorey advance growth, and regenerate an even-aged stand. The resulting stand develops under full light conditions, generally with a reduced rotation length." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATURAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATURAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - conventional clearcut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the forest in one cut operation and leaving the renewal of the area to natural means." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HARP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- harvest with regeneration protection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the dominant canopy layer in uneven-aged lowland black spruce ecosystems. HARP protects and retains stems below a set diameter limit, leaving a significant component of the overstorey." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - planting" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The establishment of trees on a site by planting seedlings, transplants or cuttings." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SCARIFY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCARIFY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - scarification" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The mechanical loosening or exposure of the topsoil or mineral soil, or breaking up the forest floor, in preparation for natural stand renewal." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The scattering of tree seed (ground broadcast or aerial) over an area to promote new stand growth." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDSIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDSIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding with site preparation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The dispersal or sowing of seed at the same time as the site preparation activity occurs, such as using a bracke with seed hopper." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - seed tree cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A method of harvesting and regenerating a forest stand in which all trees are removed from the area except for a small number of seed-bearing trees that are left singly or in small groups for renewing the area." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STRIPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STRIPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - strip cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of trees in two or more passes in a system of strips of fixed or pre-defined widths and leaving the renewal of the area to natural means." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-REGN-AR" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLAAG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLAAG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- careful logging around advance growth / regeneration" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An operational practice that can be applied with any harvest method under the clearcut silvicultural system, where the objective is to remove the overstorey, protect understorey advance growth, and regenerate an even-aged stand. The resulting stand develops under full light conditions, generally with a reduced rotation length." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATURAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATURAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - conventional clearcut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the forest in one cut operation and leaving the renewal of the area to natural means." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HARP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- harvest with regeneration protection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the dominant canopy layer in uneven-aged lowland black spruce ecosystems. HARP protects and retains stems below a set diameter limit, leaving a significant component of the overstorey." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - planting" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The establishment of trees on a site by planting seedlings, transplants or cuttings." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SCARIFY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCARIFY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - scarification" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The mechanical loosening or exposure of the topsoil or mineral soil, or breaking up the forest floor, in preparation for natural stand renewal." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The scattering of tree seed (ground broadcast or aerial) over an area to promote new stand growth." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDSIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDSIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding with site preparation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The dispersal or sowing of seed at the same time as the site preparation activity occurs, such as using a bracke with seed hopper." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - seed tree cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A method of harvesting and regenerating a forest stand in which all trees are removed from the area except for a small number of seed-bearing trees that are left singly or in small groups for renewing the area." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STRIPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STRIPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - strip cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of trees in two or more passes in a system of strips of fixed or pre-defined widths and leaving the renewal of the area to natural means." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-REGN-AR" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="For TRTMTHD1, the presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLAAG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLAAG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- careful logging around advance growth / regeneration" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An operational practice that can be applied with any harvest method under the clearcut silvicultural system, where the objective is to remove the overstorey, protect understorey advance growth, and regenerate an even-aged stand. The resulting stand develops under full light conditions, generally with a reduced rotation length." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATURAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATURAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - conventional clearcut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the forest in one cut operation and leaving the renewal of the area to natural means." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HARP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration -- harvest with regeneration protection" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the dominant canopy layer in uneven-aged lowland black spruce ecosystems. HARP protects and retains stems below a set diameter limit, leaving a significant component of the overstorey." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - planting" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The establishment of trees on a site by planting seedlings, transplants or cuttings." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SCARIFY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCARIFY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - scarification" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The mechanical loosening or exposure of the topsoil or mineral soil, or breaking up the forest floor, in preparation for natural stand renewal." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The scattering of tree seed (ground broadcast or aerial) over an area to promote new stand growth." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDSIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDSIP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="artificial regeneration - seeding with site preparation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The dispersal or sowing of seed at the same time as the site preparation activity occurs, such as using a bracke with seed hopper." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - seed tree cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A method of harvesting and regenerating a forest stand in which all trees are removed from the area except for a small number of seed-bearing trees that are left singly or in small groups for renewing the area." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STRIPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STRIPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural regeneration - strip cut" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of trees in two or more passes in a system of strips of fixed or pre-defined widths and leaving the renewal of the area to natural means." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRTCAT1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment. For example, the initial regular treatment versus a retreatment of a failed renewal operation versus a natural regeneration prescription that includes supplemental planting." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory for TRTCAT1" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="If the treatment method is populated (TRTMTHD# != null) then the associated treatment category must also be populated (TRTCAT# != null)" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="When the treatment category is retreatment (TRTCAT# = RET) then the associated treatment method must be planting or seeding (TRTMTHD# = PLANT or SEED)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="When the treatment category is supplemental (TRTCAT# = SUP) then the associated treatment method must be planting, scarifying, seeding or seeding with site preparation (TRTMTHD# = PLANT or SCARIFY or SEED or SEEDSIP)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must not be null when TRTMTHD1 is not null' ${:} ${NIN} and row['TRTMTHD1'] not in ${list_NULL}  ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: When ${name} is RET then TRTMTHD must be PLANT or SEED' ${:} row['${name}'] == 'RET'  and row['TRTMTHD1'] not in ['PLANT','SEED' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: When ${name} is SUP then TRTMTHD must be PLANT, SCARIFY, SEED or SEEDSIP' ${:} row['${name}'] == 'SUP'  and row['TRTMTHD1'] not in ['PLANT','SCARIFY','SEED','SEEDSIP' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD1':'NATURAL'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must not be null when TRTMTHD1 is not null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'RET', 'TRTMTHD1':'NATURAL'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: When ${name} is RET then TRTMTHD must be PLANT or SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SUP', 'TRTMTHD1':'NATURAL'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: When ${name} is SUP then TRTMTHD must be PLANT, SCARIFY, SEED or SEEDSIP" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment. For example, the initial regular treatment versus a retreatment of a failed renewal operation versus a natural regeneration prescription that includes supplemental planting." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory for TRTCAT1" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="If the treatment method is populated (TRTMTHD# != null) then the associated treatment category must also be populated (TRTCAT# != null)" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="When the treatment category is retreatment (TRTCAT# = RET) then the associated treatment method must be planting or seeding (TRTMTHD# = PLANT or SEED)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="When the treatment category is supplemental (TRTCAT# = SUP) then the associated treatment method must be planting, scarifying, seeding or seeding with site preparation (TRTMTHD# = PLANT or SCARIFY or SEED or SEEDSIP)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must not be null when TRTMTHD2 is not null' ${:} ${NIN} and row['TRTMTHD2'] not in ${list_NULL}  ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: When ${name} is RET then TRTMTHD must be PLANT or SEED' ${:} row['${name}'] == 'RET'  and row['TRTMTHD2'] not in ['PLANT','SEED' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: When ${name} is SUP then TRTMTHD must be PLANT, SCARIFY, SEED or SEEDSIP' ${:} row['${name}'] == 'SUP'  and row['TRTMTHD2'] not in ['PLANT','SCARIFY','SEED','SEEDSIP' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD2':'NATURAL'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must not be null when TRTMTHD2 is not null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'RET', 'TRTMTHD2':'NATURAL'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: When ${name} is RET then TRTMTHD must be PLANT or SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SUP', 'TRTMTHD2':'NATURAL'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: When ${name} is SUP then TRTMTHD must be PLANT, SCARIFY, SEED or SEEDSIP" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment. For example, the initial regular treatment versus a retreatment of a failed renewal operation versus a natural regeneration prescription that includes supplemental planting." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory for TRTCAT1" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="If the treatment method is populated (TRTMTHD# != null) then the associated treatment category must also be populated (TRTCAT# != null)" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="When the treatment category is retreatment (TRTCAT# = RET) then the associated treatment method must be planting or seeding (TRTMTHD# = PLANT or SEED)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="When the treatment category is supplemental (TRTCAT# = SUP) then the associated treatment method must be planting, scarifying, seeding or seeding with site preparation (TRTMTHD# = PLANT or SCARIFY or SEED or SEEDSIP)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must not be null when TRTMTHD3 is not null' ${:} ${NIN} and row['TRTMTHD3'] not in ${list_NULL}  ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: When ${name} is RET then TRTMTHD must be PLANT or SEED' ${:} row['${name}'] == 'RET'  and row['TRTMTHD3'] not in ['PLANT','SEED' ] ] " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: When ${name} is SUP then TRTMTHD must be PLANT, SCARIFY, SEED or SEEDSIP' ${:} row['${name}'] == 'SUP'  and row['TRTMTHD3'] not in ['PLANT','SCARIFY','SEED','SEEDSIP' ] ] " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD3':'NATURAL'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must not be null when TRTMTHD3 is not null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'RET', 'TRTMTHD3':'NATURAL'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: When ${name} is RET then TRTMTHD must be PLANT or SEED" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SUP', 'TRTMTHD3':'NATURAL'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: When ${name} is SUP then TRTMTHD must be PLANT, SCARIFY, SEED or SEEDSIP" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ESTAREA:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Estimated Area Proportion" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ESTAREA" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct format, value between 0.01 and 1.00" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The estimated area must be greater than zero and less than one (ESTAREA &gt; 0.01 and ESTAREA &lt; 1.00) when the treatment method is strip cutting (TRTMTHD# = STRIPCUT)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: The estimated area must be greater than zero and less than one (ESTAREA &gt; 0.01 and ESTAREA &lt; 1.00) when the treatment method is planting (TRTMTHD# = PLANT) and both species fields are populated (SP1 != null and SP2 != null) (Tested on SP1 Field)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The estimated area field must be one for all other treatment methods (Not STRIPCUT or PLANT)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be between 0.0 and 1.00' ${:N} ( row['${name}'] &lt;= 0.00 or  row['${name}'] &gt; 1.00 ) ]    " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be &gt; 0.0 and less than 1.0 when strip cutting' ${:N} row['${name}'] == 1.00  and  'STRIPCUT' in [ row['TRTMTHD1'], row['TRTMTHD2'], row['TRTMTHD3'] ] ]    " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be = 1.0 for all other TRTMTHDs' ${:} row['${name}'] &lt; 1.00 and  'STRIPCUT' not in [ row['TRTMTHD1'], row['TRTMTHD2'],row['TRTMTHD3'] ] and 'PLANT' not in  [ row['TRTMTHD1'], row['TRTMTHD2'],row['TRTMTHD3'] ] ]    " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':2.0}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be between 0.0 and 1.00" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':1.0, 'TRTMTHD1':None, 'TRTMTHD2':None, 'TRTMTHD3':'STRIPCUT'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be &gt; 0.0 and less than 1.0 when strip cutting" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0.5, 'TRTMTHD1':None, 'TRTMTHD2':None, 'TRTMTHD3':'CLAAG'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: ${name} must be = 1.0 for all other TRTMTHDs" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SP1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Species Planted 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SP1" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="If more than two species are planted within the same treatment area (plant polygon), report the two that cover the most area within the polygon." FOLDED="true"/>
                            <node TEXT="If two (or more) species are planted within the same treatment area (plant polygon) a subdivision of the treatment area polygon by species is NOT required. Instead, the ESTAREA (estimated area) attribute must be completed. In the ESTAREA attribute indicate the proportion of the polygon that was planted by the species entered in the SP1 attribute. The area associated with the second species will be assumed to be 1 minus the ESTAREA value. For example if species 1 is planted on 80pct of the polygon ESTAREA = 0.80, then the area planted to species 2 is 1 - 0.8 = 0.2 (or 20pct) of the polygon." FOLDED="true"/>
                            <node TEXT="If only one species is reported as being planted, then the ESTAREA attribute does not need to be completed." FOLDED="true"/>
                            <node TEXT="Valid species codes are listed in the FIM Forest Resource Inventory Technical Specifications. In these tables, codes related to individual species are listed in mixed case (e.g., Bw, La) and codes related to groups such as all conifer or all spruce are listed in uppercase (e.g., OC, SX). Even though the codes are listed this way, the letters may be entered in any case combination the data submitter desires. For example, white birch may be entered as BW, bw, Bw, or bW." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory for SP1 and SP2" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The second species field must be null (SP2 = null) if the treatment method is not planting (TRTMTHD# != PLANT)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The first species field must be populated (SP1 != null) if the treatment method is planting (TRTMTHD# = PLANT)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="If both species fields are populated (SP1 != null and SP2 != null) then the estimated area must be greater than zero and less than one (ESTAREA &gt; 0.00 and ESTAREA &lt; 1.00) and the treatment method is plant (TRTMTHD# = PLANT)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="The first species field may be populated (SP1 not null) if the treatment method is seeding or seeding with site preparation (TRTMTHD = SEED or SEEDSIP)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST2: The species fields (SP1 and SP2) should be null if all of the treatment methods are careful logging, conventional, harvesting for regeneration protection, scarification, strip cutting and seedtree (TRTMTHD = CLAAG or NATURAL or HARP or SCARIFY or STRIPCUT or SEEDTREE) or null" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must not be null when TRTMTHD# is PLANT' ${:} ${NIN} and 'PLANT' in [ row['TRTMTHD1'],  row['TRTMTHD2'], row['TRTMTHD3'] ] ] " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: If both species fields are populated (SP1 != null and SP2 != null) then the estimated area must be greater than zero and less than one (ESTAREA &gt; 0.00 and ESTAREA &lt; 1.00) and the treatment method is plant (TRTMTHD# = PLANT)' ${:N}  row['SP2'] not in ${list_NULL} and  'PLANT' in [ row['TRTMTHD1'] ,  row['TRTMTHD2'], row['TRTMTHD3'] ] and  ( row['ESTAREA'] in ${list_NULL} or row['ESTAREA'] == 1.00 ) ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: ${name} should be null when TRTMTHD# are all CLAAG, NATURAL, HARP, SCARIFY, STRIPCUT, SEEDTREE' ${:N}   (  row['TRTMTHD1'] in ['CLAAG', 'NATURAL', 'HARP', 'SCARIFY', 'STRIPCUT', 'SEEDTREE' , None ]   and    row['TRTMTHD2'] in ['CLAAG', 'NATURAL', 'HARP', 'SCARIFY', 'STRIPCUT', 'SEEDTREE' , None ]   and   row['TRTMTHD3'] in ['CLAAG', 'NATURAL', 'HARP', 'SCARIFY', 'STRIPCUT', 'SEEDTREE' , None ]      )   ] " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'TRTMTHD1':'PLANT', 'TRTMTHD2':'PLANT', 'TRTMTHD3':'PLANT'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must not be null when TRTMTHD# is PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SB', 'SP2':'PJ', 'ESTAREA':1.00, 'TRTMTHD1':'PLANT', 'TRTMTHD2':'PLANT', 'TRTMTHD3':'PLANT'}    " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: If both species fields are populated (SP1 != null and SP2 != null) then the estimated area must be greater than zero and less than one (ESTAREA &gt; 0.00 and ESTAREA &lt; 1.00) and the treatment method is plant (TRTMTHD# = PLANT)" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SB','TRTMTHD1':'SEEDTREE','TRTMTHD2':None,'TRTMTHD3':None} " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST2: ${name} should be null when TRTMTHD# are all CLAAG, NATURAL, HARP, SCARIFY, STRIPCUT, SEEDTREE" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PO" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poplar" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Or:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Or" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Oak, Red" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white birch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="HE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hemlock" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PJ:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PJ" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="jack pine" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other hardwoods" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gray birch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white pine" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spruce mix" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white spruce" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam poplar" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Ap:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Ap" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ash, Pumpkin" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="LA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="larch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="soft / red maple" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Ag:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Ag" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ash, red (or green)" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="red pine" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yellow birch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hard / sugar maple" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Aq:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Aq" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ash, Blue" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Bc:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Bc" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Birch, Cherry" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Bp:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Bp" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Birch," FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BF" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam fir" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OC" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other conifer" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cedar" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="maple mix" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="AX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="AX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ash mix" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="black spruce" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scots pine" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="oak mix" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="SP2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Species Planted 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SP2" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="If more than two species are planted within the same treatment area (plant polygon), report the two that cover the most area within the polygon." FOLDED="true"/>
                            <node TEXT="If two (or more) species are planted within the same treatment area (plant polygon) a subdivision of the treatment area polygon by species is NOT required. Instead, the ESTAREA (estimated area) attribute must be completed. In the ESTAREA attribute indicate the proportion of the polygon that was planted by the species entered in the SP1 attribute. The area associated with the second species will be assumed to be 1 minus the ESTAREA value. For example if species 1 is planted on 80pct of the polygon ESTAREA = 0.80, then the area planted to species 2 is 1 - 0.8 = 0.2 (or 20pct) of the polygon." FOLDED="true"/>
                            <node TEXT="If only one species is reported as being planted, then the ESTAREA attribute does not need to be completed." FOLDED="true"/>
                            <node TEXT="Valid species codes are listed in the FIM Forest Resource Inventory Technical Specifications. In these tables, codes related to individual species are listed in mixed case (e.g., Bw, La) and codes related to groups such as all conifer or all spruce are listed in uppercase (e.g., OC, SX). Even though the codes are listed this way, the letters may be entered in any case combination the data submitter desires. For example, white birch may be entered as BW, bw, Bw, or bW." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory for SP1 and SP2" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The second species field must be null (SP2 = null) if the treatment method is not planting (TRTMTHD# != PLANT)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The first species field must be populated (SP1 != null) if the treatment method is planting (TRTMTHD# = PLANT)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="If both species fields are populated (SP1 != null and SP2 != null) then the estimated area must be greater than zero and less than one (ESTAREA &gt; 0.00 and ESTAREA &lt; 1.00) and the treatment method is plant (TRTMTHD# = PLANT)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="The first species field may be populated (SP1 not null) if the treatment method is seeding or seeding with site preparation (TRTMTHD = SEED or SEEDSIP)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="The species fields (SP1 and SP2) should be null if all of the treatment methods are careful logging, conventional, harvesting for regeneration protection, scarification, strip cutting and seedtree (TRTMTHD = CLAAG or NATURAL or HARP or SCARIFY or STRIPCUT or SEEDTREE) or null" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be null when TRTMTHD# is not PLANT' ${:N}  'PLANT' not in [ row['TRTMTHD1'],row['TRTMTHD2'],row['TRTMTHD3'] ] ] " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST2: ${name} should be null when TRTMTHD# are all CLAAG, NATURAL, HARP, SCARIFY, STRIPCUT, SEEDTREE' ${:N}   (  row['TRTMTHD1'] in ['CLAAG', 'NATURAL', 'HARP', 'SCARIFY', 'STRIPCUT', 'SEEDTREE' , None ]   and    row['TRTMTHD2'] in ['CLAAG', 'NATURAL', 'HARP', 'SCARIFY', 'STRIPCUT', 'SEEDTREE' , None ]   and   row['TRTMTHD3'] in ['CLAAG', 'NATURAL', 'HARP', 'SCARIFY', 'STRIPCUT', 'SEEDTREE' , None ]      )   ] " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SB','TRTMTHD1':'SEED','TRTMTHD2':'SEEDSIP','TRTMTHD3':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must be null when TRTMTHD# is not PLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SB','TRTMTHD1':'SEEDTREE','TRTMTHD2':None,'TRTMTHD3':None} " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: ${name} must be null when TRTMTHD# is not PLANT" FOLDED="true"/>
                                    <node TEXT="ST2: ${name} should be null when TRTMTHD# are all CLAAG, NATURAL, HARP, SCARIFY, STRIPCUT, SEEDTREE" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PO" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poplar" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Or:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Or" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Oak, Red" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white birch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="HE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hemlock" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PJ:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PJ" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="jack pine" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other hardwoods" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gray birch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white pine" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spruce mix" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SW" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="white spruce" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam poplar" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Ap:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Ap" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ash, Pumpkin" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="LA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="larch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="soft / red maple" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Ag:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Ag" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ash, red (or green)" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="red pine" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yellow birch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="hard / sugar maple" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Aq:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Aq" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ash, Blue" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Bc:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Bc" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Birch, Cherry" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="Bp:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Bp" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Birch," FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BF" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="balsam fir" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OC" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="other conifer" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cedar" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="maple mix" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="AX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="AX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ash mix" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="black spruce" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scots pine" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="oak mix" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="RGN" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="SitePreparationTreatment:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="SitePreparationTreatment" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Site Preparation Treatment (SIP)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT=" " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is  mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: One of TRTMTHD1,2,3 must not be null' ${:} ${NIN} and ( row['TRTMTHD2'] if 'TRTMTHD2' in row.keys() else None ) in ${list_NULL} and ( row['TRTMTHD3'] if 'TRTMTHD3' in row.keys() else None ) in ${list_NULL} ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD2':None,'TRTMTHD3':None, 'APPNUM':0, 'RATE_AI':0, 'PRODTYPE': None }  " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: One of TRTMTHD1,2,3 must not be null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SIPMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of machinery to disturb the forest floor and expose topsoil or mineral soil to create suitable conditions for artificial regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SIPCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The application of herbicides, by aerial methods, to reduce undesirable competition, prepare sites for further site preparation treatment, or create suitable conditions for regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SIPCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The application of herbicides, by ground methods, to reduce undesirable competition, prepare sites for further site preparation treatment, or create suitable conditions for regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SIPPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="prescribed burn - conventional burn / high complexity burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Use of the knowledgeable application of fire to a specific area to create suitable conditions for forest renewal and regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is  mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SIPMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of machinery to disturb the forest floor and expose topsoil or mineral soil to create suitable conditions for artificial regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SIPCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The application of herbicides, by aerial methods, to reduce undesirable competition, prepare sites for further site preparation treatment, or create suitable conditions for regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SIPCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The application of herbicides, by ground methods, to reduce undesirable competition, prepare sites for further site preparation treatment, or create suitable conditions for regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SIPPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="prescribed burn - conventional burn / high complexity burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Use of the knowledgeable application of fire to a specific area to create suitable conditions for forest renewal and regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is  mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SIPMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of machinery to disturb the forest floor and expose topsoil or mineral soil to create suitable conditions for artificial regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SIPCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The application of herbicides, by aerial methods, to reduce undesirable competition, prepare sites for further site preparation treatment, or create suitable conditions for regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SIPCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The application of herbicides, by ground methods, to reduce undesirable competition, prepare sites for further site preparation treatment, or create suitable conditions for regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SIPPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIPPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="prescribed burn - conventional burn / high complexity burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Use of the knowledgeable application of fire to a specific area to create suitable conditions for forest renewal and regeneration of a forest stand." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory for TRTCAT1" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT=" If the treatment method is populated (TRTMTHD# != null) then the associated treatment  category must also be populated (TRTCAT# != null) " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be non-null when TRTMTHD is not null' ${:} ${NIN} and row['${name}'.replace('TRTCAT','TRTMTHD')] not in ${list_NULL}   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD1':'SIPPB' ,'APPNUM':0, 'RATE_AI':0, 'PRODTYPE': None  }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be non-null when TRTMTHD is not null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory for TRTCAT1" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT=" If the treatment method is populated (TRTMTHD# != null) then the associated treatment  category must also be populated (TRTCAT# != null) " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be non-null when TRTMTHD is not null' ${:} ${NIN} and row['${name}'.replace('TRTCAT','TRTMTHD')] not in ${list_NULL}   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD2':'SIPCHEMG'  }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be non-null when TRTMTHD is not null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory for TRTCAT1" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT=" If the treatment method is populated (TRTMTHD# != null) then the associated treatment  category must also be populated (TRTCAT# != null) " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be non-null when TRTMTHD is not null' ${:} ${NIN} and row['${name}'.replace('TRTCAT','TRTMTHD')] not in ${list_NULL}   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD3':'SIPCHEMG'   }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be non-null when TRTMTHD is not null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="PRODTYPE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Chemical Product Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="25" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PRODTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The chemical product type attribute contains the name of the chemical product being applied to the area along with the 5 digit Pesticide Control Product (PCP) registration number." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The product type attribute must be present and populated (PRODTYPE not null) when any of the treatment methods are aerial or ground chemical site preparation (TRTMTHD = SIPCHEMA or SIPCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The product type must be null (PRODTYPE = null) when all the treatment methods are mechanical or prescribed burn (TRTMTHD = SIPMECH or SIPPB)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must not be null when SIPCHEMA or SIPCHEMG' ${:} ${NIN}  and  (     row['TRTMTHD1'] in ['SIPCHEMA','SIPCHEMG']     or     row['TRTMTHD2']  in ['SIPCHEMA','SIPCHEMG']     or     row['TRTMTHD3']  in ['SIPCHEMA','SIPCHEMG']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be null when all TRTMTHD# are SIPMECH, SIPPB' ${:} ${NINN}  and  (     row['TRTMTHD1'] in ['SIPMECH', 'SIPPB', None]     and     row['TRTMTHD2'] in ['SIPMECH', 'SIPPB', None ]    and     row['TRTMTHD3'] in ['SIPMECH', 'SIPPB', None]    ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None,'TRTMTHD1':'SIPCHEMA','TRTMTHD2':'SIPCHEMA','TRTMTHD3':'SIPCHEMA'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must not be null when SIPCHEMA or SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':5,'TRTMTHD1':'SIPMECH','TRTMTHD2':'SIPMECH','TRTMTHD3':'SIPMECH'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be null when all TRTMTHD# are SIPMECH, SIPPB" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="RATE_AI:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Rate of Active Ingredient Applied" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RATE_AI" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The rate of active ingredient applied attribute contains the number of kilograms of active ingredient per hectare of the chemical product being applied to the area." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The attribute population must be between 0 and 9.99, inclusive." FOLDED="true"/>
                                <node TEXT="A zero value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="This attribute must be present and greater than zero (RATE_AI  0) when any of the treatment methods are aerial or ground chemical site preparation (TRTMTHD = SIPCHEMA or SIPCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="This attribute must be zero (RATE_AI = 0) when all the treatment methods are mechanical or prescribed burn (TRTMTHD = SIPMECH or SIPPB)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be &gt;= 0 and &lt;= 9.99' ${:N} ( row['${name}'] &lt; 0 or row['${name}'] &gt; 9.99 ) ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: ${name} must not be null when SIPCHEMA or SIPCHEMG' ${:} ${NIN}  and  (     row['TRTMTHD1'] in ['SIPCHEMA','SIPCHEMG']     or     row['TRTMTHD2']  in ['SIPCHEMA','SIPCHEMG']     or     row['TRTMTHD3']  in ['SIPCHEMA','SIPCHEMG']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be null when all TRTMTHD# are SIPMECH, SIPPB' ${:} ${NINN}  and  (     row['TRTMTHD1'] in ['SIPMECH', 'SIPPB', None]     and     row['TRTMTHD2'] in ['SIPMECH', 'SIPPB', None]    and     row['TRTMTHD3'] in ['SIPMECH', 'SIPPB', None]    ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':10}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be &gt;= 0 and &lt;= 9.99" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0,'TRTMTHD1':'SIPCHEMA','TRTMTHD2':'SIPCHEMA','TRTMTHD3':'SIPCHEMA'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must not be null when SIPCHEMA or SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':5,'TRTMTHD1':'SIPMECH','TRTMTHD2':'SIPMECH','TRTMTHD3':'SIPMECH'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be null when all TRTMTHD# are SIPMECH, SIPPB" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="APPNUM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Number of Application" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="APPNUM" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The number of applications attribute contains the number of times the area has been treated with the chemical product identified in PRODTYPE during this fiscal year." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="Must be between 0 and 9" FOLDED="true"/>
                                <node TEXT="A zero value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The number of applications attribute must be present and greater than zero (APPNUM  0) when any of the treatment methods are aerial or ground chemical site preparation (TRTMTHD = SIPCHEMA or SIPCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The number of applications must be zero (APPNUM = 0) when all the treatment methods are mechanical or prescribed burn (TRTMTHD = SIPMECH or SIPPB)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be &gt;= 0 and &lt;= 9' ${:N} ( row['${name}'] &lt; 0 or row['${name}'] &gt; 9 ) ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: ${name} must not be null when SIPCHEMA or SIPCHEMG' ${:} ${NIN}  and   (      row['TRTMTHD1'] in ['SIPCHEMA','SIPCHEMG'] or      row['TRTMTHD2'] in ['SIPCHEMA','SIPCHEMG'] or      row['TRTMTHD3'] in ['SIPCHEMA','SIPCHEMG']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be null when all TRTMTHD# are SIPMECH, SIPPB' ${:} ${NINN}  and   (      row['TRTMTHD1'] in ['SIPMECH', 'SIPPB', None]      and      row['TRTMTHD2'] in ['SIPMECH', 'SIPPB', None]      and      row['TRTMTHD3'] in ['SIPMECH', 'SIPPB', None]     )  ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':10}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be &gt;= 0 and &lt;= 9" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0,'TRTMTHD1':'SIPCHEMA','TRTMTHD2':'SIPCHEMA','TRTMTHD3':'SIPCHEMA'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must not be null when SIPCHEMA or SIPCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':5,'TRTMTHD1':'SIPMECH','TRTMTHD2':'SIPMECH','TRTMTHD3':'SIPMECH'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be null when all TRTMTHD# are SIPMECH, SIPPB" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SIP" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['Field %s not found in table'%(f)  for f in ['TRTMTHD1','TRTCAT1'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="TendingTreatment:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="TendingTreatment" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Tending Treatment (TND)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT="The polygon identifier attribute is a unique identifier / label for the polygon. " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TENDTRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is  mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: One of TRTMTHD1,2,3 must not be null' ${:} ${NIN} and row['TRTMTHD2'] in ${list_NULL} and row['TRTMTHD3'] in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD2':None,'TRTMTHD3':None,'RATE_AI':0, 'APPNUM':0, 'PRODTYPE':None  }  " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: One of TRTMTHD1,2,3 must not be null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code','RATE_AI':0, 'APPNUM':0, 'PRODTYPE':None } " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The application of herbicides from an aircraft to a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The ground application of herbicides in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of hand operations in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of machinery in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - prescribed burn / high complexity prescribed burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of the knowledgeable application of fire in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - uneven-aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A cutting made in an uneven-aged stand primarily to accelerate diameter increments, but also, by suitable selection, to improve the composition and the average form of the trees that remain." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - even aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A cutting made in an immature even-aged stand primarily to reduce competition and to accelerate diameter increments, but also, by suitable selection, to improve the average form of the trees that remain." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CULTIVAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULTIVAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cultivation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The act of loosening or breaking up the soil about growing plants to reduce competing vegetation and to foster growth in an established stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PRUNE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRUNE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pruning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of live or dead branches from standing trees, usually the lower branches of young trees and the removal of multiple leaders in plantation trees, for the improvement of the tree or its timber quality; to reduce risk of disease, or includes the cutting away of superfluous growth, including roots, from any tree to improve its development." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TENDTRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is  mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The application of herbicides from an aircraft to a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The ground application of herbicides in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of hand operations in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of machinery in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - prescribed burn / high complexity prescribed burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of the knowledgeable application of fire in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - uneven-aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A cutting made in an uneven-aged stand primarily to accelerate diameter increments, but also, by suitable selection, to improve the composition and the average form of the trees that remain." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - even aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A cutting made in an immature even-aged stand primarily to reduce competition and to accelerate diameter increments, but also, by suitable selection, to improve the average form of the trees that remain." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CULTIVAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULTIVAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cultivation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The act of loosening or breaking up the soil about growing plants to reduce competing vegetation and to foster growth in an established stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PRUNE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRUNE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pruning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of live or dead branches from standing trees, usually the lower branches of young trees and the removal of multiple leaders in plantation trees, for the improvement of the tree or its timber quality; to reduce risk of disease, or includes the cutting away of superfluous growth, including roots, from any tree to improve its development." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TENDTRTMTHD" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is  mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CLCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : aerial application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The application of herbicides from an aircraft to a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - chemical : ground application" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The ground application of herbicides in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of hand operations in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLMECH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLMECH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - mechanical" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of machinery in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CLPB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CLPB" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cleaning - prescribed burn / high complexity prescribed burn" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The use of the knowledgeable application of fire in a young stand, not past the sapling stage, to free the favoured trees from competition by eliminating undesirable vegetation." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - uneven-aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A cutting made in an uneven-aged stand primarily to accelerate diameter increments, but also, by suitable selection, to improve the composition and the average form of the trees that remain." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spacing / pre-commercial thin / improvement cut - even aged" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A cutting made in an immature even-aged stand primarily to reduce competition and to accelerate diameter increments, but also, by suitable selection, to improve the average form of the trees that remain." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CULTIVAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULTIVAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="cultivation" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The act of loosening or breaking up the soil about growing plants to reduce competing vegetation and to foster growth in an established stand." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PRUNE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRUNE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pruning" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of live or dead branches from standing trees, usually the lower branches of young trees and the removal of multiple leaders in plantation trees, for the improvement of the tree or its timber quality; to reduce risk of disease, or includes the cutting away of superfluous growth, including roots, from any tree to improve its development." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the treatment method is populated (TRTMTHD# != null) then the associated treatment  category must also be populated (TRTCAT# != null) " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be non-null when TRTMTHD is not null' ${:} ${NIN} and row['${name}'.replace('TRTCAT','TRTMTHD')] not in ${list_NULL}   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'TRTMTHD1':'CULTIVAT'  ,'RATE_AI':0, 'APPNUM':0, 'PRODTYPE':None }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be non-null when TRTMTHD is not null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the treatment method is populated (TRTMTHD# != null) then the associated treatment  category must also be populated (TRTCAT# != null) " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be non-null when TRTMTHD is not null' ${:} ${NIN} and row['${name}'.replace('TRTCAT','TRTMTHD')] not in ${list_NULL}   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'TRTMTHD2':'CULTIVAT'  }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be non-null when TRTMTHD is not null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the treatment method is populated (TRTMTHD# != null) then the associated treatment  category must also be populated (TRTCAT# != null) " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be non-null when TRTMTHD is not null' ${:} ${NIN} and row['${name}'.replace('TRTCAT','TRTMTHD')] not in ${list_NULL}   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'TRTMTHD3':'CULTIVAT'  }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be non-null when TRTMTHD is not null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="PRODTYPE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Chemical Product Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="25" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PRODTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The chemical product type attribute contains the name of the chemical product being applied to the area along with the 5 digit Pesticide Control Product (PCP) registration number." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The product type attribute must be present and populated (PRODTYPE not null) when any of the treatment methods are aerial or ground chemical site preparation (TRTMTHD =CLCHEMA or CLCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The product type must be null (PRODTYPE = null) when all the treatment methods are mechanical or prescribed burn (TRTMTHD = CLCHEMA or CLCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must not be null when CLCHEMA or CLCHEMG' ${:} ${NIN}  and  (     row['TRTMTHD1'] in ['CLCHEMA','CLCHEMG']     or     row['TRTMTHD2']  in ['CLCHEMA','CLCHEMG']     or     row['TRTMTHD3']  in ['CLCHEMA','CLCHEMG']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be null when all TRTMTHD# are not in CLCHEMA, CLCHEMG' ${:} ${NINN}  and  (     row['TRTMTHD1'] not in ['CLCHEMA', 'CLCHEMG']     and     row['TRTMTHD2'] not in ['CLCHEMA', 'CLCHEMG']    and     row['TRTMTHD3']not in ['CLCHEMA', 'CLCHEMG']    ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':0,'TRTMTHD1':'CLCHEMA','TRTMTHD2':'CLCHEMA','TRTMTHD3':'CLCHEMA'} " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must not be null when CLCHEMA or CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':5,'TRTMTHD1':'PRUNE','TRTMTHD2':'PRUNE','TRTMTHD3':'PRUNE'} " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be null when all TRTMTHD# are not in CLCHEMA, CLCHEMG" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="RATE_AI:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Rate of Active Ingredient Applied" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RATE_AI" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The rate of active ingredient applied attribute contains the number of kilograms of active ingredient per hectare of the chemical product being applied to the area." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The attribute population must be between 0 and 9.99, inclusive." FOLDED="true"/>
                                <node TEXT="A zero value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="This attribute must be present and greater than zero (RATE_AI greater than 0) when any of the treatment methods are aerial or ground chemical cleaning (TRTMTHD = CLCHEMA or CLCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="This attribute must be zero (RATE_AI = 0) when all the treatment methods are other than aerial or ground chemical cleaning (TRTMTHD not CLCHEMA or CLCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be &gt;= 0 and &lt;= 9.99' ${:N} ( row['${name}'] &lt; 0 or row['${name}'] &gt; 9.99 ) ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: ${name} must not be null when CLCHEMA or CLCHEMG' ${:} ${NIN}  and  (     row['TRTMTHD1'] in ['CLCHEMA','CLCHEMG']     or     row['TRTMTHD2']  in ['CLCHEMA','CLCHEMG']     or     row['TRTMTHD3']  in ['CLCHEMA','CLCHEMG']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be null when all TRTMTHD# are not in CLCHEMA, CLCHEMG' ${:} ${NINN}  and  (     row['TRTMTHD1'] not in ['CLCHEMA', 'CLCHEMG']     and     row['TRTMTHD2'] not in ['CLCHEMA', 'CLCHEMG']    and     row['TRTMTHD3']not in ['CLCHEMA', 'CLCHEMG']    ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':10}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be &gt;= 0 and &lt;= 9.99" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0,'TRTMTHD1':'CLCHEMA','TRTMTHD2':'CLCHEMA','TRTMTHD3':'CLCHEMA'} " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must not be null when CLCHEMA or CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':5,'TRTMTHD1':'PRUNE','TRTMTHD2':'PRUNE','TRTMTHD3':'PRUNE'} " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be null when all TRTMTHD# are not in CLCHEMA, CLCHEMG" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="APPNUM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Number of Applications" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="APPNUM" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The number of applications attribute contains the number of times the area has been treated with the chemical product identified in PRODTYPE during this fiscal year." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="o" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A zero value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="Must be between 0 and 9" FOLDED="true"/>
                                <node TEXT="The number of applications attribute must be present and greater than zero (APPNUM greater than 0) when any of the treatment methods are aerial or ground chemical site preparation (TRTMTHD = CLCHEMA or CLCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The number of applications must be zero (APPNUM = 0) when all the treatment methods are not aerial or ground chemical site preparation (TRTMTHD not CLCHEMA or CLCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be &gt;= 0 and &lt;= 9' ${:N} ( row['${name}'] &lt; 0 or row['${name}'] &gt; 9 ) ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: ${name} must not be null when CLCHEMA or CLCHEMG' ${:} ${NIN}  and  (     row['TRTMTHD1'] in ['CLCHEMA','CLCHEMG']     or     row['TRTMTHD2']  in ['CLCHEMA','CLCHEMG']     or     row['TRTMTHD3']  in ['CLCHEMA','CLCHEMG']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be null when all TRTMTHD# are not in CLCHEMA, CLCHEMG' ${:} ${NINN}  and  (     row['TRTMTHD1'] not in ['CLCHEMA', 'CLCHEMG']     and     row['TRTMTHD2'] not in ['CLCHEMA', 'CLCHEMG']    and     row['TRTMTHD3']not in ['CLCHEMA', 'CLCHEMG']    ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':10}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be &gt;= 0 and &lt;= 9" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0,'TRTMTHD1':'CLCHEMA','TRTMTHD2':'CLCHEMA','TRTMTHD3':'CLCHEMA'} " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must not be null when CLCHEMA or CLCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':5,'TRTMTHD1':'PRUNE','TRTMTHD2':'PRUNE','TRTMTHD3':'PRUNE'} " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be null when all TRTMTHD# are not in CLCHEMA, CLCHEMG" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="o" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="TND" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['Field %s not found in table'%(f)  for f in ['TRTMTHD1','PRODTYPE','RATE_AI','APPNUM'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ProtectionTreatment:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ProtectionTreatment" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Protection Treatment (PRT)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT="The polygon identifier attribute is a unique identifier / label for the polygon. " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="TRTMTHD1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-PROTECT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is  mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: One of TRTMTHD1,2,3 must not be null' ${:} ${NIN} and row['TRTMTHD2'] in ${list_NULL} and row['TRTMTHD3']  in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'TRTMTHD2':None,'TRTMTHD3':None }  " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: One of TRTMTHD1,2,3 must not be null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - ground insecticide" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - aerial spraying" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-PROTECT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is  mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: One of TRTMTHD1,2,3 must be not null' ${:} ${NIN} and ( row['TRTMTHD2'] if 'TRTMTHD2' in row.keys() else None ) in ${list_NULL} and ( row['TRTMTHD3'] if 'TRTMTHD3' in row.keys() else None ) in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - ground insecticide" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - aerial spraying" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTMTHD3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Method 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTMTHD3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTMTHD-PROTECT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment method attribute indicates the general type of silvicultural activity and the specific treatment or method applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="For TRTMTHD1 or TRTMTHD2 or TRTMTHD3, the population of one of these attributes is  mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: One of TRTMTHD1,2,3 must be not null' ${:} ${NIN} and ( row['TRTMTHD2'] if 'TRTMTHD2' in row.keys() else None ) in ${list_NULL} and ( row['TRTMTHD3'] if 'TRTMTHD3' in row.keys() else None ) in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="PCHEMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - ground insecticide" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PCHEMA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PCHEMA" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="chemical - aerial spraying" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PMANUAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PMANUAL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="manual" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT1:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 1" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the treatment method is populated (TRTMTHD# != null) then the associated treatment  category must also be populated (TRTCAT# != null) " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be non-null when TRTMTHD is not null' ${:} ${NIN} and row['${name}'.replace('TRTCAT','TRTMTHD')] not in ${list_NULL}   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'TRTMTHD1':'REG'  }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be non-null when TRTMTHD is not null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT2:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the treatment method is populated (TRTMTHD# != null) then the associated treatment  category must also be populated (TRTCAT# != null) " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be non-null when TRTMTHD is not null' ${:} ${NIN} and row['${name}'.replace('TRTCAT','TRTMTHD')] not in ${list_NULL}   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'TRTMTHD2':'REG'  }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be non-null when TRTMTHD is not null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TRTCAT3:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Treatment Category 3" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRTCAT3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="TRTCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural treatment category attribute identifies if the treatment applied is regular (normal) or intentionally duplicates (i.e., retreatment) or augments (i.e., supplemental) a previous treatment." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                                <node TEXT=" If the treatment method is populated (TRTMTHD# != null) then the associated treatment  category must also be populated (TRTCAT# != null) " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be non-null when TRTMTHD is not null' ${:} ${NIN} and row['${name}'.replace('TRTCAT','TRTMTHD')] not in ${list_NULL}   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'TRTMTHD3':'REG'  }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be non-null when TRTMTHD is not null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="REG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REG" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Regular" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="RET:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RET" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="retreatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SUP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="supplemental treatment" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="PRODTYPE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Chemical Product Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="25" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PRODTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The chemical product type attribute contains the name of the chemical product being applied to the area along with the 5 digit Pesticide Control Product (PCP) registration number." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The product type attribute must be present and populated (PRODTYPE != null) when any of the treatment methods are aerial or ground chemical (TRTMTHD# = PCHEMA or PCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The product type must be null (PRODTYPE = null) when all the treatment methods are manual (TRTMTHD# = PMANUAL)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must not be null when PCHEMA or PCHEMG' ${:} ${NIN}  and  (     row['TRTMTHD1'] in ['PCHEMA','PCHEMG']     or     row['TRTMTHD2']  in ['PCHEMA','PCHEMG']     or     row['TRTMTHD3']  in ['PCHEMA','PCHEMG']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be null when all TRTMTHD# are PMANUAL' ${:} ${NINN}  and  (     row['TRTMTHD1'] in ['PMANUAL']     and     row['TRTMTHD2']  in ['PMANUAL']     and     row['TRTMTHD3']  in ['PMANUAL']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None,'TRTMTHD1':'PCHEMA','TRTMTHD2':'PMANUAL','TRTMTHD3':'PMANUAL'} " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must not be null when PCHEMA or PCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'Walnuts','TRTMTHD1':'PMANUAL','TRTMTHD2':'PMANUAL','TRTMTHD3':'PMANUAL'} " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be null when all TRTMTHD# are PMANUAL" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="RATE_AI:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Rate of Active Ingredient Applied" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RATE_AI" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The rate of active ingredient applied attribute contains the number of kilograms of active ingredient per hectare of the chemical product being applied to the area." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="o" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The attribute population must be between 0 and 9.99, inclusive." FOLDED="true"/>
                                <node TEXT="A zero value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The product quantity attribute must be present and greater than zero (RATE_AI &gt; 0) when any of the treatment methods are aerial or ground chemical (TRTMTHD# = PCHEMA or PCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The product quantity must be zero (RATE_AI = 0) when all the treatment methods are manual (TRTMTHD# = PMANUAL)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be &gt;= 0 and &lt;= 9' ${:N} ( row['${name}'] &lt; 0 or row['${name}'] &gt; 9.99 ) ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: ${name} must not be null when PCHEMA or PCHEMG' ${:} ${NIN}  and  (     row['TRTMTHD1'] in ['PCHEMA','PCHEMG']     or     row['TRTMTHD2']  in ['PCHEMA','PCHEMG']     or     row['TRTMTHD3']  in ['PCHEMA','PCHEMG']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be null when all TRTMTHD# are PMANUAL' ${:} ${NINN}  and  (     row['TRTMTHD1'] in ['PMANUAL']     and     row['TRTMTHD2']  in ['PMANUAL']     and     row['TRTMTHD3']  in ['PMANUAL']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be null when PCHEMA or PCHEMG' ${:} ${NINN}  and ( row['TRTMTHD1'] in ['PCHEMA','PCHEMG',None] and  ( None if 'TRTMTD2' not in fields else row['TRTMTHD2'] ) in ['PCHEMA','PCHEMG',None] and   ( None if 'TRTMTD3' not in fields else row['TRTMTHD3'] ) in ['PCHEMA','PCHEMG',None]  ]" FOLDED="true"/>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':10}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be &gt;= 0 and &lt;= 9" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0,'TRTMTHD1':'PCHEMA','TRTMTHD2':'PMANUAL','TRTMTHD3':'PMANUAL'} " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must not be null when PCHEMA or PCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':5,'TRTMTHD1':'PMANUAL','TRTMTHD2':'PMANUAL','TRTMTHD3':'PMANUAL'} " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be null when all TRTMTHD# are PMANUAL" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="o" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="APPNUM:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Number of Applications" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="APPNUM" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The number of applications attribute contains the number of times the area has been treated with the chemical product identified in PRODTYPE during this fiscal year." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="o" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A zero value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="Must be between 0 and 9" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The number of applications attribute must be present and greater than zero (APPNUM &gt; 0) when any of the treatment methods are aerial or ground chemical (TRTMTHD# = PCHEMA or PCHEMG)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="The number of applications must be zero (APPNUM = 0) when all the treatment methods are manual (TRTMTHD# = PMANUAL)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be &gt;= 0 and &lt;= 9' ${:N} ( row['${name}'] &lt; 0 or row['${name}'] &gt; 9 ) ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: ${name} must not be null when PCHEMA or PCHEMG' ${:} ${NIN}  and  (     row['TRTMTHD1'] in ['PCHEMA','PCHEMG']     or     row['TRTMTHD2']  in ['PCHEMA','PCHEMG']     or     row['TRTMTHD3']  in ['PCHEMA','PCHEMG']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be null when all TRTMTHD# are PMANUAL' ${:} ${NINN}  and  (     row['TRTMTHD1'] in ['PMANUAL']     and     row['TRTMTHD2']  in ['PMANUAL']     and     row['TRTMTHD3']  in ['PMANUAL']   ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':10}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must be &gt;= 0 and &lt;= 9" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0,'TRTMTHD1':'PCHEMA','TRTMTHD2':'PMANUAL','TRTMTHD3':'PMANUAL'} " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must not be null when PCHEMA or PCHEMG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':5,'TRTMTHD1':'PMANUAL','TRTMTHD2':'PMANUAL','TRTMTHD3':'PMANUAL'} " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: ${name} must be null when all TRTMTHD# are PMANUAL" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="o" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PRT" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['Field %s not found in table'%(f)  for f in ['TRTMTHD1','TRTCAT1'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="FreeToGrow:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="FreeToGrow" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Free To Grow (FTG)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT="The polygon identifier attribute is a unique identifier / label for the polygon. " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="ARDSTGRP:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AR Reprot Group" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ARDSTGRP" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ARDSTGRP" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The annual report disturbance group attribute indicates whether the initial stand disturbance was by natural causes or by harvest." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="NAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural disturbance" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="HARV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="YRDEP:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Depletion" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRDEP" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The year of last disturbance attribute indicates a four digit number of the fiscal year that a productive forest area was disturbed, completely or partially, by harvest or by natural causes." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="The year of last disturbance should be greater than the annual report start year minus twenty (the error occurs when YRDEP less than or equal to [AR year] - 20)" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST2: ${name} should be greater than ${year} - 20 ' ${:N} row['${name}'] &lt;= ( ${year} - 20 )  ]    " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="DSTBFU:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Forest Unit at Time of Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DSTBFU" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The forest unit at time of disturbance attribute contains the short form label used to reference the forest unit for the area at the time of disturbance." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="must be defined in the current FMP or an associated AWS" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SGR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Ground Rule" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SGR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural ground rule identifier attribute contains the SGR code identifying the selected silvicultural ground rule applied to the area." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TARGETFU:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Target Forest Unit" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TARGETFU" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The target forest unit attribute contains the short form label used to reference the forest unit in the future condition section of the associated SGR applied to the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="must be an FU defined in the FMP and/or AWS" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="FTG:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Free to Grow Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FTG" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The free-to-grow indicator attribute indicates if the assessed area is considered to be regenerated or not." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="FTGFU:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Free to Grow FU" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FTGFU" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The free-to-grow / current forest unit attribute contains the short form label of the forest unit that is assessed as being established/regenerated on the site." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A null or blank value is not a valid code when FTG = Y" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="Stage 3 validation should include reviewing this field with the SPCOMP to ensure that the FU meets the definitions as defined in the FMP." FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when FTG is Y' ${:} ${NIN}   and row['FTG'] == 'Y' ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'FTG':'Y'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when FTG is Y" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SPCOMP:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Species Composition" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="120" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SPCOMP" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="This species composition attribute indicates the tree species that are present in the stand canopy and the relative proportion of the canopy that each species occupies." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory where FTG = Y" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory where FTG = Y" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A null or blank value is not a valid code when FTG is Y" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="The species composition field may be null (SPCOMP = null) when the free-to-grow indicator is no (FTG = N)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="Follow string pattern of SSSPPPSSSPPP (repeating pattern of 3 species code characters then corresponding 3 proportion values)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="No duplicate species codes allowed in the string" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="Proportion values in the string must sum to 100 " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="Species code characters must be a valid code listed in the FIM Forest Management Planning Technical Specification" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when FTG is Y' ${:} ${NIN} and row['FTG'] == 'Y' ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST%s: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; %s'%( '2' if 'WARNING' in i else '1'   ,i)    for i in test_spcomp(row['${name}']) if ${NINN} if i not in  [ None, [ None ] , []  ] ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="full-7"/>
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; - Invalid species code' for s in test_SpeciesInList(row['${name}'] ) if ${NINN} ]" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'FTG':'Y'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when FTG is Y" FOLDED="true"/>
                                </node>
                                <node TEXT="{'FTG':'Y','${name}':'Sb100'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP not multiples of 6 characters: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'FTG':'Y','${name}':'Sb NaN'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPComp must have numeric codes for species occurrence: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'FTG':'Y','${name}':'Sb  50Sw  50Po  50'}" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP does not add to 100: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'FTG':'Y','${name}':'Sb  50Sb  50'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP cannot have duplicate species occurrences: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'FTG':'Y','${name}':'Po  40Sb  60', 'OLEADSPC':'Sb' }" FOLDED="true">
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; WARNING SPCOMP values are not in Descending Order: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'FTG':'Y','${name}':'Oz  90Po  10', 'SOURCE':'SPECTRAL'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; - Invalid species code" FOLDED="true"/>
                                </node>
                                <node TEXT="{'FTG':'Y','${name}':'Cw  90Po  10', 'SOURCE':'SPECTRAL', 'OLEADSPC':'Cw'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="HT:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Height" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The height attribute indicates the estimated average tree height (in metres) of the predominant species." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="Valid numeric values are from 0 through 40.0." FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero value is not a valid code when FTG = Y" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="The height field may be zero (HT = 0) when the free-to-grow indicator is no (FTG = N)." FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="When the free-to-grow indicator is yes (FTG = Y) then the height must be greater than or equal to 80cm (HT greater than or equal to 0.8)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must not be null when FTG is Y' ${:} ${NIN} and row['FTG'] == 'Y' ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: ${name} must follow format' ${:N} (row['${name}']  &lt; 0 or row['${name}'] &gt; 40.0) and row['FTG'] =='Y']     " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be &gt;=0.8 when FTG is Y' ${:N} row['${name}']  &lt; 0.8 and row['FTG'] =='Y']     " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':82, 'FTG': 'Y'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: HT must follow format" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'FTG': 'Y'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: HT must not be null when FTG is Y" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0.6, 'FTG': 'Y'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: ${name} must be &gt;=0.8 when FTG is Y" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="STKG:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Stocking" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="STKG" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The stocking attribute indicates a qualitative measure of the density of tree cover in a forest stand. It is expressed as a percentage value ranging from zero, for recently disturbed stands, to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field. Stocking of a forest stand refers to all species that make up the stand's canopy, but it is generally based on the species with the most basal area." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The silvicultural ground rules in a forest management plan describe the standards for assessing the renewal of forest stands, based on forest unit, desired species composition, age, height, and stocking. If the stocking of a productive forest stand does not meet the regeneration standards in the silvicultural ground rules of an approved forest management plan, the forest stand will be considered as not satisfactorily regenerated (NSR)." FOLDED="true"/>
                            <node TEXT="In some cases, the renewal and/or management standards of a silvicultural ground rule may be expressed as a density, which usually describes the frequency or number of stems per hectare. Where density information (stems/hectare) has been collected or determined from a regeneration survey, this information must be converted to a stocking value for the purpose of updating the forest stand description information." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="Valid numeric values are from 0 through 4.0." FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero value is not a valid code when FTG = Y" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: When the free-to-grow indicator is yes (FTG = Y) then the stocking must be greater than or equal to 40% (STKG &gt;= 0.4)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must not be null when FTG is Y' ${:} ${NIN} and row['FTG'] == 'Y' ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: ${name} must follow format' ${:N} (row['${name}']  &lt; 0 or row['${name}'] &gt; 4.0) and row['FTG'] =='Y']     " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: ${name} must be &gt;=0.4 when FTG is Y' ${:N} row['${name}']  &lt; 0.4 and row['FTG'] =='Y']     " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'FTG': 'Y'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: STKG must not be null when FTG is Y" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':42, 'FTG': 'Y'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: STKG must follow format" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0.2, 'FTG': 'Y'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: ${name} must be &gt;=0.4 when FTG is Y" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="FTG" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['Field %s not found in table'%(f)  for f in ['ARDSTGRP','YRDEP','DSTBFU','SGR', 'TARGETFU' , 'FTG', 'FTGFU', 'SPCOMP', 'HT', 'STKG'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="SilviculturalGroundRuleUpdate:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="SilviculturalGroundRuleUpdate" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Silvicultural Ground Rule Update (SGR)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT="The polygon identifier attribute is a unique identifier or label for the polygon." FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="SGR:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural Ground Rule ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SGR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The silvicultural ground rule identifier attribute contains the SGR code identifying the selected silvicultural ground rule applied to the area." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="must be an SGR defined in the FMP" FOLDED="true"/>
                                <node COLOR="#ff0000" TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['${name} must not be null' for row in [row] if row['${name}'] is None]  " FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SGR" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['Field %s not found in table'%(f)  for f in ['SGR'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                    <node TEXT="test:" FOLDED="true">
                        <node TEXT="{}" FOLDED="true"/>
                    </node>
                </node>
            </node>
            <node TEXT="ForestryAggregatePitsAR:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ForestryAggregatePitsAR" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Point" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Forestry Aggregate Pits (AGG)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT="The polygon identifier attribute is a unique identifier / label for the polygon. " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="PIT_ID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Aggregate Pit ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PIT_ID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The aggregate pit identifier attribute indicates the unique identifier / label for the aggregate pit within which activities occurred during the fiscal year." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="A blank or null value is not a valid code" FOLDED="true"/>
                                    <node TEXT="A blank or null value is not a valid code" FOLDED="true"/>
                                </node>
                                <node TEXT="The PITID attribute must contain a unique value" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The PIT_ID attribute must contain a unique Value" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true"/>
                    </node>
                    <node TEXT="REHABREQ:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Hectares Requiring Rehabilitation" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="REHABREQ" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The hectares requiring rehabilitation attribute indicates the total number of hectares that still require rehabilitation. This is the remaining active area of the forestry aggregate pit." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1:The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1:The attribute population must follow the correct format (0-9.9 ha)" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A zero value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST2:If the area requiring rehabilitation is greater than zero (REHABREQ greater than 0) then the pit closure date should be null (PITCLOSE = null)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2:The hectares requiring rehabilitation should be less than or equal to three (REHABREQ less than or equal to 3.0)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must  be between 0.0 and 9.99, inclusive' ${:} (row['${name}'] &gt; 9.99 or row['${name}'] &lt; 0.0) or row['$name'] in [None, '', ' ' ] ]  " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST2: ${name} must not be &gt; 0 when PITCLOSE is not null' ${:} row['${name}'] &gt; 0.0 and row['PITCLOSE'] not in ${list_NULL} ] " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST2: ${name} should  be between 0.0 and 3.0, inclusive' ${:N} row['${name}'] &gt; 3.0 and row['${name}'] &lt; 10]  " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':1,'PITCLOSE':'21MAR30'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST2: REHABREQ must not be &gt; 0 when PITCLOSE is not null" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':42}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must  be between 0.0 and 9.99, inclusive" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':4.2}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST2: REHABREQ should  be between 0.0 and 3.0, inclusive" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="REHAB:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Hectares Rehabilitated" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="REHAB" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The hectares rehabilitated attribute indicates the total number of hectares that were rehabilitated during the fiscal year." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1:The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1:The attribute population must follow the correct format, from 0 to 9.9" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1:A zero value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST2:If the area rehabilitated is zero (REHAB = 0) then the tonnes of aggregate extracted should not be zero (TONNES not 0)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2:The hectares rehabilitated should be less than or equal to three (REHAB less than or equal to 3.0)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must  be between 0.0 and 9.99, inclusive' ${:} (row['${name}'] &gt; 9.99 or row['${name}'] &lt; 0.0) or row['${name}'] in [None, '', ' '] ]  " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST2: TONNES should be &gt; 0 when ${name} is 0' ${:} row['${name}'] == 0.0 and row['TONNES'] == 0 ] " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST2: ${name} should be between 0.0 and 3.0, inclusive' ${:N} (row['${name}'] &gt; 3.0 or row['${name}'] &lt; 0.0 ) and row['${name}'] &lt; 10 ]  " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must  be between 0.0 and 9.99, inclusive" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':42}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must  be between 0.0 and 9.99, inclusive" FOLDED="true"/>
                                </node>
                                <node TEXT="{ '${name}':0, 'TONNES':0 }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST2: TONNES should be &gt; 0 when ${name} is 0" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':4.2}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST2: ${name} should be between 0.0 and 3.0, inclusive" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="PITCLOSE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Final Reabilitation Date" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="9" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PITCLOSE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The pit closure date attribute indicates the date when the final rehabilitation has occurred on the site." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The pit closure date should be within the fiscal year" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="When the final rehabilitation date is populated (PITCLOSE != null) then the required  rehabilitation will be zero (REHABREQ = 0) " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must be in fiscal year ${year}' ${:N}  ((fimdate( row['${name}']  ) &lt;  fimdate( str ( ${year} ) + 'APR01')) or  (fimdate( row['${name}']) &gt;  fimdate( str ( ${year} +1) + 'MAR31' ))) ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: When ${name} is populated, REHABREQ must be 0' ${:N} row['REHABREQ'] not in ${list_NULL} ] " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'2017MAR30', 'REHABREQ':0}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: ${name} must be in fiscal year ${year}" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'2018MAR30', 'REHABREQ':3}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: When ${name} is populated, REHABREQ must be 0" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="TONNES:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Tonnes of Aggregate Extracted" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Float" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TONNES" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The tonnes of aggregate extracted attribute indicates the total number of tonnes of material extracted during the fiscal year" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The attribute population must follow the correct format 0-99999999" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A zero value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must  be between 0.0 and 99999999, inclusive' ${:} (row['${name}'] &lt; 0.0 or row['${name}']&gt;9999999)]   " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':420000000}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: ${name} must  be between 0.0 and 99999999, inclusive" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="AGG" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                        <node TEXT="The PIT_ID attribute must contain a unique Value" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['PIT_ID Field values must be unique' for f in ['PIT_ID' ] if not self.uniqueValueTest(f) ]" FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="SlashChipTreatment:" FOLDED="true" POSITION="right">
                <node TEXT="name" FOLDED="true">
                    <node TEXT="SlashChipTreatment" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polyline" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="AnnualReports" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Slash and Chip Pile Treatment (SCT)" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT="The polygon identifier attribute is a unique identifier / label for the polygon. " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="SLASHPIL:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Slash Piling" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SLASHPIL" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="At a minimum, one of Slash Piling, Chip Piling, Slash Burning, Onsite Mechanical Processing or Removal Offsite for Processing must occur for each record (SLASHPIL = Y or CHIPPIL = Y or BURN = Y or MECHANIC = Y or REMOVAL = Y)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must include an action' ${:} 'Y' not in [ str(row[a]) for a in ['SLASHPIL','CHIPPIL','BURN','MECHANIC' ] ]    ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CHIPPIL:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Chip Piling" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CHIPPIL" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The chip piling attribute indicates the mechanical accumulation of forest debris, most commonly from mobile chipping operations, in to piles for the purpose of increasing available planting areas or future removal of the pile through mechanical processing onsite or offsite." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="At a minimum, one of Slash Piling, Chip Piling, Slash Burning, Onsite Mechanical Processing or Removal Offsite for Processing must occur for each record (SLASHPIL = Y or CHIPPIL = Y or BURN = Y or MECHANIC = Y or REMOVAL = Y)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must include an action' ${:} 'Y' not in [ str(row[a]) for a in ['SLASHPIL','CHIPPIL','BURN','MECHANIC' ] ]    ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="BURN:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Burning" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="BURN" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="At a minimum, one of Slash Piling, Chip Piling, Slash Burning, Onsite Mechanical Processing or Removal Offsite for Processing must occur for each record (SLASHPIL = Y or CHIPPIL = Y or BURN = Y or MECHANIC = Y or REMOVAL = Y)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must include an action' ${:} 'Y' not in [ str(row[a]) for a in ['SLASHPIL','CHIPPIL','BURN','MECHANIC' ] ]    ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MECHANIC:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Mechanical Treatment" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MECHANIC" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The onsite mechanical processing attribute indicates the mechanical processing of forest debris at roadside in to a form that can be easily processed (e.g. biomass, hog fuel) the purpose of increasing available planting areas. This attribute is not intended to capture the mechanical distribution of forest debris on the road bed or within the adjacent harvest block." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="At a minimum, one of Slash Piling, Chip Piling, Slash Burning, Onsite Mechanical Processing or Removal Offsite for Processing must occur for each record (SLASHPIL = Y or CHIPPIL = Y or BURN = Y or MECHANIC = Y or REMOVAL = Y)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must include an action' ${:} 'Y' not in [ str(row[a]) for a in ['SLASHPIL','CHIPPIL','BURN','MECHANIC' ] ]    ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="REMOVAL:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Removal of Slash or Chips" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="REMOVAL" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The removal offsite for processing attribute indicates the physical removal of the forest debris from roadside for processing at an offsite facility for the purpose of increasing available planting areas." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT=" " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="At a minimum, one of Slash Piling, Chip Piling, Slash Burning, Onsite Mechanical Processing or Removal Offsite for Processing must occur for each record (SLASHPIL = Y or CHIPPIL = Y or BURN = Y or MECHANIC = Y or REMOVAL = Y)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: ${name} must include an action' ${:} 'Y' not in [ str(row[a]) for a in ['SLASHPIL','CHIPPIL','BURN','MECHANIC' ] ]    ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT=" " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="SCT" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="['Field %s not found in table'%(f)  for f in ['SLASHPIL','CHIPPIL','BURN','MECHANIC','REMOVAL'] if f not in fields ] " FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="metadata:" FOLDED="true" POSITION="left">
                <node TEXT="submission_type" FOLDED="true">
                    <node TEXT="AR_2009" FOLDED="true"/>
                </node>
                <node TEXT="submission_min_year" FOLDED="true">
                    <node TEXT="2009" FOLDED="true"/>
                </node>
                <node TEXT="submission_max_year" FOLDED="true">
                    <node TEXT="2016" FOLDED="true"/>
                </node>
                <node TEXT="id" FOLDED="true">
                    <node TEXT="fi_portal_id" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="{fmu}\{product}\{year}" FOLDED="true"/>
                </node>
                <node TEXT="Document" FOLDED="true">
                    <node STYLE="fork" TEXT="MU{fmu_id}_{year}_{product}_TXT_Text.pdf" FOLDED="true"/>
                    <node TEXT="MU{fmu_id}_{year}_{product}_TBL_Tables.pdf" FOLDED="true"/>
                    <node TEXT="MU{fmu_id}_{year}_{product}_MAP_Sum.pdf" FOLDED="true"/>
                    <node TEXT="MU{fmu_id}_{year}_{product}_MAP_SumFR.pdf" FOLDED="true"/>
                    <node TEXT="MU*_MAP_SUM*.pdf" FOLDED="true"/>
                    <node STYLE="fork" TEXT="MU*TXT*.pdf" FOLDED="true"/>
                </node>
                <node TEXT="Document_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Layer" FOLDED="true">
                    <node TEXT="mu{fmu_id}_{year}_AR.gpkg" FOLDED="true"/>
                    <node TEXT="mu{fmu_id}_{year}_AR.gdb" FOLDED="true"/>
                    <node TEXT="*.gdb/*" FOLDED="true"/>
                    <node TEXT="*.e00" FOLDED="true"/>
                    <node TEXT="*.shp" FOLDED="true"/>
                    <node TEXT="*.dbf" FOLDED="true"/>
                    <node TEXT="*.shx" FOLDED="true"/>
                    <node TEXT="*.prj" FOLDED="true"/>
                    <node TEXT="*.shp.xml" FOLDED="true"/>
                </node>
                <node TEXT="Layer_Filename" FOLDED="true">
                    <node TEXT="mu{fmu_id}_{year}_AR" FOLDED="true"/>
                </node>
                <node TEXT="Maps" FOLDED="true">
                    <node TEXT="MU{fmu_id}_{planyear}_{product}_P{phase}_MAP*.eps" FOLDED="true"/>
                    <node TEXT="*.eps" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayer" FOLDED="true">
                    <node TEXT="*.atx" FOLDED="true"/>
                    <node TEXT="*.spx" FOLDED="true"/>
                    <node TEXT="*.gdbindexes" FOLDED="true"/>
                    <node TEXT="*.gdbtablx" FOLDED="true"/>
                    <node TEXT="*.gdbtable" FOLDED="true"/>
                    <node TEXT="*.freelist" FOLDED="true"/>
                    <node STYLE="fork" TEXT="*/gdb" FOLDED="true"/>
                    <node TEXT="*/timestamps" FOLDED="true"/>
                </node>
                <node TEXT="DeferredLayer_Directory" FOLDED="true">
                    <node TEXT="LAYERS/mu{fmu_id}_{year}_AR.gdb" FOLDED="true"/>
                </node>
                <node TEXT="Maps_Directory" FOLDED="true">
                    <node STYLE="fork" TEXT="Maps" FOLDED="true"/>
                </node>
                <node TEXT="Internal_zipfile" FOLDED="true">
                    <node STYLE="fork" TEXT="*.zip" FOLDED="true"/>
                </node>
                <node TEXT="Internal_zipfile_Directory" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Catchall" FOLDED="true">
                    <node TEXT="" FOLDED="true"/>
                </node>
                <node TEXT="Catchall_Directory" FOLDED="true">
                    <node TEXT="Other" FOLDED="true"/>
                </node>
            </node>
        </node>
    </map>
