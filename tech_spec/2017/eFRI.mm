    <map version="1.0.1">
        <node TEXT="eFRI" ID="ID_1962465062">
            <node TEXT="Enhanced Forest Resource Inventory:" FOLDED="true" POSITION="right" ID="ID_1950079139">
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true" ID="ID_1894984611">
                    <node TEXT="Enhanced Forest Resource Inventory" FOLDED="true" ID="ID_349352666"/>
                </node>
                <node TEXT="shortname" FOLDED="true" ID="ID_1971638785">
                    <node TEXT="EFRI" FOLDED="true" ID="ID_683492747"/>
                </node>
                <node TEXT="alias" FOLDED="true" ID="ID_471371315">
                    <node TEXT="Enhanced Forest Resource Inventory (EFRI)" FOLDED="true" ID="ID_118638358"/>
                </node>
                <node TEXT="path" FOLDED="true" ID="ID_1349521427">
                    <node TEXT="Plan" FOLDED="true" ID="ID_125576474"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true" ID="ID_456813670">
                    <node TEXT="Polygon" FOLDED="true" ID="ID_1672647717"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true" ID="ID_1110179898">
                    <node TEXT="26915" FOLDED="true" ID="ID_920872949"/>
                </node>
                <node TEXT="fields" FOLDED="true" ID="ID_9712281">
                    <node TEXT="POLYID:" FOLDED="true" ID="ID_1537605945">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1790327456">
                            <node TEXT="25" FOLDED="true" ID="ID_964094991"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1507276496">
                            <node TEXT="Character" FOLDED="true" ID="ID_781140718"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1588417782">
                            <node TEXT="POLYID" FOLDED="true" ID="ID_1771563759"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_134671739">
                            <node TEXT="Polygon Identifier" FOLDED="true" ID="ID_1821843183"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_538570184">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1840980097"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_892230678"/>
                            <node TEXT="The POLYID attribute must contain a unique value" FOLDED="true" ID="ID_665300470"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_316753959">
                            <node TEXT="The planning inventory polygon identifier attribute is a unique id or label for the polygon often based on geographic location." FOLDED="true" ID="ID_330056615"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1509732272">
                            <node TEXT="english" FOLDED="true" ID="ID_720682507">
                                <node TEXT="ST1: Blank or null values are not a valid code" FOLDED="true" ID="ID_196120030">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: Attribute must contain a unique value" FOLDED="true" ID="ID_476340661">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_810356071"/>
                            <node TEXT="sql" FOLDED="true" ID="ID_221162195"/>
                        </node>
                    </node>
                    <node TEXT="POLYTYPE:" FOLDED="true" ID="ID_409678549">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_428467443">
                            <node TEXT="3" FOLDED="true" ID="ID_245429892"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1306246150">
                            <node TEXT="Character" FOLDED="true" ID="ID_473114825"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_239865947">
                            <node TEXT="POLYTYPE" FOLDED="true" ID="ID_414104299"/>
                        </node>
                        <node TEXT="domain" FOLDED="true" ID="ID_296568767">
                            <node TEXT="POLYTYPE" FOLDED="true" ID="ID_995950355"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1848692854">
                            <node TEXT="Polygon Type" FOLDED="true" ID="ID_1474356668"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_330154868">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_644488306"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_1628050143"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1737195031">
                            <node TEXT="The polygon type attribute indicates the classification of the area within the polygon boundaries into one of several generalized water and land types." FOLDED="true" ID="ID_1762567251"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_94072135">
                            <node TEXT="The polygon type is determined from the classification of area on a forest management unit into different water and land types. Some polygon types are derived from inventory base features, while other polygons must be created from a classification process.  All remaining land areas within a designated forest management unit are classified into various non-forested or forested lands." FOLDED="true" ID="ID_676146269"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_684156097">
                            <node TEXT="english" FOLDED="true" ID="ID_120943327">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1611379205">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true" ID="ID_1597837108">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1470332570">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true" ID="ID_592578857">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: If POLYTYPE attribute does not equal FOR, then all forest-specific attributes should be empty.  CHECKED IN INDIVIDUAL ATTRIBUTES, NOT POLYTYPE PYTHON CODE." FOLDED="true" ID="ID_1432067344">
                                    <icon BUILTIN="bookmark"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: If a delineated polygon is an island or is located on an island, the management consideration attribute is set to island MGMTCON1 = ISLD. This applies to all polygon types. This apparent redundancy in classification for polygons having a type of island i.e., POLYTYPE = ISL   and   MGMTCON1 = ISLD allow resource managers to easily identify:   1. all polygons located on islands regardless of polygon type by querying on MGMTCON1 = ISLD  2. just the small uninterpreted islands by querying on POLYTYPE = ISL depending upon the desired analysis." FOLDED="true" ID="ID_1114412794">
                                    <icon BUILTIN="bookmark"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1456628419"/>
                            <node TEXT="sql" FOLDED="true" ID="ID_543999323"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_1951975430">
                            <node TEXT="WAT:" FOLDED="true" ID="ID_1643339999">
                                <node TEXT="name" FOLDED="true" ID="ID_1228962764">
                                    <node TEXT="WAT" FOLDED="true" ID="ID_1444665941"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_120304479">
                                    <node STYLE="fork" TEXT="All water areas.  Includes lakes, ponds, reservoirs i.e., inland basin areas containing water and wide two sided rivers.  These are rivers that can be defined by area.  Generally, these rivers and streams are consistently wider than 20 meters when portrayed at a mapping scale of 1:10,000 or consistently wider than 40 meters when portrayed at a mapping scale of 1:20,000.  Smaller or narrower rivers and streams are maintained as linear features in a centre-line layers." FOLDED="true" ID="ID_945729115"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1248263723">
                                    <node TEXT="Water" FOLDED="true" ID="ID_1730833787"/>
                                </node>
                            </node>
                            <node TEXT="DAL:" FOLDED="true" ID="ID_188753529">
                                <node TEXT="name" FOLDED="true" ID="ID_1834569538">
                                    <node TEXT="DAL" FOLDED="true" ID="ID_1836603822"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1831658246">
                                    <node TEXT="Lands which are cultivated for growing crops, orchards, floral gardens, etc. These areas may include abandoned agricultural lands." FOLDED="true" ID="ID_1772596518"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_525740444">
                                    <node TEXT="Developed agricultural land" FOLDED="true" ID="ID_617775708"/>
                                </node>
                            </node>
                            <node TEXT="GRS:" FOLDED="true" ID="ID_1731282839">
                                <node TEXT="name" FOLDED="true" ID="ID_1475550900">
                                    <node TEXT="GRS" FOLDED="true" ID="ID_1970542428"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_565225169">
                                    <node TEXT="Farm areas devoted to pasture for domesticated animals. These areas may also include abandoned grass and meadows, but are not part of the productive forest land base and do not include barren and scattered areas. These areas are similar to barren and scattered, but are located near developed agriculture land or unclassified areas and are usually fenced." FOLDED="true" ID="ID_1212691828"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_196198630">
                                    <node TEXT="Grass and meadow" FOLDED="true" ID="ID_511455227"/>
                                </node>
                            </node>
                            <node TEXT="ISL:" FOLDED="true" ID="ID_1215464673">
                                <node TEXT="name" FOLDED="true" ID="ID_1884684160">
                                    <node TEXT="ISL" FOLDED="true" ID="ID_1703625269"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1117421507">
                                    <node TEXT="Islands less than 8 hectares in size, down to a lower limit of 0.0025 hectares or 25 square meters in size e.g., 5 meters X 5 meters are recorded during the inventory production process, but are not interpreted or typed for practicality and cost considerations.  Only islands 8 hectares and larger are interpreted and assigned an appropriate POLYTYPE code, such as FOR or BSH." FOLDED="true" ID="ID_1364202486"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_320915583">
                                    <node TEXT="Small island" FOLDED="true" ID="ID_180765584"/>
                                </node>
                            </node>
                            <node TEXT="UCL:" FOLDED="true" ID="ID_1111536729">
                                <node TEXT="name" FOLDED="true" ID="ID_713825744">
                                    <node TEXT="UCL" FOLDED="true" ID="ID_58402215"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1524966832">
                                    <node TEXT="Non-forested areas which were created for specific uses other than timber production, such as  roads, railroads, logging camps, mines, utility corridors, logging camps, gravel pits, airports, etc." FOLDED="true" ID="ID_800553942"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1536307978">
                                    <node TEXT="Unclassified" FOLDED="true" ID="ID_572645023"/>
                                </node>
                            </node>
                            <node TEXT="BSH:" FOLDED="true" ID="ID_1327343448">
                                <node TEXT="name" FOLDED="true" ID="ID_319732658">
                                    <node TEXT="BSH" FOLDED="true" ID="ID_633857057"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_794027756">
                                    <node TEXT="Areas covered with non-commercial tree species or shrubs.  These areas are normally associated with wetlands or water features." FOLDED="true" ID="ID_1683990375"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1016659820">
                                    <node TEXT="Brush and alder" FOLDED="true" ID="ID_1718060127"/>
                                </node>
                            </node>
                            <node TEXT="RCK:" FOLDED="true" ID="ID_1670288023">
                                <node TEXT="name" FOLDED="true" ID="ID_1318302908">
                                    <node TEXT="RCK" FOLDED="true" ID="ID_1204633434"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1832474439">
                                    <node TEXT="Areas of barren or exposed rock e.g., bedrock, cliff face, talus slope which may support a few scattered trees, but is less that 25 percent crown closure." FOLDED="true" ID="ID_117369062"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_899432932">
                                    <node TEXT="Rock" FOLDED="true" ID="ID_1057037236"/>
                                </node>
                            </node>
                            <node TEXT="OMS:" FOLDED="true" ID="ID_654611655">
                                <node TEXT="name" FOLDED="true" ID="ID_883307187">
                                    <node TEXT="OMS" FOLDED="true" ID="ID_1128114713"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_851304987">
                                    <node TEXT="Wet areas of mosses, grasses, sedges, and small herbaceous plants, often interspersed with small areas of open water" FOLDED="true" ID="ID_1901152068"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_410832789">
                                    <node TEXT="Open Wetland" FOLDED="true" ID="ID_555636194"/>
                                </node>
                            </node>
                            <node TEXT="TMS:" FOLDED="true" ID="ID_1710125112">
                                <node TEXT="name" FOLDED="true" ID="ID_600511209">
                                    <node TEXT="TMS" FOLDED="true" ID="ID_425845261"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1257822440">
                                    <node TEXT="Areas of dry or wet muskeg on which stunted trees occur as widely spaced individuals or in small groups." FOLDED="true" ID="ID_759469683"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_48884187">
                                    <node TEXT="Treed wetland" FOLDED="true" ID="ID_413887381"/>
                                </node>
                            </node>
                            <node TEXT="FOR:" FOLDED="true" ID="ID_1397206952">
                                <node TEXT="name" FOLDED="true" ID="ID_507960703">
                                    <node TEXT="FOR" FOLDED="true" ID="ID_735761020"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_870769220">
                                    <node TEXT="Areas that are capable of producing trees and can support tree growth.  These areas may or may not be capable of supporting the harvesting of timber on a sustained yield basis.  Some areas may have physical and/or biological characteristics which effect land use.  Thus this polygon type includes both production and protection forest areas." FOLDED="true" ID="ID_1363022113"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1947365">
                                    <node TEXT="Productive forest" FOLDED="true" ID="ID_1409033269"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="YRSOURCE:" FOLDED="true" ID="ID_884612531">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="name" FOLDED="true" ID="ID_1134411529">
                            <node TEXT="YRSOURCE" FOLDED="true" ID="ID_360363438"/>
                        </node>
                        <node TEXT="length" FOLDED="true" ID="ID_1373551365">
                            <node TEXT="4" FOLDED="true" ID="ID_660118968"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1244232550">
                            <node TEXT="Integer" FOLDED="true" ID="ID_1314519622"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1426118457">
                            <node TEXT="Year of Data Update" FOLDED="true" ID="ID_961736612"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_673413765">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_795654720"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_274425055"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true" ID="ID_637558940"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1699858231">
                            <node TEXT="The year of data update attribute contains a four digit number representing the calendar year (January 1 to December 31) of the source data used to determine update the polygon description, in particular the height attribute." FOLDED="true" ID="ID_286917256"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_436386535">
                            <node TEXT="The year of data update identifies the calendar year that information about a polygon was last confirmed or modified based on field inspection, photo interpretation, analysis of satellite imagery, or conversion or update to spatial or tabular data. The year of data update should not be changed to reflect error corrections to tabular attributes." FOLDED="true" ID="ID_1785060612"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1859810807">
                            <node TEXT="english" FOLDED="true" ID="ID_1448080728">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1067585658">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true" ID="ID_730435885">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format" FOLDED="true" ID="ID_599237357">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code" FOLDED="true" ID="ID_1713536358">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: YRSOURCE must be a year less than the plan period start year" FOLDED="true" ID="ID_1266647957">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="Must be a year greater than or equal to 1975" FOLDED="true" ID="ID_715071841">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_560181801">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 1975 ' for row in [row] if (row['${name}'] not in [ None , '', ' ', 0 ] ) and row['${name}']  &lt;1975  ]" FOLDED="true" ID="ID_1530475171">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than  ${planyear} ' for row in [row] if (row['${name}'] not in [None, '', ' ']) and row['${name}']  &gt;= ${planyear}  ]" FOLDED="true" ID="ID_1251910850">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1904065320"/>
                        </node>
                    </node>
                    <node TEXT="SOURCE:" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_744323157">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_688023994">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1537904978">
                            <node TEXT="Character" FOLDED="true" ID="ID_145219799"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_69652421">
                            <node TEXT="SOURCE" FOLDED="true" ID="ID_1350419437"/>
                        </node>
                        <node TEXT="domain" FOLDED="true" ID="ID_1314705959">
                            <node TEXT="SOURCE" FOLDED="true" ID="ID_1169456226"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_538557779">
                            <node TEXT="Source of Update Data " FOLDED="true" ID="ID_1072506664"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_831593743">
                            <node TEXT="The source of data update attribute identifies the methodology by which the information stored in the other tabular attributes that are associated with the same polygon was determined (i.e., how the polygon description was determined)." FOLDED="true" ID="ID_943126796"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_477679946">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1449927514"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_633848918"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_930188343"/>
                        </node>
                        <node TEXT="Description" FOLDED="true" ID="ID_1850289326">
                            <node TEXT="The source of data update attribute does not apply to the changes or updates made to the ecosite attribute." FOLDED="true" ID="ID_1745044119"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_1487511832">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_776025659">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_800819668">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true" ID="ID_1369443807">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1796023785">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true" ID="ID_301556950">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A value of FORECAST is only valid for the SOURCE attribute in the Base Model Inventory" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_357354131">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the DEVSTAGE attribute starts with EST, then the SOURCE attribute should not equal ESTIMATE" FOLDED="true" ID="ID_1809829493">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST2: If the DEVSTAGE attribute starts with NEW, then the SOURCE attribute should be ESTIMATE. FIM Difference - Allow ESTIMATE in EPC product to allow new treatment estimates." FOLDED="true" ID="ID_911962965">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_1980553437">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be FORECAST unless inventory type is Base Model Inventory ' for row in [row] if row['${name}']  == 'FORECAST' and '${tablename}' != 'BaseModelInventory' ]" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_265248202">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be ESTIMATE when DEVSTAGE LIKE EST' for row in [row] if row['${name}']  == 'ESTIMATE' and row['DEVSTAGE'] in [ 'ESTNAT','ESTPLANT','ESTSEED' ] ]" FOLDED="true" ID="ID_1605810241">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_458017613"/>
                        </node>
                        <node TEXT="values" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_248571218">
                            <node TEXT="BASECOVR:" FOLDED="true" ID="ID_1766732024">
                                <node TEXT="name" FOLDED="true" ID="ID_49689635">
                                    <node TEXT="BASECOVR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1954178806">
                                    <node TEXT="Information that is provided by the MNRF  (e.g., water or evaluated wetlands)." FOLDED="true" ID="ID_1805696638"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1009986983">
                                    <node TEXT="planimetric base layer" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true" ID="ID_1253995550">
                                <node TEXT="name" FOLDED="true" ID="ID_745966301">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_839827017">
                                    <node TEXT="Digital analysis, automated processing. (e.g., Ecognition)" FOLDED="true" ID="ID_1138436585"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_775053782">
                                    <node TEXT="multispectral scanning (digital image) - automated process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true" ID="ID_159345570">
                                <node TEXT="name" FOLDED="true" ID="ID_1644136654">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_344943107">
                                    <node TEXT="Photo-interpreted by softcopy systems" FOLDED="true" ID="ID_1079947020"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1248563233">
                                    <node TEXT="multispectral scanning (digital image) - manual process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTIMATE:" FOLDED="true" ID="ID_1521609762">
                                <node TEXT="name" FOLDED="true" ID="ID_251612992">
                                    <node TEXT="ESTIMATE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1923939159">
                                    <node TEXT="This source option is only for use in areas that have been recently renewed and have not been revisited since the renewal work was performed.  That is, where a follow-up survey has not yet been performed.  Therefore, the description of the newly regenerated stand is a 'best estimate' of the expected outcome / result of the renewal treatment that was applied to the area based on past silvicultural successes." FOLDED="true" ID="ID_1610487511"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_849076876">
                                    <node TEXT="expected / estimated outcome / result" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOC:" FOLDED="true" ID="ID_626508672">
                                <node TEXT="name" FOLDED="true" ID="ID_1182705084">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1710845553">
                                    <node TEXT="Inspection of a site after silvicultural treatment to determine whether an operator / operations conforms to the approved plan or permit.   The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here." FOLDED="true" ID="ID_436174763"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1620437978">
                                    <node TEXT="forest operations compliance inspection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FORECAST:" FOLDED="true" BACKGROUND_COLOR="#99ff99" ID="ID_1354717015">
                                <node TEXT="name" FOLDED="true" ID="ID_284225786">
                                    <node TEXT="FORECAST" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_451262059">
                                    <node TEXT="This indicates a polygon description that was updated based on expected outcomes of planned operations (which have not yet been implemented) for the remainder of the current plan term.  This code is only valid in the Base Model Inventory. " FOLDED="true" ID="ID_79105673"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1207103815">
                                    <node TEXT="forecasted description" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true" ID="ID_1158354376">
                                <node TEXT="name" FOLDED="true" ID="ID_498294497">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_912444282">
                                    <node TEXT="Current polygon description based on data conversion from traditional FRI" FOLDED="true" ID="ID_824090136"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_678475244">
                                    <node TEXT="forest resources inventory conversion" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="INFRARED:" FOLDED="true" ID="ID_471583288">
                                <node TEXT="name" FOLDED="true" ID="ID_1152521228">
                                    <node TEXT="INFRARED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1456220982">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_210478234">
                                    <node TEXT="Infrared satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true" ID="ID_1384698561">
                                <node TEXT="name" FOLDED="true" ID="ID_300772042">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_892481867">
                                    <node TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription.  Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns." FOLDED="true" ID="ID_1266504315"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1837521603">
                                    <node TEXT="pre-harvest site inspection / marking" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true" ID="ID_1471561847">
                                <node TEXT="name" FOLDED="true" ID="ID_1850148960">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_324469706">
                                    <node TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft." FOLDED="true" ID="ID_204603574"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1336710546">
                                    <node TEXT="aerial survey / reconnaissance" FOLDED="true" ID="ID_1664036666"/>
                                </node>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true" ID="ID_1097670873">
                                <node TEXT="name" FOLDED="true" ID="ID_1879082013">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1074038237">
                                    <node TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements)." FOLDED="true" ID="ID_1380020569"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_896571640">
                                    <node TEXT="ocular estimate (ground)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OPC:" FOLDED="true" ID="ID_246567154">
                                <node TEXT="name" FOLDED="true" ID="ID_1558530806">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_650145052">
                                    <node TEXT="Measuring standing trees to determine the volume of wood on a given tract of land." FOLDED="true" ID="ID_673779840"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_324015170">
                                    <node TEXT="operational cruise" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true" ID="ID_264617043">
                                <node TEXT="name" FOLDED="true" ID="ID_1982579798">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1954471937">
                                    <node TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000" FOLDED="true" ID="ID_1442320899"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_856617209">
                                    <node TEXT="air photo interpretation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true" ID="ID_120331914">
                                <node TEXT="name" FOLDED="true" ID="ID_98573163">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1025102600">
                                    <node TEXT="Photography at a scale larger than 1:10,000  (e.g., 1:500, 1:1000)." FOLDED="true" ID="ID_1834112259"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1230420981">
                                    <node TEXT="large scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true" ID="ID_1189251334">
                                <node TEXT="name" FOLDED="true" ID="ID_1289156948">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1147411627">
                                    <node TEXT="Photography at a scale smaller than 1:20,000  (e.g., 1:100,000)." FOLDED="true" ID="ID_1726509103"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1175043349">
                                    <node TEXT="small scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true" ID="ID_638334473">
                                <node TEXT="name" FOLDED="true" ID="ID_518311665">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_783233500">
                                    <node TEXT="Fixed area plot" FOLDED="true" ID="ID_661669881"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_117455556">
                                    <node TEXT="fixed area plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true" ID="ID_26036636">
                                <node TEXT="name" FOLDED="true" ID="ID_1606043984">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_993663602">
                                    <node TEXT="Variable area plots." FOLDED="true" ID="ID_107810284"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1741286642">
                                    <node TEXT="variable area (radius) plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="RADAR:" FOLDED="true" ID="ID_1488993266">
                                <node TEXT="name" FOLDED="true" ID="ID_372630028">
                                    <node TEXT="RADAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1681968119">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1612184246">
                                    <node TEXT="radar satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true" ID="ID_999822678">
                                <node TEXT="name" FOLDED="true" ID="ID_353041329">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_488093974">
                                    <node TEXT="Survey of a regenerated area to determine how well the new stand is growing.  This includes seeding, survival, and stocking assessments.." FOLDED="true" ID="ID_1919264252"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1803798516">
                                    <node TEXT="regeneration assessment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true" ID="ID_1430226310">
                                <node TEXT="name" FOLDED="true" ID="ID_302695066">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_138489441">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).   Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment." FOLDED="true" ID="ID_1840849159"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1945918239">
                                    <node TEXT="extensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true" ID="ID_301941500">
                                <node TEXT="name" FOLDED="true" ID="ID_1764640168">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_527041333">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).  Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments." FOLDED="true" ID="ID_1427217550"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1919718095">
                                    <node TEXT="intensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SPECTRAL:" FOLDED="true" ID="ID_892344696">
                                <node TEXT="name" FOLDED="true" ID="ID_73178655">
                                    <node TEXT="SPECTRAL" FOLDED="true" ID="ID_271361431"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1871491877">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1477421283">
                                    <node TEXT="spectral satellite imagery" FOLDED="true" ID="ID_719119221"/>
                                </node>
                            </node>
                            <node TEXT="SUPINFO:" FOLDED="true" ID="ID_1094546286">
                                <node TEXT="name" FOLDED="true" ID="ID_124275313">
                                    <node TEXT="SUPINFO" FOLDED="true" ID="ID_22429584"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1796431689">
                                    <node TEXT="Stand update information from a source other than those listed/defined in the other coding options that is provided by either MNR or Licensee. For example, disturbance records. The date of information capture/acquisition and the age of the stand as of the date of information was acquired must be supplied for the information to be incorporated into the inventory." FOLDED="true" ID="ID_1819841686">
                                        <icon BUILTIN="closed"/>
                                    </node>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1012570409">
                                    <node TEXT="Supplied information" FOLDED="true" ID="ID_1152086683"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="FORMOD:" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_1527781025">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_523709944">
                            <node TEXT="2" FOLDED="true" ID="ID_105971153"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_776660999">
                            <node TEXT="Character" FOLDED="true" ID="ID_291985506"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_379400286">
                            <node TEXT="FORMOD" FOLDED="true" ID="ID_1952529505"/>
                        </node>
                        <node TEXT="domain" FOLDED="true" ID="ID_4026081">
                            <node TEXT="FORMOD" FOLDED="true" ID="ID_49346250"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_437316584">
                            <node TEXT="The productive forest modifier attribute represents a further classification (sub-division) of productive forest areas based on the presence or absence of physical or biological factors which may influence the ability to practice forest management." FOLDED="true" ID="ID_139676669"/>
                        </node>
                        <node TEXT="Description" FOLDED="true" ID="ID_1974195886">
                            <node TEXT="The productive forest modifier must be identified for every productive forest stand and must be used in conjunction with the management consideration attribute." FOLDED="true" ID="ID_1712222612"/>
                            <node TEXT="There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement.  Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a forest management decision (FORMOD) that is based on more than just site class." FOLDED="true" ID="ID_240434489"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1701866914">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1726618426"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_1669490681"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_459023843">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_980068677">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1794457453">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: FORMOD MUST be NULL when POLYTYPE is NOT equal to FOR" FOLDED="true" ID="ID_584240608">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_562825933">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_20940368">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_708995617">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1539810450">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if (row['${name}'] not in [None, '', ' ']) and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1421824876">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_154630050"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_1158574882">
                            <node TEXT="RP:" FOLDED="true" ID="ID_1523421561">
                                <node TEXT="name" FOLDED="true" ID="ID_541972452">
                                    <node TEXT="RP" FOLDED="true" ID="ID_1168424730"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_982829728">
                                    <node TEXT="Productive forest areas at various stages of growth and development, including areas that have been recently disturbed (by harvest or natural causes) or renewed (by artificial or natural means), that are capable of producing adequate growth of timber to support harvesting on a sustained yield basis.  These areas have no significant physical or biological limitations on the ability to practice forest management, but may include areas which pose an operational challenge in terms of harvest, access, protection, silviculture, or renewal." FOLDED="true" ID="ID_93958516"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_762520095">
                                    <node TEXT="Production Forest - Regular" FOLDED="true" ID="ID_638831171"/>
                                </node>
                            </node>
                            <node TEXT="MR:" FOLDED="true" ID="ID_457088485">
                                <node TEXT="name" FOLDED="true" ID="ID_1934549091">
                                    <node TEXT="MR" FOLDED="true" ID="ID_1771174367"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1364768288">
                                    <node TEXT="Productive forest areas which are considered to be production forest, but that are unavailable for timber production as determined through the forest management planning process.  That is, these areas have been identified as no-cut areas through area of concern planning for the purpose of protecting values (e.g., reserve buffers applied to protect tourism values)." FOLDED="true" ID="ID_1590747649"/>
                                    <node TEXT="Designated management reserves are areas that were managed as an operational reserve during previous forest management plan terms. That is, they are actual operational or management reserve areas created by the implementation of forest management operations around them. As guides are updated, these areas should be reviewed as some may return to production forest. " FOLDED="true" ID="ID_874093559"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_735329519">
                                    <node TEXT="Production Forest - Designated Management Reserve" FOLDED="true" ID="ID_860519596"/>
                                </node>
                            </node>
                            <node TEXT="PF:" FOLDED="true" ID="ID_1337951203">
                                <node TEXT="name" FOLDED="true" ID="ID_1837161733">
                                    <node TEXT="PF" FOLDED="true" ID="ID_1266162405"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1814044522">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true" ID="ID_840251800"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_754841846">
                                    <node TEXT="Protection Forest" FOLDED="true" ID="ID_1798939763"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="DEVSTAGE:" FOLDED="true" ID="ID_1947389092">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_227330928">
                            <node TEXT="8" FOLDED="true" ID="ID_1644821698"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_785749697">
                            <node TEXT="Character" FOLDED="true" ID="ID_976824712"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1514612745">
                            <node TEXT="DEVSTAGE" FOLDED="true" ID="ID_1066845472"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1442697570">
                            <node TEXT="Stage of Development" FOLDED="true" ID="ID_29060047"/>
                        </node>
                        <node TEXT="domain" FOLDED="true" ID="ID_1974658888">
                            <node TEXT="DEVSTAGE" FOLDED="true" ID="ID_445073993"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_447334762">
                            <node TEXT="The stage of development attribute indicates the current state of growth and development for a productive forest stand.  Note that some stands stages are best described based on the last major silvicultural treatment that was applied to a stand, if the stand is being managed for timber production." FOLDED="true" ID="ID_1332151150"/>
                        </node>
                        <node TEXT="Description" FOLDED="true" ID="ID_1188173844">
                            <node TEXT="The stage of development attribute is not a derived attribute and is commonly modified from the first submission in the FRI. The population of this field requires a management decision based on both the current state and the past treatments. As an example, a forest stand was planted and the plantation failed (e.g. ingress, insect damage) yet it eventually met the regeneration standards and was declared established. The forest manager will now need to decide whether this stand receives a code of ESTPLANT or ESTNAT depending on the presence and potential influence of the planted stems that may still remain. The development stage assignment will be independent of the yield assignment despite the fact that there may be a connection between the two. The stage of development will be used to populate the stage of management in the landbase tables in the FMP  (i.e. FMP-3 and FMP-5). " FOLDED="true" ID="ID_557466393"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_814078777">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1358844194"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_1946132342"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_122120934">
                            <node TEXT="english" FOLDED="true" ID="ID_1160660167">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_596890998">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then DEVSTAGE must be null" FOLDED="true" ID="ID_1598806041">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true" ID="ID_716248588">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_463292329">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_499379083">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the STKG attribute equals 0.00, then the DEVSTAGE attribute should be either DEP* or NEW* when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1723910025">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST2: If crown closure is less than 25 percent (UCCLO + OCCLO &lt; 25) then the DEVSTAGE attribute should be LOWMGMT, LOWNAT, DEPHARV or DEPNAT" FOLDED="true" ID="ID_418533565">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1740076063">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if (row['${name}'] is not None and str(row['${name}']).strip() != '') and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1062249244">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be DEP* or NEW* when POLYTYPE is FOR and STKG is 0.00 or NULL' for row in [row] if (row['${name}'] not in [None, '', ' ']) and row['${name}'][:2] not in [ 'DEP','NEW'] and row['POLYTYPE'] == 'FOR'  and (row['OSTKG'] == 0.0 or row['OSTKG'] in [None, '', ' ']) ]" FOLDED="true" ID="ID_382166321">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST2: If crown closure is less than 25 percent then &lt;b&gt;&lt;mark&gt;DEVSTAGE&lt;/mark&gt;&lt;/b&gt; should be LOWMGMT, LOWNAT, DEPHARV or DEPNAT when POLYTYPE = FOR' for row in [row] if (row['OCCLO'] is not None and str(row['OCCLO']).strip() !='') and (row['UCCLO'] is not None and str(row['UCCLO']).strip() !='') and float(row['UCCLO'])+float(row['OCCLO'])  &lt;25 and (row['${name}'] is not None and str(row['${name}']).strip() != '') and row['${name}'] not in ['LOWMGMT' , 'LOWNAT', 'DEPHARV', 'DEPNAT'] and row['POLYTYPE' ] == 'FOR']" FOLDED="true" ID="ID_1662006366">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_74687730"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_637157137">
                            <node TEXT="LOWNAT:" FOLDED="true" ID="ID_7983662">
                                <node TEXT="name" FOLDED="true" ID="ID_1207954403">
                                    <node TEXT="LOWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1271114124">
                                    <node TEXT="Productive forest stands which were previously disturbed by natural causes and have not reached the regeneration standards as described in an approved FMP.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., crown closure less than 25 percent)." FOLDED="true" ID="ID_1091633097"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1204831081">
                                    <node TEXT="Below regeneration standards due to natural causes / succession" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DEPHARV:" FOLDED="true" ID="ID_748178926">
                                <node TEXT="name" FOLDED="true" ID="ID_566520651">
                                    <node TEXT="DEPHARV" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1898451953">
                                    <node TEXT="Productive forest area that was recently disturbed by clearcut harvesting and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer that would be released and/or protected as part of the depletion operation." FOLDED="true" ID="ID_1082909127"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_663029293">
                                    <node TEXT="recently disturbed by harvest and has received no regeneration/renewal treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DEPNAT:" FOLDED="true" ID="ID_8347679">
                                <node TEXT="name" FOLDED="true" ID="ID_416882574">
                                    <node TEXT="DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_893047279">
                                    <node TEXT="Productive forest area that was recently disturbed by natural causes (i.e., fire, blowdown, ice damage, insect and disease) and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer." FOLDED="true" ID="ID_1597849425"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_66535541">
                                    <node TEXT="recently disturbed by natural causes and has received no regeneration / renewal treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWPLANT:" FOLDED="true" ID="ID_551517759">
                                <node TEXT="name" FOLDED="true" ID="ID_1929334267">
                                    <node TEXT="NEWPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1529719771">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by planting, but have not been assessed as established" FOLDED="true" ID="ID_943475565"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1819354014">
                                    <node TEXT="recently renewed : mainly planted" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWSEED:" FOLDED="true" ID="ID_1263853226">
                                <node TEXT="name" FOLDED="true" ID="ID_1959358188">
                                    <node TEXT="NEWSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1768745885">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by seeding, but have not been assessed as established" FOLDED="true" ID="ID_802127310"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1547216905">
                                    <node TEXT="recently renewed : mainly seeded" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWNAT:" FOLDED="true" ID="ID_1349765476">
                                <node TEXT="name" FOLDED="true" ID="ID_1958545053">
                                    <node TEXT="NEWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1409213269">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by natural means, but have not been assessed as established (e.g. CLAAG, HARP)" FOLDED="true" ID="ID_1443282825"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_698658128">
                                    <node TEXT="recently renewed : mainly natural regeneration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FTGSEED:" FOLDED="true" ID="ID_523548341">
                                <node TEXT="name" FOLDED="true" ID="ID_793981793">
                                    <node TEXT="FTGSEED" FOLDED="true" ID="ID_951986239"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1012073127">
                                    <node TEXT="free growing: mainly seeded regeneration" FOLDED="true" ID="ID_63432173"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1850242740">
                                    <node TEXT="Productive forest areas which were  regenerated predominantly by  seeding and where the seeded tree  species comprise &gt;= 50 percent of the  species composition and have been  assessed as free-to-grow or are  greater than 20 years of age (age of  the trees within the polygon)." FOLDED="true" ID="ID_1382064547"/>
                                </node>
                            </node>
                            <node TEXT="FTGNAT:" FOLDED="true" ID="ID_218617943">
                                <node TEXT="name" FOLDED="true" ID="ID_276127753">
                                    <node TEXT="FTGNAT" FOLDED="true" ID="ID_21908425"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_525063332">
                                    <node TEXT="free growing: mainly natural regeneration" FOLDED="true" ID="ID_869942344"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_39006554">
                                    <node TEXT="Productive forest areas which were regenerated predominantly by natural means and the trees naturally regenerating comprise &gt;= 50 percent of the polygon species composition which have been assessed as free-to-grow or are greater than 20 years of age (age of the trees within the polygon). " FOLDED="true" ID="ID_338594519"/>
                                </node>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true" ID="ID_304463037">
                                <node TEXT="name" FOLDED="true" ID="ID_376126099">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_517412502">
                                    <node TEXT="Established productive forest areas which have received a mid-rotation thinning/spacing to promote the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume." FOLDED="true" ID="ID_384017129"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1024238852">
                                    <node TEXT="received pre-commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINCOM:" FOLDED="true" ID="ID_968197541">
                                <node TEXT="name" FOLDED="true" ID="ID_381992342">
                                    <node TEXT="THINCOM" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_34558485">
                                    <node TEXT="Established productive forest areas which have received a mid-rotation partial harvest designed to promote the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes." FOLDED="true" ID="ID_867745063"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1442891256">
                                    <node TEXT="received commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STRIPCUT:" FOLDED="true" ID="ID_386067967">
                                <node TEXT="name" FOLDED="true" ID="ID_1519834976">
                                    <node TEXT="STRIPCUT" FOLDED="true" ID="ID_821196705"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_871634097">
                                    <node TEXT="The removal of the stand in progressive strips or blocks in more than one operation. Strip and block harvest methods are prescribed to encourage natural regeneration, provide wildlife habitat, protect fragile sites, or for aesthetics." FOLDED="true" ID="ID_1865953046"/>
                                    <node TEXT="The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters wide.  It is designed to encourage regeneration on difficult and/or fragile sites." FOLDED="true" ID="ID_152467708"/>
                                    <node TEXT="Note:  Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut." FOLDED="true" ID="ID_691715813"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1111832204">
                                    <node TEXT="modified cut: block or strip" FOLDED="true" ID="ID_1163414376"/>
                                </node>
                            </node>
                            <node TEXT="BLKSTRIP:" FOLDED="true" ID="ID_55952623">
                                <node TEXT="name" FOLDED="true" ID="ID_336283239">
                                    <node TEXT="BLKSTRIP" FOLDED="true" ID="ID_901652377"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_138960860">
                                    <node TEXT="The removal of the stand in progressive strips or blocks in more than one operation. Strip and block harvest methods are prescribed to encourage natural regeneration, provide wildlife habitat, protect fragile sites, or for aesthetics." FOLDED="true" ID="ID_1021628038"/>
                                    <node TEXT="The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters wide.  It is designed to encourage regeneration on difficult and/or fragile sites." FOLDED="true" ID="ID_480277425"/>
                                    <node TEXT="Note:  Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut." FOLDED="true" ID="ID_616147753"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_344769049">
                                    <node TEXT="modified cut: block or strip" FOLDED="true" ID="ID_545734530"/>
                                </node>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true" ID="ID_1083105529">
                                <node TEXT="name" FOLDED="true" ID="ID_33186624">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1961317496">
                                    <node TEXT="A partial harvest where the first harvest operation removes target/specific merchantable tree species from a forest stand. The remaining species are merchantable and are intended to be harvested by another logger/contractor/forest resource licence holder in the next pass.  A first pass should be recorded if merchantable tree species remain in the forest stand which have been allocated for harvest - but have not yet been harvested." FOLDED="true" ID="ID_416655294"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_50655126">
                                    <node TEXT="modified cut: first pass" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PREPCUT:" FOLDED="true" ID="ID_1061249347">
                                <node TEXT="name" FOLDED="true" ID="ID_1556896509">
                                    <node TEXT="PREPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_773996878">
                                    <node TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source and to improve conditions for seed production and natural regeneration." FOLDED="true" ID="ID_1296724770"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1880708783">
                                    <node TEXT="received a preparatory cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDCUT:" FOLDED="true" ID="ID_462370654">
                                <node TEXT="name" FOLDED="true" ID="ID_1303568696">
                                    <node TEXT="SEEDCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_327851119">
                                    <node TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration." FOLDED="true" ID="ID_1740193991"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1701496343">
                                    <node TEXT="received a seed cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRSTCUT:" FOLDED="true" ID="ID_502772761">
                                <node TEXT="name" FOLDED="true" ID="ID_439935242">
                                    <node TEXT="FIRSTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1862728921">
                                    <node TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition." FOLDED="true" ID="ID_1644169405"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1065484189">
                                    <node TEXT="received a first removal harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LASTCUT:" FOLDED="true" ID="ID_6771150">
                                <node TEXT="name" FOLDED="true" ID="ID_284274364">
                                    <node TEXT="LASTCUT" FOLDED="true" ID="ID_878983410"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1281944460">
                                    <node TEXT="A shelterwood silvicultural system stage of management where all of the remaining trees in the overstorey are removed. This is the removal of the seed or shelter trees after the regeneration has been effective." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1379677569">
                                    <node TEXT="received a final removal harvest" FOLDED="true" ID="ID_1215905204"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true" ID="ID_1001938079">
                                <node TEXT="name" FOLDED="true" ID="ID_1999701866">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1280689739">
                                    <node TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition, distribution and quality by removing less desirable trees of any species." FOLDED="true" ID="ID_1160492974"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1726102258">
                                    <node TEXT="received an improvement cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SELECT:" FOLDED="true" ID="ID_1513026361">
                                <node TEXT="name" FOLDED="true" ID="ID_1864953698">
                                    <node TEXT="SELECT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1387892755">
                                    <node TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment." FOLDED="true" ID="ID_1955386237"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1310518657">
                                    <node TEXT="received a selection harvest" FOLDED="true" ID="ID_34041778"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="YRDEP:" FOLDED="true" ID="ID_1076694506">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1853978844">
                            <node TEXT="4" FOLDED="true" ID="ID_1287525156"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1155320697">
                            <node TEXT="Integer" FOLDED="true" ID="ID_1340364381"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1869671521">
                            <node TEXT="YRDEP" FOLDED="true" ID="ID_329066538"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_22243736">
                            <node TEXT="Year of Last Disturbance" FOLDED="true" ID="ID_1310107983"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_772720669">
                            <node TEXT="The year of last disturbance attribute indicates a four digit number of the most recent (or latest) fiscal year (April 1 to March 31) that a productive forest area was disturbed, completely or partially, by harvest or by natural causes. This includes mid-rotation or stand improvement operations where merchantable timber is removed. This is actual and known disturbances and not calculated from year of origin." FOLDED="true" ID="ID_1920684451"/>
                        </node>
                        <node TEXT="Description" FOLDED="true" ID="ID_1230472689">
                            <node TEXT="The inventories will be populated with all disturbances (harvest and natural) which are identified in annual reports. The year of last disturbance for each productive forest stand should correspond to the year that the disturbance occurred, as recorded in the applicable annual reports.  Forest stands that are managed under the selection or shelterwood silvicultural systems must also have a year of last disturbance. The year of last disturbance provides an estimate of the stage of forest stand development within a cutting cycle for selection stands or within the current stage of a shelterwood system.  In the case of a partial harvest where certain tree species have been removed, such as a first pass where merchantable trees remain in a forest stand, the year that the first pass was carried out must be entered as the year of last disturbance. If an additional harvest or pass is conducted, the year of last disturbance is changed to the year in which the most recent harvest/pass operation was carried out.  Commercial thinning is a mid-rotation treatment that is recorded as a disturbance and the stand area is considered as disturbed area in forest management planning. Forest stands that have received a commercial thinning must show the applicable year of last disturbance in the forest polygon coverage.  The year of last disturbance is also used to identify other stand improvement operations. A stand improvement operation is normally associated with the selection system.   Year of last disturbance must not be used to indicate tending operations, such as a chemical or manual release which are required to bring a forest stand to regeneration standards. " FOLDED="true" ID="ID_240696342"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1439495026">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_332996021"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_834350619"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1497646825">
                            <node TEXT="english" FOLDED="true" ID="ID_533759875">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1173898872">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then YRDEP must equal zero" FOLDED="true" ID="ID_110094079">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1054712507">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The YRDEP must be a year less than the plan period start year" FOLDED="true" ID="ID_931024658">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: The value must be greater than or equal to 1900 when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1533964478">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_68152609">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 when POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '' ,' ', 0] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_642833185">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than ${planyear} ' for row in [row] if row['${name}'] not in [None, '', ' '] and row['${name}']  &gt;=${planyear} and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_1874087455">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 1900 unless it is 0' for row in [row] if row['${name}'] not in [None, '', ' ', 0] and row['${name}']  &lt;1900  and row['POLYTYPE'] == 'FOR']" FOLDED="true" ID="ID_1280785508">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1158013541"/>
                        </node>
                    </node>
                    <node TEXT="DEPTYPE:" FOLDED="true" ID="ID_457561108">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_422185393">
                            <node TEXT="8" FOLDED="true" ID="ID_1236382703"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1070626928">
                            <node TEXT="Character" FOLDED="true" ID="ID_943624660"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1994533313">
                            <node TEXT="DEPTYPE" FOLDED="true" ID="ID_1821224882"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_346456949">
                            <node TEXT="Type of Disturbance" FOLDED="true" ID="ID_700310526"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_409265541">
                            <node TEXT="The type of disturbance attribute identifies the disturbance that occurred in the year recorded in the companion attribute YRDEP (year of disturbance).  The disturbance may have affected the entire stand or only a portion of it." FOLDED="true" ID="ID_1787424905"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_820887318">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1146763617"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1005809168"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1946154766">
                            <node TEXT="english" FOLDED="true" ID="ID_1572327275">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1051860637">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then DEPTYPE must be null" FOLDED="true" ID="ID_243374824">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1583802255">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is only a valid code when YRDEP == 0" FOLDED="true" ID="ID_1501184764">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: If the development stage is depleted, new or established (DEVSTAGE = DEP, NEW or EST) then the disturbance type should not have a value of unknown (DEPTYPE = UNKNOWN)" FOLDED="true" BACKGROUND_COLOR="#ffffff" ID="ID_1802138789">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: If the disturbance type is not null (DEPTYPE &lt;&gt; null) then the disturbance year should not be equal to zero (YRDEP = 0). Should actually be &gt;= 1900" FOLDED="true" ID="ID_156207696">
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="password"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1496544758">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1498372874">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be UNKNOWN if DEVSTAGE are DEP, NEW or EST' for row in [row] if row['${name}'] == 'UNKNOWN' and row['DEVSTAGE'] in ['DEPHARV', 'DEPNAT', 'NEWPLANT', 'NEWSEED', 'NEWNAT', 'ESTPLANT', 'ESTSEED', 'ESTNAT'] ]" FOLDED="true" ID="ID_1518144265">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be populated when YRDEP is populated'  for row in [ row ]  if  row['YRDEP']  not in [ None, '', ' ', 0]  and   row['${name}'] in [ None, '', ' ', 0 ] and  row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_1556943693">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_780195690"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_115638407">
                            <node TEXT="BLOWDOWN:" FOLDED="true" ID="ID_922883981">
                                <node TEXT="name" FOLDED="true" ID="ID_374378809">
                                    <node TEXT="BLOWDOWN" FOLDED="true" ID="ID_693388261"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1896136140">
                                    <node TEXT="" FOLDED="true" ID="ID_1024062595"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_757404160">
                                    <node TEXT="wind / blowdown" FOLDED="true" ID="ID_1724006532"/>
                                </node>
                            </node>
                            <node TEXT="DISEASE:" FOLDED="true" ID="ID_347422394">
                                <node TEXT="name" FOLDED="true" ID="ID_1737909525">
                                    <node TEXT="DISEASE" FOLDED="true" ID="ID_841983480"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1377909982">
                                    <node TEXT="" FOLDED="true" ID="ID_977411997"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1728127150">
                                    <node TEXT="disease" FOLDED="true" ID="ID_334698189"/>
                                </node>
                            </node>
                            <node TEXT="DROUGHT:" FOLDED="true" ID="ID_828116185">
                                <node TEXT="name" FOLDED="true" ID="ID_761729720">
                                    <node TEXT="DROUGHT" FOLDED="true" ID="ID_1541573111"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_422688875">
                                    <node TEXT="" FOLDED="true" ID="ID_1956456264"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1851748755">
                                    <node TEXT="drought" FOLDED="true" ID="ID_1487097905"/>
                                </node>
                            </node>
                            <node TEXT="FIRE:" FOLDED="true" ID="ID_1965779648">
                                <node TEXT="name" FOLDED="true" ID="ID_1153266994">
                                    <node TEXT="FIRE" FOLDED="true" ID="ID_1311076067"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1390683620">
                                    <node TEXT="" FOLDED="true" ID="ID_405939065"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_363686433">
                                    <node TEXT="fire" FOLDED="true" ID="ID_1127069888"/>
                                </node>
                            </node>
                            <node TEXT="FLOOD:" FOLDED="true" ID="ID_250360404">
                                <node TEXT="name" FOLDED="true" ID="ID_651296493">
                                    <node TEXT="FLOOD" FOLDED="true" ID="ID_802640503"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1584496505">
                                    <node TEXT="" FOLDED="true" ID="ID_1864143501"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1834989168">
                                    <node TEXT="flood" FOLDED="true" ID="ID_1103140946"/>
                                </node>
                            </node>
                            <node TEXT="HARVEST:" FOLDED="true" ID="ID_1932273948">
                                <node TEXT="name" FOLDED="true" ID="ID_1144319500">
                                    <node TEXT="HARVEST" FOLDED="true" ID="ID_385286481"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_736990643">
                                    <node TEXT="Partial or full stand removal of timber.  This includes mid-rotation or stand improvement operations where merchantable timber is removed." FOLDED="true" ID="ID_1043191301"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_943051027">
                                    <node TEXT="harvest" FOLDED="true" ID="ID_1121599313"/>
                                </node>
                            </node>
                            <node TEXT="ICE:" FOLDED="true" ID="ID_152754696">
                                <node TEXT="name" FOLDED="true" ID="ID_45464784">
                                    <node TEXT="ICE" FOLDED="true" ID="ID_954217818"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_422425095">
                                    <node TEXT="" FOLDED="true" ID="ID_1021300607"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1962746612">
                                    <node TEXT="ice damage" FOLDED="true" ID="ID_521961494"/>
                                </node>
                            </node>
                            <node TEXT="INSECTS:" FOLDED="true" ID="ID_13067202">
                                <node TEXT="name" FOLDED="true" ID="ID_153237870">
                                    <node TEXT="INSECTS" FOLDED="true" ID="ID_1054123647"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_158686880">
                                    <node TEXT="" FOLDED="true" ID="ID_781152190"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1159083914">
                                    <node TEXT="insects" FOLDED="true" ID="ID_587503780"/>
                                </node>
                            </node>
                            <node TEXT="SNOW:" FOLDED="true" ID="ID_1373123683">
                                <node TEXT="name" FOLDED="true" ID="ID_1025417388">
                                    <node TEXT="INSECTS" FOLDED="true" ID="ID_719526739"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_257500597">
                                    <node TEXT="" FOLDED="true" ID="ID_524469164"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1882578268">
                                    <node TEXT="insects" FOLDED="true" ID="ID_674462065"/>
                                </node>
                            </node>
                            <node TEXT="UNKOWN:" FOLDED="true" ID="ID_466019853">
                                <node TEXT="name" FOLDED="true" ID="ID_374080850">
                                    <node TEXT="UNKNOWN" FOLDED="true" ID="ID_654574414"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_476290152"/>
                                <node TEXT="option" FOLDED="true" ID="ID_1000684859">
                                    <node TEXT="Unknown" FOLDED="true" ID="ID_658783309"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="OYRORG:" FOLDED="true" ID="ID_1884136076">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true" ID="ID_142126633">
                            <node TEXT="Overstorey Year of Origin" FOLDED="true" ID="ID_1603937909"/>
                        </node>
                        <node TEXT="length" FOLDED="true" ID="ID_1539311256">
                            <node TEXT="4" FOLDED="true" ID="ID_336386592"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1496970756">
                            <node TEXT="Integer" FOLDED="true" ID="ID_1080360625"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_291277821">
                            <node TEXT="OYRORG" FOLDED="true" ID="ID_195169642"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1467988878">
                            <node TEXT="The overstorey  year of origin attribute contains a four digit number representing the average year that the predominant species (i.e., the species within the stand having the greatest relative abundance in terms of basal area), came into existence." FOLDED="true" ID="ID_818977274"/>
                        </node>
                        <node TEXT="Description" FOLDED="true" ID="ID_1771814238">
                            <node TEXT="The overstorey year of origin can be used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration.   Once a forest stand has been assessed as established, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site.   In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered.  For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach the regeneration standards from the forest management plan), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip is established, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required.  Normally, updates to the overstorey year of origin have a corresponding change to the year of data update attribute as this indicates the currency/vintage of the information and when the overstorey year of origin value was determined.   Overstorey year of origin information is not required for non-forested and non-productive forest land types. " FOLDED="true" ID="ID_873222209"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1866593662">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1987917261"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_975046321"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_82612646">
                            <node TEXT="english" FOLDED="true" ID="ID_758891313">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1326473938">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then OYRORG must equal zero" FOLDED="true" ID="ID_903552019">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE equals FOR" FOLDED="true" ID="ID_1511066622">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE equals FOR" FOLDED="true" ID="ID_257692229">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code when POLYTYPE equals FOR" FOLDED="true" ID="ID_844004580">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The OYRORG attribute value must be greater than or equal to 1600 when POLYTYPE equals FOR" FOLDED="true" ID="ID_202948969">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The OYRORG attribute must be less than the plan period start year when POLYTYPE equals FOR" FOLDED="true" ID="ID_1183570160">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="The OYRORG attribute value should not be greater than YRSOURCE attribute value" FOLDED="true" ID="ID_1184241044">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1538270120">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must equal zero when POLYTYPE is not FOR' for row in [row] if row['${name}'] != 0 and row['POLYTYPE'] != 'FOR']" FOLDED="true" ID="ID_1567228621">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 1600 if POLYTYPE equals FOR' for row in [row] if row['${name}']  != 0 and row['${name}'] not in [None, '', ' '] and row['${name}']  &lt;1600 and row['POLYTYPE'] == 'FOR']" FOLDED="true" ID="ID_528543765">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than Plan Period Start Year if POLYTYPE equals FOR' for row in [row] if row['${name}']  != 0 and row['${name}'] not in [None, '', ' '] and row['${name}']  &gt;=${planyear}  and row['POLYTYPE'] == 'FOR']" FOLDED="true" ID="ID_427826164">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &lt;= YRSOURCE if POLYTYPE equals FOR' for row in [row] if row['${name}']  != 0 and row['${name}'] is not None and str(row['${name}']).strip() != '' and row['${name}']  &gt; row['YRSOURCE']  and row['POLYTYPE'] == 'FOR']" FOLDED="true" ID="ID_527467820">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1583736628"/>
                        </node>
                    </node>
                    <node TEXT="OSPCOMP:" FOLDED="true" ID="ID_624876041">
                        <icon BUILTIN="button_ok"/>
                        <icon BUILTIN="help"/>
                        <node TEXT="alias" FOLDED="true" ID="ID_527344961">
                            <node TEXT="Overstorey Species Composition" FOLDED="true" ID="ID_970480083"/>
                        </node>
                        <node TEXT="length" FOLDED="true" ID="ID_1938951308">
                            <node TEXT="120" FOLDED="true" ID="ID_1376076618"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1655731008">
                            <node TEXT="Character" FOLDED="true" ID="ID_502094862"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_146430668">
                            <node TEXT="OSPCOMP" FOLDED="true" ID="ID_124260424"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1559131054">
                            <node TEXT="This species composition attribute indicates the tree species that are present in the stand canopy and the relative proportion of the canopy that each species occupies." FOLDED="true" ID="ID_1237468378"/>
                        </node>
                        <node TEXT="Description" FOLDED="true" ID="ID_8129804">
                            <node TEXT="The most common species codes (based on all the inventories as of 2009) are listed below and for the full list of species see the coding list from OSPCOMP in the FIM Forest Resource Inventory Technical Specifications.  In these tables, codes related to individual species are listed in mixed case (e.g., Bw, La) and codes related to groups such as all conifer or all spruce are listed in uppercase (e.g., OC, SX).  Even though the codes are listed this way, the letters may be entered in any case combination the data submitter desires.  For example, white birch may be entered as BW, bw, Bw, or bW.   repeating pattern of species code and corresponding proportion value each species code is 3 characters (including blanks) and is left justified each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified.   maximum of 20 species and proportions pairs in the string " FOLDED="true" ID="ID_665746126"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1262506719">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1042374745"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_999870950"/>
                            <node TEXT="pattern is SSSPPPSSSPPP" FOLDED="true" ID="ID_225843470"/>
                            <node TEXT="no duplicate species codes allowed in the string" FOLDED="true" ID="ID_1549974394"/>
                            <node TEXT="A blank or null value is not a valid code" FOLDED="true" ID="ID_1111186488"/>
                            <node TEXT="proportion values in the string must sum to 100" FOLDED="true" ID="ID_663914271"/>
                            <node TEXT="The tree species in the composition are to be coded using the scheme listed here." FOLDED="true" ID="ID_1095741962"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_147949667">
                            <node TEXT="english" FOLDED="true" ID="ID_705482218">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_315806039">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then OSPCOMP must be null" FOLDED="true" ID="ID_1161770479">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_242145895">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1672252114">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: No duplicate species codes allowed in the string" FOLDED="true" ID="ID_649843134">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Proportion values in the string must sum to 100" FOLDED="true" ID="ID_579980294">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The tree species in the composition are to be coded using the scheme listed here" FOLDED="true" ID="ID_1254375728">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="If SOURCE is NOT plot based then allow specific species in SPCOMP (eFRI tech spec)" FOLDED="true" ID="ID_1254363904">
                                    <icon BUILTIN="yes"/>
                                </node>
                                <node TEXT="Tree species in the list [     'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn',     'CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf',     'OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX',     'OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX',     'Sb','Sw','la','Wb','Wi'     ]" FOLDED="true" ID="ID_101688217">
                                    <icon BUILTIN="yes"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_786112884">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1783713180">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; formatting error' for i in test_spcomp(row['${name}'] )   for r in [row]    if r['POLYTYPE'] == 'FOR'      and   row['${name}'] not in [  None, '', ' ', 0 ]  ] " FOLDED="true" ID="ID_68307526">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;OSPCOMP&lt;/mark&gt;&lt;/b&gt; - %s is not a valid species code'%(s) for s in test_SpeciesInList(row['${name}'], [ 'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi' ]        ) if row['POLYTYPE'] == 'FOR'  and row['SOURCE'][:4] != 'PLOT'  and (row['${name}'] is not None and str(row['${name}']).strip() == '') ]" FOLDED="true" ID="ID_161240531">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;OSPCOMP&lt;/mark&gt;&lt;/b&gt; - %s is not a valid species code'%(s) for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if row['POLYTYPE'] == 'FOR' and row['SOURCE'][:4] == 'PLOT'  and (row['${name}'] is not None and str(row['${name}']).strip() == '') ]" FOLDED="true" ID="ID_1260104118">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1001095245"/>
                        </node>
                    </node>
                    <node TEXT="OLEADSPC:" FOLDED="true" ID="ID_1669242126">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_910527351">
                            <node TEXT="3" FOLDED="true" ID="ID_586964094"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_121205802">
                            <node TEXT="Character" FOLDED="true" ID="ID_423538450"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_564575776">
                            <node TEXT="OLEADSPC" FOLDED="true" ID="ID_663355839"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_403289858">
                            <node TEXT="Overstorey Leading Species" FOLDED="true" ID="ID_1780603081"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1538542638">
                            <node TEXT="The leading species  attribute indicates the most prevalent species in the forest stand (or in just the uppermost canopy layer if the stand canopy contains two or more distinct layers) based on its percentage of crown closure." FOLDED="true" ID="ID_1639944837"/>
                        </node>
                        <node TEXT="Description" FOLDED="true" ID="ID_1889750622">
                            <node TEXT="This leading species attribute replaces the working group attribute (WG) that was present in previous inventory products for determining the major species to manage for in a forest stand." FOLDED="true" ID="ID_1661407834"/>
                            <node TEXT="use the same coding as is listed in the OSPCOMP (species composition) attribute description" FOLDED="true" ID="ID_378503237"/>
                            <node TEXT="must be species listed in the overstorey species composition (OSPCOMP)" FOLDED="true" ID="ID_1886087415"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1595532657">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_958365776"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_1375402488"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true" ID="ID_257159029"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1219227063">
                            <node TEXT="english" FOLDED="true" ID="ID_1641714320">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1093129026">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then OLEADSPC must be null" FOLDED="true" ID="ID_884832138">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_146640262">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1129758184">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be a species listed in the overstorey species composition (OSPCOMP) when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_995706233">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be the species with the greatest percentage or tied for the greatest percentage in the species composition (OSPCOMP) when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1877314853">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_18933101">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1136881300">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must match the left two chars of OSPCOMP if POLYTYPE equals FOR' for row in [row] if (row['${name}'] not in [None, '', ' ']) and (row['${name}'][:2] != row['OSPCOMP'][:2]) and row['POLYTYPE'] == 'FOR'  ]" FOLDED="true" ID="ID_1448833458">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1316735939"/>
                        </node>
                    </node>
                    <node TEXT="OAGE:" FOLDED="true" ID="ID_1122737876">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1801081129">
                            <node TEXT="3" FOLDED="true" ID="ID_1617000087"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1782097030">
                            <node TEXT="Integer" FOLDED="true" ID="ID_1055131587"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1610628506">
                            <node TEXT="OAGE" FOLDED="true" ID="ID_504150498"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1301579784">
                            <node TEXT="Overstorey Age" FOLDED="true" ID="ID_98877977"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_652433156">
                            <node TEXT="The overstorey age  attribute contains the average age of the leading species in the overstorey canopy layer of the forest stand." FOLDED="true" ID="ID_201663998"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_844877771">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1083703324"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_1920575812"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true" ID="ID_708965802"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1654420556">
                            <node TEXT="english" FOLDED="true" ID="ID_589684373">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1745929304">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then OAGE must equal zero" FOLDED="true" ID="ID_1232383382">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1761075964">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1979208338">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A zero value is only a valid code if DEVSTAGE is DEPHARV or DEPNAT when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1424169038">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1409518210">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1385716981">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;OAGE&lt;/mark&gt;&lt;/b&gt; must be populated and must follow the correct format if POLYTYPE equals FOR' for row in [row] if not isinstance(row['OAGE'],int) and row['POLYTYPE'] == 'FOR' ] " FOLDED="true" ID="ID_371686653">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: OAGE can be zero only when DEVSTAGE is DEPHARV or DEPNAT (when POLYTYPE = FOR).' for row in [row] if row['OAGE']  == 0 and row['POLYTYPE'] == 'FOR'  and row['DEVSTAGE'] not in ['DEPHARV','DEPNAT'] ]   " FOLDED="true" ID="ID_465958103">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1866488041"/>
                        </node>
                    </node>
                    <node TEXT="OHT:" FOLDED="true" ID="ID_1303005093">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_289384209">
                            <node TEXT="4.1" FOLDED="true" ID="ID_166562253"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1767670812">
                            <node TEXT="Double" FOLDED="true" ID="ID_284065921"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1269011646">
                            <node TEXT="OHT" FOLDED="true" ID="ID_1974631314"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_653362418">
                            <node TEXT="Overstorey Height" FOLDED="true" ID="ID_1082321360"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_552526402">
                            <node TEXT="The overstorey height attribute indicates the estimated average tree height (in meters) of the predominant overstorey species as inventoried in the year of data update.  Estimates can be made from interpreted crown closure or field samples, or from growth algorithms." FOLDED="true" ID="ID_1995371855"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_177285358">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_637909034"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true" ID="ID_610187439"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_605902250"/>
                            <node TEXT="If the DEVSTAGE attribute does not start with DEP, NEW, or LOW, then the HT attribute must be greater than zero" FOLDED="true" ID="ID_1011786103"/>
                            <node TEXT="Valid numeric values are from 0 through 40.0" FOLDED="true" ID="ID_124299899"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1687737871">
                            <node TEXT="english" FOLDED="true" ID="ID_34322180">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_401807367">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then OHT must equal zero" FOLDED="true" ID="ID_1830101876">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1209014363">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1228282703">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1682337560">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the DEVSTAGE attribute does not start with DEP, NEW, or LOW, then the OHT attribute must be greater than zero" FOLDED="true" ID="ID_1772917760">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1912679580">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_673477534">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_932494760">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be greater than 0 if DEVSTAGE is not DEPHARV,DEPNAT, NEWNAT, NEWPLANT, NEWSEED, LOWNAT or LOWMGMT when POLYTYPE is FOR' for row in [row] if row['DEVSTAGE']  not in [ 'DEPHARV','DEPNAT','NEWNAT','NEWPLANT','NEWSEED','LOWNAT',' LOWMGMT' ] and row['POLYTYPE'] == 'FOR' and row['${name}'] &gt; 0 ]" FOLDED="true" ID="ID_1576657396">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_972732646"/>
                        </node>
                    </node>
                    <node TEXT="OCCLO:" FOLDED="true" ID="ID_895036747">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_143574490">
                            <node TEXT="3" FOLDED="true" ID="ID_293541013"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1999777241">
                            <node TEXT="Integer" FOLDED="true" ID="ID_248110691"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1035608567">
                            <node TEXT="OCCLO" FOLDED="true" ID="ID_842195697"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_795202831">
                            <node TEXT="Overstorey Crown Closure" FOLDED="true" ID="ID_210514514"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1956247749">
                            <node TEXT="The Overstorey Crown Closure  attribute contains the percent of crown closure of the forest stand (or of just the uppermost canopy layer if the stand canopy contains two or more distinct layers). Crown closure is defined as the percentage of ground area covered by the vertical projection of the tree crowns onto the ground." FOLDED="true" ID="ID_380144325"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_638489341">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1869192746"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_702987564"/>
                            <node TEXT="A zero value is a valid code" FOLDED="true" ID="ID_1861783076"/>
                            <node TEXT="The maximum overstorey crown closure value is 100 percent." FOLDED="true" ID="ID_715594065"/>
                            <node TEXT="If an understorey crown closure value is greater than 0, then the total crown closure for the two layers must never exceed 200%  (i.e., OCCLO + UCCLO &lt;=  200). ** Change wording to greater than zero" FOLDED="true" ID="ID_991800407">
                                <icon BUILTIN="help"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_268810625">
                            <node TEXT="english" FOLDED="true" ID="ID_321641369">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1936594140">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then OCCLO must equal zero" FOLDED="true" ID="ID_313316905">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1954401514">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero value is a valid code (doesn't need a check line)" FOLDED="true" ID="ID_1327315322">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be less than or equal to 100 and greater than or equal to zero (OCCLO &lt;= 100 and OCCLO &gt;=0)" FOLDED="true" ID="ID_154245384">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1794976586">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1239069068">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE = FOR' for row in [row] if (row['${name}'] is None or str(row['${name}']).strip() == '')  and row['POLYTYPE'] == 'FOR'  ]" FOLDED="true" ID="ID_983594923">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 100 when POLYTYPE equals FOR' for row in [row] if row['${name}'] not in [  None, '', ' ', 0 ] and (row['${name}'] &lt; 0 or row['${name}'] &gt; 100) and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_1515810715">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1247831788"/>
                        </node>
                    </node>
                    <node TEXT="OSI:" FOLDED="true" ID="ID_929179713">
                        <node TEXT="length" FOLDED="true" ID="ID_775587954">
                            <node TEXT="4.1" FOLDED="true" ID="ID_549166585"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_793874288">
                            <node TEXT="Double" FOLDED="true" ID="ID_194533310"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_782542418">
                            <node TEXT="OSI" FOLDED="true" ID="ID_320213395"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1504303103">
                            <node TEXT="Overstorey Site Index" FOLDED="true" ID="ID_1855837252"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1239743276">
                            <node TEXT="Overstorey site index is a numerical expression of forest quality (forest site productivity) based on the height in meters, at a specified age (traditionally 50 years), of dominant and co-dominant trees in a stand (or in just the overstorey canopy layer if the stand canopy contains two or more distinct layers). " FOLDED="true" ID="ID_906915847"/>
                            <node TEXT="None" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1585594502">
                            <node TEXT="english" FOLDED="true" ID="ID_1838093714">
                                <node TEXT="" FOLDED="true" ID="ID_191330069"/>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_286473894"/>
                            <node TEXT="sql" FOLDED="true" ID="ID_226317404"/>
                        </node>
                    </node>
                    <node TEXT="OSC:" FOLDED="true" ID="ID_1132249619">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1864093221">
                            <node TEXT="1" FOLDED="true" ID="ID_1173631462"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1167773038">
                            <node TEXT="Integer" FOLDED="true" ID="ID_1365037149"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1560686968">
                            <node TEXT="OSC" FOLDED="true" ID="ID_1355647640"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1168120943">
                            <node TEXT="Overstorey Site Class" FOLDED="true" ID="ID_1926114402"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1905100917">
                            <node TEXT="The overstorey site class attribute indicates a site quality estimate for a stand.  Overstorey site class is an indicator of site productivity and is determined using the average overstorey height, overstorey age, and leading species, based on the dominant tree species in a forest stand. These attributes are compared against height and age growth curves in Plonski's Normal Yield Tables for different species to determine the relative growth rate for a forest stand." FOLDED="true" ID="ID_517628016"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_865822517">
                            <node TEXT="Zero is the default value for an integer field. When performing a query to identify the best (site class = 0) stands be sure to include polygon type in the query (and POLYTYPE = FOR) in order to exclude non-productive forest stands in the query results. There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement.  Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a forest management decision (FORMOD) that is based on more than just site class. " FOLDED="true" ID="ID_835675111"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1206272991">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1244008733"/>
                            <node TEXT="a number from 0 through 4" FOLDED="true" ID="ID_1388019006"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1963323699">
                            <node TEXT="english" FOLDED="true" ID="ID_397301828">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_726212934">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then OSC must equal zero" FOLDED="true" ID="ID_182737676">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format (a number from 0 through 4)  when POLYTYPE equals FOR" FOLDED="true" ID="ID_1005752907">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The overstorey site class value must be greater than or equal to 0 and less than or equal to 4 (OSC &gt;=0 and OSC &lt;=4) when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_980445450">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1368096337">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1265293583">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR' for row in [row] if row['${name}'] not in [ 0,1,2,3,4 ] and row['POLYTYPE'] == 'FOR'  ]" FOLDED="true" ID="ID_1341939157">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_511777655"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_1262436086">
                            <node TEXT="0:" FOLDED="true" ID="ID_1250002317">
                                <node TEXT="name" FOLDED="true" ID="ID_1692966745">
                                    <node TEXT="0" FOLDED="true" ID="ID_1496229663"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1310559671">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1069940465">
                                    <node TEXT="Best" FOLDED="true" ID="ID_1209904556"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true" ID="ID_1204154716">
                                <node TEXT="name" FOLDED="true" ID="ID_730109609">
                                    <node TEXT="1" FOLDED="true" ID="ID_938832203"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_813339958">
                                    <node TEXT="" FOLDED="true" ID="ID_236106629"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_109636594">
                                    <node TEXT="Better" FOLDED="true" ID="ID_825195883"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true" ID="ID_1167542632">
                                <node TEXT="name" FOLDED="true" ID="ID_1620666686">
                                    <node TEXT="2" FOLDED="true" ID="ID_1327098642"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_310182688">
                                    <node TEXT="" FOLDED="true" ID="ID_1093308717"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_942617631">
                                    <node TEXT="Good" FOLDED="true" ID="ID_840824353"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true" ID="ID_1233314176">
                                <node TEXT="name" FOLDED="true" ID="ID_1795845148">
                                    <node TEXT="3" FOLDED="true" ID="ID_690409321"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1661263649">
                                    <node TEXT="" FOLDED="true" ID="ID_1930072128"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_179980159">
                                    <node TEXT="Poor" FOLDED="true" ID="ID_758276343"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true" ID="ID_1441942281">
                                <node TEXT="name" FOLDED="true" ID="ID_888966867">
                                    <node TEXT="4" FOLDED="true" ID="ID_436461122"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_871071964">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true" ID="ID_1507586975"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1761755316">
                                    <node TEXT="Very Poor" FOLDED="true" ID="ID_378496715"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="UYRORG:" FOLDED="true" ID="ID_652678887">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true" ID="ID_679427422">
                            <node TEXT="Understorey Year of Origin" FOLDED="true" ID="ID_1613110659"/>
                        </node>
                        <node TEXT="length" FOLDED="true" ID="ID_1654764297">
                            <node TEXT="4" FOLDED="true" ID="ID_1673519875"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_626570999">
                            <node TEXT="Integer" FOLDED="true" ID="ID_86047249"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1001035184">
                            <node TEXT="UYRORG" FOLDED="true" ID="ID_628290600"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1705124362">
                            <node TEXT="The understorey year of origin attribute indicates the average year that the species having the greatest relative abundance (in terms of basal area) within the understorey, came into existence." FOLDED="true" ID="ID_1186967427"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_694202777">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_191918308"/>
                            <node TEXT="The value must be greater than or equal to 1600" FOLDED="true" ID="ID_1514862853"/>
                            <node TEXT="The value should be greater than OYRORG" FOLDED="true" ID="ID_718004471"/>
                            <node TEXT="The UYRORG attribute must be less than the plan period start year" FOLDED="true" ID="ID_44915194"/>
                            <node TEXT="The population of this attribute is mandatory when POLYTYPE is equal to FOR and VERT starts with T or M (Two tiered or Mainly two-tiered)" FOLDED="true" ID="ID_520127764"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_962116971">
                            <node TEXT="english" FOLDED="true" ID="ID_202328906">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1484589180">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then UYRORG must equal zero" FOLDED="true" ID="ID_547134384">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then UYRORG must equal zero" FOLDED="true" ID="ID_1843664490">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR and VERT is equal to TO, TU, MO or MU" FOLDED="true" ID="ID_414300422">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1:The UYRORG attribute value must be greater than or equal to 1800 when POLYTYPE is equal to FOR and VERT is equal to TO, TU, MO or MU" FOLDED="true" ID="ID_424844246">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The value should be greater than OYRORG (UYRORG &gt; OYRORG) when POLYTYPE is equal to FOR and VERT is equal to TO, TU, MO or MU" FOLDED="true" ID="ID_1895164433">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The UYRORG attribute must be less than the plan period start year when POLYTYPE is equal to FOR and VERT is equal to TO, TU, MO or MU" FOLDED="true" ID="ID_1742331945">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST2: The UYRORG attribute value should not be greater than YRSOURCE attribute value when POLYTYPE is equal to FOR and VERT is equal to TO, TU, MO or MU" FOLDED="true" ID="ID_974148800">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_8430318">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1139003199">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 (if DEVSTAGE is DEPHARV or DEPNAT' for row in [row] if row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ] and row['${name}'] not in [None, '', ' ', 0 ] ]" FOLDED="true" ID="ID_1335539844">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not equal 0, null or blank  in two- or multi-canopy stands when POLYTYPE equals FOR' for row in [row] if row['${name}'] in [None, '', ' ', 0]  and row['POLYTYPE'] == 'FOR' and  row['VERT'] in ['TU','TO','MU','MO']  ]" FOLDED="true" ID="ID_228209357">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than plan period start year and greater than OYRORG and 1800 when POLYTYPE equals FOR' for row in [row] if row['POLYTYPE'] == 'FOR' and row['VERT'] in ['TO','TU','MO','MU']   if row['${name}'] &lt; 1800 or row['${name}'] &lt; row['OYRORG'] or row['${name}'] &gt;= ${planyear} ] " FOLDED="true" ID="ID_543205585">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than or equal to YRSOURCE when POLYTYPE equals FOR' for row in [row] if row['${name}'] not in  [None, '', ' ', 0] and row['YRSOURCE'] not in [None, '', ' ', 0]  and row['${name}']  &gt; row['YRSOURCE'] and row['POLYTYPE'] == 'FOR' and row['VERT'] in ['TU','TO','MU','MO']    ]" FOLDED="true" ID="ID_1093879118">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_78747035"/>
                        </node>
                    </node>
                    <node TEXT="USPCOMP:" FOLDED="true" ID="ID_1955917283">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true" ID="ID_1256396545">
                            <node TEXT="Understorey Species Composition" FOLDED="true" ID="ID_1481498486"/>
                        </node>
                        <node TEXT="length" FOLDED="true" ID="ID_820483764">
                            <node TEXT="120" FOLDED="true" ID="ID_857640943"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_380630729">
                            <node TEXT="Character" FOLDED="true" ID="ID_486727574"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1731184205">
                            <node TEXT="USPCOMP" FOLDED="true" ID="ID_1775645439"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_38795993">
                            <node TEXT="This understorey species composition attribute indicates the tree species that are present in the understorey portion of the forest stand canopy and the proportion of cover that each species occupies within the understorey." FOLDED="true" ID="ID_1220692812"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_1299616751">
                            <node TEXT="repeating pattern of species code and corresponding proportion value each species code is 3 characters (including blanks) and is left justified each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified.   maximum of 20 species and proportions pairs in the string " FOLDED="true" ID="ID_1252020066"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_758126410">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_82365325"/>
                            <node TEXT="A blank or null value is a valid code" FOLDED="true" ID="ID_1659779359"/>
                            <node TEXT="pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion)" FOLDED="true" ID="ID_294846112"/>
                            <node TEXT="no duplicate species codes allowed in the string" FOLDED="true" ID="ID_625178608"/>
                            <node TEXT="proportion values in the string must sum to 100" FOLDED="true" ID="ID_1723112667"/>
                            <node TEXT="The tree species in the understorey composition are to be coded using the scheme listed in A1.1.12 OSPCOMP." FOLDED="true" ID="ID_514047929"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_36425637">
                            <node TEXT="english" FOLDED="true" ID="ID_1825221854">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1952119846">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then USPCOMP must be null" FOLDED="true" ID="ID_1357548512">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then USPCOMP must be null" FOLDED="true" ID="ID_201062150">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code when POLYTYPE is equal to FOR (no need to test this)" FOLDED="true" ID="ID_936613871">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion)" FOLDED="true" ID="ID_1320851928">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: No duplicate species codes allowed in the string" FOLDED="true" ID="ID_1209503762">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: Proportion values in the string must sum to 100" FOLDED="true" ID="ID_131119048">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST1: The tree species in the composition are to be coded using the scheme listed here." FOLDED="true" ID="ID_1355683837">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="ST1: Where the stand canopy has been determined to be two-tiered (VERT = TO, TU, MO, MU) then an understorey species composition must be entered (USPCOMP &lt;&gt; null)" FOLDED="true" ID="ID_1045852797">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="Tree species in the list [     'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn',     'CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf',     'OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX',     'OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX',     'Sb','Sw','la','Wb','Wi'     ]" FOLDED="true" ID="ID_1612884090">
                                    <icon BUILTIN="yes"/>
                                </node>
                                <node TEXT="Plot Species are [     'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm',     'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp',     'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf',     'Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op',     'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc',     'Sk','Sn','Sr','Sy','Tp','Haz'     ]" FOLDED="true" ID="ID_671986228">
                                    <icon BUILTIN="yes"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_924440933">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR' for row in [row] if (row['${name}'] is not None and str(row['${name}']).strip() != '') and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_165994707">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if DEVSTAGE is DEPHARV or DEPNAT' for row in [row] if row['${name}'] is not None and str(row['${name}']).strip() !='' and row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ] ]" FOLDED="true" ID="ID_380571444">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; formatting error %s'%(s) for row in [row]  for s in test_spcomp(row['${name}'])  if row['POLYTYPE'] == 'FOR' and row['${name}'] not in [ None, '', ' ']  ]" FOLDED="true" ID="ID_221717837">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: %s is not a valid species code for &lt;b&gt;&lt;mark&gt;USPCOMP&lt;/mark&gt;&lt;/b&gt; when POLYTYPE equals FOR:'%(s) for s in test_SpeciesInList(row['${name}'], [ 'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi' ]        ) if row['POLYTYPE'] == 'FOR'  and row['SOURCE'][:4] != 'PLOT' and row['VERT'] in ['TO','TU','MO','MU']  and (row['${name}'] is not None and str(row['${name}']).strip() != '') ]" FOLDED="true" ID="ID_999810159">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="['ST1: %s is not a valid &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; species code when POLYTYPE is FOR'%(s) for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','Wb','Wi'    ]     )  if row['${name}'] not in [ None , '', ' ' ]  if row['POLYTYPE'] == 'FOR' and row['SOURCE'][:4] == 'PLOT' and row['VERT'] in ['TO','TU','MO','MU']  ]" FOLDED="true" ID="ID_1758650848">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be null or blank in two- or multi-canopy stands if POLYTYPE equals FOR' for row in [row] if (row['${name}'] is None or str(row['${name}']).strip() == '')  and row['VERT'] in ['TO','TU','MO','MU'] and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_1291541923">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1287909456"/>
                        </node>
                    </node>
                    <node TEXT="ULEADSPC:" FOLDED="true" ID="ID_905849922">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1930125584">
                            <node TEXT="3" FOLDED="true" ID="ID_1169370431"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1566233309">
                            <node TEXT="Character" FOLDED="true" ID="ID_167578188"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1075999408">
                            <node TEXT="ULEADSPC" FOLDED="true" ID="ID_1049392477"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1889057907">
                            <node TEXT="Understorey Leading Species" FOLDED="true" ID="ID_643137197"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1979277522">
                            <node TEXT="The understorey leading species  attribute indicates the most prevalent species in the forest stand (or in just the lower most predominant canopy layer if the stand canopy contains two or more distinct layers) based on its percentage of crown closure." FOLDED="true" ID="ID_1939757334"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_587217057">
                            <node TEXT="Use the same coding as is listed in the overstorey species composition attribute description." FOLDED="true" ID="ID_1562136469"/>
                            <node TEXT="Must be species listed in the understorey species composition" FOLDED="true" ID="ID_1802164447"/>
                            <node TEXT="This leading species attribute replaces the working group attribute (WG) that was present in previous inventory products for determining the major species to manage for in a forest stand." FOLDED="true" ID="ID_1921211453"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1794490761">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1641063663"/>
                            <node TEXT="The tree species in the understorey composition are to be coded using the scheme listed in OSPCOMP." FOLDED="true" ID="ID_1597853352"/>
                            <node TEXT="if the stand canopy has been determined to be two-tiered  (VERT = TO, TU, MO, or MU),  then an understorey leading species value must be entered (i.e., ULEADSPC &lt;&gt; null)." FOLDED="true" ID="ID_1712536788"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1884825119">
                            <node TEXT="english" FOLDED="true" ID="ID_738920457">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1467173431">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then ULEADSPC must be null" FOLDED="true" ID="ID_1824023765">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then ULEADSPC must be null" FOLDED="true" ID="ID_1696706073">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: Must be species listed in the understorey species composition (USPCOMP) when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_181650618">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: Where the stand canopy has been determined to be two-tiered (VERT = TO, TU, MO, or MU), then the understorey leading species value must be entered (ULEADSPC &lt;&gt; null) when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1933258530">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be a species listed in the understorey species composition (USPCOMP) when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_686378368">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be the species with the greatest percentage or tied for the greatest percentage in the species composition (USPCOMP) when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_483257025">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1181390598">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null when POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [None, '', ' '] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1230378880">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if DEVSTAGE is DEPHARV or DEPNAT' for row in [row] if row['${name}'] not in [None, '', ' '] and row['DEVSTAGE'] in [ 'DEPNAT', 'DEPHARV' ] ]" FOLDED="true" ID="ID_1940765487">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must match the left two chars of USPCOMP if POLYTYPE equals FOR' for row in [row] if (row['${name}'] not in [None, '', ' ']) and (row['${name}'][:1] != row['USPCOMP'][:1]) and row['POLYTYPE'] == 'FOR']" FOLDED="true" ID="ID_406762353">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_441878781"/>
                        </node>
                    </node>
                    <node TEXT="UAGE:" FOLDED="true" ID="ID_627630107">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_234464446">
                            <node TEXT="3" FOLDED="true" ID="ID_1742686599"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1719168088">
                            <node TEXT="Integer" FOLDED="true" ID="ID_1973295966"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1020558517">
                            <node TEXT="UAGE" FOLDED="true" ID="ID_1528046802"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1128565901">
                            <node TEXT="Understorey Age" FOLDED="true" ID="ID_1340380419"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_461142360">
                            <node TEXT="The understorey age  attribute contains the average age of the leading species in the understorey canopy layer of the forest stand." FOLDED="true" ID="ID_178865391"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1022393100">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_361253038"/>
                            <node TEXT="if the stand canopy has been determined to be two-tiered  (VERT = TO, TU, MO, or MU),  then an understorey age value must be entered (i.e., UAGE &lt;&gt; null)." FOLDED="true" ID="ID_493853213"/>
                            <node TEXT="A zero or null value is a valid code when VERT &lt;&gt; T or M" FOLDED="true" ID="ID_199663335"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_653217465">
                            <node TEXT="english" FOLDED="true" ID="ID_1690708014">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_808172164">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then UAGE must equal zero" FOLDED="true" ID="ID_355853305">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then UAGE must equal zero" FOLDED="true" ID="ID_1518438244">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is a valid code (does not need to be checked)" FOLDED="true" ID="ID_1576800225">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: if the stand canopy has been determined to be two-tiered  (VERT = TO, TU, MO, or MU),  then an understorey age value must be entered (i.e., UAGE &lt;&gt; 0)." FOLDED="true" ID="ID_465658489">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1497950337">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 or null when POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [  None, 0, ' ', '']  and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1209445031">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 (not null or blank) if DEVSTAGE is DEPHARV or DEPNAT' for row in [row] if row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ] and row['${name}'] != 0 ]" FOLDED="true" ID="ID_731822960">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt; 0 in two- or multi-canopy stands where management canopy is understory (VERT is TO,TU, MO, MU)' for row in [row] if row['${name}']  &lt;= 0 and row['POLYTYPE'] == 'FOR' and row['VERT'] in ['TO','TU','MO','MU'] ]" FOLDED="true" ID="ID_1203230280">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_470715919"/>
                        </node>
                    </node>
                    <node TEXT="UHT:" FOLDED="true" ID="ID_1589001440">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_755991832">
                            <node TEXT="4.1" FOLDED="true" ID="ID_550213881"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1057382273">
                            <node TEXT="Double" FOLDED="true" ID="ID_1908971204"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_11645355">
                            <node TEXT="UHT" FOLDED="true" ID="ID_1173581071"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_736836146">
                            <node TEXT="Understorey Height" FOLDED="true" ID="ID_383354259"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1980352820">
                            <node TEXT="The understorey height attribute indicates the estimated average tree height (in meters) of the species that has the most basal area as inventoried in the Year of Update.  Estimates can be made from interpreted crown canopy or field samples, or from growth algorithms." FOLDED="true" ID="ID_690258706"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1335626961">
                            <node TEXT="Valid numeric values are from 0 through 40.0" FOLDED="true" ID="ID_158725318"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true" ID="ID_1547786774"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true" ID="ID_1292687648">
                            <node TEXT="english" FOLDED="true" ID="ID_317011688">
                                <node TEXT="Height / Age relationships should be in the range of Plonski and others." FOLDED="true" ID="ID_658679415"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_218358930">
                            <node TEXT="english" FOLDED="true" ID="ID_675832230">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1911200726">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then UHT must equal zero" FOLDED="true" ID="ID_1947406603">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then UHT must equal zero" FOLDED="true" ID="ID_368873775">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1:The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_864488726">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: Where the stand canopy has been determined to be two-tiered (VERT=TO,TU,MO, or MU), then an understorey height value must be entered (UHT&lt;&gt;0)" FOLDED="true" ID="ID_1120863656">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_745252527">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The value should be at least 3 less than OHT" FOLDED="true" ID="ID_1656963137">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1387668499">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 (not null or blank) when POLYTYPE is not FOR' for row in [row] if row['${name}'] != 0 and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1113254320">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="[ 'ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 (not null or blank) if DEVSTAGE is DEPHARV or DEPNAT and POLYTYPE equals FOR' for row in [row] if row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ] and row['${name}'] != 0 and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_812233030">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; cannot be 0, null or blank if the canopy is two-tiered when POLYTYPE is FOR.' for row in [row] if  row['${name}'] in [None, '', ' ', 0]  and row['VERT'] in ['TO','TU','MO','MU'] and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_1769474787">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and  ${name} less than or equal to 40 and POLYTYPE is FOR' for row in [row] if (row['${name}'] is not None and str(row['${name}']).strip() != '') and (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) and row['POLYTYPE'] == 'FOR' and row['VERT'][0] in ['T','M'] ]" FOLDED="true" ID="ID_379660020">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be at least 3 less than OHT when POLYTYPE is FOR' for row in [row]  if (row['${name}'] not in [None, '', ' ']) and  (row['OHT'] not in [None, '', ' ']) and row['POLYTYPE'] == 'FOR' and  row['VERT'][0] in ['T','M'] if (row['${name}'] &gt;= row['OHT']-3) or  (row['OAGE'] - row['UAGE'] &lt;20) ]" FOLDED="true" ID="ID_1362643303">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1104866589"/>
                        </node>
                    </node>
                    <node TEXT="UCCLO:" FOLDED="true" ID="ID_986462139">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1980996706">
                            <node TEXT="3" FOLDED="true" ID="ID_102123868"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1834840101">
                            <node TEXT="Integer" FOLDED="true" ID="ID_205048484"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1377093008">
                            <node TEXT="UCCLO" FOLDED="true" ID="ID_580240416"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_930952563">
                            <node TEXT="Understorey Crown Closure" FOLDED="true" ID="ID_863167439"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1635445403">
                            <node TEXT="Understorey Crown closure attribute represents the percent of crown closure of the visible high intermediate, co-dominate, and dominate tree layer within the polygon for the understorey layer of trees.  Crown closure for the understorey is the percentage of ground area covered by the vertical projection of the tree crowns onto the ground.  Each defined layer within a stand requires a crown closure; however, for a polygon that is being solely interpreted with no applicable supplemental information, the combined crown closure of the two layers cannot exceed 100 percent (100 percent) . In the case of multi-layered or two-tiered stands where there is applicable field or supplemental information then, the total of crown closure for the two tiers must never exceed  200 percent (i.e., OCCLO + UCCLO &lt;=  200 percent)." FOLDED="true" ID="ID_980491301"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_472692318">
                            <node TEXT="The maximum crown closure value is 100 percent." FOLDED="true" ID="ID_1571105743"/>
                            <node TEXT="If an understorey crown closure value is entered, then the total crown closure for the two layers must never exceed 200 percent  (i.e., OCCLO + UCCLO &lt;=  200)." FOLDED="true" ID="ID_1100247983"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1395672189">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1799469753"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true" ID="ID_1637467341"/>
                            <node TEXT="A zero value is a valid code when POLYTYPE is not FOR or when POLYTYPE is FOR and VERT is not T or M" FOLDED="true" ID="ID_1905474499"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_455521673">
                            <node TEXT="english" FOLDED="true" ID="ID_1473258111">
                                <node TEXT="ST1:The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_981191310">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then UCCLO must equal zero" FOLDED="true" ID="ID_122558582">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then UCCLO must equal zero" FOLDED="true" ID="ID_334556021">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true" ID="ID_1042413096">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A zero value (not null or blank) is a valid code" FOLDED="true" ID="ID_918938857">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Where the stand canopy has been determined to be two-tiered (VERT = TO, TU, MO, or MU), then an understorey crown closure value must be entered (UCCLO &lt;&gt;0)" FOLDED="true" ID="ID_1898807447">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be less than or equal to 100 and greater than or equal to zero (UCCLO &lt;= 100 and UCCLO &gt;=0)" FOLDED="true" ID="ID_1494363399">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST1: Supplemental - if an understorey crown closure value is entered, then the total crown closure for the two layers must never exceed 200 percent  (i.e., OCCLO + UCCLO &lt;=  200)." FOLDED="true" ID="ID_246483358">
                                    <icon BUILTIN="bookmark"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_573672056">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 (not null or blank) when POLYTYPE is not FOR' for row in [row] if row['${name}'] != 0 and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1696039600">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="[ 'ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 (not null or blank) if DEVSTAGE is DEPHARV or DEPNAT ' for row in [row] if row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ] and row['${name}'] != 0 ]" FOLDED="true" ID="ID_1735044893">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null, but a 0 value is valid' for row in [row] if row['${name}'] is None and str(row['${name}']).strip() == '' ]" FOLDED="true" ID="ID_1107794944">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than 0 and  ${name} less than or equal to 100 when POLYTYPE is FOR and the canopy is two-tiered' for row in [row] if (row['${name}'] &lt; 0 or row['${name}'] &gt; 100) and row['POLYTYPE'] == 'FOR' and row['VERT'] in ['TO','TU','MO','MU']  and (row['${name}'] not in [None, '', ' '])]" FOLDED="true" ID="ID_1793349437">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and  ${name} less than or equal to 100 and POLYTYPE is FOR' for row in [row] if (row['${name}'] not in [None, '', ' ']) and (row['${name}'] &lt; 0 or row['${name}'] &gt; 100) and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_404105271">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: Supplemental - &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; plus OCCLO cannot exceed 200' for row in [row] if (row['${name}'] not in [None, '', ' ']) and (row['OCCLO'] not in [None, '', ' ']) and (row['${name}']+row['OCCLO']) &gt; 200 ]" FOLDED="true" ID="ID_866536549">
                                    <icon BUILTIN="bookmark"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_23906011"/>
                        </node>
                    </node>
                    <node TEXT="USI:" FOLDED="true" ID="ID_994105668">
                        <node TEXT="length" FOLDED="true" ID="ID_804885489">
                            <node TEXT="4.1" FOLDED="true" ID="ID_682219218"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_384683217">
                            <node TEXT="Double" FOLDED="true" ID="ID_374474459"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1830825389">
                            <node TEXT="USI" FOLDED="true" ID="ID_173024373"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1189729336">
                            <node TEXT="Understorey Site Index" FOLDED="true" ID="ID_1464141965"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1338803730">
                            <node TEXT="Site index is a numerical expression of forest quality (forest site productivity) based on the height in meters, at a specified age (traditionally 50 years), of dominant and codominant trees in a stand. The USI attribute is the site index value determined for the understorey layer of the stand canopy. The OMNR will be responsible for the derivation of this attribute from the interpreted age and height. For young stands (e.g., less than 20 years old) where the age and height can not be interpreted, a default value of 2 will be entered. " FOLDED="true" ID="ID_106511785"/>
                            <node TEXT="None" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_598535855">
                            <node TEXT="english" FOLDED="true" ID="ID_81033672">
                                <node TEXT="" FOLDED="true" ID="ID_1363698629"/>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_344847959"/>
                            <node TEXT="sql" FOLDED="true" ID="ID_455517613"/>
                        </node>
                    </node>
                    <node TEXT="USC:" FOLDED="true" ID="ID_836156208">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1508063710">
                            <node TEXT="1" FOLDED="true" ID="ID_88653842"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_512984691">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1101671460">
                            <node TEXT="USC" FOLDED="true" ID="ID_1596268964"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1978938588">
                            <node TEXT="Understorey Site Class" FOLDED="true" ID="ID_320564977"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1687667106">
                            <node TEXT="The understorey site class attribute indicates a site quality estimate for the understorey of a forest stand.  It is determined using the average height, age, and working group, based on the dominant tree species of the understorey. These attributes are compared against height and age growth curves in Plonskis Normal Yield Tables for different species to determine the relative growth rate for a forest stand." FOLDED="true" ID="ID_1606908170"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_886646206">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1552269301"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1130940901"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1706508022">
                            <node TEXT="english" FOLDED="true" ID="ID_429438189">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1260562805">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then USC must equal zero" FOLDED="true" ID="ID_1482470408">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then USC must equal zero" FOLDED="true" ID="ID_1815564407">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_595543093">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: The site class value must be greater than or equal to 0 and less than or equal to 4 (SC&gt;=0 and SC&lt;=4)" FOLDED="true" ID="ID_769652647">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_837437488">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 when POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1465167968">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="[ 'ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 (not null or blank) if DEVSTAGE is DEPHARV or DEPNAT ' for row in [row] if row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ] and row['${name}'] not in [ None, '', ' ',  0] ]" FOLDED="true" ID="ID_1275819644">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR' for row in [row] if row['${name}'] not in [ 0,1,2,3,4 ] and row['POLYTYPE'] == 'FOR'  ]" FOLDED="true" ID="ID_217966158">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1060123914"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_840105849">
                            <node TEXT="0:" FOLDED="true" ID="ID_1535070327">
                                <node TEXT="name" FOLDED="true" ID="ID_1357868236">
                                    <node TEXT="0" FOLDED="true" ID="ID_1291611707"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_347856648">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_398010442">
                                    <node TEXT="Best" FOLDED="true" ID="ID_1294263021"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true" ID="ID_614563410">
                                <node TEXT="name" FOLDED="true" ID="ID_608725358">
                                    <node TEXT="1" FOLDED="true" ID="ID_15590982"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1834295545">
                                    <node TEXT="" FOLDED="true" ID="ID_1162531817"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_530885978">
                                    <node TEXT="Better" FOLDED="true" ID="ID_1609506900"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true" ID="ID_320925544">
                                <node TEXT="name" FOLDED="true" ID="ID_150221271">
                                    <node TEXT="2" FOLDED="true" ID="ID_1601930356"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_389540871">
                                    <node TEXT="" FOLDED="true" ID="ID_1437840695"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_849663884">
                                    <node TEXT="Good" FOLDED="true" ID="ID_1230081639"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true" ID="ID_47038980">
                                <node TEXT="name" FOLDED="true" ID="ID_798663809">
                                    <node TEXT="3" FOLDED="true" ID="ID_214912435"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1264597042">
                                    <node TEXT="" FOLDED="true" ID="ID_1236606814"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_920907738">
                                    <node TEXT="Poor" FOLDED="true" ID="ID_577644519"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true" ID="ID_959042600">
                                <node TEXT="name" FOLDED="true" ID="ID_322079536">
                                    <node TEXT="4" FOLDED="true" ID="ID_201425559"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1818632205">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true" ID="ID_1082201360"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_914076070">
                                    <node TEXT="Very Poor" FOLDED="true" ID="ID_867501588"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="INCIDSPC:" FOLDED="true" ID="ID_58473029">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_264162535">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1931075050">
                            <node TEXT="Character" FOLDED="true" ID="ID_1844650017"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_703778566">
                            <node TEXT="INCIDSPC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1540974916">
                            <node TEXT="Incidental Species" FOLDED="true" ID="ID_637117786"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1143304183">
                            <node TEXT="The incidental species attribute represents a species that is identified within the polygon but does not represent ten percent of the basal area in order to be included in the overstorey species composition attribute (OSPCOMP).  The tree species represents a tree species that is present and is important to note.(e.g. important for wildlife assessment, high market value, or significant ecological value)." FOLDED="true" ID="ID_1126421493"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_465210922">
                            <node TEXT="use the same coding as listed in the OSPCOMP (species composition) attribute descriptin" FOLDED="true" ID="ID_229511778"/>
                            <node TEXT="if an incidental species is not identified within the polygon, then a value of NON will be entered" FOLDED="true" ID="ID_533951065"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_584609558">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_580916991"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1607429079"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_416554894">
                            <node TEXT="english" FOLDED="true" ID="ID_515090446">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1639112520">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then INCIDSPC must be null" FOLDED="true" ID="ID_712179377">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_930499377">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute populatin must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_639655554">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_214824592">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: The attribute value should not be in the overstorey species composotion (OSPCOMP) and less than ten percent" FOLDED="true" ID="ID_997328901">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1115425444">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null when POLYTYPE is not FOR' for row in [row] if (row['${name}'] is not None and str(row['${name}']).strip() != '')  and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1325682773">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE equals FOR' for row in [row] if (row['${name}'] is None or str(row['${name}']).strip() == '')  and row['POLYTYPE'] == 'FOR'  ]" FOLDED="true" ID="ID_590287195">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: %s is not a valid &lt;b&gt;&lt;mark&gt;OSPCOMP&lt;/mark&gt;&lt;/b&gt; species code when POLYTYPE equals FOR' for row in [row] if row['${name}'] not in ['AX', 'Ab', 'Aw', 'Pl', 'Pt', 'Bd', 'Be', 'Bw', 'By', 'Bn', 'CE', 'Cr', 'CH', 'Cb', 'OC', 'EX', 'Ew', 'Bf', 'OH', 'He', 'Hi', 'Iw', 'La', 'Mh', 'Mr', 'Ms', 'Mh', 'Ob', 'Or', 'Ow', 'Pn', 'Pi', 'Pr', 'Ps', 'Pw', 'PO', 'Pb', 'SX', 'Sb', 'Sr', 'Sw', 'La', 'NON'] and row['${name}'] not in[None, '', ' '] and row['POLYTYPE'] == 'FOR'  ]" FOLDED="true" ID="ID_903978861">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should not be in OSPCOMP' for row in [row] if (row['${name}'] is not None and str(row['${name}']).strip() != '') and row ['${name}'] in row['OSPCOMP'] and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_960654630">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_398536627"/>
                        </node>
                    </node>
                    <node TEXT="VERT:" FOLDED="true" ID="ID_949670676">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1644296843">
                            <node TEXT="2" FOLDED="true" ID="ID_42044575"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1669279447">
                            <node TEXT="Character" FOLDED="true" ID="ID_268622468"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_564544871">
                            <node TEXT="VERT" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1272915353">
                            <node TEXT="Vertical Stand Structure" FOLDED="true" ID="ID_445247360"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_176725847">
                            <node TEXT="The vertical stand structure attribute is intended to describe the number of distinct tree layers (storeys) that can be identified within a forested polygon." FOLDED="true" ID="ID_1564567648"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_295414141">
                            <node TEXT="If the vertical stand structure is set to single story or complex (VERT = SI, SV, or CX), then a description of the stand is entered using only the overstorey attributes (e.g. OSPCOMP, OHT, OSC). If the vertical stand structure is set to TO, TU, MO, or MU, then a separate description must be entered for each of the main canopy layers using the overstorey and understorey sets of attributes accordingly.  " FOLDED="true" ID="ID_1013985202"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1165482338">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_182405003"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1260124152"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1123903188">
                            <node TEXT="english" FOLDED="true" ID="ID_43793296">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1089258744">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1026740688">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_917865136">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_965667446">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: When &lt;b&gt;&lt;mark&gt;VERT&lt;/mark&gt;&lt;/b&gt; is two or multi canopy (T* or M*), and both UAGE AND OAGE are greater than 50, UAGE  must be at least 20 years less than OAGE." FOLDED="true" ID="ID_134163188">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_553943747">
                                <node COLOR="#333333" TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow coding: %s'%(row['${name}']) for row in [row] if row['${name}'] not in [ 'SI','SV','TO','TU','MO','MU','CX' ] and (row['${name}'] is not None and str(row['${name}']).strip() != '')  and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_550391575">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: When &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; is two or multi canopy (T* or M*), and both UAGE AND OAGE are greater than 50, UAGE  must be at least 20 years less than OAGE' for row in [row]   if (row['${name}'] not in [None, '', ' ']) and (row['OAGE'] not in [None, 0, 0.0]) and (row['UAGE'] not in [None, 0, 0.0]) and row['POLYTYPE'] == 'FOR' and row['${name}'][0] in ['T','M']  if (row['UAGE'] &gt;= 50) and (row['UAGE'] &gt;= 50) and (row['OAGE'] - row['UAGE'] &lt; 20)  ]" FOLDED="true" ID="ID_1015485588">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1770658387"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_1587071312">
                            <node TEXT="SI:" FOLDED="true" ID="ID_727427691">
                                <node TEXT="name" FOLDED="true" ID="ID_968561899">
                                    <node TEXT="SI" FOLDED="true" ID="ID_359985844"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1311984548">
                                    <node TEXT="Mainly a single story stand." FOLDED="true" ID="ID_567499409"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1285658472">
                                    <node TEXT="single storey" FOLDED="true" ID="ID_383995864"/>
                                </node>
                            </node>
                            <node TEXT="SV:" FOLDED="true" ID="ID_170717397">
                                <node TEXT="name" FOLDED="true" ID="ID_1880019363">
                                    <node TEXT="SV" FOLDED="true" ID="ID_1772858244"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1951668009">
                                    <node TEXT="Mainly a single story stand with a veteran (super canopy) component representing less than 10 percent of the total crown closure for the stand." FOLDED="true" ID="ID_1023612252"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_166635383">
                                    <node TEXT="single storey with veterans" FOLDED="true" ID="ID_1249588091"/>
                                </node>
                            </node>
                            <node TEXT="TO:" FOLDED="true" ID="ID_980923233">
                                <node TEXT="name" FOLDED="true" ID="ID_1864864749">
                                    <node TEXT="TO" FOLDED="true" ID="ID_799995204"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_49184300">
                                    <node TEXT="The stand canopy is composed of mainly two distinct layers that have at least 3 meters in height difference or 20 years of age difference, and each layer represents at least 10 percent of the total canopy crown closure for the stand.  The overstorey is the layer used to assign the DEVSTAGE value during photo interpretation / inventory creation." FOLDED="true" ID="ID_176380673"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1555831402">
                                    <node TEXT="two-tiered - overstorey used to set DEVSTAGE" FOLDED="true" ID="ID_1623993297"/>
                                </node>
                            </node>
                            <node TEXT="TU:" FOLDED="true" ID="ID_924974329">
                                <node TEXT="name" FOLDED="true" ID="ID_1758880801">
                                    <node TEXT="TU" FOLDED="true" ID="ID_309584466"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1698393260">
                                    <node TEXT="The stand canopy is composed of mainly two distinct layers that have at least 3 meters in height difference or 20 years of age difference, and each layer represents at least 10 percent of the total canopy crown closure for the stand.  The understorey is the layer used to assign the DEVSTAGE value during photo interpretation / inventory creation." FOLDED="true" ID="ID_1546822405"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_775870134">
                                    <node TEXT="two-tiered - understorey used to set DEVSTAGE" FOLDED="true" ID="ID_576175378"/>
                                </node>
                            </node>
                            <node TEXT="MO:" FOLDED="true" ID="ID_414808424">
                                <node TEXT="name" FOLDED="true" ID="ID_1711148555">
                                    <node TEXT="MO" FOLDED="true" ID="ID_1296886106"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_403095754">
                                    <node TEXT="Mainly a two-tiered canopy with an additional veteran (super canopy) component of less than 10 percent of the total stand canopy crown closure.  The overstorey is the layer used to assign the DEVSTAGE value during photo interpretation / inventory creation" FOLDED="true" ID="ID_360397326"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1020214964">
                                    <node TEXT="two-tiered with veterans - overstorey used to set DEVSTAGE" FOLDED="true" ID="ID_654415984"/>
                                </node>
                            </node>
                            <node TEXT="MU:" FOLDED="true" ID="ID_475898511">
                                <node TEXT="name" FOLDED="true" ID="ID_453484474">
                                    <node TEXT="MU" FOLDED="true" ID="ID_1114859792"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_528190461">
                                    <node TEXT="Mainly a two-tiered canopy with an additional veteran (super canopy) component of less than 10 percent of the total stand canopy crown closure.  The understorey is the layer used to assign the DEVSTAGE value during photo interpretation / inventory creation." FOLDED="true" ID="ID_402950291"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_261115513">
                                    <node TEXT="two-tiered with veterans- understorey used to set DEVSTAGE" FOLDED="true" ID="ID_1768902868"/>
                                </node>
                            </node>
                            <node TEXT="CX:" FOLDED="true" ID="ID_1606266635">
                                <node TEXT="name" FOLDED="true" ID="ID_1621802483">
                                    <node TEXT="CX" FOLDED="true" ID="ID_207672587"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1062230036">
                                    <node TEXT="A stand with a wide range of heights and ages to the point of no distinct layers being identifiable." FOLDED="true" ID="ID_966541686"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_427235396">
                                    <node TEXT="complex or continuous" FOLDED="true" ID="ID_607237148"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="HORIZ:" FOLDED="true" ID="ID_1949607686">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_252032155">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_565438088">
                            <node TEXT="Character" FOLDED="true" ID="ID_1436541341"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1322875281">
                            <node TEXT="HORIZ" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1234654923">
                            <node TEXT="Horizontal  Stand Structure" FOLDED="true" ID="ID_1957990521"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_240952544">
                            <node TEXT="The horizontal stand structure attribute represents the distribution of the forest layers over the polygon area and is a solely interpreted attribute.  The distribution of each tree layer and the associated presence of openings are assessed. If more than one condition is present, only the most prevalent one is recorded.  The standard used for the interpretation of the attribute is an opening." FOLDED="true" ID="ID_1772215027"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_829836977">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1162929630"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1081824512"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_892289293">
                            <node TEXT="english" FOLDED="true" ID="ID_823013591">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_70458948">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_202702589">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1349179330">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1504068989">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1063166282"/>
                            <node TEXT="sql" FOLDED="true" ID="ID_1850236545"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_1119756572">
                            <node TEXT="SS:" FOLDED="true" ID="ID_222890642">
                                <node TEXT="name" FOLDED="true" ID="ID_1527893486">
                                    <node TEXT="SS" FOLDED="true" ID="ID_1976396407"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1319456308">
                                    <node TEXT="mainly single stem canopy structure" FOLDED="true" ID="ID_838198958"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1196167488">
                                    <node TEXT="single stem" FOLDED="true" ID="ID_1293517944"/>
                                </node>
                            </node>
                            <node TEXT="SP:" FOLDED="true" ID="ID_346125734">
                                <node TEXT="name" FOLDED="true" ID="ID_988213166">
                                    <node TEXT="SP" FOLDED="true" ID="ID_598268145"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1017235782">
                                    <node TEXT="single patch distinct from the rest of the canopy" FOLDED="true" ID="ID_728822189"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1851776381">
                                    <node TEXT="single patch" FOLDED="true" ID="ID_818284944"/>
                                </node>
                            </node>
                            <node TEXT="FP:" FOLDED="true" ID="ID_133574807">
                                <node TEXT="name" FOLDED="true" ID="ID_417614496">
                                    <node TEXT="FP" FOLDED="true" ID="ID_271520434"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_596554663">
                                    <node TEXT="two or three distinct patches" FOLDED="true" ID="ID_1470947481"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1991648417">
                                    <node TEXT="few patches" FOLDED="true" ID="ID_1031089288"/>
                                </node>
                            </node>
                            <node TEXT="MP:" FOLDED="true" ID="ID_1187743149">
                                <node TEXT="name" FOLDED="true" ID="ID_391669359">
                                    <node TEXT="MP" FOLDED="true" ID="ID_774465076"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1224894847">
                                    <node TEXT="several distinct patches" FOLDED="true" ID="ID_1608328354"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_4792521">
                                    <node TEXT="multiple patches" FOLDED="true" ID="ID_664989411"/>
                                </node>
                            </node>
                            <node TEXT="OC:" FOLDED="true" ID="ID_1181417848">
                                <node TEXT="name" FOLDED="true" ID="ID_381947514">
                                    <node TEXT="OC" FOLDED="true" ID="ID_162543528"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1259048101">
                                    <node TEXT="openings common - 3 or more" FOLDED="true" ID="ID_1812473277"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_270108691">
                                    <node TEXT="openings common" FOLDED="true" ID="ID_426959704"/>
                                </node>
                            </node>
                            <node TEXT="OU:" FOLDED="true" ID="ID_1257410360">
                                <node TEXT="name" FOLDED="true" ID="ID_1231249597">
                                    <node TEXT="OU" FOLDED="true" ID="ID_1812118007"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_162147460">
                                    <node TEXT="openings uncommon - 1or 2" FOLDED="true" ID="ID_575107151"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1150929305">
                                    <node TEXT="openings uncommon" FOLDED="true" ID="ID_885948335"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="PRI_ECO:" FOLDED="true" ID="ID_834097809">
                        <icon BUILTIN="button_ok"/>
                        <icon BUILTIN="idea"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1107607574">
                            <node TEXT="13" FOLDED="true" ID="ID_496171077"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_961193228">
                            <node TEXT="Character" FOLDED="true" ID="ID_284206785"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1517427209">
                            <node TEXT="PRI_ECO" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_144103406">
                            <node TEXT="Primary Ecosite" FOLDED="true" ID="ID_1518919798"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_56142999">
                            <node TEXT="Ecosite is defined as an ecological unit comprised of relatively uniform geology, parent material, soils, topography, and hydrology and consists of related vegetation conditions.  An ecosite description is a vegetation description related to major vegetative attributes influencing site productivity and biological legacy, and should be relatively stable over moderate periods (20-40 years).  Ecosite is the primary unit for delineation for both the forested and non-forested land.  A complex of two forested ecosites is allowed to be recorded when more than one ecosite is present as long as the secondary ecosite represents at least 20 percent of the area of the polygon and the area associated with the secondary ecosite does not exist in a manner suitable for meeting the minimum polygon size for creating a new polygon. The PRI_CODE  attribute indicates the primary or dominant ecosite present with in the stand.  Simple ecosite:  (i.e. only PRI_ECO completed)  A polygon assigned a single ecosite label and assumed to have as much as 20 percent of the polygon consisting of acceptable inclusions (as defined by the fact sheet) or eco-elements other than those considered diagnostic of the ecosite.  Complex ecosite:  (i.e. PRI_ECO and SEC_ECO are completed)  A polygon is assigned two ecosite attributes when one ecosite condition exceeds 50 percent of the polygon (primary ecosite) and another ecosite condition exceeds 20 percent of the polygon (secondary ecosite), and the secondary ecosite does not exist in a manner suitable for representation meeting minimum polygon size. A common example of complex ecosites for a polygon would be a very shallow pine/spruce mix not large enough to differentiate from the surrounding dominant stand of moderately deep aspen/birch mix.   For more information about ecosites, including a fact sheet for each ecosite description, refer to:   Ecosystems of Ontario: Provincial Ecosites, May 21st, 2008, Ecological Land Classifications Working Group. " FOLDED="true" ID="ID_44725298"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_1265882914">
                            <node TEXT="See eFRI Tech Spec" FOLDED="true" ID="ID_12315032"/>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_1258494463">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_1252498332">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_1877897830">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_1787251661">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_1584392381">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_1493532479">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_1044860386">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_1294126549">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_1652691765">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_1760037167">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_1682296652">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_765678400">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_1335523670">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_1911211938">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_565514854">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_340889698">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_835969536">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_1944266435">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1751314119">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_497688299">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1478052227">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_1742047733">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_225131413">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_345260355">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_1424296900">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_1953942913">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_919094213">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_1254238151">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_1718462800">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_1483646887">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_22870370">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_1979164778">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_176315760">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_1316187182">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_1693366587">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_1271120365">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_1560018213">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_1354796077">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_919970545">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_829354295">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_1831296519">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_542125510">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_717954604">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_1054704477">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_970767833">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_1676335032">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_1506300629">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_440798312">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_1412737220">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_864222337">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_951413180">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_352063734">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_936207487">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_283516830">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_661154072">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_614043283">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_1503229397">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_992846744">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_1813172179">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_766996121">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1466906681">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_340879861">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1841447414">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_1581365939">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_1456763101">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_793282697">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_1714679250">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_869122304">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_702254979">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_74671488">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_1075680180">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_794497181">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_1893329588">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_1862090028">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_1449978360">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_36483494">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_1253582312">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_1622181436">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_1327163962">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_901382801">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_801808680">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1197486215">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_633181318">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1446150035">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_350690808">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_349893284">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_853669893">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_418506106">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_1483244130">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_1816322937">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_36231068">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_1058581633">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_840432138">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_328393517">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_559623289">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_1666801367">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_658969617">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_963058511">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_1208365355">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_1636517647">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_159672566">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_1910798010">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_573427395">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_590214426">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_648967629">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_230796963">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_763094462">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_754548283">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_748851328">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_1931485727">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_185086455">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_1386593935">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_1990973850">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_1929817778">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_62649143">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_1578505413">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_311591107">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_352907879">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_244355691">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_190089356">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_1895594745">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_687587037">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_1378563455">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_802291219">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_282142525">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_869947999">
                                <icon BUILTIN="bookmark"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1636362883">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_783497193"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_809483918"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_826095105">
                            <node TEXT="english" FOLDED="true" ID="ID_188675737">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1441919050">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR, BSH, RCK, TMS or OMS" FOLDED="true" ID="ID_1258101784">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR, BSH, RCK, TMS or OMS" FOLDED="true" ID="ID_1766922058">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST2: The attribute population must follow the correct coding scheme when POLYTYPE is equal to BSH, RCK, TMS or OMS" FOLDED="true" ID="ID_1319121917">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR, BSH, RCK, TMS or OMS" FOLDED="true" ID="ID_1787835482">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If SEC_ECO is not null then PRI_ECO must not be null when POLYTYPE is equal to FOR, BSH, RCK, TMS or OMS" FOLDED="true" ID="ID_602645207">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1387215071">
                                <node TEXT="['ST1: Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be null' for row in [row] if (row['${name}'] is None and str(row['${name}']).strip() == '') and row['POLYTYPE'] in ['FOR', 'BSH','RCK','TMS','OMS']   ]" FOLDED="true" ID="ID_1360027795">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Geographic Range' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] == 'FOR' if row['${name}'][0] not in ['A', 'B', 'G', 'S', 'U']   ]" FOLDED="true" ID="ID_560150832">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Vegetative Modifier' for row in [row] if row['POLYTYPE'] == 'FOR' and row['${name}'] not in [None, '', ' ', 0]  if row['${name}'][4:6].strip() not in  ['TT','TL','S','N','X'] ]" FOLDED="true" ID="ID_1880835178">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node COLOR="#000000" TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Depth Modifier: %s'%(row['${name}'][6:8]) for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] == 'FOR' if row['${name}'][6:8].strip() not in ['R','VS','S','M','MD','D'] ]" FOLDED="true" ID="ID_1645376779">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST2:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should have valid Geographic Range' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and  row['POLYTYPE'] in ['BSH','RCK','TMS','OMS'] if row['${name}'][0] not in ['A', 'B', 'G', 'S', 'U']   ]" FOLDED="true" ID="ID_996737988">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST2:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should have valid Ecosite Number' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] in ['BSH','RCK','TMS','OMS'] if  int(row['${name}'][1:3]) &lt; 0  or ( int(row['${name}'][1:3]) &gt; 224 and int(row['${name}'][1:3]) &lt; 997 )  or int(row['${name}'][1:3]) &gt; 1000  ]" FOLDED="true" ID="ID_1877889431">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST2:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should have valid Vegetative Modifier' for row in [row] if row['POLYTYPE'] in ['BSH','RCK','TMS','OMS'] and row['${name}'] not in [None, '', ' ', 0] if row['${name}'][4:6].strip() not in  ['TT','TL','S','N','X'] ]" FOLDED="true" ID="ID_1720337023">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node COLOR="#000000" TEXT="['ST2:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should have valid Depth Modifier' for row in [row] if row['${name}'] not in [ None, '', ' ', 0 ] and row['POLYTYPE'] in ['BSH','RCK','TMS','OMS'] if row['${name}'][6:8].strip() not in ['R','VS','S','M','MD','D'] ]" FOLDED="true" ID="ID_1651270758">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be null if SEC_ECO is not null' for row in [row] if (row['SEC_ECO'] not in [None, '', ' ']) and (row['${name}'] in [None, '', ' ']) and row['POLYTYPE'] in ['FOR', 'BSH','RCK','TMS','OMS']   ]" FOLDED="true" ID="ID_1045702028">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_450157742"/>
                        </node>
                    </node>
                    <node TEXT="SEC_ECO:" FOLDED="true" ID="ID_1173105014">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_218119745">
                            <node TEXT="13" FOLDED="true" ID="ID_1639717007"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1542009017">
                            <node TEXT="Character" FOLDED="true" ID="ID_347855293"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1136068700">
                            <node TEXT="SEC_ECO" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_309033572">
                            <node TEXT="Secondary Ecosite" FOLDED="true" ID="ID_265579617"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_899716034">
                            <node TEXT="Ecosite is defined as an ecological unit comprised of relatively uniform geology, parent material, soils, topography, and hydrology and consists of related vegetation conditions.  An ecosite description is a vegetation description related to major vegetative attributes influencing site productivity and biological legacy, and should be relatively stable over moderate periods (20-40 years).   Ecosite is the primary unit for delineation for both the forested and non-forested land. A complex of two forested ecosites is allowed to be recorded when more than one ecosite is present as long as the secondary ecosite represents at least 20 percent of the area of the polygon and the area associated with the secondary ecosite does not exist in a manner suitable for meeting the minimum polygon size for creating a new polygon. The SEC_ECO  attribute indicates the secondary or lesser (in terms of area) ecosite present with in the stand." FOLDED="true" ID="ID_1703974433"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_1515898441">
                            <node TEXT="See eFRI Tech Spec" FOLDED="true" ID="ID_1887023947"/>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_606069734">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_1343265471">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_1161535434">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_1430270386">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_1484570114">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_1032574609">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_209616623">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_1249585012">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_1746823729">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_1729355882">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_1562666652">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_800618361">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_681420333">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_1986480916">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_93236322">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_84729052">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_595776587">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_1908309398">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1043944654">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_1772820210">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1547660088">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_149912513">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_485743606">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_595878422">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_950610627">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_1982792268">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_109563485">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_252539545">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_1767396585">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_629798068">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_230460585">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_154253401">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_1643391302">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_486426883">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_1942246320">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_1573535657">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_449042739">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_1390265157">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_203566775">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1543788577">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_87051410">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1083371597">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_747700285">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_1644007873">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_983817333">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_981851606">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_982961264">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_1843298960">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_1492750625">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_728647131">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_479354960">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_1148453591">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_1385703185">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_1977037632">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_1703870494">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_888796170">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_1337248862">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_524627715">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_1284077456">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_261182145">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_327860179">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_1411655072">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1509996342">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_576253123">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_1983737755">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_412165949">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_1531247014">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_1315563369">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_887970922">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_1273594678">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_387804647">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_1472629410">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_1491668915">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_831577181">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_1364414591">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_873404070">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_971667604">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_1060413569">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_1587670982">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_927771495">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_1571082699">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1154429531">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_299618994">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1149103032">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_282556161">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_74152942">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_1799062327">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_412800509">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_1376612078">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_1219508212">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_1117037190">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_568362742">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_571536163">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_1048122498">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_970547643">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_1195751138">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_1320992962">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_1249713379">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_1351792877">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_1616905860">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_838758822">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_1061769139">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1562441387">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_440124374">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_994786651">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true" ID="ID_1686600332">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true" ID="ID_1022289357">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true" ID="ID_1958150613">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true" ID="ID_834727599">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true" ID="ID_368983977">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true" ID="ID_326258051">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true" ID="ID_1129838875">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true" ID="ID_79674955">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true" ID="ID_571701365">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true" ID="ID_1132425751">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true" ID="ID_1068852625">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true" ID="ID_593395011">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true" ID="ID_1039195390">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true" ID="ID_1093750089">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true" ID="ID_914440846">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true" ID="ID_383625037">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true" ID="ID_561286042">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true" ID="ID_64337820">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1215567330">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true" ID="ID_1526078805">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true" ID="ID_1890154419">
                                <icon BUILTIN="bookmark"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_660512647">
                            <node TEXT="english" FOLDED="true" ID="ID_1176139886">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1555040273">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR, BSH, RCK, TMS or OMS" FOLDED="true" ID="ID_1296221600">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code" FOLDED="true" ID="ID_1483011214">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1819541070">
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Geographic Range'  for row in [row] if row['PRI_ECO'] not in [None, '', ' ', 0]  and row['${name}'] not in [None, '', ' ', 0] and row['POLYTYPE'] == 'FOR' if row['${name}'][0] not in ['A', 'B', 'G', 'S', 'U']   ]" FOLDED="true" ID="ID_811308184">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Ecosite Number' for row in [row]  if (row['PRI_ECO'] not in [None, '', ' ']) and row['${name}']  not in [None, '', ' '] and row['POLYTYPE'] in ['FOR', 'BSH','RCK','TMS']  if row['${name}'][1:3].isdigit()  if int(row['${name}'][1:3]) &lt; 0  or ( int(row['${name}'][1:3]) &gt; 224 and int(row['${name}'][1:3]) &lt; 997 )  or int(row['${name}'][1:3]) &gt; 1000 ]" FOLDED="true" ID="ID_698863858">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Vegetative Modifier: %s'%(row['${name}'][4:6]) for row in [row] if row['PRI_ECO'] not in [ None, '', ' ', 0 ]  and row['${name}'] not in [ None, '', ' ', 0 ]  and row['POLYTYPE'] == 'FOR' if row['${name}'][4:6].strip() not in  ['TT','TL','S','N','X'] ]" FOLDED="true" ID="ID_420464521">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node COLOR="#000000" TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Depth Modifier: %s'%(row['${name}'][6:8]) for row in [row] if row['PRI_ECO'] not in [ None, '', ' ', 0 ]  and  row['${name}'] not in [ None, '', ' ', 0 ]  and row['POLYTYPE'] == 'FOR' if row['${name}'][6:8].strip() not in ['R','VS','S','M','MD','D'] ]" FOLDED="true" ID="ID_1904917060">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true" ID="ID_477013006">
                                <node TEXT="{ '${name}':&quot;B055Tt R  &quot;, 'PRI_ECO':&quot;B055Tt R  &quot;}" FOLDED="true" ID="ID_1133552394">
                                    <node TEXT="" FOLDED="true" ID="ID_1517343288"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ACCESS1:" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_113526034">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_506380791">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_951198969">
                            <node TEXT="Character" FOLDED="true" ID="ID_691821032"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1527123942">
                            <node TEXT="ACCESS1" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1699687138">
                            <node TEXT="Accessibility Indicator" FOLDED="true" ID="ID_1448306942"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1356628887">
                            <node TEXT="The accessibility indicator attribute specifies whether or not there are any restrictions to accessing a productive forest stand.  These restrictions may be legal (i.e. ownership), political / land use policy (i.e. land use designation, road closures), and/or a natural barrier.  The focus of this field is identification of Crown productive forest stands that are inaccessible and therefore are not considered as part of the managed landbase for forest management planning purposes, but the principle may be applied to any area." FOLDED="true" ID="ID_1213511832"/>
                            <node TEXT="NOTE:   These attributes are not completed by the photo interpreters during inventory production. Access restriction information is to be determined by the forest management planning team." FOLDED="true" ID="ID_64149469"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1599909056">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_766528038"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1617312412"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_1324285003">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_1676389819">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1" FOLDED="true" ID="ID_1110411817">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then ACCESS1 (and ACCESS2) must be null" FOLDED="true" ID="ID_190571184">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_514462023">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1867997932">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_401790711">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If ACCESS2 is not equal to NON then ACCESS1 must not be NON when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1524924838">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: ACCESS1 and ACCESS2 can't contain the same value unless both are NON when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_851991881">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST2: If ACCESS1 or ACCESS2 do not equal NON then AVAIL should be U when POLYTYPE is equal to FOR. Tested by the AVAIL attribute.  ONLY FOR BMI/OPI." FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_1616392783">
                                    <icon BUILTIN="yes"/>
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="[ 'ST2: If ${name} is not NON, AVAIL should be U' for row in [row] if row['${name}'] != 'NON' and row['AVAIL'] != 'U' ]" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_202036368">
                                        <icon BUILTIN="full-8"/>
                                    </node>
                                </node>
                                <node TEXT="Note: The ACCESS2 attribute may not exist in the submitted coverage as this attribute is not mandatory.  If this attribute exists, it may not be populated.  Therefore, this validation will be performed on the ACCESS2 attribute for the application of the correct coding scheme." FOLDED="true" ID="ID_1012469844">
                                    <icon BUILTIN="bookmark"/>
                                </node>
                                <node TEXT="*   If the code of GEO is entered, then a management consideration attribute (MGMTCON) must be completed with the appropriate associated explanation/details, such as island or natural barrier. Refer to the MGMTCON attribute description." FOLDED="true" ID="ID_1233321210">
                                    <icon BUILTIN="bookmark"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1850067027">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null when POLYTYPE is not FOR' for row in [row] if (row['${name}'] is not None and str(row['${name}']).strip() != '') and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1811348482">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="['ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON'  for row in [row] if row['${name}'] == 'NON'       and (             row['ACCESS2'] != 'NON' and             str(row['ACCESS2']).strip() != ''              and row['ACCESS2'] is not None and row['POLYTYPE'] == 'FOR'             ) ]" FOLDED="true" ID="ID_376597453">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not equal ACCESS2 ' for row in [row] if row['${name}'] not in [None, '', ' ', 'Non'] and row['ACCESS2'] not in [None, '', ' ', 'NON'] and row['${name}'] == row['ACCESS2'] and row['POLYTYPE'] =='FOR' ]" FOLDED="true" ID="ID_1632316923">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: Supplemental - If &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; is GEO, MGMTCON1 must not be blank' for row in [row] if row['${name}'] == 'GEO' if str(row['MGMTCON1']).strip() == '' and row['MGMTCON1'] is None ]" FOLDED="true" ID="ID_416865977">
                                    <icon BUILTIN="bookmark"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_993512243"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_1682349079">
                            <node TEXT="GEO:" FOLDED="true" ID="ID_1256232977">
                                <node TEXT="name" FOLDED="true" ID="ID_458268555">
                                    <node TEXT="GEO" FOLDED="true" ID="ID_786370919"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_501946952">
                                    <node TEXT="Area is not accessible due to geographic reasons." FOLDED="true" ID="ID_1274493123"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1183454916">
                                    <node TEXT="geography" FOLDED="true" ID="ID_254751523"/>
                                </node>
                            </node>
                            <node TEXT="LUD:" FOLDED="true" ID="ID_998747502">
                                <node TEXT="name" FOLDED="true" ID="ID_747221440">
                                    <node TEXT="LUD" FOLDED="true" ID="ID_228706509"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_796194199">
                                    <node TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands)." FOLDED="true" ID="ID_1976068517"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1347135349">
                                    <node TEXT="land use designation" FOLDED="true" ID="ID_1725677001"/>
                                </node>
                            </node>
                            <node TEXT="NON:" FOLDED="true" ID="ID_1302901709">
                                <node TEXT="name" FOLDED="true" ID="ID_524199883">
                                    <node TEXT="NON" FOLDED="true" ID="ID_954924720"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_404795251">
                                    <node TEXT="The area is accessible/reachable." FOLDED="true" ID="ID_1061274049"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_709137439">
                                    <node TEXT="no accessibility considerations" FOLDED="true" ID="ID_902450191"/>
                                </node>
                            </node>
                            <node TEXT="OWN:" FOLDED="true" ID="ID_958853471">
                                <node TEXT="name" FOLDED="true" ID="ID_1866050577">
                                    <node TEXT="OWN" FOLDED="true" ID="ID_1176313159"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_282772617">
                                    <node TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)." FOLDED="true" ID="ID_1623048251"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_969308449">
                                    <node TEXT="ownership" FOLDED="true" ID="ID_1429390897"/>
                                </node>
                            </node>
                            <node TEXT="PRC:" FOLDED="true" ID="ID_679518354">
                                <node TEXT="name" FOLDED="true" ID="ID_1161636284">
                                    <node TEXT="PRC" FOLDED="true" ID="ID_1762993153"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_204949907">
                                    <node TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area." FOLDED="true" ID="ID_631044542"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1125300453">
                                    <node TEXT="road closure" FOLDED="true" ID="ID_579372768"/>
                                </node>
                            </node>
                            <node TEXT="STO:" FOLDED="true" ID="ID_1028007200">
                                <node TEXT="name" FOLDED="true" ID="ID_1137137633">
                                    <node TEXT="STO" FOLDED="true" ID="ID_1741903161"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_664389507">
                                    <node TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time." FOLDED="true" ID="ID_451026619"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_903428714">
                                    <node TEXT="subject to ownership" FOLDED="true" ID="ID_1139264090"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ACCESS2:" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_1162622736">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1752177065">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_1952547719">
                            <node TEXT="Character" FOLDED="true" ID="ID_1641711818"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_726451199">
                            <node TEXT="ACCESS2" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1316584590">
                            <node TEXT="Accessibility Indicator" FOLDED="true" ID="ID_493984699"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_206340198">
                            <node TEXT="The accessibility indicator attributes specifies whether or not there are any restrictions to accessing a productive forest stand. These restrictions may be legal (i.e., ownership), political / land use policy (i.e., land use designation, road closures), and/or a natural barrier. The focus of this field is identification of Crown productive forest stands that are inaccessible and therefore are not considered as part of the managed landbase for forest management planning purposes, but the principle may be applied to any area." FOLDED="true" ID="ID_1236978066"/>
                            <node TEXT="NOTE:   These attributes are not completed by the photo interpreters during inventory production. Access restriction information is to be determined by the forest management planning team." FOLDED="true" ID="ID_1265094503"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_33637326">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1683411055"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1761191458"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_752650626">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_400559570">
                                <node TEXT="Note: The ACCESS2 attribute may not exist in the submitted coverage as this attribute is not mandatory.  If this attribute exists, it may not be populated.  Therefore, this validation will be performed on the ACCESS2 attribute for the application of the correct coding scheme." FOLDED="true" ID="ID_1187282448"/>
                                <node TEXT="ST1: When POLYTYPE is not equal to FOR then ACCESS1 (and ACCESS2) must be null" FOLDED="true" ID="ID_1313773191">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_869431254">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST2: If ACCESS1 or ACCESS2 do not equal NON then AVAIL should be U when POLYTYPE is equal to FOR. Tested by the AVAIL attribute.  ONLY FOR BMI/OPI." FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_1310584890">
                                    <icon BUILTIN="yes"/>
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="[ 'ST2: If ${name} is not NON, AVAIL should be U' for row in [row] if row['${name}'] != 'NON' and row['AVAIL'] != 'U' ]" FOLDED="true" BACKGROUND_COLOR="#ccffcc" ID="ID_1189099906">
                                        <icon BUILTIN="full-3"/>
                                    </node>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1405869699">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null when POLYTYPE is not FOR' for row in [row] if row['${name}'] not in [ None, '', ' ' ] and row['POLYTYPE'] != 'FOR' ]" FOLDED="true" ID="ID_1878204292">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1510482754"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_1778286956">
                            <node TEXT="GEO:" FOLDED="true" ID="ID_202020692">
                                <node TEXT="name" FOLDED="true" ID="ID_1120920804">
                                    <node TEXT="GEO" FOLDED="true" ID="ID_137094989"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1607168431">
                                    <node TEXT="Area is not accessible due to geographic reasons." FOLDED="true" ID="ID_1605179526"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_704367655">
                                    <node TEXT="geography" FOLDED="true" ID="ID_1666004547"/>
                                </node>
                            </node>
                            <node TEXT="LUD:" FOLDED="true" ID="ID_1875051154">
                                <node TEXT="name" FOLDED="true" ID="ID_732278140">
                                    <node TEXT="LUD" FOLDED="true" ID="ID_1964615295"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_94082542">
                                    <node TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands)." FOLDED="true" ID="ID_1357125025"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1889112705">
                                    <node TEXT="land use designation" FOLDED="true" ID="ID_1770574024"/>
                                </node>
                            </node>
                            <node TEXT="NON:" FOLDED="true" ID="ID_1191693790">
                                <node TEXT="name" FOLDED="true" ID="ID_394516406">
                                    <node TEXT="NON" FOLDED="true" ID="ID_759198054"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_930333664">
                                    <node TEXT="The area is accessible/reachable." FOLDED="true" ID="ID_1174734388"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1830217504">
                                    <node TEXT="no accessibility considerations" FOLDED="true" ID="ID_1756041947"/>
                                </node>
                            </node>
                            <node TEXT="OWN:" FOLDED="true" ID="ID_600826025">
                                <node TEXT="name" FOLDED="true" ID="ID_510924875">
                                    <node TEXT="OWN" FOLDED="true" ID="ID_1881385997"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1577605794">
                                    <node TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)." FOLDED="true" ID="ID_1430533154"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1432026542">
                                    <node TEXT="ownership" FOLDED="true" ID="ID_304348410"/>
                                </node>
                            </node>
                            <node TEXT="PRC:" FOLDED="true" ID="ID_970084439">
                                <node TEXT="name" FOLDED="true" ID="ID_973159686">
                                    <node TEXT="PRC" FOLDED="true" ID="ID_1811229743"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_740808718">
                                    <node TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area." FOLDED="true" ID="ID_449117556"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1300634654">
                                    <node TEXT="road closure" FOLDED="true" ID="ID_1639103395"/>
                                </node>
                            </node>
                            <node TEXT="STO:" FOLDED="true" ID="ID_467294350">
                                <node TEXT="name" FOLDED="true" ID="ID_1388124792">
                                    <node TEXT="STO" FOLDED="true" ID="ID_986891049"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_773639931">
                                    <node TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time." FOLDED="true" ID="ID_1735862186"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1028430838">
                                    <node TEXT="subject to ownership" FOLDED="true" ID="ID_485889440"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MGMTCON1:" FOLDED="true" ID="ID_435315638">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_85108766">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_243949972">
                            <node TEXT="Character" FOLDED="true" ID="ID_1558904696"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_487142424">
                            <node TEXT="MGMTCON1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true" ID="ID_23801767">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_766523716">
                            <node TEXT="Management Consideration" FOLDED="true" ID="ID_548814272"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1053975464">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning" FOLDED="true" ID="ID_1713758244"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_1610381980">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true" ID="ID_178417677"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1373458113">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true" ID="ID_1376913623"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1622273258"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1055856913">
                            <node TEXT="english" FOLDED="true" ID="ID_1726498690">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for MGMTCON1" FOLDED="true" ID="ID_501354957">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory for MGMTCON1 when POLYTYPE is equal to FOR" FOLDED="true" ID="ID_1744814822">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1450920781">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: IF ACCESS1 or ACCESS2 is equal to GEO then MGMTCON1 must not be NONE" FOLDED="true" ID="ID_869736690">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: IF MGMTCON2 is not None then MGMTCON1 must not be NONE" FOLDED="true" ID="ID_510631178">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST1: IF MGMTCON3 is not None then MGMTCON2 and MGMTCON1 must not be NONE" FOLDED="true" ID="ID_845858425">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="ST1: If FORMOD equals PF, then the MGMTCON1 attribute should not equal NONE" FOLDED="true" ID="ID_1081104927">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="ST1: The attribute value in MGMTCON1 must not be equal to the attribute value in MGMTCON2 or the attribute value in MGMTCON3 unless all are NONE" FOLDED="true" ID="ID_1418662924">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                </node>
                                <node TEXT="ST2: If the SC attribute equals 4, then the MGMTCON1 attribute should not equal NONE (BMI and OPI only)" FOLDED="true" ID="ID_1906439768">
                                    <icon BUILTIN="yes"/>
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1469034666">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE equals FOR' for row in [row] if row['${name}'] in [ None, '', ' ', 0 ] and row['POLYTYPE'] == 'FOR'  ]" FOLDED="true" ID="ID_1764287157">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: If ACCESS1 or ACCESS2 are GEO, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null' for row in [row] if (row['ACCESS1'] == 'GEO' or row['ACCESS2'] == 'GEO')  and row['${name}'] in [None, '', ' '] and row['POLYTYPE'] == 'FOR']" FOLDED="true" ID="ID_633530842">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: If MGMTCON2 is not NONE or null, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE ' for row in [row] if row['MGMTCON2'] != 'NONE' and row['MGMTCON2'] is not None and str(row['MGMTCON2']).strip() !='' and row['${name}'] == 'NONE' and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_950039964">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: If MGMTCON3 is not NONE or null, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE ' for row in [row] if row['MGMTCON3'] != 'NONE' and row['MGMTCON3'] is not None and str(row['MGMTCON3']).strip() !='' and row['${name}'] == 'NONE' and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_1683605499">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="['ST1: If FORMOD equals PF, &lt;b&gt;&lt;mark&gt;MGMTCON1&lt;/mark&gt;&lt;/b&gt; should not equal NONE when POLYTYPE equals FOR' for row in [row] if row['${name}'] == 'NONE' and row['FORMOD'] == 'PF' and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_837066152">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not equal MGMTCON2 or MGMTCON3 when POLYTYPE equals FOR' for row in [row] if row['${name}'] not in [None, '', ' ', 'NONE'] and (row['${name}'] == row['MGMTCON2'] or row['${name}'] == row['MGMTCON3']) and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_1666832842">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_908069708"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_668472035">
                            <node TEXT="NONE:" FOLDED="true" ID="ID_1884706464">
                                <node TEXT="name" FOLDED="true" ID="ID_1848086826">
                                    <node TEXT="NONE" FOLDED="true" ID="ID_1002652995"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1958525737">
                                    <node TEXT="There are no physical or ecological restrictions in the site that need to be considered when determining management of the stand." FOLDED="true" ID="ID_265894436"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1416458875">
                                    <node TEXT="no management consideration" FOLDED="true" ID="ID_773213900"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true" ID="ID_1587681959">
                                <node TEXT="name" FOLDED="true" ID="ID_738388857">
                                    <node TEXT="ISLD" FOLDED="true" ID="ID_604280152"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_474596959">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true" ID="ID_129070702"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_594699224">
                                    <node TEXT="island" FOLDED="true" ID="ID_1510545712"/>
                                </node>
                            </node>
                            <node TEXT="COLD:" FOLDED="true" ID="ID_1466250281">
                                <node TEXT="name" FOLDED="true" ID="ID_116323124">
                                    <node TEXT="COLD" FOLDED="true" ID="ID_1753270724"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1224001946">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true" ID="ID_1072869775"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_63372119">
                                    <node TEXT="permafrost" FOLDED="true" ID="ID_1907703319"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true" ID="ID_198674958">
                                <node TEXT="name" FOLDED="true" ID="ID_774978232">
                                    <node TEXT="DAMG" FOLDED="true" ID="ID_1691955892"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1122725623">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true" ID="ID_194388927"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1283537737">
                                    <node TEXT="physical/natural damage" FOLDED="true" ID="ID_131729215"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true" ID="ID_824718771">
                                <node TEXT="name" FOLDED="true" ID="ID_698703518">
                                    <node TEXT="NATB" FOLDED="true" ID="ID_1999549404"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1799467471">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true" ID="ID_931198727"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1187514747">
                                    <node TEXT="natural barrier" FOLDED="true" ID="ID_633481913"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true" ID="ID_671647497">
                                <node TEXT="name" FOLDED="true" ID="ID_321946303">
                                    <node TEXT="PENA" FOLDED="true" ID="ID_812966789"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1284082243">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true" ID="ID_1943939282"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1819624849">
                                    <node TEXT="peninsula" FOLDED="true" ID="ID_1285510239"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true" ID="ID_549328942">
                                <node TEXT="name" FOLDED="true" ID="ID_214186908">
                                    <node TEXT="POOR" FOLDED="true" ID="ID_1613996716"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_223816645">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true" ID="ID_521845217"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1510360883">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true" ID="ID_434348947"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true" ID="ID_539776970">
                                <node TEXT="name" FOLDED="true" ID="ID_1648320592">
                                    <node TEXT="ROCK" FOLDED="true" ID="ID_393994458"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_210948251">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true" ID="ID_769337571"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_763044553">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true" ID="ID_111665224"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true" ID="ID_253744667">
                                <node TEXT="name" FOLDED="true" ID="ID_1005006437">
                                    <node TEXT="SAND" FOLDED="true" ID="ID_1398625301"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_459361710">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true" ID="ID_1239525513"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1648812099">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true" ID="ID_489468017"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true" ID="ID_76572343">
                                <node TEXT="name" FOLDED="true" ID="ID_794450664">
                                    <node TEXT="SHRB" FOLDED="true" ID="ID_210486234"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_87850962">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true" ID="ID_1481122077"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_149542272">
                                    <node TEXT="heavy shrub / brush" FOLDED="true" ID="ID_1390663855"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true" ID="ID_635973083">
                                <node TEXT="name" FOLDED="true" ID="ID_1442593950">
                                    <node TEXT="SOIL" FOLDED="true" ID="ID_1475578153"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_102871143">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true" ID="ID_1584522311"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1627997148">
                                    <node TEXT="shallow soils" FOLDED="true" ID="ID_1889970869"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true" ID="ID_535761769">
                                <node TEXT="name" FOLDED="true" ID="ID_1877056438">
                                    <node TEXT="STEP" FOLDED="true" ID="ID_1218637072"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1321631182">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion." FOLDED="true" ID="ID_71080872"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1658134663">
                                    <node TEXT="steep slopes" FOLDED="true" ID="ID_1140808942"/>
                                </node>
                            </node>
                            <node TEXT="UPFR:" FOLDED="true" ID="ID_409157589">
                                <node TEXT="name" FOLDED="true" ID="ID_17576943">
                                    <node TEXT="UPFR" FOLDED="true" ID="ID_1000740917"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1802132238">
                                    <node TEXT="Productive forest land containing obvious physical features which may limit, but does not prevent, the ability to practice forest management.  The feature(s) must be considered during forest management planning, but does not make the stand unmanageable.  The specific reason/limitation is not known." FOLDED="true" ID="ID_1366793545"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_223339362">
                                    <node TEXT="unknown concern - historic production forest reserve (PRF) area" FOLDED="true" ID="ID_353358745"/>
                                </node>
                            </node>
                            <node TEXT="U_PF:" FOLDED="true" ID="ID_1739999754">
                                <node TEXT="name" FOLDED="true" ID="ID_1610645955">
                                    <node TEXT="UPF" FOLDED="true" ID="ID_1508690894"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_145385909">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitations such as steep slopes or shallow soils over bedrock.  The specific reason/limitation is not known." FOLDED="true" ID="ID_1981196237"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1277533739">
                                    <node TEXT="unknown concern - historic protection forest (PF) area" FOLDED="true" ID="ID_1008066303"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true" ID="ID_1194962604">
                                <node TEXT="name" FOLDED="true" ID="ID_90628193">
                                    <node TEXT="WATR" FOLDED="true" ID="ID_1147357026"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1543382179">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true" ID="ID_1347624513"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1735249242">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true" ID="ID_173791865"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true" ID="ID_347161411">
                                <node TEXT="name" FOLDED="true" ID="ID_1734541066">
                                    <node TEXT="WETT" FOLDED="true" ID="ID_121863686"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_833080358">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true" ID="ID_469070389"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_660808698">
                                    <node TEXT="poorly drained - high water table" FOLDED="true" ID="ID_1523983304"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MGMTCON2:" FOLDED="true" ID="ID_1209467779">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1013649624">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_924192062">
                            <node TEXT="Character" FOLDED="true" ID="ID_1509457229"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_1279416303">
                            <node TEXT="MGMTCON2" FOLDED="true" ID="ID_140083252"/>
                        </node>
                        <node TEXT="domain" FOLDED="true" ID="ID_891843770">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_356472199">
                            <node TEXT="Management Consideration" FOLDED="true" ID="ID_820684607"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_972307027">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning." FOLDED="true" ID="ID_944469703"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_1966048846">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true" ID="ID_304072099"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_492821900">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_308277714"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_185043036">
                            <node TEXT="english" FOLDED="true" ID="ID_1788606318">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1961915000">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: IF MGMTCON3 is not None then MGMTCON2 and MGMTCON1 must not be NONE" FOLDED="true" ID="ID_850856850">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_870189820">
                                <node TEXT="['ST1: If MGMTCON3 is not None, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE ' for row in [row] if row['MGMTCON3'] != 'None' and row['${name}'] == 'None' and row['MGMTCON1'] == 'None' and row['POLYTYPE'] == 'FOR' ]" FOLDED="true" ID="ID_771421198">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true" ID="ID_1577648393"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_1403054580">
                            <node TEXT="NONE:" FOLDED="true" ID="ID_1329389489">
                                <node TEXT="name" FOLDED="true" ID="ID_1225095348">
                                    <node TEXT="NONE" FOLDED="true" ID="ID_1890674114"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_840542300">
                                    <node TEXT="There are no physical or ecological restrictions in the site that need to be considered when determining management of the stand." FOLDED="true" ID="ID_1510316684"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_723240422">
                                    <node TEXT="no management consideration" FOLDED="true" ID="ID_51399182"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true" ID="ID_902988227">
                                <node TEXT="name" FOLDED="true" ID="ID_101462729">
                                    <node TEXT="ISLD" FOLDED="true" ID="ID_1540815328"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1967866254">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true" ID="ID_316951336"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1738176314">
                                    <node TEXT="island" FOLDED="true" ID="ID_1919329029"/>
                                </node>
                            </node>
                            <node TEXT="COLD:" FOLDED="true" ID="ID_1161752029">
                                <node TEXT="name" FOLDED="true" ID="ID_22903166">
                                    <node TEXT="COLD" FOLDED="true" ID="ID_242726781"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1707207746">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true" ID="ID_1592035481"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1497427092">
                                    <node TEXT="permafrost" FOLDED="true" ID="ID_1113715575"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true" ID="ID_775565168">
                                <node TEXT="name" FOLDED="true" ID="ID_989036056">
                                    <node TEXT="DAMG" FOLDED="true" ID="ID_1417442107"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1004808087">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true" ID="ID_1729840848"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_162285911">
                                    <node TEXT="physical/natural damage" FOLDED="true" ID="ID_1613322071"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true" ID="ID_733543869">
                                <node TEXT="name" FOLDED="true" ID="ID_1092954645">
                                    <node TEXT="NATB" FOLDED="true" ID="ID_522420555"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_32829965">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true" ID="ID_331065094"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1929005723">
                                    <node TEXT="natural barrier" FOLDED="true" ID="ID_1780995020"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true" ID="ID_323278187">
                                <node TEXT="name" FOLDED="true" ID="ID_516559818">
                                    <node TEXT="PENA" FOLDED="true" ID="ID_539689717"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1405779150">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true" ID="ID_898367982"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_364287398">
                                    <node TEXT="peninsula" FOLDED="true" ID="ID_871764468"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true" ID="ID_278643218">
                                <node TEXT="name" FOLDED="true" ID="ID_1251518673">
                                    <node TEXT="POOR" FOLDED="true" ID="ID_358173566"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1608462677">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true" ID="ID_1881421781"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1757743697">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true" ID="ID_460885080"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true" ID="ID_1803489117">
                                <node TEXT="name" FOLDED="true" ID="ID_129629291">
                                    <node TEXT="ROCK" FOLDED="true" ID="ID_972766243"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_626087025">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true" ID="ID_1549715557"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1203034205">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true" ID="ID_1563901894"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true" ID="ID_1655801686">
                                <node TEXT="name" FOLDED="true" ID="ID_1645399049">
                                    <node TEXT="SAND" FOLDED="true" ID="ID_1053525803"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1653717512">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true" ID="ID_1945009153"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_93283702">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true" ID="ID_823394514"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true" ID="ID_1659162630">
                                <node TEXT="name" FOLDED="true" ID="ID_1969735900">
                                    <node TEXT="SHRB" FOLDED="true" ID="ID_1626008156"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_201158561">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true" ID="ID_1575769335"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_231702852">
                                    <node TEXT="heavy shrub / brush" FOLDED="true" ID="ID_1630805124"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true" ID="ID_1659883081">
                                <node TEXT="name" FOLDED="true" ID="ID_973785837">
                                    <node TEXT="SOIL" FOLDED="true" ID="ID_700729355"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1417515347">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true" ID="ID_1370600948"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_378307688">
                                    <node TEXT="shallow soils" FOLDED="true" ID="ID_958663827"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true" ID="ID_582330514">
                                <node TEXT="name" FOLDED="true" ID="ID_345374178">
                                    <node TEXT="STEP" FOLDED="true" ID="ID_1221466093"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1453885829">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion." FOLDED="true" ID="ID_1712866636"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1064636773">
                                    <node TEXT="steep slopes" FOLDED="true" ID="ID_1606664980"/>
                                </node>
                            </node>
                            <node TEXT="UPFR:" FOLDED="true" ID="ID_1592560138">
                                <node TEXT="name" FOLDED="true" ID="ID_1400934904">
                                    <node TEXT="UPFR" FOLDED="true" ID="ID_1462014922"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_357038507">
                                    <node TEXT="Productive forest land containing obvious physical features which may limit, but does not prevent, the ability to practice forest management.  The feature(s) must be considered during forest management planning, but does not make the stand unmanageable.  The specific reason/limitation is not known." FOLDED="true" ID="ID_1708697058"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_741282758">
                                    <node TEXT="unknown concern - historic production forest reserve (PRF) area" FOLDED="true" ID="ID_1918422789"/>
                                </node>
                            </node>
                            <node TEXT="U_PF:" FOLDED="true" ID="ID_1055182052">
                                <node TEXT="name" FOLDED="true" ID="ID_1942449601">
                                    <node TEXT="UPF" FOLDED="true" ID="ID_1687878385"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1858295527">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitations such as steep slopes or shallow soils over bedrock.  The specific reason/limitation is not known." FOLDED="true" ID="ID_1739176386"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_505238538">
                                    <node TEXT="unknown concern - historic protection forest (PF) area" FOLDED="true" ID="ID_1070456430"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true" ID="ID_1873803390">
                                <node TEXT="name" FOLDED="true" ID="ID_1975998048">
                                    <node TEXT="WATR" FOLDED="true" ID="ID_489075131"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1127153029">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true" ID="ID_1551777903"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_128660826">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true" ID="ID_1577874317"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true" ID="ID_1691530259">
                                <node TEXT="name" FOLDED="true" ID="ID_1045481631">
                                    <node TEXT="WETT" FOLDED="true" ID="ID_16374096"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1470997615">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true" ID="ID_1638804576"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_874560337">
                                    <node TEXT="poorly drained - high water table" FOLDED="true" ID="ID_1963680682"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MGMTCON3:" FOLDED="true" ID="ID_1422794938">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true" ID="ID_1569808467">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_500713554">
                            <node TEXT="Character" FOLDED="true" ID="ID_122599641"/>
                        </node>
                        <node TEXT="name" FOLDED="true" ID="ID_370058806">
                            <node TEXT="MGMTCON3" FOLDED="true" ID="ID_1747348690"/>
                        </node>
                        <node TEXT="domain" FOLDED="true" ID="ID_204007001">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1815609484">
                            <node TEXT="Management Consideration" FOLDED="true" ID="ID_1899353853"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_75991854">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning." FOLDED="true" ID="ID_1937363015"/>
                        </node>
                        <node TEXT="description" FOLDED="true" ID="ID_1156569145">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true" ID="ID_1497580055"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true" ID="ID_1675558610">
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_1277114566"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_663159051">
                            <node TEXT="english" FOLDED="true" ID="ID_1173215275">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true" ID="ID_883670302">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_443081581"/>
                            <node TEXT="sql" FOLDED="true" ID="ID_1361019597"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_278958990">
                            <node TEXT="COLD:" FOLDED="true" ID="ID_1836364217">
                                <node TEXT="name" FOLDED="true" ID="ID_896160647">
                                    <node TEXT="COLD" FOLDED="true" ID="ID_1212535669"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1178536976">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true" ID="ID_1087279263"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1714819780">
                                    <node TEXT="permafrost" FOLDED="true" ID="ID_1311288446"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true" ID="ID_605575907">
                                <node TEXT="name" FOLDED="true" ID="ID_1161322267">
                                    <node TEXT="DAMG" FOLDED="true" ID="ID_931721938"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_994662408">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true" ID="ID_97647103"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_382168983">
                                    <node TEXT="physical/natural damage" FOLDED="true" ID="ID_127401886"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true" ID="ID_139863641">
                                <node TEXT="name" FOLDED="true" ID="ID_409444869">
                                    <node TEXT="ISLD" FOLDED="true" ID="ID_1142944888"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1036731529">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true" ID="ID_1946121451"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1722122947">
                                    <node TEXT="island" FOLDED="true" ID="ID_195829320"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true" ID="ID_1928325788">
                                <node TEXT="name" FOLDED="true" ID="ID_1104634539">
                                    <node TEXT="NATB" FOLDED="true" ID="ID_1372618523"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_977221683">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true" ID="ID_704360081"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1796040283">
                                    <node TEXT="natural barrier" FOLDED="true" ID="ID_306473013"/>
                                </node>
                            </node>
                            <node TEXT="NONE:" FOLDED="true" ID="ID_911104125">
                                <node TEXT="name" FOLDED="true" ID="ID_1415156944">
                                    <node TEXT="NONE" FOLDED="true" ID="ID_1545655167"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_321108636">
                                    <node TEXT="There are no physical or ecological restrictions in the site that need to be considered when determining management of the stand." FOLDED="true" ID="ID_1582173517"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1715071424">
                                    <node TEXT="no management consideration" FOLDED="true" ID="ID_670411844"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true" ID="ID_1209881754">
                                <node TEXT="name" FOLDED="true" ID="ID_781783958">
                                    <node TEXT="PENA" FOLDED="true" ID="ID_127803968"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1639237795">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true" ID="ID_1978103181"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1015440516">
                                    <node TEXT="peninsula" FOLDED="true" ID="ID_1626369924"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true" ID="ID_387768743">
                                <node TEXT="name" FOLDED="true" ID="ID_603515633">
                                    <node TEXT="POOR" FOLDED="true" ID="ID_1102837251"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1689300168">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true" ID="ID_206761685"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_827135136">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true" ID="ID_1977442402"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true" ID="ID_1120128808">
                                <node TEXT="name" FOLDED="true" ID="ID_1735246906">
                                    <node TEXT="ROCK" FOLDED="true" ID="ID_693005992"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_60593482">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true" ID="ID_368253486"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_357471695">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true" ID="ID_741629756"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true" ID="ID_1671795722">
                                <node TEXT="name" FOLDED="true" ID="ID_274946167">
                                    <node TEXT="SAND" FOLDED="true" ID="ID_744682730"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_324724897">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true" ID="ID_1219025282"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_490785441">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true" ID="ID_1526402820"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true" ID="ID_1763810681">
                                <node TEXT="name" FOLDED="true" ID="ID_1813731635">
                                    <node TEXT="SHRB" FOLDED="true" ID="ID_1131812978"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1532017455">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true" ID="ID_1661011749"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_594727075">
                                    <node TEXT="heavy shrub / brush" FOLDED="true" ID="ID_938822167"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true" ID="ID_1920708254">
                                <node TEXT="name" FOLDED="true" ID="ID_1221132722">
                                    <node TEXT="SOIL" FOLDED="true" ID="ID_1647404043"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1182796681">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true" ID="ID_240834364"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1704584095">
                                    <node TEXT="shallow soils" FOLDED="true" ID="ID_993969569"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true" ID="ID_108548469">
                                <node TEXT="name" FOLDED="true" ID="ID_87593091">
                                    <node TEXT="STEP" FOLDED="true" ID="ID_1093469922"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_125172620">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion." FOLDED="true" ID="ID_965854492"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_226282624">
                                    <node TEXT="steep slopes" FOLDED="true" ID="ID_1302551666"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true" ID="ID_784503520">
                                <node TEXT="name" FOLDED="true" ID="ID_1428434823">
                                    <node TEXT="WATR" FOLDED="true" ID="ID_993052407"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_48356723">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true" ID="ID_1305551844"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1384560759">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true" ID="ID_1562509115"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true" ID="ID_323833834">
                                <node TEXT="name" FOLDED="true" ID="ID_1144635885">
                                    <node TEXT="WETT" FOLDED="true" ID="ID_1129287960"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1008835101">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true" ID="ID_1171035339"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1320316001">
                                    <node TEXT="poorly drained - high water table" FOLDED="true" ID="ID_352630807"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="VERDATE:" FOLDED="true" ID="ID_1915072125">
                        <node TEXT="name" FOLDED="true" ID="ID_1889947937">
                            <node TEXT="VERDATE" FOLDED="true" ID="ID_407041998"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1034429577">
                            <node TEXT="Verification Status Date" FOLDED="true" ID="ID_1702984574"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_883468400">
                            <node TEXT="date" FOLDED="true" ID="ID_418629177"/>
                        </node>
                        <node TEXT="length" FOLDED="true" ID="ID_334531432">
                            <node TEXT="8" FOLDED="true" ID="ID_1369775711"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_1798488239">
                            <node TEXT="The verification status date attribute contains the date that the geographic unit  was verified/validated. " FOLDED="true" ID="ID_1455338225"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_553621925">
                            <node TEXT="english" FOLDED="true" ID="ID_1374924938">
                                <node TEXT="" FOLDED="true" ID="ID_1384623924"/>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_969850535"/>
                        </node>
                    </node>
                    <node TEXT="SENSITIV:" FOLDED="true" ID="ID_952814530">
                        <node TEXT="name" FOLDED="true" ID="ID_430057574">
                            <node TEXT="SENSITIV" FOLDED="true" ID="ID_871576124"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1969207321">
                            <node TEXT="Verification Status Date" FOLDED="true" ID="ID_908792567"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_675670651">
                            <node TEXT="date" FOLDED="true" ID="ID_1068729781"/>
                        </node>
                        <node TEXT="length" FOLDED="true" ID="ID_1296292159">
                            <node TEXT="8" FOLDED="true" ID="ID_1655439319"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_885825731">
                            <node TEXT="The verification status date attribute contains the date that the geographic unit  was verified/validated. " FOLDED="true" ID="ID_359617192"/>
                        </node>
                        <node TEXT="values" FOLDED="true" ID="ID_1232461303">
                            <node TEXT="yes:" FOLDED="true" ID="ID_1938709210">
                                <node TEXT="name" FOLDED="true" ID="ID_825202152">
                                    <node TEXT="yes" FOLDED="true" ID="ID_182260268"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1310374513">
                                    <node TEXT="Yes" FOLDED="true" ID="ID_970738814"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_801268625"/>
                            </node>
                            <node TEXT="no:" FOLDED="true" ID="ID_1088192574">
                                <node TEXT="name" FOLDED="true" ID="ID_832221117">
                                    <node TEXT="no" FOLDED="true" ID="ID_483131894"/>
                                </node>
                                <node TEXT="option" FOLDED="true" ID="ID_1998569855">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true" ID="ID_1162519492"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1534302513">
                            <node TEXT="english" FOLDED="true" ID="ID_810205551">
                                <node TEXT=" " FOLDED="true" ID="ID_556801770"/>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_997557456"/>
                            <node TEXT="sql" FOLDED="true" ID="ID_795641705"/>
                        </node>
                    </node>
                    <node TEXT="BED:" FOLDED="true" ID="ID_1571338412">
                        <node TEXT="name" FOLDED="true" ID="ID_835170808">
                            <node TEXT="BED" FOLDED="true" ID="ID_577831666"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_1972413514">
                            <node TEXT="Business Effective Date" FOLDED="true" ID="ID_153722244"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_37900917">
                            <node TEXT="date" FOLDED="true" ID="ID_829430024"/>
                        </node>
                        <node TEXT="length" FOLDED="true" ID="ID_707125430">
                            <node TEXT="8" FOLDED="true" ID="ID_1101093095"/>
                        </node>
                        <node TEXT="definition" FOLDED="true" ID="ID_463068213">
                            <node TEXT="The business effective date attribute contains the date that the record becomes effective in relation to the business (i.e., the date MNRF became aware of its existence)." FOLDED="true" ID="ID_1275158166"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_1855156092">
                            <node TEXT="english" FOLDED="true" ID="ID_601232123">
                                <node TEXT=" " FOLDED="true" ID="ID_896319282"/>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1453802993"/>
                            <node TEXT="sql" FOLDED="true" ID="ID_581861883"/>
                        </node>
                    </node>
                    <node TEXT="STKG:" FOLDED="true" ID="ID_1703819329">
                        <node TEXT="name" FOLDED="true" ID="ID_1865135787">
                            <node TEXT="STKG" FOLDED="true" ID="ID_933902488"/>
                        </node>
                        <node TEXT="alias" FOLDED="true" ID="ID_454188404">
                            <node TEXT="Stocking Value" FOLDED="true" ID="ID_528742789"/>
                        </node>
                        <node TEXT="type" FOLDED="true" ID="ID_831245973">
                            <node TEXT="float" FOLDED="true" ID="ID_1509075438"/>
                        </node>
                        <node TEXT="length" FOLDED="true" ID="ID_1370707382">
                            <node TEXT="8" FOLDED="true" ID="ID_1459569572"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" ID="ID_152415637">
                            <node TEXT="english" FOLDED="true" ID="ID_1516697663">
                                <node TEXT="Valid numeric values are from 0.0 to 5.0" FOLDED="true" ID="ID_455924016"/>
                            </node>
                            <node TEXT="python" FOLDED="true" ID="ID_1603137505">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be between 0.0 and 5.0' for row in [row] if  row['${name}'] not in [ None, '', ' ' ]  if row['${name}'] &gt; 5.0 or  row['${name}'] &lt; 0.0   ]" FOLDED="true" ID="ID_1369026733"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true" ID="ID_978292901">
                    <node TEXT="english" FOLDED="true" ID="ID_1297823861">
                        <node TEXT="POLYID must contain unique values" FOLDED="true" ID="ID_1847177353"/>
                        <node TEXT="This table must contain all mandatory fields." FOLDED="true" ID="ID_473974032"/>
                    </node>
                    <node TEXT="python" FOLDED="true" ID="ID_597336895"/>
                    <node TEXT="sql" FOLDED="true" ID="ID_776210561">
                        <node TEXT="[ 'ST1: &lt;b&gt;&lt;mark&gt;POLYID&lt;/mark&gt;&lt;/b&gt; field values must be unique' for f in ['POLYID' ] if not self.uniqueValueTest(f) ]" FOLDED="true" ID="ID_165846143"/>
                    </node>
                </node>
                <node TEXT="Description" FOLDED="true" ID="ID_1726513376">
                    <node TEXT="The Planning Composite Inventory is conceptually an update of the 2009 specification of the Planning Composite updated to use the forest value description from the Enhanced Forest Resource Inventory" FOLDED="true" ID="ID_1881738369"/>
                </node>
                <node TEXT="DomainValidation:" FOLDED="true" ID="ID_1109294052">
                    <node TEXT="english" FOLDED="true" ID="ID_637793416">
                        <node TEXT="Identify all species that occur in OSPCOMP / USPCOMP" FOLDED="true" ID="ID_1520525110"/>
                        <node TEXT="Age class distributions by" FOLDED="true" ID="ID_1083806826"/>
                    </node>
                </node>
            </node>
        </node>
    </map>
