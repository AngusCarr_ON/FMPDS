    <map version="1.0.1">
        <node TEXT="FMP_2017">
            <node TEXT="PlanningCompositeInventory:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlanningCompositeInventory" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PCI" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planning Composite Inventory (PCI)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26915" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="POLYID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="25" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="POLYID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polygon Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The planning inventory polygon identifier attribute is a unique id or label for the polygon often based on geographic location." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: Attribute must contain a unique value" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="POLYTYPE:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="POLYTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="POLYTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Polygon Type" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The polygon type attribute indicates the classification of the area within the polygon boundaries into one of several generalized water and land types." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The polygon type is determined from the classification of area on a forest management unit into different water and land types. Some polygon types are derived from inventory base features, while other polygons must be created from a classification process.  All remaining land areas within a designated forest management unit are classified into various non-forested or forested lands." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: If POLYTYPE attribute does not equal FOR, then all forest-specific attributes should be empty.  CHECKED IN INDIVIDUAL ATTRIBUTES, NOT POLYTYPE PYTHON CODE." FOLDED="true">
                                    <icon BUILTIN="bookmark"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: If a delineated polygon is an island or is located on an island, the management consideration attribute is set to island MGMTCON1 = ISLD. This applies to all polygon types. This apparent redundancy in classification for polygons having a type of island i.e., POLYTYPE = ISL   and   MGMTCON1 = ISLD allow resource managers to easily identify:   1. all polygons located on islands regardless of polygon type by querying on MGMTCON1 = ISLD  2. just the small uninterpreted islands by querying on POLYTYPE = ISL depending upon the desired analysis." FOLDED="true">
                                    <icon BUILTIN="bookmark"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST2: ${name} ISL implies MGMTCON1 ISLD' ${:}  row['${name}'] == 'ISL' and row['MGMTCON1'] != 'ISLD' and ${FRI} ]" FOLDED="true"/>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None,'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'ISL', 'MGMTCON1':'NONE', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None } " FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="FOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas that are capable of producing trees and can support tree growth.  These areas may or may not be capable of supporting the harvesting of timber on a sustained yield basis.  Some areas may have physical and/or biological characteristics which effect land use.  Thus this polygon type includes both production and protection forest areas." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Productive forest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node STYLE="fork" TEXT="All water areas.  Includes lakes, ponds, reservoirs i.e., inland basin areas containing water and wide two sided rivers.  These are rivers that can be defined by area.  Generally, these rivers and streams are consistently wider than 20 meters when portrayed at a mapping scale of 1:10,000 or consistently wider than 40 meters when portrayed at a mapping scale of 1:20,000.  Smaller or narrower rivers and streams are maintained as linear features in a centre-line layers." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Lands which are cultivated for growing crops, orchards, floral gardens, etc. These areas may include abandoned agricultural lands." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Developed agricultural land" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="GRS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GRS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Farm areas devoted to pasture for domesticated animals. These areas may also include abandoned grass and meadows, but are not part of the productive forest land base and do not include barren and scattered areas. These areas are similar to barren and scattered, but are located near developed agriculture land or unclassified areas and are usually fenced." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Grass and meadow" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Islands less than 8 hectares in size, down to a lower limit of 0.0025 hectares or 25 square meters in size e.g., 5 meters X 5 meters are recorded during the inventory production process, but are not interpreted or typed for practicality and cost considerations.  Only islands 8 hectares and larger are interpreted and assigned an appropriate POLYTYPE code, such as FOR or BSH." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Small island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="UCL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UCL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non-forested areas which were created for specific uses other than timber production, such as  roads, railroads, logging camps, mines, utility corridors, logging camps, gravel pits, airports, etc." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unclassified" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BSH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas covered with non-commercial tree species or shrubs.  These areas are normally associated with wetlands or water features." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Brush and alder" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="RCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas of barren or exposed rock e.g., bedrock, cliff face, talus slope which may support a few scattered trees, but is less that 25 percent crown closure." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Rock" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OMS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OMS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Wet areas of mosses, grasses, sedges, and small herbaceous plants, often interspersed with small areas of open water" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Open Wetland" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="TMS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="TMS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas of dry or wet muskeg on which stunted trees occur as widely spaced individuals or in small groups." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Treed wetland" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OWNER:" FOLDED="true">
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OWNER" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="OWNER" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Ownership" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="This ownership attribute contains the traditional FRI ownership information as assigned by the Office of the Surveyor General." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The ownership designation attribute is derived from the ownership and land tenure, and parks and reserves layers which are maintained in the MNRFs values information system. This attribute also identifies the managed Crown area in a forest management unit. The ownership information is used to create table FMP-1 Part B, Section 8.0." FOLDED="true"/>
                            <node TEXT="Any discrepancies regarding the information contained in the ownership designation attribute should be reported to the appropriate MNRF district office. The most accurate source for ownership information is located in the appropriate regional Land Registry Office." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'u'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'1'}" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="None" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unknown or unassigned ownership" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown Managed" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Crown Land" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Patented Crown Timber" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Patent land with timber rights reserved to the Crown" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non Crown" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Patented land fee simple private" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non Crown" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Patented land Company Freehold" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="5:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="5" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown Other" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Provincial Park" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="6:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="6" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non Crown" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Indian Reserve" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="7:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="7" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown Other" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Recreation Reserve" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="8:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="8" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Non Crown" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Agreement Forest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="9:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="9" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown Federal" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Federal Reserve" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="1 " FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="YRSOURCE:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRSOURCE" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Data Update" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The year of data update attribute contains a four digit number representing the calendar year (January 1 to December 31) of the source data used to determine update the polygon description, in particular the height attribute." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The year of data update identifies the calendar year that information about a polygon was last confirmed or modified based on field inspection, photo interpretation, analysis of satellite imagery, or conversion or update to spatial or tabular data. The year of data update should not be changed to reflect error corrections to tabular attributes." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: YRSOURCE must be a year less than the plan period start year" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be a year less than the plan period start year' ${:N} row['${name}']  &gt;= ${planyear}  ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct format' ${:N} row['${name}']  &lt; 1000  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;YRSOURCE&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':999 }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct format" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':${planyear} }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;YRSOURCE&lt;/mark&gt;&lt;/b&gt; must be a year less than the plan period start year" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;YRSOURCE&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="2017" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SOURCE:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SOURCE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SOURCE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Source of Update Data " FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The source of data update attribute identifies the methodology by which the information stored in the other tabular attributes that are associated with the same polygon was determined (i.e., how the polygon description was determined)." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The source of data update attribute does not apply to the changes or updates made to the ecosite attribute." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A value of FORECAST is only valid for the SOURCE attribute in the Base Model Inventory." FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the DEVSTAGE attribute starts with EST, then the SOURCE attribute should not equal ESTIMATE" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                <node TEXT="['ST1: A value of FORECAST is only valid for the ${name} attribute in the Base Model Inventory' ${:} '${tablename}' not in [ 'BaseModelInventory' ] and row['${name}'] == 'FORECAST' ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be ESTIMATE when DEVSTAGE LIKE EST or NAT' ${:}  row['${name}']  == 'ESTIMATE' and row['DEVSTAGE'] in [ 'ESTNAT','ESTPLANT','ESTSEED', 'NAT' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'FORECAST' }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: A value of FORECAST is only valid for the ${name} attribute in the Base Model Inventory" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'ESTIMATE', 'DEVSTAGE':'ESTNAT'} " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be ESTIMATE when DEVSTAGE LIKE EST or NAT" FOLDED="true">
                                        <icon BUILTIN="full-6"/>
                                    </node>
                                </node>
                                <node TEXT="{'${name}':'PLOTFIXD', 'COMMENT':&quot;Specific test for the SuperChecker&quot;}" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="BASECOVR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BASECOVR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Information that is provided by the MNRF  (e.g., water or evaluated wetlands)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="planimetric base layer" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTIMATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTIMATE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This source option is only for use in areas that have been recently renewed and have not been revisited since the renewal work was performed.  That is, where a follow-up survey has not yet been performed.  Therefore, the description of the newly regenerated stand is a 'best estimate' of the expected outcome / result of the renewal treatment that was applied to the area based on past silvicultural successes." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="expected / estimated outcome / result" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photo-interpreted by softcopy systems" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - manual process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Digital analysis, automated processing. (e.g., Ecognition)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - automated process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Inspection of a site after silvicultural treatment to determine whether an operator / operations conforms to the approved plan or permit.   The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest operations compliance inspection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FORECAST:" FOLDED="true" BACKGROUND_COLOR="#99ff99">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FORECAST" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates a polygon description that was updated based on expected outcomes of planned operations (which have not yet been implemented) for the remainder of the current plan term.  This code is only valid in the Base Model Inventory. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forecasted description" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Current polygon description based on data conversion from traditional FRI" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest resources inventory conversion" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="INFRARED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="INFRARED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Infrared satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription.  Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pre-harvest site inspection / marking" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="aerial survey / reconnaissance" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ocular estimate (ground)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OPC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Measuring standing trees to determine the volume of wood on a given tract of land." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="operational cruise" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale larger than 1:10,000  (e.g., 1:500, 1:1000)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="large scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale smaller than 1:20,000  (e.g., 1:100,000)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="small scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Fixed area plot" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fixed area plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Variable area plots." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="variable area (radius) plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="RADAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RADAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="radar satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Survey of a regenerated area to determine how well the new stand is growing.  This includes seeding, survival, and stocking assessments.." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regeneration assessment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).   Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="extensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).  Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="intensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SPECTRAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SPECTRAL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spectral satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SUPINFO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUPINFO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Stand update information from a source other than those listed/defined in the other coding options that is provided by either MNR or Licensee. For example, disturbance records. The date of information capture/acquisition and the age of the stand as of the date of information was acquired must be supplied for the information to be incorporated into the inventory." FOLDED="true">
                                        <icon BUILTIN="closed"/>
                                    </node>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Supplied information" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="DIGITALP" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="FORMOD:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FORMOD" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="FORMOD" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The productive forest modifier attribute represents a further classification (sub-division) of productive forest areas based on the presence or absence of physical or biological factors which may influence the ability to practice forest management." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The productive forest modifier must be identified for every productive forest stand and must be used in conjunction with the management consideration attribute." FOLDED="true"/>
                            <node TEXT="There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement.  Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a forest management decision (FORMOD) that is based on more than just site class." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: IF SC attribute equals 4, then FORMOD should be PF (see SC - BMI/OPI only)" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                    <icon BUILTIN="password"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'RP' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="RP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas at various stages of growth and development, including areas that have been recently disturbed (by harvest or natural causes) or renewed (by artificial or natural means), that are capable of producing adequate growth of timber to support harvesting on a sustained yield basis.  These areas have no significant physical or biological limitations on the ability to practice forest management, but may include areas which pose an operational challenge in terms of harvest, access, protection, silviculture, or renewal." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Production Forest - Regular" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which are considered to be production forest, but that are unavailable for timber production as determined through the forest management planning process.  That is, these areas have been identified as no-cut areas through area of concern planning for the purpose of protecting values (e.g., reserve buffers applied to protect tourism values)." FOLDED="true"/>
                                    <node TEXT="Designated management reserves are areas that were managed as an operational reserve during previous forest management plan terms. That is, they are actual operational or management reserve areas created by the implementation of forest management operations around them. As guides are updated, these areas should be reviewed as some may return to production forest. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Production Forest - Designated Management Reserve" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PF" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Protection Forest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="DEVSTAGE:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DEVSTAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Stage of Development" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="DEVSTAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The stage of development attribute indicates the current state of growth and development for a productive forest stand.  Note that some stands stages are best described based on the last major silvicultural treatment that was applied to a stand, if the stand is being managed for timber production." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The stage of development attribute is not a derived attribute and is commonly modified from the first submission in the FRI. The population of this field requires a management decision based on both the current state and the past treatments. As an example, a forest stand was planted and the plantation failed (e.g. ingress, insect damage) yet it eventually met the regeneration standards and was declared established. The forest manager will now need to decide whether this stand receives a code of ESTPLANT or ESTNAT depending on the presence and potential influence of the planted stems that may still remain. The development stage assignment will be independent of the yield assignment despite the fact that there may be a connection between the two. The stage of development will be used to populate the stage of management in the landbase tables in the FMP  (i.e. FMP-3 and FMP-5). " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the OSTKG attribute equals 0.00, then the DEVSTAGE attribute should be either DEP* when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST2: If crown closure is less than 25 percent (UCCLO + OCCLO &lt; 25) then the DEVSTAGE attribute should be LOWMGMT, LOWNAT, DEPHARV or DEPNAT or NEWNAT or NEWPLANT" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be DEP* or NEW* when POLYTYPE is FOR and OSTKG is 0.00 or NULL' ${:NF} row['${name}'] not in [  'DEPHARV','DEPNAT','NEWNAT','NEWPLANT','NEWSEED'] and row['OSTKG'] in [ 0, '', ' ', None ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST2: If crown closure is less than 25 percent then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be LOWMGMT, LOWNAT, DEPHARV or DEPNAT, NEWNAT, NEWPLANT when POLYTYPE = FOR' ${:F} row['OCCLO'] not in [ None, '', ' ' ] and row['UCCLO'] not in [ None, '', ' ' ]   and row['${name}'] not in ['LOWMGMT' , 'LOWNAT', 'DEPHARV', 'DEPNAT', 'NEWNAT', 'NEWPLANT' ]  if float(row['UCCLO'])+float(row['OCCLO'])  &lt; 25  ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR','OSTKG':1 }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NAT', 'OSTKG':0, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be DEP* or NEW* when POLYTYPE is FOR and OSTKG is 0.00 or NULL" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NAT', 'OCCLO':12, 'UCCLO':0, 'POLYTYPE':'FOR', 'OSTKG':1 }" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST2: If crown closure is less than 25 percent then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be LOWMGMT, LOWNAT, DEPHARV or DEPNAT, NEWNAT, NEWPLANT when POLYTYPE = FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'NAT' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NAT', 'OCCLO':None, 'UCCLO':None, 'POLYTYPE':'FOR', 'OSTKG':1 }" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="DEPHARV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPHARV" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest area that was recently disturbed by clearcut harvesting and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer that would be released and/or protected as part of the depletion operation." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by harvest and has received no regeneration/renewal treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DEPNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest area that was recently disturbed by natural causes (i.e., fire, blowdown, ice damage, insect and disease) and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by natural causes and has received no regeneration / renewal treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LOWMGMT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWMGMT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest stands which were previously harvested and have not reached the regeneration standards as described in an approved FMP.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., crown closure less than 25%)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to past management" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LOWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest stands which were previously disturbed by natural causes and have not reached the regeneration standards as described in an approved FMP.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., crown closure less than 25 percent)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to natural causes / succession" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by planting, but have not been assessed as established" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly planted" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by seeding, but have not been assessed as established" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly seeded" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by natural means, but have not been assessed as established (e.g. CLAAG, HARP)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly natural regeneration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly from planted stock and which have been assessed as established." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="established mainly planted" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly by seeding and which have been assessed as established." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="established mainly seeded" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest harvest areas which were regenerated predominantly by natural means and which have been assessed as established." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="established  mainly natural regeneration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural stands" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This classification will be used to describe the natural forest areas that have never been treated to date (original forest)." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Established productive forest areas which have received a mid-rotation thinning/spacing to promote the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received pre-commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINCOM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINCOM" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Established productive forest areas which have received a mid-rotation partial harvest designed to promote the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BLKSTRIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLKSTRIP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the stand in progressive strips or blocks in more than one operation. Strip and block harvest methods are prescribed to encourage natural regeneration, provide wildlife habitat, protect fragile sites, or for aesthetics." FOLDED="true"/>
                                    <node TEXT="The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters wide.  It is designed to encourage regeneration on difficult and/or fragile sites." FOLDED="true"/>
                                    <node TEXT="Note:  Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: block or strip" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged, silvicultural system  that retains mature standing trees  scattered throughout the cutblock to  provide seed sources for natural  regeneration.  " FOLDED="true"/>
                                    <node TEXT="A method of harvesting and  regenerating a forest stand in which  all trees are removed from the area  except for a small number of seed- bearing trees that are left singly or in  small groups. The objective is to  create an even-aged stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut:  seed tree " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A partial harvest where the first harvest operation removes target/specific merchantable tree species from a forest stand. The remaining species are merchantable and are intended to be harvested by another logger/contractor/forest resource licence holder in the next pass.  A first pass should be recorded if merchantable tree species remain in the forest stand which have been allocated for harvest - but have not yet been harvested." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PREPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PREPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source and to improve conditions for seed production and natural regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a preparatory cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a seed cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRSTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRSTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a first removal harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LASTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LASTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where all of the remaining trees in the overstorey are removed. This is the removal of the seed or shelter trees after the regeneration has been effective." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a final removal harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition, distribution and quality by removing less desirable trees of any species." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received an improvement cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SELECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SELECT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a selection harvest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="NAT" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="YRDEP:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRDEP" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Last Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The year of last disturbance attribute indicates a four digit number of the most recent (or latest) fiscal year (April 1 to March 31) that a productive forest area was disturbed, completely or partially, by harvest or by natural causes. This includes mid-rotation or stand improvement operations where merchantable timber is removed. This is actual and known disturbances and not calculated from year of origin." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The inventories will be populated with all disturbances (harvest and natural) which are identified in annual reports. The year of last disturbance for each productive forest stand should correspond to the year that the disturbance occurred, as recorded in the applicable annual reports.  Forest stands that are managed under the selection or shelterwood silvicultural systems must also have a year of last disturbance. The year of last disturbance provides an estimate of the stage of forest stand development within a cutting cycle for selection stands or within the current stage of a shelterwood system.  In the case of a partial harvest where certain tree species have been removed, such as a first pass where merchantable trees remain in a forest stand, the year that the first pass was carried out must be entered as the year of last disturbance. If an additional harvest or pass is conducted, the year of last disturbance is changed to the year in which the most recent harvest/pass operation was carried out.  Commercial thinning is a mid-rotation treatment that is recorded as a disturbance and the stand area is considered as disturbed area in forest management planning. Forest stands that have received a commercial thinning must show the applicable year of last disturbance in the forest polygon coverage.  The year of last disturbance is also used to identify other stand improvement operations. A stand improvement operation is normally associated with the selection system.   Year of last disturbance must not be used to indicate tending operations, such as a chemical or manual release which are required to bring a forest stand to regeneration standards. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The YRDEP must be a year less than the plan period start year" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: The value must be greater than or equal to 1900 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be greater than or equal to 1900 unless it is 0' ${:N}  row['${name}']  &lt;1900 ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be a year less than the plan period start year' ${:N} row['${name}']  &gt;= ${planyear}]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':2010 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':2020, 'DEPTYPE':'ICE' }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;YRDEP&lt;/mark&gt;&lt;/b&gt; must be a year less than the plan period start year" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1850, 'DEPTYPE':'ICE' }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;YRDEP&lt;/mark&gt;&lt;/b&gt; should be greater than or equal to 1900 unless it is 0" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':0}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true"/>
                    </node>
                    <node TEXT="DEPTYPE:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DEPTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Type of Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The type of disturbance attribute identifies the disturbance that occurred in the year recorded in the companion attribute YRDEP (year of disturbance).  The disturbance may have affected the entire stand or only a portion of it." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct coding scheme" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme where YRDEP &lt;&gt; 0" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A null or blank value is not a valid code where YRDEP &lt;&gt; 0" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: If the development stage is depleted, new or established (DEVSTAGE = DEP, NEW or EST) then the disturbance type should not have a value of unknown (DEPTYPE = UNKNOWN)" FOLDED="true" BACKGROUND_COLOR="#ffffff">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: If the disturbance type is not null (DEPTYPE &lt;&gt; null) then the disturbance year should not be equal to zero (YRDEP = 0). Should actually be &gt;= 1900" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="password"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be populated when YRDEP is populated'  ${:F} ${NIN} and row['YRDEP'] not in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be UNKNOWN if DEVSTAGE are DEP, NEW or EST' ${:} row['${name}'] == 'UNKNOWN' and row['DEVSTAGE'] in ['DEPHARV', 'DEPNAT', 'NEWPLANT', 'NEWSEED', 'NEWNAT', 'ESTPLANT', 'ESTSEED', 'ESTNAT'] ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: YRDEP should be &gt;= 1900 when &lt;mark&gt;${name}&lt;/mark&gt; is populated'  ${:NF}  row['YRDEP'] not in ${list_NULL} and row['YRDEP'] &lt; 1900 ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'YRDEP':1999 , 'POLYTYPE':'FOR' }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None, 'YRDEP':1999 }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be populated when YRDEP is populated" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'UNKNOWN', 'DEVSTAGE':'NEWNAT', 'YRDEP':1999 }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be UNKNOWN if DEVSTAGE are DEP, NEW or EST" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'FIRE', 'YRDEP':1799 }" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST2: YRDEP should be &gt;= 1900 when &lt;mark&gt;${name}&lt;/mark&gt; is populated" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'ICE' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BLOWDOWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLOWDOWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="wind / blowdown" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DISEASE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DISEASE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="disease" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DROUGHT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DROUGHT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="drought" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fire" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FLOOD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FLOOD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="flood" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="HARVEST:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="HARVEST" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Partial or full stand removal of timber.  This includes mid-rotation or stand improvement operations where merchantable timber is removed." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ICE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ICE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ice damage" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="INSECTS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="INSECTS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="insects" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SNOW:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SNOW" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="insects" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="UNKNOWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UNKNOWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unknown" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="None" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OYRORG:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Overstorey Year of Origin" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OYRORG" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The overstorey  year of origin attribute contains a four digit number representing the average year that the predominant species (i.e., the species within the stand having the greatest relative abundance in terms of basal area), came into existence." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The overstorey year of origin can be used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration.   Once a forest stand has been assessed as established, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site.   In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered.  For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach the regeneration standards from the forest management plan), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip is established, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required.  Normally, updates to the overstorey year of origin have a corresponding change to the year of data update attribute as this indicates the currency/vintage of the information and when the overstorey year of origin value was determined.   Overstorey year of origin information is not required for non-forested and non-productive forest land types. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The OYRORG attribute value must be greater than or equal to 1600 when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The OYRORG attribute must be less than the plan period start year when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="The OYRORG attribute value should not be greater than YRSOURCE attribute value" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 1600' ${:NF} row['${name}']  &lt;1600 ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than plan period start year when POLYTYPE equals FOR' ${:NF}  row['${name}']  &gt;=${planyear} ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than or equal to YRSOURCE when POLYTYPE equals FOR' ${:NF}  row['${name}']  &gt; row['YRSOURCE']   ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':1999 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1599, 'YRSOURCE':2000 } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 1600" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':2029, 'YRSOURCE':2030  } " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than plan period start year when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1981, 'YRSOURCE':1980  } " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than or equal to YRSOURCE when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="1950" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OSPCOMP:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Overstorey Species Composition" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="120" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OSPCOMP" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="This species composition attribute indicates the tree species that are present in the stand canopy and the relative proportion of the canopy that each species occupies." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="The most common species codes (based on all the inventories as of 2009) are listed below and for the full list of species see the coding list from OSPCOMP in the FIM Forest Resource Inventory Technical Specifications.  In these tables, codes related to individual species are listed in mixed case (e.g., Bw, La) and codes related to groups such as all conifer or all spruce are listed in uppercase (e.g., OC, SX).  Even though the codes are listed this way, the letters may be entered in any case combination the data submitter desires.  For example, white birch may be entered as BW, bw, Bw, or bW.   repeating pattern of species code and corresponding proportion value each species code is 3 characters (including blanks) and is left justified each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified.   maximum of 20 species and proportions pairs in the string " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="pattern is SSSPPPSSSPPP" FOLDED="true"/>
                            <node TEXT="no duplicate species codes allowed in the string" FOLDED="true"/>
                            <node TEXT="A blank or null value is not a valid code" FOLDED="true"/>
                            <node TEXT="proportion values in the string must sum to 100" FOLDED="true"/>
                            <node TEXT="The tree species in the composition are to be coded using the scheme listed here." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: No duplicate species codes allowed in the string" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Proportion values in the string must sum to 100" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The tree species in the composition are to be coded using the scheme listed here" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="If SOURCE is NOT plot based then allow specific species in SPCOMP (eFRI tech spec)" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                </node>
                                <node TEXT="Tree species in the list [     'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn',     'CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf',     'OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX',     'OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX',     'Sb','Sw','la','Wb','Wi'     ]" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST%s: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; %s'%( '2' if 'WARNING' in i else '1'   ,i)    for i in test_spcomp(row['${name}']) if ${NINN} if i not in  [ None, [ None ] , []  ] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; - Invalid species code' for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if ${FOR} and ${NINN} ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="EFRI Only" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; Invalid species code where SOURCE is not plot' for s in test_SpeciesInList(row['${name}'],  [ 'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi' ]         )  if ${FOR} and ${NINN} and row['SOURCE'][:4] != 'PLOT'    ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; - Invalid species code where SOURCE is plot-based' for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if ${FOR} and ${NINN} and row['SOURCE'][:4] == 'PLOT'  ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Ob  90Po  10', 'SOURCE':'PLOTVAR'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Ob  90Po  10', 'SOURCE':'SPECTRAL'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; Invalid species code where SOURCE is not plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'Sb 100' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR' }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb100'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP not multiples of 6 characters: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb NaN'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPComp must have numeric codes for species occurrence: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb  50Sw  50Po  50'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP does not add to 100: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb  50Sb  50'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP cannot have duplicate species occurrences: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Po  40Sb  60', 'OLEADSPC':'Sb' }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; WARNING SPCOMP values are not in Descending Order: " FOLDED="true"/>
                                </node>
                                <node COLOR="#338800" TEXT="{'POLYTYPE':'FOR','${name}':'Oz  90Po  10', 'SOURCE':'SPECTRAL'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; - Invalid species code" FOLDED="true"/>
                                </node>
                                <node COLOR="#338800" TEXT="{'POLYTYPE':'FOR','${name}':'Cw  90Po  10', 'SOURCE':'SPECTRAL', 'OLEADSPC':'Cw'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="SB 100" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OLEADSPC:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OLEADSPC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Overstorey Leading Species" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The leading species  attribute indicates the most prevalent species in the forest stand (or in just the uppermost canopy layer if the stand canopy contains two or more distinct layers) based on its percentage of crown closure." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="This leading species attribute replaces the working group attribute (WG) that was present in previous inventory products for determining the major species to manage for in a forest stand." FOLDED="true"/>
                            <node TEXT="use the same coding as is listed in the OSPCOMP (species composition) attribute description" FOLDED="true"/>
                            <node TEXT="must be species listed in the overstorey species composition (OSPCOMP)" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="The tree species in the understorey leading species are to be coded using the scheme listed in OSPCOMP. This test is implied by the requirement to have the OLEADSPC in the OSPCOMP, which are already tested." FOLDED="true"/>
                                <node TEXT="ST1: The attribute value must be a species listed in the overstorey species composition (OSPCOMP) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be the species with the greatest percentage or tied for the greatest percentage in the species composition (OSPCOMP) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be listed in the OSPCOMP' ${:NF} row['${name}'] not in row['OSPCOMP']  ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be the most common species in OSPCOMP if populated' ${:NF} row['${name}'] in row['OSPCOMP'] if row['${name}'].upper() not in  get_leadspc(row['OSPCOMP'])   ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None,  '${name}':'Sb' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Po', 'OSPCOMP':'Sb 100'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be listed in the OSPCOMP" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Pt', 'OSPCOMP':'Sb  90Pt  10'}" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be the most common species in OSPCOMP if populated" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Bf ', 'OSPCOMP':'Bf  50Sb  40La  10' }" FOLDED="true">
                                    <icon BUILTIN="smily_bad"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be the most common species in OSPCOMP if populated" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="tests not being performed...:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR', '${name}':'XXX', 'USPCOMP':'XXX100' }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: XXX is not a valid species code. This effectively tests that the species code is a valid code... The warning is going to happen on the USPCOMP test." FOLDED="true"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must match the left three chars of USPCOMP if populated' ${:NF} row['${name}'].strip(' ') != row['USPCOMP'][:3].strip(' ')  and row['${name}'] in row['USPCOMP']  ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;ULEADSPC&lt;/mark&gt;&lt;/b&gt; must match the left three chars of USPCOMP if populated" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="Sb" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OAGE:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Overstorey Age" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The overstorey age  attribute contains the average age of the leading species in the overstorey canopy layer of the forest stand." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A null or blank value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A zero value is only a valid code if DEVSTAGE is DEPHARV or DEPNAT when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; can be zero only when DEVSTAGE is DEPHARV or DEPNAT (when POLYTYPE = FOR).' ${:F} ${NIN} and row['DEVSTAGE'] not in ['DEPHARV','DEPNAT'] ]   " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ '${name}':12, 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None }" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':0, 'DEVSTAGE':'NAT' } " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; can be zero only when DEVSTAGE is DEPHARV or DEPNAT (when POLYTYPE = FOR)." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="80" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OHT:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4.1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Double" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OHT" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Overstorey Height" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The overstorey height attribute indicates the estimated average tree height (in meters) of the predominant overstorey species as inventoried in the year of data update.  Estimates can be made from interpreted crown closure or field samples, or from growth algorithms." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="If the DEVSTAGE attribute does not start with DEP, NEW, or LOW, then the HT attribute must be greater than zero" FOLDED="true"/>
                            <node TEXT="Valid numeric values are from 0 through 40.0" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A null or blank value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the DEVSTAGE attribute does not start with DEP, NEW, or LOW, then the OHT attribute must be greater than zero" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE is FOR' ${:F}  row['${name}'] in [ '',' ', None ] and row['DEVSTAGE']  not in [ 'DEPHARV','DEPNAT','NEWNAT','NEWPLANT','NEWSEED','LOWNAT',' LOWMGMT' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' ${:NF}  (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than 0 if DEVSTAGE is not DEPHARV,DEPNAT, NEWNAT, NEWPLANT, NEWSEED, LOWNAT or LOWMGMT when POLYTYPE is FOR' ${:F} row['${name}'] == 0 and  row['DEVSTAGE']  not in [ 'DEPHARV','DEPNAT','NEWNAT','NEWPLANT','NEWSEED','LOWNAT',' LOWMGMT' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ '${name}':12, 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None, 'DEVSTAGE':'NAT' } " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':42}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':0, 'DEVSTAGE':'NAT' } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;OHT&lt;/mark&gt;&lt;/b&gt; must be greater than 0 if DEVSTAGE is not DEPHARV,DEPNAT, NEWNAT, NEWPLANT, NEWSEED, LOWNAT or LOWMGMT when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="20" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OCCLO:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OCCLO" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Overstorey Crown Closure" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The Overstorey Crown Closure  attribute contains the percent of crown closure of the forest stand (or of just the uppermost canopy layer if the stand canopy contains two or more distinct layers). Crown closure is defined as the percentage of ground area covered by the vertical projection of the tree crowns onto the ground." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="A zero value is a valid code" FOLDED="true"/>
                            <node TEXT="The maximum overstorey crown closure value is 100 percent." FOLDED="true"/>
                            <node TEXT="If an understorey crown closure value is greater than 0, then the total crown closure for the two layers must never exceed 200%  (i.e., OCCLO + UCCLO &lt;=  200). ** Change wording to greater than zero" FOLDED="true">
                                <icon BUILTIN="help"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero value is a valid code (doesn't need a check line)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be less than or equal to 100 and greater than or equal to zero (OCCLO &lt;= 100 and OCCLO &gt;=0)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE = FOR' ${:F} row['${name}'] in [ None, '', ' ' ] and row['POLYTYPE'] == 'FOR'  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 100 when POLYTYPE equals FOR' ${:NF} (row['${name}'] &lt; 0 or row['${name}'] &gt; 100)  ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':80 }" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE = FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':101, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 100 when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="70" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OSTKG:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="double" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OSTKG" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Overstorey Stocking" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The stocking attribute indicates a qualitative measure of the density of tree cover in a forest stand.  It is expressed as a percentage value ranging from zero, for recently disturbed stands, to a maximum of 5.00, although 2.50 is the typical maximum value encountered in the field.  Stocking of a forest stand refers to all species that make up the stand's canopy, but it is generally based on the species with the most basal area." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT=" Stocking is determined by comparing the actual basal area measured from field sampling to the relative basal area of a fully-stocked stand using Plonskis Normal Yield Tables. (Plonskis Normal Yield Tables were developed from permanent sample plots established for several of the major tree species in Ontario.) Stocking can also be determined from aerial photography based on the degree of canopy closure, average age, height, and species composition. Actual basal area collected from field sampling is used to calibrate stocking assessments made from photo-interpretation. In some cases, the regeneration and/or management standards of a silvicultural ground rule may be expressed as a density, which usually describes the frequency or number of stems per hectare. Where density information (stems/hectare) has been collected or determined from a regeneration survey, this information must be converted to a stocking value for the purpose of updating the forest stand description information. " FOLDED="true"/>
                            <node TEXT="The maximum crown closure value is 100 percent." FOLDED="true"/>
                            <node TEXT="If an understorey crown closure value is entered, then the total crown closure for the two layers must never exceed 200 percent  (i.e., OCCLO + UCCLO &lt;=  200)." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="The attribute population must follow the correct format" FOLDED="true"/>
                            <node TEXT="If the DEVSTAGE attribute starts with EST, then the OSTKG attribute must be greater than 0.00 **Specify OSTK (No USTKG in an EST)" FOLDED="true">
                                <icon BUILTIN="help"/>
                            </node>
                            <node TEXT="Valid numeric values are from 0 through 4.00" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A null or blank value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: Valid numeric values are from 0 through 4.00 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the DEVSTAGE attribute is not DEP or NEW then the stocking attribute must be greater than zero (OSTKG &gt; 0.00) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST2: If the DEVSTAGE attribute is NAT, starts with EST, or is IMPROVE, SELECT, SNGLTREE, THINCOM, FIRSTCUT, SEEDCUT, PREPCUT, FIRSTPASS, BLKSTRIP, or THINPRE then the stocking attribute should be greater than or equal to forty percent (OSTKG &gt;= 0.40)" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST2: If DEVSTAGE attribute starts with DEP, then the stocking attributes should equal zero (OSTKG + USTKG = 0.00)" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="OSTKG should be less than 2.5" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be null or blank when POLYTYPE equals FOR' ${:F} row['${name}']  in [ None, '', ' ' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 4.0 when POLYTYPE equals FOR' ${:NF} (row['${name}'] &lt; 0 or row['${name}'] &gt; 4.0) ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt; 0 if DEVSTAGE is not DEP* or NEW* when POLYTYPE is FOR' ${:F} row['DEVSTAGE'] not in [ 'DEPNAT','DEPHARV', 'NEWPLANT', 'NEWSEED', 'NEWNAT' ] and row['${name}'] &lt;= 0 ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be &gt;= 0.4 when DEVSTAGE is EST* or NAT, IMPROVE, SELECT, SNGLTREE, THINCOM, FIRSTCUT, SEEDCUT, PREPCUT, FIRSTPASS, BLKSTRIP or THINPRE when POLYTYPE is FOR.' ${:NF} row['DEVSTAGE']  in [ 'NAT','ESTNAT','ESTPLANT','ESTSEED','IMPROVE','SELECT','SNGLTREE','THINCOM','FIRSTCUT','SEEDCUT','PREPCUT','FIRSTPASS','BLKSTRIP','THINPRE' ] and row['${name}'] &lt; 0.4 and ${FRI_PCI} ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; plus USTKG must = 0.0 when DEVSTAGE is DEP* when POLYTYPE is FOR' ${:F} row['${name}'] not in [ None, '', ' ' ] and row['USTKG'] not in [ None, '', ' ' ] and row['DEVSTAGE']  in [ 'DEPNAT','DEPHARV' ] if row['USTKG'] + row['${name}'] != 0  and ${FRI_PCI} ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than 2.5' ${:NF} (row['${name}'] &gt; 2.5) and ${FRI_PCI}]" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':1.0 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be null or blank when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':-1, 'POLYTYPE':'FOR', 'DEVSTAGE':'NEWNAT'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 4.0 when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0, 'POLYTYPE':'FOR' , 'DEVSTAGE':'LOWNAT'} " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt; 0 if DEVSTAGE is not DEP* or NEW* when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0.1, 'POLYTYPE':'FOR' , 'DEVSTAGE':'SELECT'} " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be &gt;= 0.4 when DEVSTAGE is EST* or NAT, IMPROVE, SELECT, SNGLTREE, THINCOM, FIRSTCUT, SEEDCUT, PREPCUT, FIRSTPASS, BLKSTRIP or THINPRE when POLYTYPE is FOR." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0, 'POLYTYPE':'FOR' , 'DEVSTAGE':'DEPNAT', 'USTKG':1.0, 'UYRORG':None} " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; plus USTKG must = 0.0 when DEVSTAGE is DEP* when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':3.5, 'POLYTYPE':'FOR' }" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than 2.5" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="1.0" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="OSC:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OSC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Overstorey Site Class" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The overstorey site class attribute indicates a site quality estimate for a stand.  Overstorey site class is an indicator of site productivity and is determined using the average overstorey height, overstorey age, and leading species, based on the dominant tree species in a forest stand. These attributes are compared against height and age growth curves in Plonski's Normal Yield Tables for different species to determine the relative growth rate for a forest stand." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Zero is the default value for an integer field. When performing a query to identify the best (site class = 0) stands be sure to include polygon type in the query (and POLYTYPE = FOR) in order to exclude non-productive forest stands in the query results. There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement.  Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a forest management decision (FORMOD) that is based on more than just site class. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="a number from 0 through 4" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format (a number from 0 through 4)  when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The overstorey site class value must be greater than or equal to 0 and less than or equal to 4 (OSC &gt;=0 and OSC &lt;=4) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR' ${:F} row['${name}'] not in [ 0,1,2,3,4 ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':12 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':-1, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':28, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Best" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Better" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Good" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poor" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Very Poor" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UYRORG:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Year of Origin" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UYRORG" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey year of origin attribute indicates the average year that the species having the greatest relative abundance (in terms of basal area) within the understorey, came into existence." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The value must be greater than or equal to 1600" FOLDED="true"/>
                            <node TEXT="The value should be greater than OYRORG" FOLDED="true"/>
                            <node TEXT="The UYRORG attribute must be less than the plan period start year" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory when POLYTYPE is equal to FOR and VERT starts with T or M (Two tiered or Mainly two-tiered)" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then attribute must equal zero" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when VERT is equal to TO, TU, MO or MU" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1:The UYRORG attribute value must be greater than or equal to 1800 when POLYTYPE is equal to FOR and VERT is equal to TO, TU, MO or MU" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The value should be greater than OYRORG (UYRORG &gt; OYRORG) when POLYTYPE is equal to FOR and VERT is equal to TO, TU, MO or MU" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The UYRORG attribute must be less than the plan period start year when POLYTYPE is equal to FOR and VERT is equal to TO, TU, MO or MU" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST2: The UYRORG attribute value should not be greater than YRSOURCE attribute value when POLYTYPE is equal to FOR and VERT is equal to TO, TU, MO or MU" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 or Null if DEVSTAGE is DEPHARV or DEPNAT' ${:N} row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not equal 0, null or blank  in two- or multi-canopy stands' ${:V} ${NIN} ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node COLOR="#111111" TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 1800' ${:NV} row['${name}']  &lt;1800  ]" FOLDED="true">
                                    <edge COLOR="#111111" WIDTH="thin"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be greater than OYRORG' ${:NV} row['OYRORG']  not in [None, '', ' ', 0 ]  and  row['${name}']  &lt; row['OYRORG']    ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than plan period start year when POLYTYPE equals FOR' ${:NV} row['${name}']  &gt;= ${planyear}   ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than or equal to YRSOURCE when POLYTYPE equals FOR' ${:NV}  row['YRSOURCE']   not in ${list_NULL} and (row['${name}']  &gt; row['YRSOURCE'])     ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':1999 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1999, 'DEVSTAGE':'DEPNAT', 'OSTKG':0, 'USTKG':0, 'STKG':0 } " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 or Null if DEVSTAGE is DEPHARV or DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE': 'FOR', 'VERT': 'TU', '${name}':0,  'UAGE':30, 'USPCOMP':'Sb 100', 'ULEADSPC':'Sb', 'UCCLO':80, 'UHT':5, 'USTKG':1.0 }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not equal 0, null or blank  in two- or multi-canopy stands" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1799, 'VERT':'TO' ,  'OYRORG':1798, 'UAGE':30, 'USPCOMP':'Sb 100', 'ULEADSPC':'Sb', 'UCCLO':80, 'UHT':5, 'USTKG':1.0 } " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 1800" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1979, 'OYRORG':1980, 'VERT':'TO',  'UAGE':30, 'USPCOMP':'Sb 100', 'ULEADSPC':'Sb', 'UCCLO':80, 'UHT':5, 'USTKG':1.0  } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be greater than OYRORG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':2029, 'VERT':'TO' , 'YRSOURCE':2017, 'OYRORG':1950, 'UAGE':30, 'USPCOMP':'Sb 100', 'ULEADSPC':'Sb', 'UCCLO':80, 'UHT':5, 'USTKG':1.0  } " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than plan period start year when POLYTYPE equals FOR" FOLDED="true"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than or equal to YRSOURCE when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1981, 'YRSOURCE':1980, 'VERT':'TO', 'UAGE':30, 'USPCOMP':'Sb 100', 'ULEADSPC':'Sb', 'UCCLO':80, 'UHT':5, 'USTKG':1.0 } " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than or equal to YRSOURCE when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="2000" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="USPCOMP:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Species Composition" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="120" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="USPCOMP" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="This understorey species composition attribute indicates the tree species that are present in the understorey portion of the forest stand canopy and the proportion of cover that each species occupies within the understorey." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="repeating pattern of species code and corresponding proportion value each species code is 3 characters (including blanks) and is left justified each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified.   maximum of 20 species and proportions pairs in the string " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            <node TEXT="pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion)" FOLDED="true"/>
                            <node TEXT="no duplicate species codes allowed in the string" FOLDED="true"/>
                            <node TEXT="proportion values in the string must sum to 100" FOLDED="true"/>
                            <node TEXT="The tree species in the understorey composition are to be coded using the scheme listed in A1.1.12 OSPCOMP." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then USPCOMP must be null" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code when POLYTYPE is equal to FOR (no need to test this)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: No duplicate species codes allowed in the string" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: Proportion values in the string must sum to 100" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST1: The tree species in the composition are to be coded using the scheme listed here." FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="ST1: Where the stand canopy has been determined to be two-tiered (VERT = TO, TU, MO, MU) then an understorey species composition must be entered (USPCOMP &lt;&gt; null)" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="Tree species in the list [     'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn',     'CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf',     'OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX',     'OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX',     'Sb','Sw','la','Wb','Wi'     ]" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                </node>
                                <node TEXT="Plot Species are [     'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm',     'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp',     'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf',     'Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op',     'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc',     'Sk','Sn','Sr','Sy','Tp','Haz'     ]" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be Null if DEVSTAGE is DEPHARV or DEPNAT' ${:N} row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST%s: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; %s'%( '2' if 'WARNING' in i else '1'   ,i)    for i in test_spcomp(row['${name}']) if ${NINN} if i not in  [ None, [ None ] , []  ] ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be null or blank in two- or multi-canopy stands if POLYTYPE equals FOR' ${:F} ${NIN} and ${VERT} ]" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="['ST1: Invalid &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; species code when POLYTYPE is FOR and VERT is T* or M*' for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if ${NINN} and ${FOR} and row['VERT'] in ['TO','TU','MO','MU']  ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="EFRI_Only" FOLDED="true">
                                <node TEXT="['ST1: Invalid species code for &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; when POLYTYPE equals FOR and VERT is T* or M* and SOURCE is not Plot:' for s in test_SpeciesInList(row['${name}'], [ 'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi' ]        ) if row['SOURCE'][:4] != 'PLOT' and row['VERT'] in ['TO','TU','MO','MU']  and ${NINN} and ${FOR} ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="['ST1: Invalid &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; species code when POLYTYPE is FOR and VERT is T* or M* and SOURCE is from Plot' for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if ${NINN} and ${FOR} and row['SOURCE'][:4] == 'PLOT' and row['VERT'] in ['TO','TU','MO','MU']  ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Ob  90Po  10', 'VERT':'TU', 'SOURCE':'PLOTVAR'}" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'Sb 100' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb 100', 'DEVSTAGE':'DEPNAT', 'UYRORG':None, 'OSTKG':None, 'USTKG':None  } " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be Null if DEVSTAGE is DEPHARV or DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb  100'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP not multiples of 6 characters: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb NaN'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPComp must have numeric codes for species occurrence: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb  50Sw  50Po  50'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP does not add to 100: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb  50Sb  50'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP cannot have duplicate species occurrences: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Po  40Sb  60'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; WARNING SPCOMP values are not in Descending Order: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'', 'VERT':'TU', 'ULEADSPC':'', 'USTKG':1, 'UHT':5, 'UCCLO':0.1, 'UAGE':10, 'UYRORG':2010  }" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be null or blank in two- or multi-canopy stands if POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Pt  90Oz  10', 'VERT':'TU', 'SOURCE':'SPECTRAL', 'UAGE':10, 'UYRORG':2010, 'UCCLO':10, 'UHT':10, 'USTKG':1.0, 'ULEADSPC':'Pt'}" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST1: Invalid &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; species code when POLYTYPE is FOR and VERT is T* or M*" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ULEADSPC:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ULEADSPC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Leading Species" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey leading species  attribute indicates the most prevalent species in the forest stand (or in just the lower most predominant canopy layer if the stand canopy contains two or more distinct layers) based on its percentage of crown closure." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Use the same coding as is listed in the overstorey species composition attribute description." FOLDED="true"/>
                            <node TEXT="Must be species listed in the understorey species composition" FOLDED="true"/>
                            <node TEXT="This leading species attribute replaces the working group attribute (WG) that was present in previous inventory products for determining the major species to manage for in a forest stand." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The tree species in the understorey composition are to be coded using the scheme listed in OSPCOMP." FOLDED="true"/>
                            <node TEXT="if the stand canopy has been determined to be two-tiered  (VERT = TO, TU, MO, or MU),  then an understorey leading species value must be entered (i.e., ULEADSPC &lt;&gt; null)." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then attribute must be Null" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be a species listed in the understorey species composition (USPCOMP) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Where the stand canopy has been determined to be two-tiered (VERT = TO, TU, MO, or MU), then the understorey leading species value must be entered (ULEADSPC &lt;&gt; null) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="The tree species in the understorey leading species are to be coded using the scheme listed in OSPCOMP. This test is implied by the requirement to have the ULEADSPC in the USPCOMP, which are already tested." FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be the species with the greatest percentage or tied for the greatest percentage in the species composition (USPCOMP)" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be Null if DEVSTAGE is DEPHARV or DEPNAT and POLYTYPE equals FOR' ${:NF} row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be listed in the USPCOMP' ${:NF} row['${name}'] not in row['USPCOMP']  ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be Null in two- or multi-canopy stands where management canopy is understory (VERT is TU, MU)' ${:F} ${NIN} and row['VERT'] in ['TU','MU'] ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be the most common species in USPCOMP if populated' ${:NF} row['${name}'].strip(' ').upper() not in get_leadspc(row['USPCOMP']) and row['${name}'].upper() in row['USPCOMP'].upper()   ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true">
                                <node TEXT="['HUMBUG: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be the most common species in USPCOMP if populated %s %s %s '%(row['${name}'], get_leadspc(row['USPCOMP']), row['USPCOMP']) ${:NF} row['${name}'].strip(' ') not in get_leadspc(row['USPCOMP'])  and row['${name}'] in row['USPCOMP']  ]" FOLDED="true"/>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'Sb' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Po', 'DEVSTAGE':'DEPNAT' , 'USPCOMP':'Po  90Sb  10', 'UYRORG':None, 'USTKG':None, 'OSTKG':None } " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be Null if DEVSTAGE is DEPHARV or DEPNAT and POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Po', 'USPCOMP':'Sb 100'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be listed in the USPCOMP" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Po', 'USPCOMP':'Sb  90Po  10'}" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;ULEADSPC&lt;/mark&gt;&lt;/b&gt; must be the most common species in USPCOMP if populated" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="tests not being performed...:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR', '${name}':'XXX', 'USPCOMP':'XXX100' }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: XXX is not a valid species code. This effectively tests that the species code is a valid code... The warning is going to happen on the USPCOMP test." FOLDED="true"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must match the left three chars of USPCOMP if populated' ${:NF} row['${name}'].strip(' ') != row['USPCOMP'][:3].strip(' ')  and row['${name}'] in row['USPCOMP']  ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;ULEADSPC&lt;/mark&gt;&lt;/b&gt; must match the left three chars of USPCOMP if populated" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="UAGE:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Age" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey age  attribute contains the average age of the leading species in the understorey canopy layer of the forest stand." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then UAGE must equal zero" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is a valid code (does not need to be checked)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: if the stand canopy has been determined to be two-tiered  (VERT = TO, TU, MO, or MU),  then an understorey age value must be entered (i.e., UAGE &lt;&gt; 0)." FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 if DEVSTAGE is DEPHARV or DEPNAT' ${:N} row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt; 0 in two- or multi-canopy stands (VERT is TO,TU, MO, MU)' ${:F} ${NIN} and ${VERT} ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':12 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':4, 'DEVSTAGE':'DEPNAT', 'UYRORG':0, 'OSTKG':0, 'USTKG':0 } " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 if DEVSTAGE is DEPHARV or DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE': 'FOR', 'VERT': 'TU', '${name}':0, 'USPCOMP':'Sb 100', 'ULEADSPC':'Sb', 'UCCLO':80, 'UHT':5, 'USTKG':1.0, 'UYRORG':2000 }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt; 0 in two- or multi-canopy stands (VERT is TO,TU, MO, MU)" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="0" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UHT:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4.1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Double" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UHT" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Height" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey height attribute indicates the estimated average tree height (in meters) of the species that has the most basal area as inventoried in the Year of Update.  Estimates can be made from interpreted crown canopy or field samples, or from growth algorithms." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Height / Age relationships should be in the range of Plonski and others." FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be zero or null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then UHT must equal zero or be null" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1:The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: Where the stand canopy has been determined to be two-tiered (VERT=TO,TU,MO, or MU), then an understorey height value must be entered (UHT&lt;&gt;0)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Valid numeric values are from 0 through 40.0" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The value must be at least 3 less than OHT or UAGE must be at least 20 years less than OAGE when VERT indicates two-tiered (TO, TU, MO, MU)" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 if DEVSTAGE is DEPHARV or DEPNAT and POLYTYPE equals FOR' ${:NF} row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; cannot be 0 or null if the canopy is two-tiered when POLYTYPE is FOR.' ${:F} ${NIN} and ${VERT}   ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' ${:NF}  (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be at least 3 less than OHT  or UAGE must be at least 20 years less than OAGE' ${:NV} row['OHT']  not in ${list_NULL} and (row['${name}'] &gt; row['OHT']-3) and ( row['UAGE'] &gt; row['OAGE']-20)   ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':12 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':4, 'DEVSTAGE':'DEPNAT', 'OSTKG':0, 'USTKG':0, 'UYRORG':0 } " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 if DEVSTAGE is DEPHARV or DEPNAT and POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE': 'FOR', 'VERT': 'TU', '${name}':0, 'USTKG':1, 'UCCLO':50, 'OHT':12, 'USPCOMP':'SB 100', 'ULEADSPC':'SB', 'UAGE':10, 'UYRORG':2010    }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; cannot be 0 or null if the canopy is two-tiered when POLYTYPE is FOR." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':42}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1, 'OHT':3.9, 'UAGE':10, 'OAGE':29 , 'VERT':'TU' }" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be at least 3 less than OHT  or UAGE must be at least 20 years less than OAGE" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="0" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="UCCLO:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="UCCLO" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Crown Closure" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="Understorey Crown closure attribute represents the percent of crown closure of the visible high intermediate, co-dominate, and dominate tree layer within the polygon for the understorey layer of trees.  Crown closure for the understorey is the percentage of ground area covered by the vertical projection of the tree crowns onto the ground.  Each defined layer within a stand requires a crown closure; however, for a polygon that is being solely interpreted with no applicable supplemental information, the combined crown closure of the two layers cannot exceed 100 percent (100 percent) . In the case of multi-layered or two-tiered stands where there is applicable field or supplemental information then, the total of crown closure for the two tiers must never exceed  200 percent (i.e., OCCLO + UCCLO &lt;=  200 percent)." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The maximum crown closure value is 100 percent." FOLDED="true"/>
                            <node TEXT="If an understorey crown closure value is entered, then the total crown closure for the two layers must never exceed 200 percent  (i.e., OCCLO + UCCLO &lt;=  200)." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1:The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be zero or null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then UCCLO must equal zero or be null" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A zero value or null is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Where the stand canopy has been determined to be two-tiered (VERT = TO, TU, MO, or MU), then an understorey crown closure value must be entered (UCCLO &gt;0)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be less than or equal to 100 and greater than or equal to zero (UCCLO &lt;= 100 and UCCLO &gt;=0)" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 or null if DEVSTAGE is DEPHARV or DEPNAT' ${:NF} row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: Where the stand canopy has been determined to be two-tiered (VERT = TO, TU, MO, or MU), then an understorey crown closure value must be entered (UCCLO &gt;0)'   ${:V} ${NIN} ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 100 when POLYTYPE is FOR if entered' ${:NF} (row['${name}'] &lt; 0 or row['${name}'] &gt; 100) ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':50 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':50, 'POLYTYPE':'FOR','DEVSTAGE':'DEPHARV', 'USTKG':1, 'OSTKG':1,  'UYRORG':None }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 or null if DEVSTAGE is DEPHARV or DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}': 0,'VERT':'TU', 'USPCOMP':'SB 100','ULEADSPC':'SB', 'UAGE':10, 'UYRORG':2010, 'UHT':5, 'USTKG':1  }" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: Where the stand canopy has been determined to be two-tiered (VERT = TO, TU, MO, or MU), then an understorey crown closure value must be entered (UCCLO &gt;0)" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':101, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 100 when POLYTYPE is FOR if entered" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="0" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="USTKG:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="double" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4.2" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="USTKG" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Stocking" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey stocking attribute indicates a qualitative measure of the density of tree cover within the understorey.  It is expressed as a percentage value ranging from zero to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field.  Should this not read 5.00 like in OSTKG" FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="a percentage value ranging from zero to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field. should this not be 5.00 to match OSTKG For additional information see OSTKG." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV and DEPNAT then USTKG must equal zero" FOLDED="true">
                                    <icon BUILTIN="full-0"/>
                                </node>
                                <node TEXT="ST1: Valid numeric values are from 0 through 4.00 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: If the DEVSTAGE attribute is not DEP or NEW then the stocking attribute must be greater than zero (OSTKG &gt; 0.00) when VERT is multicanopy (TU, TO, MU, MO)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="USTKG should be less than 2.5" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 when DEVSTAGE is DEP*' ${:NF} row['DEVSTAGE'] in ['DEPNAT','DEPHARV'] ]" FOLDED="true">
                                    <icon BUILTIN="full-0"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 4.0 when POLYTYPE equals FOR' ${:NF} (row['${name}'] &lt; 0 or row['${name}'] &gt; 4.0) ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be &gt; 0 if DEVSTAGE is not DEP* or NEW*  in multicanopy stand' ${:V} row['DEVSTAGE'] not in [ 'DEPNAT','DEPHARV', 'NEWPLANT', 'NEWSEED', 'NEWNAT' ] and row['${name}'] &lt;= 0 ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than 2.5' ${:NF} (row['${name}'] &gt; 2.5) ]" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':1.0 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':1.0, 'POLYTYPE':'FOR' , 'DEVSTAGE':'DEPNAT', 'UYRORG':None } " FOLDED="true">
                                    <icon BUILTIN="full-0"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be 0 when DEVSTAGE is DEP*" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':-1, 'POLYTYPE':'FOR', 'OSTKG':2}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 4.0 when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0, 'POLYTYPE':'FOR' , 'VERT':'TU', 'DEVSTAGE':'SELECT',  'UCCLO':50, 'USPCOMP':'SB 100', 'ULEADSPC':'SB', 'UAGE':10, 'UYRORG':2010, 'OAGE':40, 'OHT':12 } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be &gt; 0 if DEVSTAGE is not DEP* or NEW*  in multicanopy stand" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':3.5, 'POLYTYPE':'FOR' }" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than 2.5" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="0" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="USC:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="USC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Understorey Site Class" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The understorey site class attribute indicates a site quality estimate for the understorey of a forest stand.  It is determined using the average height, age, and working group, based on the dominant tree species of the understorey. These attributes are compared against height and age growth curves in Plonskis Normal Yield Tables for different species to determine the relative growth rate for a forest stand." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format (a number from 0 through 4)  when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The value must be greater than or equal to 0 and less than or equal to 4 (${name} &gt;=0 and ${name} &lt;=4) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE is DEPHARV or DEPNAT then USC must equal zero" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR' ${:F} row['${name}'] not in [ 0,1,2,3,4 ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if DEVSTAGE is DEPHARV or DEPNAT' ${:N} row['DEVSTAGE'] in [ 'DEPNAT','DEPHARV' ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':2 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':4, 'DEVSTAGE':'DEPNAT','POLYTYPE':'FOR', 'USTKG':None, 'OSTKG':None, 'UYRORG':None} " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if DEVSTAGE is DEPHARV or DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':-1, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':28, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Best" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Better" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Good" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poor" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Very Poor" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="0" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="INCIDSPC:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="INCIDSPC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Incidental Species" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The incidental species attribute represents a species that is identified within the polygon but does not represent ten percent of the basal area in order to be included in the overstorey species composition attribute (OSPCOMP).  The tree species represents a tree species that is present and is important to note.(e.g. important for wildlife assessment, high market value, or significant ecological value)." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="use the same coding as listed in the OSPCOMP (species composition) attribute descriptin" FOLDED="true"/>
                            <node TEXT="if an incidental species is not identified within the polygon, then a value of NON will be entered" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow correct coding for an individual species when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: The attribute value should not be in the overstorey species composotion (OSPCOMP) and less than ten percent (VERT != SV)" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; Invalid Species code' ${:NF}  row['${name}'].upper() not in [ s.upper() for s in ['NON', 'AX', 'Ab', 'Aw', 'Pl', 'Pt', 'Bd', 'Be', 'Bw', 'By', 'Bn', 'CE', 'Cr', 'CH', 'Cb', 'OC', 'EX', 'Ew', 'Bf', 'OH', 'He', 'Hi', 'Iw', 'La', 'Mh', 'Mr', 'Ms', 'Mh', 'Ob', 'Or', 'Ow', 'Pn', 'Pi', 'Pr', 'Ps', 'Pw', 'PO', 'Pb', 'SX', 'Sb', 'Sr', 'Sw', 'La' , 'Pj', 'Cw'] ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; may be in PCI OSPCOMP but should be less than 10%' ${:NF} row['${name}'] in row['OSPCOMP'] and row['VERT'] != 'SV' if len([ row['${name}'].ljust(3) + str(n).rjust(3) for n in range(0,10) if row['${name}'].ljust(3) + str(n).rjust(3) in row['OSPCOMP'] ]) == 0  and ${PCI} ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; may be in BMI or OPI SPCOMP but should be less than 10%' ${:NF} row['${name}'] in row['SPCOMP'] and row['VERT'] != 'SV' if len([ row['${name}'].ljust(3) + str(n).rjust(3) for n in range(0,10) if row['${name}'].ljust(3) + str(n).rjust(3) in row['SPCOMP'] ]) == 0  and ${BMI_OPI} ]" FOLDED="true"/>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR','OSPCOMP':'SB 100'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'NON' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code','POLYTYPE':'FOR','OSPCOMP':'SB 100'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; Invalid Species code" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SB', 'POLYTYPE':'FOR','OSPCOMP':'PT  91SB   9', 'VERT':'SI', 'OLEADSPC':'Pt' } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="{'${name}':'SB', 'POLYTYPE':'FOR','OSPCOMP':'PT  81SB  19', 'VERT':'SI', 'OLEADSPC':'Pt' } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; may be in PCI OSPCOMP but should be less than 10%" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SB', 'POLYTYPE':'FOR','SPCOMP':'PT  81SB  19', 'VERT':'SI', 'OLEADSPC':'Pt' } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; may be in BMI or OPI SPCOMP but should be less than 10%" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="NON" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="VERT:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="VERT" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Vertical Stand Structure" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The vertical stand structure attribute is intended to describe the number of distinct tree layers (storeys) that can be identified within a forested polygon." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="If the vertical stand structure is set to single story or complex (VERT = SI, SV, or CX), then a description of the stand is entered using only the overstorey attributes (e.g. OSPCOMP, OHT, OSC). If the vertical stand structure is set to TO, TU, MO, or MU, then a separate description must be entered for each of the main canopy layers using the overstorey and understorey sets of attributes accordingly.  " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SI:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SI" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Mainly a single story stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="single storey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SV" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Mainly a single story stand with a veteran (super canopy) component representing less than 10 percent of the total crown closure for the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="single storey with veterans" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="TO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="TO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is composed of mainly two distinct layers that have at least 3 meters in height difference or 20 years of age difference, and each layer represents at least 10 percent of the total canopy crown closure for the stand.  The overstorey is the layer used to assign the DEVSTAGE value during photo interpretation / inventory creation." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="two-tiered - overstorey used to set DEVSTAGE" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="TU:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="TU" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The stand canopy is composed of mainly two distinct layers that have at least 3 meters in height difference or 20 years of age difference, and each layer represents at least 10 percent of the total canopy crown closure for the stand.  The understorey is the layer used to assign the DEVSTAGE value during photo interpretation / inventory creation." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="two-tiered - understorey used to set DEVSTAGE" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Mainly a two-tiered canopy with an additional veteran (super canopy) component of less than 10 percent of the total stand canopy crown closure.  The overstorey is the layer used to assign the DEVSTAGE value during photo interpretation / inventory creation" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="two-tiered with veterans - overstorey used to set DEVSTAGE" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MU:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MU" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Mainly a two-tiered canopy with an additional veteran (super canopy) component of less than 10 percent of the total stand canopy crown closure.  The understorey is the layer used to assign the DEVSTAGE value during photo interpretation / inventory creation." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="two-tiered with veterans- understorey used to set DEVSTAGE" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CX" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand with a wide range of heights and ages to the point of no distinct layers being identifiable." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="complex or continuous" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="HORIZ:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HORIZ" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Horizontal  Stand Structure" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The horizontal stand structure attribute represents the distribution of the forest layers over the polygon area and is a solely interpreted attribute.  The distribution of each tree layer and the associated presence of openings are assessed. If more than one condition is present, only the most prevalent one is recorded.  The standard used for the interpretation of the attribute is an opening." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="mainly single stem canopy structure" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="single stem" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="single patch distinct from the rest of the canopy" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="single patch" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="two or three distinct patches" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="few patches" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="several distinct patches" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multiple patches" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="openings common - 3 or more" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="openings common" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OU:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OU" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="openings uncommon - 1or 2" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="openings uncommon" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="PRI_ECO:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="13" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PRI_ECO" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Primary Ecosite" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="Ecosite is defined as an ecological unit comprised of relatively uniform geology, parent material, soils, topography, and hydrology and consists of related vegetation conditions.  An ecosite description is a vegetation description related to major vegetative attributes influencing site productivity and biological legacy, and should be relatively stable over moderate periods (20-40 years).  Ecosite is the primary unit for delineation for both the forested and non-forested land.  A complex of two forested ecosites is allowed to be recorded when more than one ecosite is present as long as the secondary ecosite represents at least 20 percent of the area of the polygon and the area associated with the secondary ecosite does not exist in a manner suitable for meeting the minimum polygon size for creating a new polygon. The PRI_CODE  attribute indicates the primary or dominant ecosite present with in the stand.  Simple ecosite:  (i.e. only PRI_ECO completed)  A polygon assigned a single ecosite label and assumed to have as much as 20 percent of the polygon consisting of acceptable inclusions (as defined by the fact sheet) or eco-elements other than those considered diagnostic of the ecosite.  Complex ecosite:  (i.e. PRI_ECO and SEC_ECO are completed)  A polygon is assigned two ecosite attributes when one ecosite condition exceeds 50 percent of the polygon (primary ecosite) and another ecosite condition exceeds 20 percent of the polygon (secondary ecosite), and the secondary ecosite does not exist in a manner suitable for representation meeting minimum polygon size. A common example of complex ecosites for a polygon would be a very shallow pine/spruce mix not large enough to differentiate from the surrounding dominant stand of moderately deep aspen/birch mix.   For more information about ecosites, including a fact sheet for each ecosite description, refer to:   Ecosystems of Ontario: Provincial Ecosites, May 21st, 2008, Ecological Land Classifications Working Group. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="See eFRI Tech Spec" FOLDED="true"/>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR, BSH, RCK, TMS or OMS" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST2: The attribute population should follow the correct coding scheme when POLYTYPE is equal to BSH, RCK, TMS or OMS" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If SEC_ECO is not null then PRI_ECO must not be null when POLYTYPE is equal to FOR, BSH, RCK, TMS or OMS" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Geographic Range'  ${:NF} row['${name}'][0] not in ['A', 'B', 'G', 'S', 'U']   ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have numeric Ecosite Number' ${:NF} not row['${name}'][1:4].isdigit()   ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have a valid Ecosite Number' ${:NF} row['${name}'][1:4] in [ '%03d'%(i) for i in range(225,997)]    ]  " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Vegetative Modifier'${:NF}  row['${name}'][4:6].strip() not in  ['TT','TL','S','N','X', '', 'Tt','Tl'] ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node COLOR="#000000" TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Depth Modifier' ${:NF} row['${name}'][6:8].strip() not in ['R','VS','S','M','MD','D', ''] ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST2:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should have valid Geographic Range' ${:N}  row['POLYTYPE'] in ['BSH','RCK','TMS','OMS'] and row['${name}'][0] not in ['A', 'B', 'G', 'S', 'U']   ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST2:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should have valid Ecosite Number' ${:N} row['POLYTYPE'] in ['BSH','RCK','TMS','OMS'] if  int(row['${name}'][1:4]) &lt; 0  or ( int(row['${name}'][1:4]) &gt; 224 and int(row['${name}'][1:4]) &lt; 997 )  or int(row['${name}'][1:4]) &gt; 1000  ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST2:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should have valid Vegetative Modifier' ${:N} row['POLYTYPE'] in ['BSH','RCK','TMS','OMS']  if row['${name}'][4:6].strip() not in  ['TT','TL','S','N','X','','Tt','Tl'] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node COLOR="#000000" TEXT="['ST2:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should have valid Depth Modifier' ${:N} row['POLYTYPE'] in ['BSH','RCK','TMS','OMS'] if row['${name}'][6:8].strip() not in ['R','VS','S','M','MD','D', ''] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be null if SEC_ECO is not null' ${:F} ${NIN} and row['SEC_ECO'] not in ${list_NULL}   ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true">
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have a valid Ecosite Number' ${:NF} int(row['${name}'][1:3]) &gt; 224 and int(row['${name}'][1:4]) &lt; 997   ]  " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node STYLE="fork" TEXT="{ 'POLYTYPE':'FOR','${name}':'B300TlD ','SEC_ECO':''}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have a valid Ecosite Number" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'FOR','${name}':'TlD ','SEC_ECO':''}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have numeric Ecosite Number" FOLDED="true"/>
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Geographic Range" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have numeric Ecosite Number" FOLDED="true"/>
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Depth Modifier" FOLDED="true"/>
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Vegetative Modifier" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'FOR','${name}':None,'SEC_ECO':'B100TlD '}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be null if SEC_ECO is not null" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="B100TtD n" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SEC_ECO:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="13" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SEC_ECO" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Secondary Ecosite" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="Ecosite is defined as an ecological unit comprised of relatively uniform geology, parent material, soils, topography, and hydrology and consists of related vegetation conditions.  An ecosite description is a vegetation description related to major vegetative attributes influencing site productivity and biological legacy, and should be relatively stable over moderate periods (20-40 years).   Ecosite is the primary unit for delineation for both the forested and non-forested land. A complex of two forested ecosites is allowed to be recorded when more than one ecosite is present as long as the secondary ecosite represents at least 20 percent of the area of the polygon and the area associated with the secondary ecosite does not exist in a manner suitable for meeting the minimum polygon size for creating a new polygon. The SEC_ECO  attribute indicates the secondary or lesser (in terms of area) ecosite present with in the stand." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="See eFRI Tech Spec" FOLDED="true"/>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[1] is Geographic Range, from the list ['A', 'B', 'G', 'S', or 'U']. Values mean ['Sub-arctic','Boreal','Great Lakes-St Lawrence','Southern','Unclassified - Special Case']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[2:4] is Ecosite Number. Values range from 001 to 224, plus 997 to 999 (Unclassified areas). Refer to EFRI spec. Not all numbers are used." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should only be found in certain POLYTYPEs" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosite Numbers should be consistent with SPPCOMP, POLYTYPE, Overstory composition and Understory Composition" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[5:6] is Vegetative Modifier. Values can be for the list ['Tt','Tl','S','N','X']. Describes conditions existing at the site. Values mean ['Tall treed','Low treed','Shrub','Not Woody','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[7:8] is Depth Modifier. Values can be from the list ['R',VS','S','M','MD','D']. Values Mean ['Rock','Very Shallow','Shallow','Moderate','Moderately Deep','Deep']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP' then Depth Modifier is ['M','D'] only" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Where SOURCE = 'DIGITALP', there should be no Depth Modifier for Flooded or Hydric ecosites (126-156)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Ecosites which are based on soil depth should have no depth modifier. Ecosites from 001-028 are Very Shallow, 157-22 are Rocky, 997-999 are Anthropogenic and Coastal Tide sites." FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[9] is the Substrate Moisture Modifier, from the list ['d','f,'h','m','s','v','w','x']. These represent ['Dry','Fresh','Humid','Moist','Saturated','Very Moist','Wet','Xeric']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="If SOURCE = 'DIGITALP', there should be no Substrate Moisture Modifier" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[10] is the substrate Chemistry Modifier. Values from ['a','b','k','n','z']. Meaning ['Acidic','Basic','Calcareous','Non-calcareous','Saline']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Chemistry Modifiers are not applicable to anthropogenic ecosites (189-200, 997-999)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Permanently flooded or hydric ecosites (126-156, 222-224) are assigned the chemistry modifier 'n' where SOURCE != 'PLOTVAR'" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO[11:13] is a vegetative cover class modifier. ['cTt','oTt','sTt','Tt','Tl','sTl',St','sSt','Sl','sSl','H','sH','Nv','X']. Descriptions are ['Closed Tall Treed','Open Tall Treed','Sparse Tall Treed','Greater than 25 percent Tall Treed','Greater than 25 percent Low Treed','Sparse Low Treed','Tall Shrub','Sparse Tall Shrub','Low Shrub','Sparse Low Shrub','Herbaceous','Sparse Herbaceous', 'Non-vascular','Not Vegetated']" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="PRI_ECO cannot equal SEC_ECO" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v126_not_sc4: Ecosite Number is 126, OSC Must Be 4, except if SOURCE is PLOTVAR" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="v127_and_SC&gt;3 or v128_and_SC&gt;3 or 136&lt;&gt;SC4and=FOR: Ecosite number is 127 or 128 or 136, OSC Must be 4" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B223TlD n' OR \&quot;PRI_ECO\&quot; = 'B223TtD n' OR \&quot;PRI_ECO\&quot; = 'B223TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: \&quot;PRI_ECO\&quot; = 'B224TlD n' OR \&quot;PRI_ECO\&quot; = 'B224TtD n' OR \&quot;PRI_ECO\&quot; = 'B224TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;)" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                            <node TEXT="Error: &quot;(\&quot;PRI_ECO\&quot; = 'B222TlD n' OR \&quot;PRI_ECO\&quot; = 'B222TtD n' OR \&quot;PRI_ECO\&quot; = 'B222TtS n' ) AND \&quot;OSC\&quot; &gt; 3&quot;" FOLDED="true">
                                <icon BUILTIN="bookmark"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR, BSH, RCK, TMS or OMS" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Geographic Range'  ${:NF} row['${name}'][0] not in ['A', 'B', 'G', 'S', 'U']   ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have numeric Ecosite Number' ${:NF} not row['${name}'][1:4].isdigit()   ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have a valid Ecosite Number' ${:NF} row['${name}'][1:4] in [ '%03d'%(i) for i in range(225,997)]    ]  " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Vegetative Modifier' ${:NF} row['${name}'][4:6].strip() not in  ['TT','TL','S','N','X','','Tt','Tl' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node COLOR="#000000" TEXT="['ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Depth Modifier' ${:NF}  row['${name}'][6:8].strip() not in ['R','VS','S','M','MD','D', ''] ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'FOR','${name}':'B100TlD '}" FOLDED="true"/>
                                <node STYLE="fork" TEXT="{ 'POLYTYPE':'FOR','${name}':'B300TlD '}" FOLDED="true">
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have a valid Ecosite Number" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'FOR','${name}':'TlD '}" FOLDED="true">
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have numeric Ecosite Number" FOLDED="true"/>
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Geographic Range" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true"/>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have numeric Ecosite Number" FOLDED="true"/>
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Depth Modifier" FOLDED="true"/>
                                    <node TEXT="ST1:  Potential MNRF Source: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must have valid Vegetative Modifier" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS1:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS1" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Accessibility Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The accessibility indicator attribute specifies whether or not there are any restrictions to accessing a productive forest stand.  These restrictions may be legal (i.e. ownership), political / land use policy (i.e. land use designation, road closures), and/or a natural barrier.  The focus of this field is identification of Crown productive forest stands that are inaccessible and therefore are not considered as part of the managed landbase for forest management planning purposes, but the principle may be applied to any area." FOLDED="true"/>
                            <node TEXT="NOTE:   These attributes are not completed by the photo interpreters during inventory production. Access restriction information is to be determined by the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR (ACCESS1 and ACCESS2)" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory for ACCESS1 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR (ACCESS1)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If ACCESS2 is not equal to NON then ACCESS1 must not be NON when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: ACCESS1 and ACCESS2 can't contain the same value unless both are NON when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON'  ${:F} row['${name}'] == 'NON' and row['ACCESS2'] != 'NON' and row['ACCESS2'] not in ${list_NULL}  ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="['ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON'  for row in [row] if row['${name}'] == 'NON'       and (             row['ACCESS2'] != 'NON' and             str(row['ACCESS2']).strip() != ''              and row['ACCESS2'] is not None and row['POLYTYPE'] == 'FOR'             ) ]" FOLDED="true">
                                        <icon BUILTIN="full-6"/>
                                    </node>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not equal ACCESS2' ${:F} (row['${name}'] != 'NON') and (row['ACCESS2'] != 'NON') and (row['${name}'] not in [None, '', ' ']) and (row['ACCESS2'] not in [None, '', ' ']) and (row['${name}'] == row['ACCESS2']) ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'LUD', 'ACCESS2':'LUD', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not equal ACCESS2" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NON', 'ACCESS2':'LUD', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'NON' }" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NON', 'POLYTYPE':'FOR', 'ACCESS2':None}" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="NON:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NON" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is accessible/reachable." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no accessibility considerations" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="GEO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GEO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Area is not accessible due to geographic reasons." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="geography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LUD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LUD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="land use designation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ownership" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PRC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="road closure" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="subject to ownership" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="NON" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS2:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS2" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Accessibility Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The accessibility indicator attributes specifies whether or not there are any restrictions to accessing a productive forest stand. These restrictions may be legal (i.e., ownership), political / land use policy (i.e., land use designation, road closures), and/or a natural barrier. The focus of this field is identification of Crown productive forest stands that are inaccessible and therefore are not considered as part of the managed landbase for forest management planning purposes, but the principle may be applied to any area." FOLDED="true"/>
                            <node TEXT="NOTE:   These attributes are not completed by the photo interpreters during inventory production. Access restriction information is to be determined by the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="english" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                                <node TEXT="Note: The ACCESS2 attribute may not exist in the submitted coverage as this attribute is not mandatory.  If this attribute exists, it may not be populated.  Therefore, this validation will be performed on the ACCESS2 attribute for the application of the correct coding scheme." FOLDED="true"/>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR', 'ACCESS1':'LUD' }" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'NON' }" FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="GEO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GEO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Area is not accessible due to geographic reasons." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="geography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LUD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LUD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="land use designation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NON:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NON" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is accessible/reachable." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no accessibility considerations" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OWN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OWN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ownership" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PRC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="road closure" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="subject to ownership" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="NON" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MGMTCON1:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTCON1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Management Consideration" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning" FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for MGMTCON1" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory for MGMTCON1 when POLYTYPE is equal to FOR (ROD Footnote: ISL POLYTPE implies MGMTCON1 POLYTYPE)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR for MGMTCON1" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: IF ACCESS1 or ACCESS2 is equal to GEO then MGMTCON1 must not be NONE" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: IF MGMTCON2 is not None then MGMTCON1 must not be NONE" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST1: IF MGMTCON3 is not None then MGMTCON2 and MGMTCON1 must not be NONE" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="ST1: If FORMOD equals PF, then the MGMTCON1 attribute should not equal NONE" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="ST1: The attribute value in MGMTCON1 must not be equal to the attribute value in MGMTCON2 or the attribute value in MGMTCON3 unless all are NONE" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                </node>
                                <node TEXT="ST2: If the SC attribute equals 4, then the MGMTCON1 attribute should not equal NONE (BMI and OPI only)" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: If ACCESS1 or ACCESS2 are GEO, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE' ${:F} row['${name}'] == 'NONE' and row['ACCESS1'] == 'GEO' ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: If ACCESS1 or ACCESS2 are GEO, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE' ${:F} row['${name}'] == 'NONE' and row['ACCESS2'] == 'GEO'  ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: If MGMTCON2 is not NONE or null, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE' ${:F} row['MGMTCON2'] != 'NONE' and row['MGMTCON2'] not in [ None, '', ' ' ]  and row['${name}'] == 'NONE' ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST1: If MGMTCON3 is not NONE or null, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE' ${:F} row['MGMTCON3'] != 'NONE' and row['MGMTCON3'] not in [ None , '', ' ' ] and row['${name}'] == 'NONE' ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="['ST1: If FORMOD equals PF, &lt;b&gt;&lt;mark&gt;MGMTCON1&lt;/mark&gt;&lt;/b&gt; should not equal NONE when POLYTYPE equals FOR' ${:F} row['${name}'] in [ 'NONE' ] and row['FORMOD'] in [ 'PF' ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not equal MGMTCON2 or MGMTCON3 when POLYTYPE equals FOR' ${:F} row['${name}'] not in [None, '', ' ', 'NONE'] and (row['${name}'] == row['MGMTCON2'] or row['${name}'] == row['MGMTCON3']) ]" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'COLD' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'ISLD','MGMTCON2':'NONE','POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NONE', 'POLYTYPE':'FOR', 'ACCESS1':'GEO'}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: If ACCESS1 or ACCESS2 are GEO, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NONE','MGMTCON2':'COLD','POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: If MGMTCON2 is not NONE or null, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NONE','MGMTCON3':'COLD','POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST1: If MGMTCON3 is not NONE or null, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'NONE','FORMOD':'PF','POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST1: If FORMOD equals PF, &lt;b&gt;&lt;mark&gt;MGMTCON1&lt;/mark&gt;&lt;/b&gt; should not equal NONE when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'COLD','MGMTCON2':'COLD','MGMTCON3':'NONE','POLYTYPE':'FOR'} " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not equal MGMTCON2 or MGMTCON3 when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="NONE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There are no physical or ecological restrictions in the site that need to be considered when determining management of the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no management consideration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="COLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="COLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="permafrost" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAMG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="physical/natural damage" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural barrier" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PENA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="peninsula" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="POOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SAND" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SHRB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="heavy shrub / brush" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SOIL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shallow soils" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STEP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="steep slopes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="UPFR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UPFR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land containing obvious physical features which may limit, but does not prevent, the ability to practice forest management.  The feature(s) must be considered during forest management planning, but does not make the stand unmanageable.  The specific reason/limitation is not known." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="unknown concern - historic production forest reserve (PRF) area" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="U_PF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UPF" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitations such as steep slopes or shallow soils over bedrock.  The specific reason/limitation is not known." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="unknown concern - historic protection forest (PF) area" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WETT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="poorly drained - high water table" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="NONE" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MGMTCON2:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTCON2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Management Consideration" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: IF MGMTCON3 is not None then MGMTCON2 and MGMTCON1 must not be NONE" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: If MGMTCON3 is not Null or NONE, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE' ${:F} row['MGMTCON3'] not in [ None, 'NONE', '' ]  and row['${name}'] == 'NONE'  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'COLD' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'MGMTCON1':'NONE','MGMTCON2':'NONE','MGMTCON3':'ROCK','POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: If MGMTCON3 is not Null or NONE, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR', 'MGMTCON1':'ROCK' }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'MGMTCON1':'NONE','MGMTCON2':None,'MGMTCON3':None,'POLYTYPE':'FOR'}" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="NONE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There are no physical or ecological restrictions in the site that need to be considered when determining management of the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no management consideration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="COLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="COLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="permafrost" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAMG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="physical/natural damage" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural barrier" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PENA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="peninsula" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="POOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SAND" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SHRB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="heavy shrub / brush" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SOIL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shallow soils" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STEP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="steep slopes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="UPFR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UPFR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land containing obvious physical features which may limit, but does not prevent, the ability to practice forest management.  The feature(s) must be considered during forest management planning, but does not make the stand unmanageable.  The specific reason/limitation is not known." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="unknown concern - historic production forest reserve (PRF) area" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="U_PF:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="UPF" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitations such as steep slopes or shallow soils over bedrock.  The specific reason/limitation is not known." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="unknown concern - historic protection forest (PF) area" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WETT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="poorly drained - high water table" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="NONE" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MGMTCON3:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MGMTCON3" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MGMTCON" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Management Consideration" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':'A Decidedly Invalid Code', 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'COLD' }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="COLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="COLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="permafrost" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DAMG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DAMG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="physical/natural damage" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ISLD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ISLD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="island" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NATB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NATB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural barrier" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NONE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NONE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There are no physical or ecological restrictions in the site that need to be considered when determining management of the stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="no management consideration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PENA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PENA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area of land that is nearly surrounded by water and is connected to the mainland." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="peninsula" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="POOR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="POOR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="stagnated, poor tree growth - no indicator" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ROCK:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ROCK" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="exposed bedrock / rocky outcrops" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SAND:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SAND" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="blow sand / exposed fine sand, shallow or no humus" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SHRB:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SHRB" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="heavy shrub / brush" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SOIL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SOIL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="shallow soils" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="STEP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="STEP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="steep slopes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WATR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="telluric / highly fluctuating, moving ground water" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="WETT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WETT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="poorly drained - high water table" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="NONE" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: POLYID Field values must be unique' for f in ['POLYID' ] if not self.uniqueValueTest(f) ]" FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The Base Model Inventory is conceptually an update of the 2009 specification of the Base Model updated to use the forest value description from the Enhanced Forest Resource Inventory" FOLDED="true"/>
                </node>
                <node TEXT="DomainValidation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="Identify all species that occur in OSPCOMP / USPCOMP" FOLDED="true"/>
                        <node TEXT="Age class distributions by..." FOLDED="true"/>
                    </node>
                </node>
            </node>
            <node TEXT="ForecastDepletions:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ForecastDepletions" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="FSOURCE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Source of Forecast" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FSOURCE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                            <node TEXT="BASECOVR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BASECOVR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Information that is provided by the MNRF  (e.g., water or evaluated wetlands)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="planimetric base layer" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTIMATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTIMATE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This source option is only for use in areas that have been recently renewed and have not been revisited since the renewal work was performed.  That is, where a follow-up survey has not yet been performed.  Therefore, the description of the newly regenerated stand is a 'best estimate' of the expected outcome / result of the renewal treatment that was applied to the area based on past silvicultural successes." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="expected / estimated outcome / result" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photo-interpreted by softcopy systems" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - manual process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DIGITALA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DIGITALA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Digital analysis, automated processing. (e.g., Ecognition)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="multispectral scanning (digital image) - automated process" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FOC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FOC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Inspection of a site after silvicultural treatment to determine whether an operator / operations conforms to the approved plan or permit.   The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest operations compliance inspection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FORECAST:" FOLDED="true" BACKGROUND_COLOR="#99ff99">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FORECAST" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates a polygon description that was updated based on expected outcomes of planned operations (which have not yet been implemented) for the remainder of the current plan term.  This code is only valid in the Base Model Inventory. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forecasted description" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRICNVRT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRICNVRT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Current polygon description based on data conversion from traditional FRI" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="forest resources inventory conversion" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="INFRARED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="INFRARED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Infrared satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="MARKING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MARKING" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription.  Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pre-harvest site inspection / marking" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARA:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARA" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="aerial survey / reconnaissance" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OCULARG:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OCULARG" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ocular estimate (ground)" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="OPC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OPC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Measuring standing trees to determine the volume of wood on a given tract of land." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="operational cruise" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="air photo interpretation" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOLS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOLS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale larger than 1:10,000  (e.g., 1:500, 1:1000)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="large scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PHOTOSS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PHOTOSS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Photography at a scale smaller than 1:20,000  (e.g., 1:100,000)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="small scale aerial photography" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTFIXD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTFIXD" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Fixed area plot" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="fixed area plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PLOTVAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PLOTVAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Variable area plots." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="variable area (radius) plot" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="RADAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="RADAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="radar satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGENASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGENASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Survey of a regenerated area to determine how well the new stand is growing.  This includes seeding, survival, and stocking assessments.." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regeneration assessment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMEXTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMEXTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).   Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="extensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEMINTEN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEMINTEN" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An appraisal of a forest stand's structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).  Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="intensive silvicultural effectiveness monitoring survey" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SPECTRAL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SPECTRAL" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="spectral satellite imagery" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SUPINFO:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SUPINFO" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Stand update information from a source other than those listed/defined in the other coding options that is provided by either MNR or Licensee. For example, disturbance records. The date of information capture/acquisition and the age of the stand as of the date of information was acquired must be supplied for the information to be incorporated into the inventory." FOLDED="true">
                                        <icon BUILTIN="closed"/>
                                    </node>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Supplied information" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="FYRDEP:" FOLDED="true">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Forecast Year of Disturbance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FYRDEP" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: The value should not be less than the plan period start year minus 4" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should not be less than the plan period start year minus 4' ${:N} row['${name}'] &lt; (${planyear}-4) or row['${name}']&gt;${planyear}]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT='{"${name}":1000 }' FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should not be less than the plan period start year minus 4" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="2019" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="FDEVSTAGE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Forecast Development Stage" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="FDEVSTAGE" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="DEPHARV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPHARV" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest area that was recently disturbed by clearcut harvesting and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer that would be released and/or protected as part of the depletion operation." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by harvest and has received no regeneration/renewal treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="DEPNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="DEPNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest area that was recently disturbed by natural causes (i.e., fire, blowdown, ice damage, insect and disease) and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently disturbed by natural causes and has received no regeneration / renewal treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LOWMGMT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWMGMT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest stands which were previously harvested and have not reached the regeneration standards as described in an approved FMP.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., crown closure less than 25%)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to past management" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LOWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LOWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest stands which were previously disturbed by natural causes and have not reached the regeneration standards as described in an approved FMP.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., crown closure less than 25 percent)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Below regeneration standards due to natural causes / succession" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by planting, but have not been assessed as established" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly planted" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by seeding, but have not been assessed as established" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly seeded" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NEWNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NEWNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which have been regenerated predominantly by natural means, but have not been assessed as established (e.g. CLAAG, HARP)" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="recently renewed : mainly natural regeneration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTPLANT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTPLANT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly from planted stock and which have been assessed as established." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="established mainly planted" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTSEED:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTSEED" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest areas which were regenerated predominantly by seeding and which have been assessed as established." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="established mainly seeded" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ESTNAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ESTNAT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest harvest areas which were regenerated predominantly by natural means and which have been assessed as established." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="established  mainly natural regeneration" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="NAT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="NAT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="natural stands" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This classification will be used to describe the natural forest areas that have never been treated to date (original forest)." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Established productive forest areas which have received a mid-rotation thinning/spacing to promote the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received pre-commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINCOM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINCOM" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Established productive forest areas which have received a mid-rotation partial harvest designed to promote the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BLKSTRIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLKSTRIP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of the stand in progressive strips or blocks in more than one operation. Strip and block harvest methods are prescribed to encourage natural regeneration, provide wildlife habitat, protect fragile sites, or for aesthetics." FOLDED="true"/>
                                    <node TEXT="The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters wide.  It is designed to encourage regeneration on difficult and/or fragile sites." FOLDED="true"/>
                                    <node TEXT="Note:  Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: block or strip" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged, silvicultural system  that retains mature standing trees  scattered throughout the cutblock to  provide seed sources for natural  regeneration.  " FOLDED="true"/>
                                    <node TEXT="A method of harvesting and  regenerating a forest stand in which  all trees are removed from the area  except for a small number of seed- bearing trees that are left singly or in  small groups. The objective is to  create an even-aged stand." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut:  seed tree " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A partial harvest where the first harvest operation removes target/specific merchantable tree species from a forest stand. The remaining species are merchantable and are intended to be harvested by another logger/contractor/forest resource licence holder in the next pass.  A first pass should be recorded if merchantable tree species remain in the forest stand which have been allocated for harvest - but have not yet been harvested." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PREPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PREPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source and to improve conditions for seed production and natural regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a preparatory cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a seed cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRSTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRSTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a first removal harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LASTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LASTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where all of the remaining trees in the overstorey are removed. This is the removal of the seed or shelter trees after the regeneration has been effective." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a final removal harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition, distribution and quality by removing less desirable trees of any species." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received an improvement cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SELECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SELECT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="received a selection harvest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Forecast Depletions (FDP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Definition" FOLDED="true">
                    <node TEXT=" The forecast depletions layer contains the expected changes to the forest to the start of the  planning period. The expected changes can be any combination of planned harvest areas that  are expected to be harvested before the end of the current plan period and any natural  disturbances that have occurred since the last submitted annual report which was used in the  development of the planning composite inventory. The natural disturbances that are added  here help to provide a more accurate inventory without actually being a forecast activity.   " FOLDED="true"/>
                    <node TEXT="The forecast depletions will be provided as a separate component of the planning inventory. As  better information is attained, the forecast of areas to be depleted may change. This will not  require a resubmission of the forecast depletions layer as part of the planning inventory  submission for the planning inventory progress checkpoint, nor will it impact the long-term  management direction modeling since the base model inventory will not be recreated and  resubmitted unless agreed to by the planning team. Instead, the changes will be used to  facilitate operational planning and the selection of stands for operations in the new plan. The  changes will also be used for any spatial analysis during operational planning. The updated  forecast depletions layer will be submitted with the planning composite layer as part of the  draft and final plan submissions. " FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="FDP" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="BaseModelInventory:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="BaseModelInventory" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="BMI" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Base Model Inventory (BMI)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26915" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: POLYID Field values must be unique' for f in ['POLYID' ] if not self.uniqueValueTest(f) ]" FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The Base Model Inventory is conceptually an update of the 2009 specification of the Base Model updated to use the forest value description from the Enhanced Forest Resource Inventory" FOLDED="true"/>
                </node>
                <node TEXT="DomainValidation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="Identify all species that occur in OSPCOMP / USPCOMP" FOLDED="true"/>
                        <node TEXT="Age class distributions by..." FOLDED="true"/>
                    </node>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="YRORG:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YRORG" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Year of Origin" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The modelled year of origin attribute indicates the average year that the species having the greatest relative abundance (in terms of basal area) within the species composition field (SPCOMP), came into existence. Note: The population of this field is a management decision based on the information found in the overstorey and understorey attributes delivered in the FRI. This value will be used in the modelling and analysis of the forest management plan and provided with the submission of the BMI and OPI." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The year of origin is used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration.   Once a forest stand has been assessed as free-to-grow, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site.   In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered.  For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach free-growing conditions), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip has achieved free-to-grow status, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required.  Normally, updates to the year of origin have a corresponding change to the year of update attribute as this indicates the currency/vintage of the information and when the year of origin value was determined.   Year of origin information is not required for non-forested and non-productive forest land types. " FOLDED="true"/>
                            <node TEXT="This is a calculated value. Year of origin is calculated as the year of the data source from which age is determined  minus  the understorey age as determined using the source  (i.e.,  YRORG  =  year of age source - UAGE).  For example, using imagery taken in 2007, the age of the stand is interpreted to be 50 years at that time (in 2007).  Thus the year of origin for the stand is 2007 - 50 = 1957.  Note that the source used to determine age may not be the data source listed in the SOURCE attribute if multiple data sources were used to generate the stand description.  " FOLDED="true">
                                <icon BUILTIN="help"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR (checked via other validations)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A zero or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The YRORG attribute value must be greater than or equal to 1600 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The YRORG attribute must be less than the plan period start year when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST2: The YRORG attribute value should be greater than or equal to OYRORG when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="ST2: The YRORG attribute value should be less than or equal to UYRORG when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="ST2: The YRORG attribute value should not be greater than YRSOURCE when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 1600' ${:NF} row['${name}'] &lt; 1600 ]   " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than the plan period start year when POLYTYPE is FOR' ${:NF} row['${name}'] &gt; ${planyear}  ]   " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be greater than or equal to OYRORG' ${:NF} row['${name}'] &lt; row['OYRORG'] ]   " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than or equal to UYRORG when POLYTYPE is FOR' ${:NF} row['UYRORG'] not in ${list_NULL} and row['${name}'] &gt; row['UYRORG'] and row['${name}'] != row['OYRORG']    ]   " FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should not be greater than YRSOURCE' ${:NF} row['${name}'] &gt; row['YRSOURCE'] ]   " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':1999 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1599, 'YRSOURCE':2000 } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 1600" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':2029, 'YRSOURCE':2030, 'AGE':-9  } " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than the plan period start year when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1999, 'OYRORG':2000, 'YRSOURCE':2011 } " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be greater than or equal to OYRORG" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1999, 'UYRORG':1990, 'OYRORG':1900, 'YRSOURCE':2011 } " FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than or equal to UYRORG when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':1981, 'YRSOURCE':1980, 'AGE':39  } " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-0"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should not be greater than YRSOURCE" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="1950" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SPCOMP:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="120" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SPCOMP" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Modelled Species Composition" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="This modelled species composition attribute indicates the tree species that are present in the forest stand canopy and the proportion of cover that each species occupies. Note: The population of this field is a management decision based on the information found in the overstorey and understorey attributes delivered in the FRI. This value will be used in the modelling and analysis of the forest management plan and provided with the submission of the BMI and OPI." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="repeating pattern of species code and corresponding proportion value each species code is 3 characters (including blanks) and is left justified each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified.   maximum of 20 species and proportions pairs in the string " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: No duplicate species codes allowed in the string" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Proportion values in the string must sum to 100" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: The tree species in the composition are to be coded using the scheme listed here" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="If SOURCE is NOT plot based then allow specific species in SPCOMP (eFRI tech spec)" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                </node>
                                <node TEXT="Tree species in the list [     'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn',     'CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf',     'OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX',     'OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX',     'Sb','Sw','la','Wb','Wi'     ]" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST%s: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; %s'%( '2' if 'WARNING' in i else '1'   ,i)    for i in test_spcomp(row['${name}']) if ${NINN} if i not in  [ None, [ None ] , []  ] ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; - Invalid species code' for s in test_SpeciesInList(row['${name}'],    [ 'AL','Aq','Ap','Ag','Bc','Bp','Gb','Bb','Cat','Cc','Cm', 'Cp','Cs','Ct','Er','Eu','Hk','Ht','Hl','Hb','Hm','Hp', 'Hs','Hc','Kk','Le','Lj','Bl','Ll','Lb','Gt','Mb','Mf','Mm','Mt','Mn','Mp','AM','Ema','Mo','Obl','Ob','Och','Op', 'Os','Osw','Pa','Pn','Pp','Pc','Ph','Pe','Red','Ss','Sc','Sk','Sn','Sr','Sy','Tp','Haz' ,                               'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi'    ]     ) if ${FOR} and ${NINN}  ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="EFRI Only" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; Invalid species code where SOURCE is not plot' for s in test_SpeciesInList(row['${name}'],  [ 'AX','Ab','Aw','Pl','Pt','Bd','Be','Bg','Bw','By','Bn','CE','Cr','Cw','CH','Cb','Cd','OC','Pd','EX','Ew','Bf','OH','He','Hi','Iw','La','LO','MX','Mh','Mr','Ms','OX','Or','Ow','PX','Pj','Pr','Ps','Pw','PO','Pb','SX', 'Sb','Sw','la','Wb','Wi' ]         )  if ${FOR} and ${NINN} and row['SOURCE'][:4] != 'PLOT'    ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Ob  90Po  10', 'SOURCE':'SPECTRAL', 'LEADSPC':'Ob'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; Invalid species code where SOURCE is not plot" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Ob  90Po  10', 'SOURCE':'PLOTVAR', 'LEADSPC':'Ob'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'Sb 100' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR' }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb  100'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP not multiples of 6 characters: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb NaN'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPComp must have numeric codes for species occurrence: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb  50Sw  50Po  50'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP does not add to 100: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Sb  50Sb  50'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP cannot have duplicate species occurrences: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Po  40Sb  60'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; WARNING SPCOMP values are not in Descending Order: " FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Pt  90Oz  10', 'LEADSPC':'Pt'}" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; - Invalid species code" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="Sb 100" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="LEADSPC:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="LEADSPC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Modelled Leading Species" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The modelled leading species attribute indicates the most prevalent species in the forest stand canopy. Note: The population of this field is a management decision based on the information found in the overstorey and understorey attributes delivered in the FRI. This value will be used in the modelling and analysis of the forest management plan and provided with the submission of the BMI and OPI." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Use the same coding as is listed in the overstorey species composition attribute description." FOLDED="true"/>
                            <node TEXT="Must be species listed in the understorey species composition" FOLDED="true"/>
                            <node TEXT="This leading species attribute replaces the working group attribute (WG) that was present in previous inventory products for determining the major species to manage for in a forest stand." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="The tree species in the understorey leading species are to be coded using the scheme listed in SPCOMP. This test is implied by the requirement to have the LEADSPC in the SPCOMP, which are already tested." FOLDED="true"/>
                                <node TEXT="ST1: Must be species listed in the species composition (SPCOMP) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be the species with the greatest percentage or tied for the greatest percentage in the species composition (SPCOMP) when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be listed in the SPCOMP' ${:NF} row['${name}'] not in row['SPCOMP']  ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be the most common species in SPCOMP if populated' ${:NF} row['${name}'].strip(' ') not in get_leadspc(row['SPCOMP'])  and row['${name}'] in row['SPCOMP']  ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Po', 'SPCOMP':'Sb 100'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be listed in the SPCOMP" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'Po', 'SPCOMP':'Sb  90Po  10'}" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be the most common species in SPCOMP if populated" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'Sb' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="SB" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AGE:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Modelled Age" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The modelled age attribute indicates the average age of the dominant and co-dominant trees based on the leading species in the forest stand canopy as of the start date of the new plan period. This is a numeric value calculated on the difference between the plan start year and the YRORG value." FOLDED="true"/>
                            <node TEXT="The age attribute must be calculated based on the difference between the plan period start date and the forest stand year of origin. For example, if the start date of the plan period is April 1, 2028 and the year of origin for a forest stand is 1948, then the average age of the forest stand is 80 years." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Age must be determined for all productive forest areas on the forest management unit and is used to determine the age class information which is required in the preparation of several FMP tables, schedules, and reports. Age class, similar to age, must also be determined based on the start date of the plan period." FOLDED="true"/>
                            <node TEXT="Note: The population of this field is a management decision based on the information found in the overstorey and understorey attributes delivered in the FRI. This value will be used in the modelling and analysis of the forest management plan and provided with the submission of the BMI and OPI." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory " FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A zero value is a valid code, but only when DEVSTAGE is DEPHARV or DEPNAT and POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1:  The plan period start year minus the YRORG attribute value should equal the AGE attribute value." FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE = FOR' ${:F}  row['${name}'] in [ None, '', ' ']  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; can be zero only when DEVSTAGE is DEPHARV or DEPNAT (when POLYTYPE = FOR).' ${:F} ${NIN} and row['DEVSTAGE'] not in ['DEPHARV','DEPNAT'] ]   " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must equal plan period start year minus YRORG when POLYTYPE equals FOR' ${:NF} row['YRORG'] not in ${list_NULL} and row['${name}'] != (int(${planyear}) - int(row['YRORG']))]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':12 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':0, 'DEVSTAGE':'NAT' } " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; can be zero only when DEVSTAGE is DEPHARV or DEPNAT (when POLYTYPE = FOR)." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':10, 'YRORG':1950 } " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must equal plan period start year minus YRORG when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="70" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="HT:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4.1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Double" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HT" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Modelled Height" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The modelled height attribute indicates the estimated average tree height (in meters)." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Note: The population of this field is a management decision based on the information found in the overstorey and understorey attributes delivered in the FRI. This value will be used in the modelling and analysis of the forest management plan and provided with the submission of the BMI and OPI." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="Height / Age relationships should be in the range of Plonski and others." FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory " FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1:The attribute population must follow the correct format when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A zero value is a valid code when POLYTYPE equals FOR (not explicitly tested)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST1: If DEVSTAGE does not start with DEP, NEW, or LOW then the HT attribute must be greater than zero (HT &gt; 0) when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST2: The HT attribute should be greater than or equal to UHT when POLYTYPE equals FOR (BMI Only)" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="ST2: The HT attribute should be less than or equal to OHT when POLYTYPE equals FOR (BMI Only)" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE = FOR' ${:F} row['${name}'] in [ None, '', ' ']  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR' ${:NF}  (row['${name}'] &lt; 0 or row['${name}'] &gt; 40) ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than 0 if DEVSTAGE is not DEPHARV,DEPNAT, NEWNAT, NEWPLANT, NEWSEED, LOWNAT or LOWMGMT when POLYTYPE is FOR' ${:F} row['${name}'] not in [None, '', ' ' ] and row['DEVSTAGE']  not in [ 'DEPHARV','DEPNAT','NEWNAT','NEWPLANT','NEWSEED','LOWNAT',' LOWMGMT' ] and row['${name}'] &lt;= 0 ]" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be &gt;= UHT' ${:NF}  row['${name}'] &lt; row['UHT'] and ${BMI} ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be &lt;= OHT' ${:NF}  row['${name}'] &gt; row['OHT'] and ${BMI} ]" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':12 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None, 'DEVSTAGE':'DEPNAT' } " FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE = FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':42}" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':0, 'DEVSTAGE':'NAT' } " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than 0 if DEVSTAGE is not DEPHARV,DEPNAT, NEWNAT, NEWPLANT, NEWSEED, LOWNAT or LOWMGMT when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':12, 'UHT':15 }" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be &gt;= UHT" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':12, 'OHT':9}" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be &lt;= OHT" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="20" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="CCLO:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CCLO" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Modelled Crown Closure" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The modelled crown closure attribute contains the percent of crown closure of the forest stand. Crown closure is defined as the percentage of ground area covered by the vertical projection of the tree crowns onto the ground." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Note: The population of this field is a management decision based on the information found in the overstorey and understorey attributes delivered in the FRI. This value may be used in the modelling and analysis of the forest management plan and provided with the submission of the BMI and OPI." FOLDED="true"/>
                            <node TEXT="The maximum crown closure value is 100 percent. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                            <node TEXT="A zero value is a valid code when POLYTYPE is not FOR or when POLYTYPE is FOR and VERT is not T or M" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1:The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A zero value is a valid code when POLYTYPE equals FOR (not explicitly tested)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: The attribute value must be less than or equal to 100 and greater than or equal to zero (CCLO &lt;= 100 and CCLO &gt;=0) when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE = FOR' ${:F} row['${name}'] in [ None, '', ' ' ]   ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 100 when POLYTYPE is FOR' ${:NF} (row['${name}'] &lt; 0 or row['${name}'] &gt; 100)  ]   " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':50 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when POLYTYPE = FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':101, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 100 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="90" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="STKG:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4.2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="double" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="STKG" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Stocking" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The stocking attribute indicates a qualitative measure of the density of tree cover. It is expressed as a percentage value ranging from zero to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field. The population of this field is a management decision based on the information found in the overstorey and understory attributes delivered in the FRI. This value will be used in the modelling and analysis of the forest management plan and provided with the submission of the BMI and OPI." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Stocking is determined by comparing the actual basal area measured from field sampling to the relative basal area of a fully-stocked stand using Plonski's Normal Yield Tables. (Plonski's Normal Yield Tables were developed from permanent sample plots established for several of the major tree species in Ontario.) Stocking can also be determined from aerial photography based on the degree of canopy closure, average age, height, and species composition. Actual basal area collected from field sampling is used to calibrate stocking assessments made from photo-interpretation.  The silvicultural ground rules in a forest management plan describe the standards for assessing the regeneration of forest stands, based on forest unit, desired species composition, age, height, and stocking. If the stocking of a productive forest stand does not meet the regeneration standards in the silvicultural ground rules of an approved forest management plan, the forest stand will be considered as NSR and must be classified into the appropriate below regeneration standards stage of development.  In some cases, the regeneration and/or management standards of a silvicultural ground rule may be expressed as a density, which usually describes the frequency or number of stems per hectare. Where density information (stems/hectare) has been collected or determined from a regeneration survey, this information must be converted to a stocking value for the purpose of updating the forest stand description information. " FOLDED="true"/>
                            <node TEXT="This definition needs to be brought up to date using the current eFRI specification. There may (or may not) be a difference between traditional FRI and eFRI stocking calculations." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1:  The attribute population must follow the correct format  when POLYTYPE is equal to FOR (valid numeric values are between 0 and 4) " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: If the DEVSTAGE attribute is NAT or EST*, then STKG must be greater than 0.00 when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST2: If the DEVSTAGE attribute starts with NAT or EST, then the STKG attribute should be greater than or equal to 0.40  when POLYTYPE is equal to FOR." FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node COLOR="#000000" TEXT="ST2: If DEVSTAGE attribute starts with DEP, then STKG attribute should equal 0.00 when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST2: The attribute value should be less than 2.5 when POLYTYPE equals FOR" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 4.0 when POLYTYPE equals FOR' ${:F} (row['${name}'] &lt; 0 or row['${name}'] &gt; 4.0 or row['${name}'] in ['', ' ', None ] ) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than 0.00 when DEVSTAGE is NAT or EST* and POLYTYPE equals FOR' ${:F} row['${name}'] &lt;= 0.00 and row['DEVSTAGE']  in [ 'NAT','ESTPLANT','ESTSEED','ESTNAT' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be greater than or equal to 0.40 when DEVSTAGE is NAT or EST* and POLYTYPE equals FOR' ${:NF}  row['${name}'] &lt; 0.40 and row['DEVSTAGE']  in [ 'NAT','ESTPLANT','ESTSEED','ESTNAT' ]   ]" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="['ST2: &lt;mark&gt;${name}&lt;/mark&gt; should = 0.0 when DEVSTAGE is DEP* when POLYTYPE is FOR' ${:NF} row['DEVSTAGE']  in [ 'DEPNAT','DEPHARV' ]  ] " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="['ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than 2.5' ${:NF} row['${name}'] &gt; 2.5   ]" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':1.0 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR', 'DEVSTAGE':'BLKSTRIP'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 4.0 when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':-1, 'POLYTYPE':'FOR', 'DEVSTAGE':'BLKSTRIP'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 0 and  ${name} &lt;= 4.0 when POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0, 'POLYTYPE':'FOR' , 'DEVSTAGE':'NAT'} " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than 0.00 when DEVSTAGE is NAT or EST* and POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0.1, 'POLYTYPE':'FOR' , 'DEVSTAGE':'NAT'} " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be greater than or equal to 0.40 when DEVSTAGE is NAT or EST* and POLYTYPE equals FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':0.1, 'POLYTYPE':'FOR' , 'DEVSTAGE':'DEPNAT'} " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should = 0.0 when DEVSTAGE is DEP* when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':3.5, 'POLYTYPE':'FOR' }" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be less than 2.5" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SC:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SC" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Site Class" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The site class attribute indicates a site quality estimate for the of a forest stand.  Note: The population of this field is a management decision based on the information found in the overstorey and understory attributes delivered in the FRI. This value will be used in the modelling and analysis of the forest management plan and provided with the submission of the BMI and OPI." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The site class value must be greater than or equal to 0 and less than or equal to 4 (SC &gt;=0 and SC &lt;=4)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: Transferred from FORMOD Attribute - If the FORMOD attribute equals PF, then the SC attribute should equal 4  when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR' ${:F} row['${name}'] not in [ 0,1,2,3,4 ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="[ 'ST2: Transferred from FORMOD attribute - when &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; is 4 then FORMOD is most likely PF' ${:F}   row['${name}'] == 4 and row['FORMOD'] in [ 'RP','MR' ]  ]   " FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':12 }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':None, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':-1, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':28, 'POLYTYPE':'FOR'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be greater than or equal to 0 and less than or equal to 4 when POLYTYPE is FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':4, 'FORMOD':'RP' , 'POLYTYPE':'FOR' }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST2: Transferred from FORMOD attribute - when &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; is 4 then FORMOD is most likely PF" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="0:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="0" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Best" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="1:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="1" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Better" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="2:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="2" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Good" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="3:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="3" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Poor" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="4:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="4" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Protection Forest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MANAGED:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MANAGED" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Managed / Unmanaged Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="MANAGED" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The managed / unmanaged indicator attribute applies to Crown forest areas only.  The attribute indicates whether or not there is a legal or land use planning decision which prevents the land from being managed for timber." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'M' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="M:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="M" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The Crown forest area can be managed for timber production." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Managed" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="U:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="U" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="There exists a legal or land use planning decision which prevents the Crown forest from being managed for timber production." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unmanaged" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SMZ:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SMZ" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Strategic Management Zone" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The strategic management zone attribute indicates the unique short form identifier given to a strategic management zone.  " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="A strategic management zone is a geographical separation or sub-division of the area within a forest management unit.  In relation to forest diversity and landscape classes, the strategic management zone refers to similar areas within a forest management unit which are physically or geographically separated from each other but are grouped for management purposes.    " FOLDED="true"/>
                            <node TEXT=" Examples of strategic management zones are ecological zones (eco-regions or ecodistricts), watershed zones, large landscape patches (LLP), wilderness zones, industrial working circles/operating units, and intensive forest management areas.  Strategic management zone information may be used in the preparation of forest management plan tables. Strategic management zone information may also influence forest modelling and landscape diversity analysis. If a forest management unit is divided into strategic management zones, Sustainable Forest Licence holders (plan holders) must provide the strategic management zone identification code and a name that describes the strategic management zone for all licenced Crown lands within a designated forest management unit. i.e every polygon with a POLYID" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="PLANFU:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Plan Forest Unit" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="PLANFU" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The forest unit attribute indicates the unique short form label / ID given to an aggregation of forest stands for management purposes which have similar species composition, develop in a similar manner (both naturally and in response to silvicultural treatments), and are managed under the same silvicultural system." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Sustainable forest licensees must identify a forest unit for all productive forest areas on Crown lands within a forest management unit." FOLDED="true"/>
                            <node TEXT="The forest unit information is used to create tables FMP-2, Description of Forest Units, and FMP-3, Summary of Managed Crown Productive Forest by Forest Unit. Forest unit information is also used to support the preparation of several other FMP tables, schedules, and reports as well as to support forest modeling, landscape diversity analysis, and the development of a management strategy." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'PLANFU_default' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AU:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Analysis Unit" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="20" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AU" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="Analysis units are refined forest units used in modelling to more accurately project forest development. The degree to which forest units and analysis units are represented in the models depends on the strategic models used in the decision support system (i.e. spatial versus non-spatial). " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Analysis units are a subdivision of a forest unit used in the models to represent a refinement of model inputs, transitions or constraints that typically reflect differences in forest cover or site qualities within the forest unit. The degree to which analysis units are represented in the models may depend on the strategic model used in the decision support system." FOLDED="true"/>
                            <node TEXT="Standardized model inputs will be provided on an analysis unit basis." FOLDED="true"/>
                            <node TEXT="Analysis units are a modelling construct, plan forest units are the basis for plan decisions and harvest regulation. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory." FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'AU_default' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="PjPure" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AVAIL:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Availability Indicator" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AVAIL" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="AVAIL" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The availability indicator attribute identifies which portions of the managed Crown production forest are available for timber production or not. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Sustainable forest licensees must identify the areas of managed Crown production forest, by forest stand, which are available or unavailable for timber production. The determination of availability is a management planning decision based on considering the productive forest modifier, recent changes to any operational guidelines since the last plan, and prohibitions of operation areas that were identified during area of concern planning (both past and present). The productive forest modifier attribute identifies whether a forest stand is designated as production forest (RF), production forest - designated management reserve (MR), or protection forest (PF). Normally, productive forest areas which are designated as production forest are considered as forest stands which are available for timber production. Productive forest areas that are designated as protection forest are usually considered as forest stands which are not available for timber production. Productive forest areas which are designated as production forest - designated management reserve are also normally considered as forest stands which are not available for timber production. The decision regarding the availability of a forest stand area for forest management must be identified in the availability indicator attribute as either available or unavailable. The sum of the available production forest area, by forest stand and age class, as determined from the age attribute, should correspond to the forest unit and age class subtotals in table FMP-3, Summary of Managed Crown Productive Forest by Forest Unit. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                                <node TEXT="ST1: If the MANAGED attribute equals U, then the AVAIL attribute should be U when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="ST2: If the FORMOD attribute equals PF, then AVAIL attribute should be equal to U when POLYTYPE equals FOR and OWNER equals 1" FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="ST2: If the SC attribute equals 4, then the AVAIL attribute should be U when POLYTYPE is equal to FOR and OWNER equals 1" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="ST2: If the ACCESS1 and ACCESS2 attributes are not equal to NON, then the AVAIL attribute should be U when POLYTYPE is equal to FOR and OWNER equals 1" FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and MANAGED IS U' ${:F} row['${name}'] != 'U'  and row['MANAGED'] == 'U'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                </node>
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when FORMOD is PF and OWNER is 1' ${:F} row['${name}'] != 'U' and row['FORMOD'] == 'PF' and row['OWNER'] == '1'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                </node>
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when POLYTYPE is FOR and SC equals 4 ( and OWNER 1)' ${:F} row['${name}'] != 'U' and row['SC'] == 4  and row['OWNER'] == '1']   " FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                </node>
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when ACCESS1 and ACCESS2 are not NON (and OWNER 1)' ${:F} row['${name}'] !='U' and row['ACCESS1'] not in [  'NON' ]  and row['ACCESS2'] not in [ None, 'NON' ] and row['OWNER'] == '1'  ]   " FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'A' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A', 'MANAGED':'U' }" FOLDED="true">
                                    <icon BUILTIN="full-6"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and MANAGED IS U" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A', 'OWNER':'1', 'FORMOD':'PF' , 'MGMTCON1':'ROCK' }    " FOLDED="true">
                                    <icon BUILTIN="full-7"/>
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when FORMOD is PF and OWNER is 1" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A', 'OWNER':'1', 'SC':4 }" FOLDED="true">
                                    <icon BUILTIN="full-8"/>
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when POLYTYPE is FOR and SC equals 4 ( and OWNER 1)" FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A', 'OWNER':'1', 'ACCESS1':'LUD', 'ACCESS2':'OWN' }  " FOLDED="true">
                                    <icon BUILTIN="full-9"/>
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when ACCESS1 and ACCESS2 are not NON (and OWNER 1)" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="A:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="A" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Crown production forest that can be managed for timber production." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Available" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="U:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="U" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Managed Crown production forest that is not available for timber production as a result of reserve prescriptions developed in a forest management plan (e.g., area of concern reserve)." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Unavailable" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SILVSYS:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural System" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The silviculture system attribute indicates the process by which a productive forest stand will be managed for timber production purposes. The process/system is classified according to the method of harvesting that will be used." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="There are three basic silvicultural systems: clear-cut, shelterwood, and selection. Sustainable forest licensees must identify the applicable silvicultural system for those forest stands that have been identified as available for timber production in the availability indicator attribute. The stage of development attribute provides an indication of the most recent silvicultural system that was applied to each forest stand. Therefore, the silvicultural system management decision attribute should normally correspond to the silvicultural system that is associated with the stage of development attribute. However, sustainable forest licensees may identify a more appropriate silvicultural system based on the forest unit for the purposes of the new FMP. Therefore, the silvicultural system attribute represents the silvicultural system that will be applied to a forest stand. In some cases, this may differ from the silvicultural system that was employed in the past. The silvicultural system must be identified for each stand, although it is normally assigned on a forest unit basis." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="Rule we aren't testiing - policy compliance but not a sensible error" FOLDED="true">
                                <node TEXT="ST1: A null or blank value is not a valid code when AVAIL is equal to A" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true"/>
                                </node>
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null if POLYTYPE equals FOR and AVAIL is A' ${:F} ${NIN} and row ['AVAIL'] == 'A'    ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'CC' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A silvicultural system of regenerating an even-aged forest stand in which new seedlings become established in fully exposed micro-environments after most or all of the existing trees have been removed. Regeneration can originate naturally or can be applied artificially. Clearcutting may be done in blocks, strips or patches." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An uneven-aged silvicultural system where mature and/or undesirable trees are removed individually or in small groups over the whole area, usually in the course of a cutting cycle. Regeneration is generally natural." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged silvicultural system where mature trees are harvested in a series of two or more cuts (preparatory, seed, first removal, final removal) for the purpose of obtaining natural regeneration under shelter of the residual trees, either by cutting uniformly over the entire stand area or in narrow strips. Regeneration is natural or artificial. The regeneration interval determines the degree of even-aged uniformity." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Shelterwood" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="NEXTSTG:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="NEXTSTG" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="NEXTSTG" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The next stage attribute indicates the next harvest or cut treatment that is planned to occur in an available productive forest stand being managed for timber production. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="The next stage attribute indicates the next harvest or cut treatment that is planned to occur for an available productive forest stand. The next stage depends on the silvicultural system employed. Licensees must identify the next harvest treatment that will occur for each forest stand which is available for timber production based on the availability indicator attribute.   The next stage often corresponds to the stage of development attribute. The stage of development attribute represents the current development state, and/or the current stage of silvicultural management for each productive forest stand.   The next stage is most applicable to the forest stands that have been selected for planned operations (harvest) within the new plan term. The next stage will be used to populate the stage of management in the operational tables in the forest management plan (i.e., FMP-11 and FMP-16). " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR and AVAIL is equal to A" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR and AVAIL is equal to A" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: A null or blank value is not a valid code when AVAIL is equal to A" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null if POLYTYPE equals FOR and AVAIL is A' ${:F} ${NIN} and row ['AVAIL'] == 'A'    ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'CONVENT' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':'A Decidedly Invalid Code'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None, 'AVAIL':'A' }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null if POLYTYPE equals FOR and AVAIL is A" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="comment" FOLDED="true">
                            <node TEXT=" (MGMTSTG)" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="THINPRE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINPRE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Free-growing productive forest areas which receive a mid-rotation partial harvest (reduction in the growing stock)  that is designed to meet various objectives such as improving tree spacing, removing trees not suited to the site, and promoting the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a pre-commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="THINCOM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="THINCOM" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Free-growing productive forest areas which receive a mid-rotation partial harvest (reduction in the growing stock)  that is designed to meet various objectives such as improving tree spacing, removing trees not suited to the site, and promoting the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a commercial thinning/spacing treatment" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CONVENT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CONVENT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of most or all of the existing trees in a stand (or a number of adjacent stands) in one operation, so that new seedlings become established in a fully exposed micro-environments.  Harvesting patterns include conventional clearcuts, block cuts and patch cuts.  " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a conventional clearcut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BLKSTRIP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BLKSTRIP" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="The removal of a portion of the existing trees in a stand in progressive strips in more than one operation so that the non-disturbed portion of the stand is left primarily to provide a natural seed source for regeneration of the disturbed area.  Several cutting patterns are available to achieve same goal.  The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters (5 chains) wide.  It is designed to encourage regeneration on difficult and/or fragile sites.  Note:  Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a modified cut: block or strip" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDTREE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDTREE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged, silvicultural system that retains mature standing trees scattered throughout the cutblock to provide seed sources for natural regeneration.  A method of harvesting and regenerating a forest stand in which all trees are removed from the area except for a small number of seed-bearing trees that are left singly or in small groups.  The objective is to create an even-aged stand. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a modified cut: seed tree" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SCNDPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system, harvest may be planned in two passes.  This is normally when species within the stand are harvested and utilized by different logger/contractor/forest resource Licensee in different years (e.g., first pass is conifer and second-pass is hardwood).  A first pass should have been recorded in the annual report if merchantable tree species remained in the forest stand which have been allocated for harvest - but not yet harvested.  The second-pass option should be denoted when merchantable tree species are selected to be harvested from forest stands which have been previously recorded as harvested in a first pass." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a modified cut: next / second-pass" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="PREPCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PREPCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source.  The removal of undesirable trees opens the canopy and enables the crowns of remaining seed-bearing trees to enlarge; to improve conditions for seed production and natural regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a preparatory cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SEEDCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SEEDCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand in order to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a seed cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FIRSTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FIRSTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a first removal harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="LASTCUT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="LASTCUT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A shelterwood silvicultural system stage of management where all of the remaining trees in the overstorey are removed.  This is the removal of the seed or shelter trees after the regeneration has been effective." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a last removal harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="IMPROVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="IMPROVE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition. distribution and quality by removing less desirable trees of any species." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive an improvement cut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SELECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SELECT" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment. " FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Will receive a selection harvest" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="CONVENT" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="YIELD:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="YIELD" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="YIELD" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The yield attribute contains a term used in forest modeling that means the projected yield and not the treatments to be implemented." FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="Not present for PCI" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'PRSNT' }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'POLYTYPE':'FOR','${name}':None}" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="fields_include:" FOLDED="true">
                    <node TEXT="PCI" FOLDED="true">
                        <node TEXT="POLYID" FOLDED="true"/>
                        <node TEXT="POLYTYPE" FOLDED="true"/>
                        <node TEXT="OWNER" FOLDED="true"/>
                        <node TEXT="AUTHORTY" FOLDED="true"/>
                        <node TEXT="YRSOURCE" FOLDED="true"/>
                        <node TEXT="SOURCE" FOLDED="true"/>
                        <node TEXT="FORMOD" FOLDED="true"/>
                        <node TEXT="DEVSTAGE" FOLDED="true"/>
                        <node TEXT="YRDEP" FOLDED="true"/>
                        <node TEXT="DEPTYPE" FOLDED="true"/>
                        <node TEXT="OYRORG" FOLDED="true"/>
                        <node TEXT="OSPCOMP" FOLDED="true"/>
                        <node TEXT="OLEADSPC" FOLDED="true"/>
                        <node TEXT="OAGE" FOLDED="true"/>
                        <node TEXT="OHT" FOLDED="true"/>
                        <node TEXT="OCCLO" FOLDED="true"/>
                        <node TEXT="OSTKG" FOLDED="true"/>
                        <node TEXT="OSC" FOLDED="true"/>
                        <node TEXT="UYRORG" FOLDED="true"/>
                        <node TEXT="USPCOMP" FOLDED="true"/>
                        <node TEXT="ULEADSPC" FOLDED="true"/>
                        <node TEXT="UAGE" FOLDED="true"/>
                        <node TEXT="UHT" FOLDED="true"/>
                        <node TEXT="UCCLO" FOLDED="true"/>
                        <node TEXT="USTKG" FOLDED="true"/>
                        <node TEXT="USC" FOLDED="true"/>
                        <node TEXT="INCIDSPC" FOLDED="true"/>
                        <node TEXT="VERT" FOLDED="true"/>
                        <node TEXT="HORIZ" FOLDED="true"/>
                        <node TEXT="PRI_ECO" FOLDED="true"/>
                        <node TEXT="SEC_ECO" FOLDED="true"/>
                        <node TEXT="ACCESS1" FOLDED="true"/>
                        <node TEXT="ACCESS2" FOLDED="true"/>
                        <node TEXT="MGMTCON1" FOLDED="true"/>
                        <node TEXT="MGMTCON2" FOLDED="true"/>
                        <node TEXT="MGMTCON3" FOLDED="true"/>
                    </node>
                </node>
            </node>
            <node TEXT="OperationalPlanningInventory:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="OperationalPlanningInventory" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="OPI" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Operational Planning Inventory (OPI)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26915" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="OMZ:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="OMZ" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Operational Management Zone" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="" FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{'${name}':None}" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SGR:" FOLDED="true" BACKGROUND_COLOR="#ccffcc">
                        <icon BUILTIN="flag"/>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="25" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Character" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SGR" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silviculture Ground Rule" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="" FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="list"/>
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A null or blank value is not a valid code when  AVAIL is A and AGE &gt;= 30" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="The attribute must be Null when POLYTYPE is not equal to FOR" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when  AVAIL is A and AGE &gt;=30' ${:F} ${NIN} and row['AVAIL'] == 'A'  and row['AGE'] &gt;= 30 ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:None, 'AVAIL':'A', 'AGE':70, 'POLYTYPE':'FOR'  }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null when  AVAIL is A and AGE &gt;=30" FOLDED="true"/>
                                </node>
                                <node TEXT="{ 'POLYTYPE':'GRS', 'FORMOD':None, 'DEVSTAGE':None, 'OYRORG':None, 'OSPCOMP':None, 'OLEADSPC':None, 'OAGE':None, 'OHT':None, 'OCCLO':None, 'OSTKG':None, 'OSC':None, 'UYRORG':None, 'USPCOMP':None, 'ULEADSPC':None, 'UAGE':None, 'UHT':None, 'UCCLO':None, 'USTKG':None, 'USC':None, 'INCIDSPC':None, 'ACCESS1':None, 'ACCESS2':None, 'MGMTCON1':None, 'MGMTCON2':None, 'MGMTCON3':None, 'YRORG':None, 'SPCOMP':None, 'LEADSPC':None, 'AGE':None, 'HT':None, 'CCLO':None, 'STKG':None, 'SC':None, 'MANAGED':None, 'PLANFU':None, 'AU':None, 'AGESTR':None, 'AVAIL':None, 'SILVSYS':None, 'NEXTSTG':None, 'YIELD':None, 'SGR':None, '${name}':'COLD' }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be null if POLYTYPE is not FOR..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true">
                        <node TEXT="[ 'ST1: POLYID Field values must be unique' for f in ['POLYID' ] if not self.uniqueValueTest(f) ]" FOLDED="true"/>
                    </node>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The Base Model Inventory is conceptually an update of the 2009 specification of the Base Model updated to use the forest value description from the Enhanced Forest Resource Inventory" FOLDED="true"/>
                </node>
                <node TEXT="DomainValidation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="Identify all species that occur in OSPCOMP / USPCOMP" FOLDED="true"/>
                        <node TEXT="Age class distributions by..." FOLDED="true"/>
                    </node>
                </node>
                <node TEXT="fields_include:" FOLDED="true">
                    <node TEXT="PCI" FOLDED="true">
                        <node TEXT="POLYID" FOLDED="true"/>
                        <node TEXT="POLYTYPE" FOLDED="true"/>
                        <node TEXT="OWNER" FOLDED="true"/>
                        <node TEXT="AUTHORTY" FOLDED="true"/>
                        <node TEXT="YRSOURCE" FOLDED="true"/>
                        <node TEXT="SOURCE" FOLDED="true"/>
                        <node TEXT="FORMOD" FOLDED="true"/>
                        <node TEXT="DEVSTAGE" FOLDED="true"/>
                        <node TEXT="YRDEP" FOLDED="true"/>
                        <node TEXT="DEPTYPE" FOLDED="true"/>
                        <node TEXT="INCIDSPC" FOLDED="true"/>
                        <node TEXT="VERT" FOLDED="true"/>
                        <node TEXT="HORIZ" FOLDED="true"/>
                        <node TEXT="PRI_ECO" FOLDED="true"/>
                        <node TEXT="SEC_ECO" FOLDED="true"/>
                        <node TEXT="ACCESS1" FOLDED="true"/>
                        <node TEXT="ACCESS2" FOLDED="true"/>
                        <node TEXT="MGMTCON1" FOLDED="true"/>
                        <node TEXT="MGMTCON2" FOLDED="true"/>
                        <node TEXT="MGMTCON3" FOLDED="true"/>
                    </node>
                    <node TEXT="BMI" FOLDED="true">
                        <node TEXT="YRORG" FOLDED="true"/>
                        <node TEXT="SPCOMP" FOLDED="true"/>
                        <node TEXT="LEADSPC" FOLDED="true"/>
                        <node TEXT="AGE" FOLDED="true"/>
                        <node TEXT="HT" FOLDED="true"/>
                        <node TEXT="CCLO" FOLDED="true"/>
                        <node TEXT="STKG" FOLDED="true"/>
                        <node TEXT="SC" FOLDED="true"/>
                        <node TEXT="MANAGED" FOLDED="true"/>
                        <node TEXT="SMZ" FOLDED="true"/>
                        <node TEXT="PLANFU" FOLDED="true"/>
                        <node TEXT="AU" FOLDED="true"/>
                        <node TEXT="AVAIL" FOLDED="true"/>
                        <node TEXT="SILVSYS" FOLDED="true"/>
                        <node TEXT="NEXTSTG" FOLDED="true"/>
                        <node TEXT="YIELD" FOLDED="true"/>
                    </node>
                </node>
            </node>
            <node TEXT="PlannedHarvest:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlannedHarvest" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PHR" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="BLOCKID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Block Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="25" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="BLOCKID" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="SILVSYS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Silvicultural System" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="2" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="SILVSYS" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The silviculture system attribute indicates the process by which a productive forest stand will be managed for timber production purposes.  The process/system is classified according to the method of harvesting that will be used. " FOLDED="true"/>
                        </node>
                        <node TEXT="description" FOLDED="true">
                            <node TEXT="There are three basic silvicultural systems: clear-cut, shelterwood, and selection. Licensees must identify the applicable silvicultural system for those forest stands that have been identified as available for timber production in the availability indicator attribute.  The stage of development attribute provides an indication of the most recent silvicultural system that was applied to each forest stand. Therefore, the silvicultural system management decision attribute should normally correspond to the silvicultural system that is associated with the stage of development attribute. However, Licensees may identify a more appropriate silvicultural system based on the forest unit for the purposes of the new forest management plan. Therefore, the silvicultural system attribute represents the silvicultural system that will be applied to a forest stand. In some cases, this may differ from the silvicultural system that was employed in the past. The silvicultural system must be identified for each stand, although it is normally assigned on a forest unit basis. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT=" ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT=" ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="CC:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CC" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A silvicultural system of regenerating an even-aged forest stand in which new seedlings become established in fully exposed micro-environments after most or all of the existing trees have been removed. Regeneration can originate naturally or can be applied artificially. Clearcutting may be done in blocks, strips or patches." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Clearcut" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SE" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An uneven-aged silvicultural system where mature and/or undesirable trees are removed individually or in small groups over the whole area, usually in the course of a cutting cycle. Regeneration is generally natural." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Selection" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SH" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="An even-aged silvicultural system where mature trees are harvested in a series of two or more cuts (preparatory, seed, first removal, final removal) for the purpose of obtaining natural regeneration under shelter of the residual trees, either by cutting uniformly over the entire stand area or in narrow strips. Regeneration is natural or artificial. The regeneration interval determines the degree of even-aged uniformity." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Shelterwood" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="HARVCAT:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Harvest Category" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="8" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="HARVCAT" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT=" The harvest disturbance category attribute indicates the planned type of harvest that  was completed." FOLDED="true"/>
                        </node>
                        <node TEXT="Description" FOLDED="true">
                            <node TEXT="Road Right-of-Way" FOLDED="true"/>
                            <node TEXT="The attribute coding scheme for harvest disturbances contains an option for recording spatial  information on road right-of-ways, aggregate pits and landings. The code is ROADROW. The  primary purpose of this code is to identify harvest area for newly constructed road right-of-ways for  primary and branch roads, aggregate pits and landings that are outside of an area of operations,  where the depleted area is not intended for future regeneration purposes and will be maintained for  its intended use. The area associated with road right-of-way, aggregate pit and landing harvests are  not included in the balancing of available harvest area as these areas are usually accounted for in  forest modelling using the estimated roads and landings percentage allowance in SFMM. There  may be exceptions, which should be discussed locally between Districts and Licensees.   " FOLDED="true"/>
                            <node TEXT="The road right-of-way code (ROADROW) can be used for roads (primary and branch), aggregate  pits or landings delineated within an area of operations provided this consideration was included in  the strategic modeling. Normally harvest polygons within area of operations do not include the road  right-of-way area for primary and branch roads, aggregate pits and landings as part of the harvested  area. Primary and branch road areas are identified by delineating the boundary of the harvest area  up to the road edge or by buffering out the approximate road width using the road centre-line to  remove the area of road. The District and licensee may agree on other ways to identify or remove  roads, aggregate pits and landings in harvest areas.   " FOLDED="true"/>
                            <node TEXT="Operational roads within a harvest polygon will be reported as part of the harvest area. Operational  roads outside of a harvest polygon but within an area of operations are not normally reported on the  harvest disturbance layer." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST1: If the harvest category is second pass (HARVCAT = SCNDPASS), then the silvicultural  system must be clearcut (SILVSYS = CC)" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: If &lt;mark&gt;HARVCAT&lt;/mark&gt; is second pass (SCNDPASS), then the silvicultural  system must be clearcut (SILVSYS = CC)' ${:} row['${name}'] in [ 'SCNDPASS' ] and row['SILVSYS'] not in [ 'CC' ]  ]" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'SCNDPASS', 'SILVSYS':'SH' }" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <node TEXT="ST1: If &lt;mark&gt;HARVCAT&lt;/mark&gt; is second pass (SCNDPASS), then the silvicultural  system must be clearcut (SILVSYS = CC)" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BRIDGING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BRIDGING" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="bridging harvest areas" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This category identifies the harvested areas that were approved as bridging under the FMP. This code is only valid in the first AR under a new FMP as these areas can only be harvested within the first AWS. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="CONTNGNT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CONTNGNT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REGULAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REGULAR" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These harvest areas were categorized as regular under the FMP." FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="regular harvest" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SALVAGE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SALVAGE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="salvage harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Salvage is not considered to be the initial form of disturbance  since most areas which require a salvage operation have been  previously disturbed by natural causes, such as fire, insect,  disease, blowdown, etc. All salvage operations are considered to  be harvesting which is often the first silviculture treatment needed  to bring a forest stand back to a more productive state, more  quickly than if left for natural succession. As such, salvage is also  considered to be a form of protection from further loss of  merchantable volume due to insects or fire (e.g., bark  beetles/borers, or fire after insect damage). Salvage operations  also reduce the risk of further natural disturbances, or volume  loss. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REDIRECT:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REDIRECT" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="redirected harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These areas are harvested under a pest management plan and count against the available harvest area of the FMP." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ACCELER:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ACCELER" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="accelerated harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="These areas are harvested under a pest management plan and  are areas in addition to the available harvest area of the FMP. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="FRSTPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FRSTPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified cut: first pass" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system,  harvest may be planned in two passes. This is normally when  species within the stand are harvested and utilized by different  logger/contractor/forest resource Licensee in different years (e.g.,  first pass is conifer and second pass is hardwood). The first pass  should be recorded if merchantable tree species remain in the  forest stands which have been allocated for harvest, but not yet  harvested." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="SCNDPASS:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCNDPASS" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Second-pass harvest" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="For areas managed using the clearcut silvicultural system,  harvest may be planned in two passes. This is normally when  species within the stand are harvested and utilized by different  logger/contractor/forest resource licensee in different years (e.g.,  first pass is conifer and second pass is hardwood). The second  pass should be recorded when merchantable tree species have  been harvested from forest stands which have been previously  reported as harvested. " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="REGULAR" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planned Harvest (PHR)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="AreaOfConcern:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="AreaOfConcern" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AOCID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AOC identifier attribute is the label assigned to a specific AOC prescription which must correspond to the label on FMP and AWS Areas Selected for Operations maps and the area of concern prescriptions in table FMP-10. The prescription can represent either a group of areas of concern with a common prescription or an individual area of concern with a unique prescription." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="Must be a code from FMP-11 - Manual Check" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="AOCTYPE:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="AOC Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AOCTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="AOCTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The AOC type attribute indicates the type of AOC prescription as either modified or reserved." FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="M:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="M" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="modified" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas which are scheduled for operations but have specific conditions or restrictions on operations as required by an AOC prescription." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="R:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="R" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="reserved" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Areas which are reserved (prohibited) from operations as required by an AOC prescription." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Area Of Concern (AOC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="AOC" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="PlannedResidualPatches:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlannedResidualPatches" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PRP" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="RESID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Redisual Patch ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="10" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RESID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The residual patch identifier attribute is a number, label or name assigned to a residual patch(es) as defined in the FMP text. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure or layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be banana' for row in [row] if row['${name}'] in [ 'banana' ] ]" FOLDED="true"/>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None  }' FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'banana'  }" FOLDED="true">
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be banana" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planned Residual Patches (PRP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="PlannedRoadCorridors:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="help"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlannedRoadCorridors" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PRC" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="ROADID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code." FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADCLAS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road class attribute identifies the class of the proposed forest access road in terms of the road use management strategy in the FMP. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="B:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="B" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="branch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="P:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="P" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Primary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRANS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Transfer Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRANS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road transfer year attribute indicates a four-digit number representing the first year of the 5-year operational term that the transfer of responsibility to the MNR is anticipated to take effect. If there is no intent to transfer responsibility to MNR during the future 20-year period there is no need to specify a year. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A zero or null value is a valid code" FOLDED="true"/>
                                <node TEXT="If TRANS value is populated, then INTENT must be populated" FOLDED="true"/>
                                <node TEXT="ST1: The value must be greater than or equal to the plan term start year" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;=  ${planyear}' ${:N} row['${name}']  &lt; ${planyear} ]   " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:1999, 'INTENT':'Detente'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;=  ${planyear}" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACYEAR:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACYEAR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control year attribute indicates a four-digit number representing the expected fiscal year (April 1 to March 31) that the access control is anticipated to take effect. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A zero value is a valid code" FOLDED="true"/>
                                <node TEXT="ST1: The value must be greater than or equal to the plan term start year and less than or equal to the plan end year" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: If access control year does not equal zero (ACYEAR NOT 0) then access control must be APPLY, REMOVE or BOTH" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;=  ${planyear} and &lt;= ${planyear} + 10' ${:N} ( row['${name}']  &lt; ${planyear}  or  row['${name}']  &gt; ${planyear} + 10 )  ]" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="[ 'ST1: If &lt;mark&gt;${name}&lt;/mark&gt; &lt;&gt; 0 then ACCESS must be APPLY, REMOVE or BOTH' ${:N} row['ACCESS'] not in [ 'APPLY','REMOVE','BOTH']   ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:2099, 'ACCESS':'APPLY'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;=  ${planyear} and &lt;= ${planyear} + 10" FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:2021, 'ACCESS':None}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: If &lt;mark&gt;${name}&lt;/mark&gt; &lt;&gt; 0 then ACCESS must be APPLY, REMOVE or BOTH" FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:2021, 'ACCESS':None}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: If &lt;mark&gt;${name}&lt;/mark&gt; &lt;&gt; 0 then ACCESS must be APPLY, REMOVE or BOTH" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road access control attribute is a field used to identify where access control activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="APPLY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="APPLY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="apply" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied  to the road segment. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REMOVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REMOVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="remove" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being  removed from the road segment." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BOTH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BOTH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="both" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied  and removed from the road segment in the same  annual report year. " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population if this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS = APPLY or BOTH)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control type should be null (CONTROL1 = null and CONTROL2 = null)" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                                <node TEXT="ST2: At a minimum, one of Construction, Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (CONSTRCT = Y or DECOM IS NOT NULL or MAINTAIN = Y or MONITOR = Y or [ACCESS = APPLY or ACCESS = REMOVE OR ACCESS = BOTH])" FOLDED="true">
                                    <icon BUILTIN="full-5"/>
                                    <icon BUILTIN="stop-sign"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:N} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR'] ]  and row['ACCESS'] not in ['APPLY', 'BOTH'] and row['DECOM']  not in [ 'BERM','SLSH','SCAR','WATX' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="['ST2: When the road access control status is remove (ACCESS = REMOVE) then the control type should be null (CONTROL1 = null and CONTROL2 = null)' ${:} row['${name}'] == 'REMOVE' and (row['CONTROL1'] not in ${list_NULL} or  row['CONTROL2'] not in ${list_NULL}) ]" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{'${name}':'REMOVE' , 'CONTROL1':'GATE', 'MONITOR':'Y' }" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control type should be null (CONTROL1 = null and CONTROL2 = null)" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="DECOM:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioning" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Berm and or Ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Scarify Road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Pile Slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Water crossing removal" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="INTENT:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="MNR Intent" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="INTENT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The decommissioning attribute is a field used to identify where decommissioning activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: If TRANS value is populated, then INTENT must be populated" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must not be null when TRANS is populated' ${:} ${NIN} and row['TRANS'] not in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:None,'TRANS':2025}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: ${name} must not be null when TRANS is populated" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MAINTAIN:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Maintenance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MAINTAIN" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="CONTROL1:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control or decommissioning type attributes indicate the method of access control to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute may also be used to identify the type of decommissioning activities scheduled on primary or branch roads that will be constructed during the year." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                            <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true"/>
                            <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of CONTROL1 or CONTROL2 is mandatory where ACCESS = BOTH or ACCESS = APPLY" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: CONTROL1 and CONTROL2 must not both be null when ACCESS is APPLY or BOTH' ${:} row['CONTROL1'] in ${list_NULL} and row['CONTROL2'] in ${list_NULL} and row['ACCESS'] in [ 'APPLY', 'BOTH' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana' }" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:None, 'CONTROL2':None, 'ACCESS':'APPLY'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: CONTROL1 and CONTROL2 must not both be null when ACCESS is APPLY or BOTH" FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:None, 'CONTROL2':None, 'ACCESS':'REMOVE'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL2:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The population of CONTROL1 or CONTROL2 is mandatory where ACCESS = BOTH or ACCESS = APPLY" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: CONTROL1 and CONTROL2 must not both be null when ACCESS is APPLY or BOTH' ${:} row['CONTROL1'] in ${list_NULL} and row['CONTROL2'] in ${list_NULL} and row['ACCESS'] in [ 'APPLY', 'BOTH' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana' }" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:None, 'CONTROL1':None, 'ACCESS':'APPLY'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: CONTROL1 and CONTROL2 must not both be null when ACCESS is APPLY or BOTH" FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:None, 'CONTROL1':None, 'ACCESS':'REMOVE'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planned Road Corridors (PRC)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="OperationalRoadBoundaries:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="OperationalRoadBoundaries" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="ORBID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Operational Road Boundaries ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="20" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ORBID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The operational road boundaries identifier attribute indicates the user defined unique number, label or name assigned to the operational road boundaries. The operational road boundary is the perimeter of, the planned harvest area plus the area from an existing road or planned road corridor to the harvest area within which an operational road is planned to be constructed." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default-orbid" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Operational Road Boundaries (ORB)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="ORB" FOLDED="true"/>
                </node>
                <node TEXT="default" FOLDED="true">
                    <node TEXT="orbid_default" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ExistingRoadUseManagementStrategy:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ExistingRoadUseManagementStrategy" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="ERU" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="ROADID:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code." FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="default" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ROADCLAS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Class" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="ROADCLAS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road class attribute identifies the class of the proposed forest access road in terms of the road use management strategy in the FMP. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="P:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="P" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Primary" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Primary roads are roads that provide principal access for the management  unit, and are constructed, maintained and used as part of the main road  system on the management unit.  Primary roads are normally permanent  roads. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="B:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="B" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="branch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="A branch road is a road, other than a primary road, that branches off an  existing or new primary or branch road, providing access to, through or  between areas of operations on a management unit." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="O:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="O" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Operational" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Operational roads are roads within operational road boundaries, other than  primary or branch roads, that provide short-term access for harvest,  renewal and tending operations. Operational roads are normally not  maintained after they are no longer required for forest management  purposes, and are often site prepared and regenerated." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="TRANS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Transfer Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="TRANS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road transfer year attribute indicates a four-digit number representing the first year of the 5-year operational term that the transfer of responsibility to the MNR is anticipated to take effect. If there is no intent to transfer responsibility to MNR during the future 20-year period there is no need to specify a year. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="A blank or zero value is a valid code" FOLDED="true"/>
                                <node TEXT="If TRANS value is populated, then INTENT must be populated" FOLDED="true"/>
                                <node TEXT="ST1: If road transfer year does not equal zero (TRANS != 0) then the value must be greater  than or equal to the plan term start year" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="Road transfer year should not be greater than plan start year plus twenty years" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;=  ${planyear}' ${:N} row['${name}']  &lt; ${planyear} ]   " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="[ 'ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be &lt;=  ${planyear} + 20' ${:N} row['${name}']  &gt; ${planyear} + 20  ]   " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:1999, 'INTENT':'Banana' }" FOLDED="true">
                                    <node TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;=  ${planyear}" FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:2999, 'INTENT':'Banana' }" FOLDED="true">
                                    <node TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be &lt;=  ${planyear} + 20" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACYEAR:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control Year" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="Integer" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACYEAR" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control year attribute indicates a four-digit number representing the expected fiscal year (April 1 to March 31) that the access control is anticipated to take effect. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true"/>
                                <node TEXT="A zero or Null value is a valid code" FOLDED="true"/>
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The value must be greater than or equal to the plan term start year when ACCESS  not EXISTING or REMOVE" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: If access control year does not equal zero (ACYEAR NOT 0) then access control must not be null" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must be &gt;=  ${planyear} when ACCESS  not EXISTING or REMOVE' ${:N} row['${name}']  &lt; ${planyear} and row['ACCESS']  in ['APPLY','ADD','BOTH','ADDREMOVE']  ]" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="[ 'ST1: If ${name} is not NULL then ACCESS must be populated' ${:N}  row['ACCESS'] in ${list_NULL}  ] " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:1999, 'ACCESS':'APPLY'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: ${name} must be &gt;=  ${planyear} when ACCESS  not EXISTING or REMOVE" FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:2025, 'ACCESS':None}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: If ${name} is not NULL then ACCESS must be populated" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="2025" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="ACCESS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ACCESS" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road access control attribute is a field used to identify where access control activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. " FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="APPLY:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="APPLY" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="apply" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied  to the road segment. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="REMOVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="REMOVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="remove" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being  removed from the road segment." FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="BOTH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BOTH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="both" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control is being applied  and removed from the road segment in the same  annual report year. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ADDREMOVE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ADDREMOVE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="addition and removal" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control exists on the  road segment, that a new access control is being  applied to the road segment and that an access  control is being removed from the road segment in  the plan period" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="ADD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ADD" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="additional" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control exists on the  road segment and that a new access control is being  applied to the road segment. " FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="EXISTING:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="EXISTING" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="existing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="This indicates that an access control exists on the  road segment. " FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code " FOLDED="true"/>
                                <node TEXT="ST1: When the road access control status is apply, additional, both, or additional with  removal (ACCESS = APPLY or ADD or BOTH or ADDREMOVE) then the control type must  be a code other than null (CONTROL1 is not null)  - Tested in the CONTROL1 attribute" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS not null)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST2: When the road access control status is remove (ACCESS = REMOVE) then the control  type should be null (CONTROL1 = null and CONTROL2 = null) " FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="['ST2: When the road access control status is remove (ACCESS = REMOVE) then the control  type should be null (CONTROL1 = null and CONTROL2 = null)' ${:} row['${name}'] == 'REMOVE' and ( row['CONTROL1'] not in ${list_NULL} or  row['CONTROL2'] not in ${list_NULL} ) ]" FOLDED="true"/>
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR'] ]  and row['ACCESS'] in ${list_NULL} and row['DECOM']  in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;ACCESS&quot;:None, 'MAINTAIN':'N', 'MONITOR':'N', 'DECOM':None, 'ACYEAR':None }" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="DECOM:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Decommissioning" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="DECOM" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Berm and or Ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Scarify Road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Pile Slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Water crossing removal" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is a valid code " FOLDED="true"/>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS not null)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR'] ]  and row['ACCESS'] in ${list_NULL} and row['DECOM']  in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;ACCESS&quot;:None, 'MAINTAIN':'N', 'MONITOR':'N', 'DECOM':None, 'ACYEAR':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="INTENT:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="MNR Intent" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="INTENT" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The decommissioning attribute is a field used to identify where decommissioning activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. " FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: If TRANS value is populated, then INTENT must be populated" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must not be null when TRANS is populated' ${:} ${NIN} and row['TRANS'] not in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:None,'TRANS':2025}" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: ${name} must not be null when TRANS is populated" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="MAINTAIN:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Maintenance" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MAINTAIN" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS not null)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR'] ]  and row['ACCESS'] in ${list_NULL} and row['DECOM']  in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:None, 'MONITOR':'Y', 'ACCESS':None, 'DECOM':None, 'ACYEAR':None }" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana', 'MONITOR':'Y', 'ACCESS':None, 'DECOM':None, 'ACYEAR':None }" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;ACCESS&quot;:None, 'MAINTAIN':'N', 'MONITOR':'N', 'DECOM':None, 'ACYEAR':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="MONITOR:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Monitoring" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="MONITOR" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Yes" FOLDED="true"/>
                                </node>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true">
                                    <node TEXT="Road maintenance NOT planned to occur during plan term" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory " FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme " FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code " FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM not null or MAINTAIN = Y or MONITOR = Y or ACCESS not null)" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})' ${:} 'Y' not in [ row[fff] for fff in ['MAINTAIN','MONITOR'] ]  and row['ACCESS'] in ${list_NULL} and row['DECOM']  in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:None, 'MAINTAIN':'Y', 'ACCESS':None, 'DECOM':None, 'ACYEAR':None}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana', 'MAINTAIN':'Y', 'ACCESS':None, 'DECOM':None, 'ACYEAR':None  }" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;ACCESS&quot;:None, 'MAINTAIN':'N', 'MONITOR':'N', 'DECOM':None, 'ACYEAR':None}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})" FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="RESPONS:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Responsibility" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RESPONS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="RESPONS" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SFL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SFL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Sustainable Forest Licensee" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MNR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MNR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ministry of Natural Resources and Forestry" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="At a minimum, one record should equal SFL" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="CONTROL1:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL1" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The access control or decommissioning type attributes indicate the method of access control to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute may also be used to identify the type of decommissioning activities scheduled on primary or branch roads that will be constructed during the year." FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The presence and population of CONTROL1 is mandatory where ACCESS is APPLY, ADD, BOTH or ADDREMOVE (Rec'd from the ACCESS attribute)" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must not be null when ACCESS is APPLY, ADD, BOTH or ADDREMOVE' ${:} ${NIN} and row['ACCESS'] in [ 'APPLY', 'ADD','BOTH','ADDREMOVE' ] ]" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:None, 'ACCESS':'APPLY', 'CONTROL2':None }" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: ${name} must not be null when ACCESS is APPLY, ADD, BOTH or ADDREMOVE" FOLDED="true"/>
                                </node>
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="CONTROL2:" FOLDED="true">
                        <icon BUILTIN="button_ok"/>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Access Control / Decom Type 2" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="4" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="CONTROL2" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="CONTROL" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="Def" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BERM:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BERM" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="berm and/or ditch" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="GATE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="GATE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="gated / physical barrier" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SCAR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SCAR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="scarify and/or plant road" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SIGN:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SIGN" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="signed" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="PRIV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="PRIV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="private land" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="SLSH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SLSH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="pile slash" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="WATX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="WATX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="water crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="A blank or null value is a valid code" FOLDED="true"/>
                            </node>
                            <node TEXT="python" FOLDED="true">
                                <node TEXT="[ 'ST1: ${name} must  be null when CONTROL1 is null' ${:N} row['CONTROL1'] in ${list_NULL} ]" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                            </node>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana' , 'CONTROL1':'BERM'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polyline" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Existing Road Use Management Strategy (ERU)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="PlannedAggregateExtractionAreas:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="PlannedAggregateExtractionAreas" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="AGAREAID:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Aggregate Extraction Area ID" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="15" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="AGAREAID" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition:" FOLDED="true">
                            <node TEXT="The aggregate extraction area identifier attribute indicates the unique identifier for the area where forestry aggregate pits may be established." FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="agg_null" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Planned Aggregate Extraction Areas (PAG)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="mandatory" FOLDED="true">
                    <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="PAG" FOLDED="true"/>
                </node>
            </node>
            <node TEXT="TreeImprovement:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="TreeImprovement" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="IMP" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="IMPROVE:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Tree Improvement Activities" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="1" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="IMPROVE" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="YESNO" FOLDED="true"/>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT=" The tree improvement activities attribute indicates the area to support the  production of improved seed" FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="Y:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="Y" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="yes" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="N:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="N" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="No" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                    </node>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Polygon" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Tree Improvement (IMP)" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="The Tree improvement layer contains the attributes and characteristics of tree improvement  activities for the 10-year plan period to support the production of improved seed." FOLDED="true"/>
                </node>
            </node>
            <node TEXT="ExistingRoadWaterCrossingInventory:" FOLDED="true" POSITION="right">
                <icon BUILTIN="go"/>
                <icon BUILTIN="button_ok"/>
                <node TEXT="name" FOLDED="true">
                    <node TEXT="ExistingRoadWaterCrossingInventory" FOLDED="true"/>
                </node>
                <node TEXT="alias" FOLDED="true">
                    <node TEXT="Existing Road Water Crossing Inventory (WXI)" FOLDED="true"/>
                </node>
                <node TEXT="shortname" FOLDED="true">
                    <node TEXT="WXI" FOLDED="true"/>
                </node>
                <node TEXT="spatial_reference" FOLDED="true">
                    <node TEXT="26916" FOLDED="true"/>
                </node>
                <node TEXT="geometry_type" FOLDED="true">
                    <node TEXT="Point" FOLDED="true"/>
                </node>
                <node TEXT="path" FOLDED="true">
                    <node TEXT="Plan" FOLDED="true"/>
                </node>
                <node TEXT="Validation:" FOLDED="true">
                    <node TEXT="english" FOLDED="true">
                        <node TEXT="This table must contain all mandatory fields" FOLDED="true"/>
                    </node>
                    <node TEXT="python" FOLDED="true"/>
                    <node TEXT="sql" FOLDED="true"/>
                </node>
                <node TEXT="Description" FOLDED="true">
                    <node TEXT="Using the most up-to-date information available, this point layer will identify all known existing  water crossings for roads identified in the existing road use management strategy inventory  layer. Responsibility assignment of the water crossing will be identified in the layer which does  not necessarily reflect or represent the characteristics of the Existing Road Use Management  Strategy layer. " FOLDED="true"/>
                </node>
                <node TEXT="fields" FOLDED="true">
                    <node TEXT="WATXID:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="12" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WATXID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing identifier attribute is a unique identifier label assigned to the crossing location. This water crossing ID will be unique in perpetuity." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="water_null" FOLDED="true"/>
                        </node>
                    </node>
                    <node TEXT="WATXTYPE:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="5" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="WATXTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Type" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="WATXTYPE" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The water crossing type attribute identifies the type of water crossing structure being scheduled." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-4"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="BRID:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BRID" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="bridge" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="TEMP:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="TEMP" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Temporary Bridge" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="CULV:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="CULV" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Culvert (Span &lt; 3m)" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MULTI:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MULTI" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Multiple Culvert" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="FORD:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="FORD" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="engineered ford" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ICE:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ICE" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ice crossing" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="BOX:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="BOX" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Box Culvert" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="ARCH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="ARCH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="ARCH Culvert" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                    </node>
                    <node TEXT="RESPONS:" FOLDED="true">
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Water Crossing Responsibility" FOLDED="true"/>
                        </node>
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="3" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="RESPONS" FOLDED="true"/>
                        </node>
                        <node TEXT="domain" FOLDED="true">
                            <node TEXT="RESPONS" FOLDED="true"/>
                        </node>
                        <node TEXT="definition" FOLDED="true">
                            <node TEXT="The water crossing responsibility attribute indicates the custodian responsible  for the maintenance and monitoring of water crossing conditions. " FOLDED="true"/>
                        </node>
                        <node TEXT="values" FOLDED="true">
                            <node TEXT="SFL:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="SFL" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Sustainable Forest Licensee" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="MNR:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="MNR" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Ministry of Natural Resources and Forestry" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                            <node TEXT="OTH:" FOLDED="true">
                                <node TEXT="name" FOLDED="true">
                                    <node TEXT="OTH" FOLDED="true"/>
                                </node>
                                <node TEXT="option" FOLDED="true">
                                    <node TEXT="Other (e.g. Private, Joint)" FOLDED="true"/>
                                </node>
                                <node TEXT="desctext" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                </node>
                                <node TEXT="ST1: The attribute population must follow the correct coding scheme" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="ST1: A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                                <node TEXT="At a minimum, one record should equal SFL (except for Crown managed units)" FOLDED="true">
                                    <icon BUILTIN="yes"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT="{&quot;${name}&quot;:'Banana'}" FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..." FOLDED="true"/>
                                </node>
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node TEXT="ROADID:" FOLDED="true">
                        <node TEXT="length" FOLDED="true">
                            <node TEXT="30" FOLDED="true"/>
                        </node>
                        <node TEXT="type" FOLDED="true">
                            <node TEXT="String" FOLDED="true"/>
                        </node>
                        <node TEXT="name" FOLDED="true">
                            <node TEXT="ROADID" FOLDED="true"/>
                        </node>
                        <node TEXT="alias" FOLDED="true">
                            <node TEXT="Road Identifier" FOLDED="true"/>
                        </node>
                        <node TEXT="DomainValidation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="" FOLDED="true"/>
                            </node>
                        </node>
                        <node TEXT="Definition" FOLDED="true">
                            <node TEXT="The road identifier attribute is the unique number, label or name assigned to the road or network of roads that the water crossing feature is located on." FOLDED="true"/>
                        </node>
                        <node TEXT="mandatory" FOLDED="true">
                            <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true"/>
                        </node>
                        <node TEXT="Validation:" FOLDED="true">
                            <node TEXT="english" FOLDED="true">
                                <node TEXT="The presence of this attribute in the file structure of the layer is mandatory" FOLDED="true">
                                    <icon BUILTIN="full-1"/>
                                    <icon BUILTIN="list"/>
                                </node>
                                <node TEXT="The population of this attribute is mandatory." FOLDED="true">
                                    <icon BUILTIN="full-2"/>
                                </node>
                                <node TEXT="A blank or null value is not a valid code" FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                </node>
                            </node>
                            <node TEXT="python" FOLDED="true"/>
                            <node TEXT="sql" FOLDED="true"/>
                            <node TEXT="test:" FOLDED="true">
                                <node TEXT='{"${name}":None}' FOLDED="true">
                                    <icon BUILTIN="full-3"/>
                                    <node TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..." FOLDED="true"/>
                                </node>
                            </node>
                        </node>
                        <node TEXT="default" FOLDED="true">
                            <node TEXT="road_null" FOLDED="true"/>
                        </node>
                    </node>
                </node>
            </node>
        </node>
    </map>
