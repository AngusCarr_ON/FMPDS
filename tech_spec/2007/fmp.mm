<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1562265931182" ID="ID_954803283" MODIFIED="1562266050969" TEXT="FMP_2007">
<node CREATED="1562265931183" ID="ID_52655735" MODIFIED="1562265931183" POSITION="right" TEXT="PlanningComposite:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="PlanningComposite"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="shortname">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="PCM"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="spatial_reference">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="26916"/>
</node>
<node CREATED="1562265931183" ID="ID_1987143990" MODIFIED="1562265931183" TEXT="fields">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="POLYID:">
<icon BUILTIN="flag-green"/>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="length">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="25"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="type">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Character"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="POLYID"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="alias">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Polygon Identifier"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="mandatory">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="definition">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="The planning inventory polygon identifier attribute is a unique id or label for the polygon often based on geographic location."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Validation:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="english">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: Blank or null values are not a valid code">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: Attribute must contain a unique value">
<icon BUILTIN="list"/>
<icon BUILTIN="full-2"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="python"/>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="test:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="{&apos;${name}&apos;:None}">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..."/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="POLYTYPE:">
<icon BUILTIN="flag"/>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="length">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="3"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="type">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Character"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="POLYTYPE"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="domain">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="POLYTYPE"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="alias">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Polygon Type"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="definition">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="The polygon type attribute indicates the classification of the area within the polygon boundaries into one of several generalized water and land types."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="description">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="The polygon type is determined from the classification of area on a forest management unit into different water and land types. Some polygon types are derived from inventory base features, while other polygons must be created from a classification process.  All remaining land areas within a designated forest management unit are classified into various non-forested or forested lands."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Validation:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="english">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: The population of this attribute is mandatory">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: The attribute population must follow the correct coding scheme">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: A blank or null value is not a valid code">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST2: If POLYTYPE attribute does not equal FOR, then all forest-specific attributes should be empty.">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST2: If a delineated polygon is an island or is located on an island, the management consideration attribute is set to island MGMTCON1 = ISLD. This applies to all polygon types. This apparent redundancy in classification for polygons having a type of island i.e., POLYTYPE = ISL   and   MGMTCON1 = ISLD allow resource managers to easily identify:   1. all polygons located on islands regardless of polygon type by querying on MGMTCON1 = ISLD  2. just the small uninterpreted islands by querying on POLYTYPE = ISL depending upon the desired analysis.">
<icon BUILTIN="bookmark"/>
<icon BUILTIN="full-6"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="python">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="[ &apos;ST1: ${name} ISL implies MGMTCON1 ISLD&apos; ${:}  row[&apos;${name}&apos;] == &apos;ISL&apos; and row[&apos;MGMTCON1&apos;] not in [  &apos;ISLD&apos;, None, &apos;&apos; , &apos; &apos;  ] ]"/>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="[ &apos;ST2: &lt;mark&gt;${name}&lt;/mark&gt; non-FOR -&gt; forest attributes should all be null&apos; ${:} row[&apos;${name}&apos;] != &apos;FOR&apos; and len([ row[a] for a in [ &apos;FORMOD&apos;, &apos;DEVSTAGE&apos;, &apos;YRDEP&apos;, &apos;SPCOMP&apos;, &apos;WG&apos;, &apos;YRORG&apos;, &apos;HT&apos;, &apos;STKG&apos;, &apos;SC&apos;, &apos;ECOSRC&apos;, &apos;ECOSITE1&apos;, &apos;ECOPCT1&apos;, &apos;ECOSITE2&apos;, &apos;ECOPCT2&apos;, &apos; ACCESS1&apos;, &apos;ACCESS2&apos;, &apos;MGMTCON1&apos;, &apos;MGMTCON2&apos;, &apos;MGMTCON3&apos;, &apos;AGS_POLE&apos;, &apos;AGS_SML&apos;, &apos;AGS_MED&apos;, &apos;AGS_LGE&apos;, &apos;UGS_POLE&apos;, &apos;UGS_SML&apos;, &apos;UGS_MED&apos;, &apos;UGS_LGE&apos;, &apos;USPCOMP&apos;, &apos;UWG&apos;, &apos;UYRORG&apos;, &apos;UHT&apos;, &apos;USTKG&apos;, &apos;USC&apos;, &apos;MANAGED&apos;, &apos;TYPE&apos;, &apos;MNRCODE&apos;, &apos;SMZ&apos;, &apos;OMZ&apos;, &apos;PLANFU&apos;, &apos;AGESTR&apos;, &apos;AGE&apos;, &apos;AVAIL&apos;, &apos;SILVSYS&apos;, &apos;NEXTSTG&apos;, &apos;SI&apos;, &apos;SISRC&apos;] if row[a] not in ${list_NULL} ]) != 0  ]"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="test:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="{&apos;${name}&apos;:None}">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="{&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;}">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="{ &apos;POLYTYPE&apos;:&apos;GRS&apos;, &apos;FORMOD&apos;:&apos;RP&apos;,  &apos;DEVSTAGE&apos;:None, &apos;YRDEP&apos;:None, &apos;SPCOMP&apos;:None, &apos;WG&apos;:None, &apos;YRORG&apos;:None, &apos;HT&apos;:None, &apos;STKG&apos;:None, &apos;SC&apos;:None, &apos;ECOSRC&apos;:None, &apos;ECOSITE1&apos;:None, &apos;ECOPCT1&apos;:None, &apos;ECOSITE2&apos;:None, &apos;ECOPCT2&apos;:None, &apos; ACCESS1&apos;:None, &apos;ACCESS2&apos;:None, &apos;MGMTCON1&apos;:None, &apos;MGMTCON2&apos;:None, &apos;MGMTCON3&apos;:None, &apos;AGS_POLE&apos;:None, &apos;AGS_SML&apos;:None, &apos;AGS_MED&apos;:None, &apos;AGS_LGE&apos;:None, &apos;UGS_POLE&apos;:None, &apos;UGS_SML&apos;:None, &apos;UGS_MED&apos;:None, &apos;UGS_LGE&apos;:None, &apos;USPCOMP&apos;:None, &apos;UWG&apos;:None, &apos;UYRORG&apos;:None, &apos;UHT&apos;:None, &apos;USTKG&apos;:None, &apos;USC&apos;:None, &apos;MANAGED&apos;:None, &apos;TYPE&apos;:None, &apos;MNRCODE&apos;:None, &apos;SMZ&apos;:None, &apos;OMZ&apos;:None, &apos;PLANFU&apos;:None, &apos;AGESTR&apos;:None, &apos;AGE&apos;:None, &apos;AVAIL&apos;:None, &apos;SILVSYS&apos;:None, &apos;NEXTSTG&apos;:None, &apos;SI&apos;:None, &apos;SISRC&apos;:None }">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; non-FOR -&gt; forest attributes should all be null"/>
</node>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="values">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="WAT:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="WAT"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" STYLE="fork" TEXT="All water areas.  Includes lakes, ponds, reservoirs i.e., inland basin areas containing water and wide two sided rivers.  These are rivers that can be defined by area.  Generally, these rivers and streams are consistently wider than 20 meters when portrayed at a mapping scale of 1:10,000 or consistently wider than 40 meters when portrayed at a mapping scale of 1:20,000.  Smaller or narrower rivers and streams are maintained as linear features in a centre-line layers."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Water"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#6bcbff"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="DAL:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="DAL"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Lands which are cultivated for growing crops, orchards, floral gardens, etc. These areas may include abandoned agricultural lands."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Developed agricultural land"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#d6f0ff"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="GRS:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="GRS"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Farm areas devoted to pasture for domesticated animals. These areas may also include abandoned grass and meadows, but are not part of the productive forest land base and do not include barren and scattered areas. These areas are similar to barren and scattered, but are located near developed agriculture land or unclassified areas and are usually fenced."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Grass and meadow"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#c9ffce"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ISL:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ISL"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Islands less than 8 hectares in size, down to a lower limit of 0.0025 hectares or 25 square meters in size e.g., 5 meters X 5 meters are recorded during the inventory production process, but are not interpreted or typed for practicality and cost considerations.  Only islands 8 hectares and larger are interpreted and assigned an appropriate POLYTYPE code, such as FOR or BSH."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Small island"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#ffffff"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="UCL:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="UCL"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Non-forested areas which were created for specific uses other than timber production, such as  roads, railroads, logging camps, mines, utility corridors, logging camps, gravel pits, airports, etc."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Unclassified"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#bfbfbf"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="BSH:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="BSH"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Areas covered with non-commercial tree species or shrubs.  These areas are normally associated with wetlands or water features."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Brush and alder"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#9cbc9f"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="RCK:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="RCK"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Areas of barren or exposed rock e.g., bedrock, cliff face, talus slope which may support a few scattered trees, but is less that 25 percent crown closure."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Rock"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#b2b2b2"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="TMS:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="TMS"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Areas of dry or wet muskeg on which stunted trees occur as widely spaced individuals or in small groups."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Treed wetland"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#9cbc9f"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="OMS:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="OMS"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Wet areas of mosses, grasses, sedges, and small herbaceous plants, often interspersed with small areas of open water"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Open Wetland"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#9cbc9f"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="FOR:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="FOR"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Areas that are capable of producing trees and can support tree growth.  These areas may or may not be capable of supporting the harvesting of timber on a sustained yield basis.  Some areas may have physical and/or biological characteristics which effect land use.  Thus this polygon type includes both production and protection forest areas."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Productive forest"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="fill color">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="#52b770"/>
</node>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="mandatory">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="default">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="FOR"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" ID="ID_1712335552" MODIFIED="1562265931183" TEXT="OWNER:">
<icon BUILTIN="flag-green"/>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="OWNER"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="length">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="1"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="type">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Character"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="domain">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="OWNER"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="alias">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Ownership"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="mandatory">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="definition">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="This ownership attribute contains the traditional FRI ownership information as assigned by the Office of the Surveyor General."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Description">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="The ownership designation attribute is derived from the ownership and land tenure, and parks and reserves layers which are maintained in the MNRFs values information system. This attribute also identifies the managed Crown area in a forest management unit. The ownership information is used to create table FMP-1 Part B, Section 8.0."/>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Any discrepancies regarding the information contained in the ownership designation attribute should be reported to the appropriate MNRF district office. The most accurate source for ownership information is located in the appropriate regional Land Registry Office."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Validation:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="english">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: The population of this attribute is mandatory">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: The attribute population must follow the correct coding scheme">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: A blank or null value is not a valid code">
<icon BUILTIN="full-4"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="python"/>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="sql"/>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="test:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="{&apos;${name}&apos;:&apos;u&apos;}">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="{&apos;${name}&apos;:None}">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..."/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="{&apos;${name}&apos;:&apos;1&apos;}">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT=""/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="test:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="{}">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="values">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="0:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="0"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="None"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Unknown or unassigned ownership"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="1:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="1"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Crown Managed"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Crown Land"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="2:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="2"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Patented Crown Timber"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Patent land with timber rights reserved to the Crown"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="3:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="3"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Non Crown"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Patented land fee simple private"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="4:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="4"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Non Crown"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Patented land Company Freehold"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="5:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="5"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Crown Other"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Provincial Park"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="6:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="6"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="desctext">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Non Crown"/>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="option">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="Indian Reserve"/>
</node>
</node>
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="7:">
<node CREATED="1562265931183" FOLDED="true" MODIFIED="1562265931183" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="7"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Crown Other"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Recreation Reserve"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="8:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="8"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Non Crown"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Agreement Forest"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="9:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="9"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Crown Federal"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Federal Reserve"/>
</node>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="default">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="1 "/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" ID="ID_902269889" MODIFIED="1562265931184" TEXT="AUTHORTY:">
<icon BUILTIN="flag-green"/>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="AUTHORTY"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="length">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="3"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="type">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="string"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="domain">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="AUTHORTY"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="alias">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Ownership Designation"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="definition">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="The ownership designation attribute indicates the custodian responsible for providing the inventory information about the polygon.  (The party responsible for maintaining and providing the forest resources inventory information in accordance with the Forest Information Manual; that is, for determining the polygon description.) "/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Description">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="As of 2006 the forest resource inventory is the responsibility of MNR and therefore the default value for the inventory is MNR.  "/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Validation:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="english">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="python"/>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="sql"/>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="test:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="{}">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="values">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="SFL:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="SFL"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT=""/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Licensee"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="MNR:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="MNR"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT=""/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Ministry of Natural Resources"/>
</node>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="mandatory">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" ID="ID_383197570" MODIFIED="1562265931184" TEXT="YRUPD:">
<icon BUILTIN="flag"/>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="YRUPD"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="length">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="4"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="type">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Integer"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="alias">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Year of Data Update"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="mandatory">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="definition">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="The year of data update attribute contains a four digit number representing the calendar year (January 1 to December 31) of the source data used to determine update the polygon description, in particular the height attribute."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="description">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="The year of data update identifies the calendar year that information about a polygon was last confirmed or modified based on field inspection, photo interpretation, analysis of satellite imagery, or conversion or update to spatial or tabular data. The year of data update should not be changed to reflect error corrections to tabular attributes."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Validation:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="english">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: The population of this attribute is mandatory">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: The attribute population must follow the correct format">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: A zero or null value is not a valid code">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: YRSOURCE must be a year less than the plan period start year">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Must be a year greater than or equal to 1975">
<icon BUILTIN="full-6"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="python">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="[&apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 1975&apos; ${:N} row[&apos;${name}&apos;]  &lt;1975  ]">
<icon BUILTIN="full-3"/>
<icon BUILTIN="full-4"/>
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="[&apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be less than  ${planyear} &apos; ${:N} row[&apos;${name}&apos;]  &gt;= ${planyear}  ]">
<icon BUILTIN="full-5"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="sql"/>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="test:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="{&apos;${name}&apos;:None}">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="{&apos;${name}&apos;:1950 }">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be &gt;= 1975"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931184" FOLDED="true" ID="ID_841754982" MODIFIED="1562265931184" TEXT="SOURCE:">
<icon BUILTIN="flag"/>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="length">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="8"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="type">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Character"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="SOURCE"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="domain">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="SOURCE"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="alias">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Source of Update Data "/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="definition">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="The source of data update attribute identifies the methodology by which the information stored in the other tabular attributes that are associated with the same polygon was determined (i.e., how the polygon description was determined)."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="mandatory">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Description">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="The source of data update attribute does not apply to the changes or updates made to the ecosite attribute."/>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Validation:">
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="english">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: The population of this attribute is mandatory">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: The attribute population must follow the correct coding scheme">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: A blank or null value is not a valid code">
<icon BUILTIN="full-4"/>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: A value of FORECAST is only valid for the SOURCE attribute in the Base Model Inventory.  NO NEED TO CHECK IN BMI.">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: If the DEVSTAGE attribute starts with EST, then the SOURCE attribute should not equal ESTIMATE">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST2: If the DEVSTAGE attribute starts with NEW, then the SOURCE attribute should be ESTIMATE. FIM Difference - Allow ESTIMATE in EPC product to allow new treatment estimates.">
<icon BUILTIN="full-7"/>
</node>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="python">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="[&apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be FORECAST unless table is Base Model Inventory&apos; ${:}  ${PCM} and row[&apos;${name}&apos;]  == &apos;FORECAST&apos;   ]">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="[&apos;ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be ESTIMATE when DEVSTAGE LIKE EST&apos; ${:}  row[&apos;${name}&apos;]  == &apos;ESTIMATE&apos; and row[&apos;DEVSTAGE&apos;] in [ &apos;ESTNAT&apos;,&apos;ESTPLANT&apos;,&apos;ESTSEED&apos; ] ]">
<icon BUILTIN="full-6"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="sql"/>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="test:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="{&apos;${name}&apos;:None}">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null..."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="{&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;}">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme..."/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="values">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="BASECOVR:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="BASECOVR"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Information that is provided by the MNRF  (e.g., water or evaluated wetlands)."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="planimetric base layer"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="DIGITALA:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="DIGITALA"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Digital analysis, automated processing. (e.g., Ecognition)"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="multispectral scanning (digital image) - automated process"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="DIGITALP:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="DIGITALP"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Photo-interpreted by softcopy systems"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="multispectral scanning (digital image) - manual process"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ESTIMATE:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ESTIMATE"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="This source option is only for use in areas that have been recently renewed and have not been revisited since the renewal work was performed.  That is, where a follow-up survey has not yet been performed.  Therefore, the description of the newly regenerated stand is a &apos;best estimate&apos; of the expected outcome / result of the renewal treatment that was applied to the area based on past silvicultural successes."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="expected / estimated outcome / result"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="FOC:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="FOC"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Inspection of a site after silvicultural treatment to determine whether an operator / operations conforms to the approved plan or permit.   The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="forest operations compliance inspection"/>
</node>
</node>
<node BACKGROUND_COLOR="#99ff99" CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="FORECAST:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="FORECAST"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="This indicates a polygon description that was updated based on expected outcomes of planned operations (which have not yet been implemented) for the remainder of the current plan term.  This code is only valid in the Base Model Inventory. "/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="forecasted description"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="FRICNVRT:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="FRICNVRT"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Current polygon description based on data conversion from traditional FRI"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="forest resources inventory conversion"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="INFRARED:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="INFRARED"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT=""/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Infrared satellite imagery"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="MARKING:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="MARKING"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription.  Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="pre-harvest site inspection / marking"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="OCULARA:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="OCULARA"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="aerial survey / reconnaissance"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="OCULARG:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="OCULARG"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements)."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="ocular estimate (ground)"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="OPC:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="OPC"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Measuring standing trees to determine the volume of wood on a given tract of land."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="operational cruise"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PHOTO:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PHOTO"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="air photo interpretation"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PHOTOLS:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PHOTOLS"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Photography at a scale larger than 1:10,000  (e.g., 1:500, 1:1000)."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="large scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PHOTOSS:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PHOTOSS"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Photography at a scale smaller than 1:20,000  (e.g., 1:100,000)."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="small scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PLOTFIXD:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PLOTFIXD"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Fixed area plot"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="fixed area plot"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PLOTVAR:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="PLOTVAR"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Variable area plots."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="variable area (radius) plot"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="RADAR:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="RADAR"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT=""/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="radar satellite imagery"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="REGENASS:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="REGENASS"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="Survey of a regenerated area to determine how well the new stand is growing.  This includes seeding, survival, and stocking assessments.."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="regeneration assessment"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="SEMEXTEN:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="SEMEXTEN"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="An appraisal of a forest stand&apos;s structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).   Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="extensive silvicultural effectiveness monitoring survey"/>
</node>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="SEMINTEN:">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="name">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="SEMINTEN"/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="desctext">
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="An appraisal of a forest stand&apos;s structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).  Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments."/>
</node>
<node CREATED="1562265931184" FOLDED="true" MODIFIED="1562265931184" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="intensive silvicultural effectiveness monitoring survey"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="SPECTRAL:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="SPECTRAL"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT=""/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="spectral satellite imagery"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931185" FOLDED="true" ID="ID_519374623" MODIFIED="1562265931185" TEXT="FORMOD:">
<icon BUILTIN="flag-green"/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="length">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="2"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="type">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Character"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FORMOD"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="domain">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FORMOD"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="definition">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The productive forest modifier attribute represents a further classification (sub-division) of productive forest areas based on the presence or absence of physical or biological factors which may influence the ability to practice forest management."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Description">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The productive forest modifier must be identified for every productive forest stand and must be used in conjunction with the management consideration attribute."/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement.  Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a forest management decision (FORMOD) that is based on more than just site class."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="mandatory">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The population of this attribute is mandatory"/>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Validation:">
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="english">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: FORMOD MUST be NULL when POLYTYPE is NOT equal to FOR">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR">
<icon BUILTIN="full-5"/>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST2: IF SC attribute equals 4, then FORMOD should be PF (see SC - BMI/OPI only)">
<icon BUILTIN="password"/>
<icon BUILTIN="full-6"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="python"/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="sql"/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="test:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="{}">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="values">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="RP:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="RP"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest areas at various stages of growth and development, including areas that have been recently disturbed (by harvest or natural causes) or renewed (by artificial or natural means), that are capable of producing adequate growth of timber to support harvesting on a sustained yield basis.  These areas have no significant physical or biological limitations on the ability to practice forest management, but may include areas which pose an operational challenge in terms of harvest, access, protection, silviculture, or renewal."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Production Forest - Regular"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="MR:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="MR"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest areas which are considered to be production forest, but that are unavailable for timber production as determined through the forest management planning process.  That is, these areas have been identified as no-cut areas through area of concern planning for the purpose of protecting values (e.g., reserve buffers applied to protect tourism values)."/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Designated management reserves are areas that were managed as an operational reserve during previous forest management plan terms. That is, they are actual operational or management reserve areas created by the implementation of forest management operations around them. As guides are updated, these areas should be reviewed as some may return to production forest. "/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Production Forest - Designated Management Reserve"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="PF:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="PF"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Protection Forest"/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" ID="ID_924503555" MODIFIED="1562265931185" TEXT="DEVSTAGE:">
<icon BUILTIN="flag"/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="length">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="8"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="type">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Character"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="DEVSTAGE"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="alias">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Stage of Development"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="domain">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="DEVSTAGE"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="definition">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The stage of development attribute indicates the current state of growth and development for a productive forest stand.  Note that some stands stages are best described based on the last major silvicultural treatment that was applied to a stand, if the stand is being managed for timber production."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Description">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The stage of development attribute is not a derived attribute and is commonly modified from the first submission in the FRI. The population of this field requires a management decision based on both the current state and the past treatments. As an example, a forest stand was planted and the plantation failed (e.g. ingress, insect damage) yet it eventually met the regeneration standards and was declared established. The forest manager will now need to decide whether this stand receives a code of ESTPLANT or ESTNAT depending on the presence and potential influence of the planted stems that may still remain. The development stage assignment will be independent of the yield assignment despite the fact that there may be a connection between the two. The stage of development will be used to populate the stage of management in the landbase tables in the FMP  (i.e. FMP-3 and FMP-5). "/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="mandatory">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Validation:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="english">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The attribute must be Null when POLYTYPE is not equal to FOR">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: The population of this attribute is mandatory">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="ST1: If the STKG attribute equals 0.00, then the DEVSTAGE attribute should be either DEP* when POLYTYPE is equal to FOR">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="If the YRDEP attribute equals 0 (unknown), and DEPTYPE is not empty, then the DEVSTAGE attribute should only contain LOWNAT FTGNAT values when POLYTYPE is equal to FOR    *** FIM DIFFERENCE - added DEPTYPE clause for unknown deptypes">
<icon BUILTIN="full-8"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="If SC or USC  equals 4, then the DEVSTAGE attribute should not be DEPHARV">
<icon BUILTIN="full-7"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="python">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="[&apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be DEP* when POLYTYPE is FOR and STKG is 0.00 or NULL&apos; ${:NF} row[&apos;${name}&apos;] not in [  &apos;DEPHARV&apos;,&apos;DEPNAT&apos;] and row[&apos;STKG&apos;] in [ 0, &apos;&apos;, &apos; &apos;, None ]  ]">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="[ &apos;${name} must be LOWNAT or FTGNAT when POLYTYPE is FOR and ( YRDEP = 0 and DEPTYPE is not null) &apos; for row in [row] if row[&apos;${name}&apos;] not in [ &apos;LOWNAT&apos;,&apos;FTGNAT&apos;  ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;YRDEP&apos;] == 0 and ( row[&apos;DEPTYPE&apos;] != &apos;&apos; and row[&apos;DEPTYPE&apos;] is not None )  ]   ">
<icon BUILTIN="full-8"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="[ &apos;${name} must not be DEPHARV SC or USC = 4&apos; ${:F} row[&apos;${name}&apos;] == &apos;DEPHARV&apos; and 4 in [row[&apos;SC&apos;] , row[&apos;USC&apos;] ]  ]   ">
<icon BUILTIN="full-7"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="sql"/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="test:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="{}">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="values">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="DEPHARV:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="DEPHARV"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest area that was recently disturbed by clearcut harvesting and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer that would be released and/or protected as part of the depletion operation."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="recently disturbed by harvest and has received no regeneration/renewal treatment "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="DEPNAT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="DEPNAT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest area that was recently disturbed by natural causes (i.e., fire, blowdown, ice damage, insect and disease) and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="recently disturbed by natural causes and has received no regeneration / renewal treatment "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="LOWMGMT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="LOWMGMT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest stands which were previously harvested and have not reached the regeneration standards as described in an approved forest management plan within the estimated timeframe.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., stocking less than 25%).  "/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Below regeneration standards due to past management "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="LOWNAT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="LOWNAT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest stands which were previously disturbed by natural causes and have not reached the regeneration standards as described in an approved forest management plan within the estimated timeframe.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., stocking less than 25%).  "/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Below regeneration standards due to natural causes / succession "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="NEWPLANT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="NEWPLANT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest areas which have been regenerated predominantly by planting, but have not been assessed as free-to-grow"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="recently renewed : mainly planted"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="NEWSEED:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="NEWSEED"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest areas which have been regenerated predominantly by seeding, but have not been assessed as free-to-grow"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="recently renewed : mainly seeded "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="NEWNAT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="NEWNAT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest areas which have been regenerated predominantly by natural means, but have not been assessed as free-to-grow"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="recently renewed : mainly natural regeneration "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FTGPLANT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FTGPLANT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest areas which were regenerated predominantly from planted stock and which have been assessed as free-to-grow.  "/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="free-to-grow mainly planted"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FTGSEED:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FTGSEED"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest areas which were regenerated predominantly by seeding and which have been assessed as free-to-grow.  "/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="free-to-grow mainly seeded"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FTGNAT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FTGNAT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Productive forest areas which were regenerated predominantly by natural means and which have been assessed as free-to-grow. This classification will also be used to describe the forested areas that have never been treated to date (original forest) that would be considered as free growing.  "/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="free-to-grow mainly natural regeneration "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="THINPRE:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="THINPRE"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Free-growing productive forest areas which have received a mid-rotation thinning/spacing to promote the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume. "/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="received pre-commercial thinning/spacing treatment"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="THINCOM:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="THINCOM"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Free-growing productive forest areas which have received a mid-rotation partial harvest designed to promote the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes. "/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="received commercial thinning/spacing treatment "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="BLKSTRIP:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="BLKSTRIP"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The removal of a portion of the existing trees in a stand in progressive strips in more than one operation so that the non-depleted portion of the stand is left primarily to provide a natural seed source for regeneration of the depleted area. Several cutting patterns are available to achieve same goal."/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters (5 chains) wide. It is designed to encourage regeneration on difficult and/or fragile sites."/>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="Note: Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="modified cut: strip"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FRSTPASS:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FRSTPASS"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="A partial harvest where the first harvest operation removes target/specific merchantable tree species from a forest stand. The remaining species are merchantable and are intended to be harvested by another logger/contractor/forest resource licence holder in the next pass.  A first pass should be recorded if merchantable tree species remain in the forest stand which have been allocated for harvest - but have not yet been harvested."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="modified cut: first pass "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="PREPCUT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="PREPCUT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source and to improve conditions for seed production and natural regeneration."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="received a preparatory cut "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="SEEDCUT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="SEEDCUT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="received a seed cut"/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FIRSTCUT:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="FIRSTCUT"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition."/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="option">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="received a first removal harvest "/>
</node>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="IMPROVE:">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="name">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="IMPROVE"/>
</node>
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="desctext">
<node CREATED="1562265931185" FOLDED="true" MODIFIED="1562265931185" TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition, distribution and quality by removing less desirable trees of any species."/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="received an improvement cut "/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="SELECT:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="SELECT"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment. "/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="received a selection harvest "/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="SNGLTREE:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="SNGLTREE"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The stand canopy is (periodically) opened uniformly throughout  the entire stand to achieve a post-harvest, basal area target. "/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Selection:  single-tree "/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="GROUPSE:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="GROUPSE"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The stand canopy is (periodically) opened by harvesting trees  in small groups. The resulting canopy opening usually occupies  a fraction of a hectare. "/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Selection: Group"/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" ID="ID_1285703911" MODIFIED="1562265931186" TEXT="YRDEP:">
<icon BUILTIN="flag"/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="length">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="4"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="type">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Integer"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="YRDEP"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="alias">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Year of Last Depletion or Disturbance"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="definition">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The year of last disturbance attribute indicates a four digit number of the most recent (or latest) fiscal year (April 1 to March 31) that a productive forest area was disturbed, completely or partially, by harvest or by natural causes. This includes mid-rotation or stand improvement operations where merchantable timber is removed. This is actual and known disturbances and not calculated from year of origin."/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Description">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The planning composite inventory must be updated with all disturbances (harvest and natural) which are identified in annual reports. The year of last disturbance for each productive forest stand must correspond to the year that the disturbance occurred, as recorded in the applicable annual reports.  Forest stands that are managed under the selection or shelterwood silvicultural systems must also have a year of last disturbance. The year of last disturbance provides an estimate of the stage of forest stand development within a cutting cycle for selection stands or within the current stage of a shelterwood system.  In the case of a partial harvest where certain tree species have been removed, such as a first pass where merchantable trees remain in a forest stand, the year that the first pass was carried out must be entered as the year of last disturbance. If an additional harvest or pass is conducted, the year of last disturbance is changed to the year in which the most recent harvest/pass operation was carried out.  Commercial thinning is a mid-rotation treatment that is recorded as a disturbance and the stand area is considered as disturbed area in forest management planning. Forest stands that have received a commercial thinning must show the applicable year of last disturbance in the forest polygon coverage.  The year of last disturbance is also used to identify other stand improvement operations. A stand improvement operation is normally associated with the selection system.   Year of last disturbance must not be used to indicate tending operations, such as a chemical or manual release which are required to bring a forest stand to free-to-grow standards. "/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The year of depletion attribute (YRDEP) and the associated depletion type attribute (DEPTYPE) are only completed during the photo interpretation process when the stage of development attribute is set to one of the &apos;newly depleted&apos; options (i.e., DEVSTAGE = DEPHARV or DEPNAT).  The fields are also generally completed for newly regenerated stages of development (i.e., DEVSTAGE = NEWNAT, NEWPLANT, or NEWSEED).  For other stages of development, the fields may be completed if the interpreter has access to additional information pertaining to past disturbances."/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="mandatory">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Validation:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="english">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The attribute must be 0 when POLYTYPE is not equal to FOR">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST2: The value must be greater than or equal to 1900 when POLYTYPE is equal to FOR">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The YRDEP attribute value should not be less than the YRORG attribute value when POLYTYPE is equal to FOR, unless the flag value 0 is used.***** FIM DIFFERENCE - removed this rule. Counterexamples abound..."/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="python">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="[&apos;ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be greater than or equal to 1900 unless it is 0&apos; ${:NF}  row[&apos;${name}&apos;]  &lt;1900 ]">
<icon BUILTIN="full-5"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="sql"/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="test:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="{}">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" ID="ID_1074828572" MODIFIED="1562265931186" TEXT="SPCOMP:">
<icon BUILTIN="flag-green"/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="alias">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Species Composition"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="length">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="120"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="type">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="String"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="SPCOMP"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Definition">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=" This species composition attribute indicates the tree species that are present in the stand canopy and the relative proportion of the canopy that each species occupies."/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Description">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=" "/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="DomainValidation:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="english">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=" "/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="mandatory">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Validation:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="english">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: No duplicate species codes allowed in the string">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: Proportion values in the string must sum to 100">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The tree species in the composition are to be coded using the scheme listed here">
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="If SOURCE is NOT plot based then allow specific species in SPCOMP (eFRI tech spec)">
<icon BUILTIN="yes"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Tree species in the list [     &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,     &apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,     &apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,     &apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;,     &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos;     ]">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="python">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="[&apos;ST%s: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; %s&apos;%( &apos;2&apos; if &apos;WARNING&apos; in i else &apos;1&apos;   ,i)    for i in test_spcomp(row[&apos;${name}&apos;]) if ${NINN} if i not in  [ None, [ None ] , []  ] ]">
<icon BUILTIN="full-4"/>
<icon BUILTIN="full-5"/>
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="[&apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; Invalid species code&apos; for s in test_SpeciesInList(row[&apos;${name}&apos;],  [ &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,&apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,&apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;, &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos; ]         )  if ${FOR} and ${NINN} and row[&apos;SOURCE&apos;][:4] != &apos;PLOT&apos;    ]">
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="[&apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; - %s is not a valid species code&apos;%(s) for s in test_SpeciesInList(row[&apos;${name}&apos;],    [ &apos;AL&apos;,&apos;Aq&apos;,&apos;Ap&apos;,&apos;Ag&apos;,&apos;Bc&apos;,&apos;Bp&apos;,&apos;Gb&apos;,&apos;Bb&apos;,&apos;Cat&apos;,&apos;Cc&apos;,&apos;Cm&apos;, &apos;Cp&apos;,&apos;Cs&apos;,&apos;Ct&apos;,&apos;Er&apos;,&apos;Eu&apos;,&apos;Hk&apos;,&apos;Ht&apos;,&apos;Hl&apos;,&apos;Hb&apos;,&apos;Hm&apos;,&apos;Hp&apos;, &apos;Hs&apos;,&apos;Hc&apos;,&apos;Kk&apos;,&apos;Le&apos;,&apos;Lj&apos;,&apos;Bl&apos;,&apos;Ll&apos;,&apos;Lb&apos;,&apos;Gt&apos;,&apos;Mb&apos;,&apos;Mf&apos;,&apos;Mm&apos;,&apos;Mt&apos;,&apos;Mn&apos;,&apos;Mp&apos;,&apos;AM&apos;,&apos;Ema&apos;,&apos;Mo&apos;,&apos;Obl&apos;,&apos;Ob&apos;,&apos;Och&apos;,&apos;Op&apos;, &apos;Os&apos;,&apos;Osw&apos;,&apos;Pa&apos;,&apos;Pn&apos;,&apos;Pp&apos;,&apos;Pc&apos;,&apos;Ph&apos;,&apos;Pe&apos;,&apos;Red&apos;,&apos;Ss&apos;,&apos;Sc&apos;,&apos;Sk&apos;,&apos;Sn&apos;,&apos;Sr&apos;,&apos;Sy&apos;,&apos;Tp&apos;,&apos;Haz&apos; ,                               &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,&apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,&apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;, &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos;    ]     ) if ${FOR} and ${NINN} and row[&apos;SOURCE&apos;][:4] == &apos;PLOT&apos;  ]">
<icon BUILTIN="full-7"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="sql"/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="test:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="{&apos;${name}&apos;:None, &apos;POLYTYPE&apos;:&apos;FOR&apos;, &apos;SOURCE&apos;:&apos;Banana&apos;}">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="{&apos;${name}&apos;:&apos;An Invalid Species Comp&apos;, &apos;POLYTYPE&apos;:&apos;FOR&apos;, &apos;SOURCE&apos;:&apos;Banana&apos;}">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; SPCOMP not multiples of 6 characters: "/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="{&apos;${name}&apos;:&apos;SB 100&apos;, &apos;POLYTYPE&apos;:&apos;FOR&apos;, &apos;SOURCE&apos;:&apos;Banana&apos;}">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" ID="ID_1195999079" MODIFIED="1562265931186" TEXT="WG:">
<icon BUILTIN="flag-green"/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="alias">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Working Group"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="length">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="2"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="type">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="String"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="WG"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="domain">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="WG"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="values">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="BF:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="BF"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="balsam fir"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="CE:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="CE"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="cedar"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="HE:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="HE"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="hemlock"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="LA:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="LA"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="larch"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="PJ:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="PJ"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="jack pine"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="PR:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="PR"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="red pine"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="PS:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="PS"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="scots pine"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="PW:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="PW"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="white pine"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="SB:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="SB"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="black spruce"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="SW:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="SW"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="white spruce"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="SX:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="SX"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="spruce mix"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="OC:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="OC"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="other conifer"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="AX:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="AX"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ash mix"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="BG:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="BG"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="gray birch"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="BW:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="BW"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="white birch"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="BY:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="BY"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="yellow birch"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="MH:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="MH"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="hard / sugar maple"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="MR:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="MR"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="soft / red maple"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="MX:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="MX"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="maple mix"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="OX:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="OX"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="oak mix"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="PO:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="PO"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Poplar"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="PB:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="PB"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="balsam poplar"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="OH:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="OH"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="desctext">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="option">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="other hardwoods"/>
</node>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="definition">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="The working group attribute indicates a grouping of productive forest stands for timber management purposes. Stands which are grouped together have the same predominant species composition and are being managed under the same silvicultural system. The grouping of stands is generally labelled based on the species which dictates the management regime to be applied. Therefore, the working group normally indicates the dominant species in a forest stand based on the tree species that occupies the greatest canopy closure or the greatest amount of basal area in the stand as indicated by the species composition attribute. "/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Description">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Validation:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="english">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="python"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="sql"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="test:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="{&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;, &apos;POLYTYPE&apos;:&apos;FOR&apos;}">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" ID="ID_1238662263" MODIFIED="1562265931187" TEXT="YRORG:">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="alias">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Year of Origin"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="length">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="2"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="type">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Integer"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="YRORG"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Description">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="The year of origin attribute contains a four digit number representing the average year that the predominant species (i.e., the species within the stand having the greatest relative abundance in terms of basal area), came into existence. "/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Description">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=" The year of origin is used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or  Appendix 1 Tabular Attribute Descriptions artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration. Once a forest stand has been assessed as free-to-grow, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site. In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered. For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach free-growing conditions), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip has achieved free-to-grow status, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required. Normally, updates to the year of origin have a corresponding change to the year of update attribute as this indicates the currency/vintage of the information and when the year of origin value was determined. Year of origin information is not required for non-forested and non-productive forest land types."/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Validation:">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="english">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="A zero value is not a valid code when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="The YRORG attribute value must be greater than or equal to 1600 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="The YRORG attribute must be less than the plan period start year when POLYTYPE is equal to FOR">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="The YRDEP attribute value should not be less than the YRORG attribute value (addressed in YRDEP)"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="The YRORG attribute value should not be greater than YRUPD attribute value"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="python">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must not be = 0&apos; for row in [row] if row[&apos;${name}&apos;]  == 0 and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must be &gt;= 1600 &apos; for row in [row] if row[&apos;${name}&apos;]  &lt;1600 and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must be &lt;= YRSOURCE&apos; for row in [row] if row[&apos;${name}&apos;]  &gt; row[&apos;YRSOURCE&apos;]  and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]   ">
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must be &lt;= YRUPD &apos; for row in [row] if row[&apos;${name}&apos;]  &gt; row[&apos;YRUPD&apos;]  and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]   "/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="sql"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="test:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="{}">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" ID="ID_250219293" MODIFIED="1562265931187" TEXT="HT:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="alias">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Height"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="length">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="4"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="type">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Float"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="HT"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="definition">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="The height attribute indicates the estimated average tree height (in meters) of the predominant species as inventoried in the year of update. Estimates can be made from interpreted crown closure or field samples, or from growth algorithms. "/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Validation:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="english">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="If the DEVSTAGE attribute does not start with DEP, NEW or LOW then the HT attribute must be greater than zero when POLYTYPE is equal to FOR.">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="The value of HT should not be greater than 0.5 when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=" If the DEVSTAGE attribute starts with FTG, then the HT attribute should be greater than or equal to 0.8 when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="python">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos; and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must not be 0 if DEVSTAGE is not DEPHARV or DEPNAT or NEWNAT or NEWPLANT or NEWSEED, LOWNAT, LOWMGMT when POLYTYPE is FOR. Consider updating DEVSTAGE?&apos; for row in [row] if row[&apos;DEVSTAGE&apos;]  not in [ &apos;DEPHARV&apos;,&apos;DEPNAT&apos;,&apos;NEWNAT&apos;,&apos;NEWPLANT&apos;,&apos;NEWSEED&apos;,&apos;LOWNAT&apos;,&apos; LOWMGMT&apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;${name}&apos;] == 0.0 ]   ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;WARNING: ${name} / (${year} - YRORG) should not be &gt; 0.5 when POLYTYPE is FOR&apos; for row in [row] if (row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; and row[&apos;YRORG&apos;] &gt; 0) if row[&apos;${name}&apos;] / (${year} - row[&apos;YRORG&apos;]) &gt; 0.5  ]   ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must be &gt;= 0.8 when DEVSTAGE like FTG% and POLYTYPE is FOR&apos; ${:F} row[&apos;${name}&apos;] &lt; 0.8 and row[&apos;DEVSTAGE&apos;] in [ &apos;FTGNAT&apos;,&apos;FTGPLANT&apos;,&apos;FTGSEED&apos;]  ]   ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 40) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 40) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   ">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="sql"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="test:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="{}">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="DomainValidation:">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="english">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Height / Age relationships should be in the range of Plonski and others."/>
</node>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="mandatory">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1562265931187" FOLDED="true" ID="ID_531733783" MODIFIED="1562265931187" TEXT="STKG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="type">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="double"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="length">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="32"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="name">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="STKG"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="alias">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Stocking"/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="definition">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="The stocking attribute indicates a qualitative measure of the density of tree cover in a forest stand.  It is expressed as a percentage value ranging from zero, for recently disturbed stands, to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field.  Stocking of a forest stand refers to all species that make up the stand&apos;s canopy, but it is generally based on the species with the most basal area. "/>
</node>
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="description">
<node CREATED="1562265931187" FOLDED="true" MODIFIED="1562265931187" TEXT="Stocking is determined by comparing the actual basal area measured from field sampling to the relative basal area of a fully-stocked stand using Plonski&apos;s Normal Yield Tables. (Plonski&apos;s Normal Yield Tables were developed from permanent sample plots established for several of the major tree species in Ontario.) Stocking can also be determined from aerial photography based on the degree of canopy closure, average age, height, and species composition. Actual basal area collected from field sampling is used to calibrate stocking assessments made from photo-interpretation.  The silvicultural ground rules in a forest management plan describe the standards for assessing the regeneration of forest stands, based on forest unit, desired species composition, age, height, and stocking. If the stocking of a productive forest stand does not meet the regeneration standards in the silvicultural ground rules of an approved forest management plan, the forest stand will be considered as NSR and must be classified into the appropriate below regeneration standards stage of development.  In some cases, the regeneration and/or management standards of a silvicultural ground rule may be expressed as a density, which usually describes the frequency or number of stems per hectare. Where density information (stems/hectare) has been collected or determined from a regeneration survey, this information must be converted to a stocking value for the purpose of updating the forest stand description information. "/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="This definition needs to be brought up to date using the current eFRI specification. There may (or may not) be a difference between traditional FRI and eFRI stocking calculations."/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Validation:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="english">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="* Valid numeric values are from 0 through 4.00"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="If the DEVSTAGE attribute starts with FTG, then the STKG attribute should be greater than or equal to 0.40   when POLYTYPE is equal to FOR.">
<icon BUILTIN="full-5"/>
</node>
<node COLOR="#999900" CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should equal 0.00   when POLYTYPE is equal to FOR and VERT is not SV (Veterans contribute to stocking) "/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should not exceed 0.1   when POLYTYPE is equal to FOR and VERT is SV (Veterans contribute to stocking) "/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should not exceed 0.3 when POLYTYPE is equal to FOR and VERT is TU or MU "/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ST1: If the DEVSTAGE attribute starts with FTG, then the STKG attribute must be greater than 0.0">
<icon BUILTIN="full-9"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="python">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 4.0 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 4.0) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  ]   "/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="[ &apos;ST2: ${name} should be &gt; 0.4 when DEVSTAGE is FTG* when POLYTYPE is FOR.&apos; ${:NF} row[&apos;DEVSTAGE&apos;]  in [ &apos;FTGNAT&apos;,&apos;FTGPLANT&apos;,&apos;FTGSEED&apos; ] and row[&apos;${name}&apos;] &lt; 0.4 ]   ">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="[ &apos;${name} must = 0.0 when DEVSTAGE is DEP* when POLYTYPE is FOR in single-canopy stands&apos; ${:F} row[&apos;DEVSTAGE&apos;]  in [ &apos;DEPNAT&apos;,&apos;DEPHARV&apos; ] and row[&apos;${name}&apos;] &gt; 0 and row[&apos;VERT&apos;] not in [ &apos;SV&apos;,&apos;TU&apos;,&apos;MU&apos;]  ]   "/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="[ &apos;WARNING: ${name} should not exceed 0.1 when DEVSTAGE is DEP* when POLYTYPE is FOR and VERT is SV.&apos; for row in [row] if row[&apos;DEVSTAGE&apos;]  in [ &apos;DEPNAT&apos;,&apos;DEPHARV&apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;${name}&apos;] &gt; 0.1 and row[&apos;VERT&apos;] == &apos;SV&apos; ]   "/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="[ &apos;WARNING: ${name} should not exceed 0.3 when DEVSTAGE is DEP* when POLYTYPE is FOR and VERT is TU or MU.&apos; for row in [row] if row[&apos;DEVSTAGE&apos;]  in [ &apos;DEPNAT&apos;,&apos;DEPHARV&apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;${name}&apos;] &gt; 0.3 and row[&apos;VERT&apos;] in [&apos;TU&apos;,&apos;MU&apos;] ]   "/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="[ &apos;ST1: ${name} must be &gt; 0.0 when DEVSTAGE is FTG* when POLYTYPE is FOR.&apos; ${:F} ${NIN} and row[&apos;DEVSTAGE&apos;]  in [ &apos;FTGNAT&apos;,&apos;FTGPLANT&apos;,&apos;FTGSEED&apos; ]  ]   ">
<icon BUILTIN="full-9"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="sql"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="test:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="{ &apos;STKG&apos;:0.39 , &apos;POLYTYPE&apos;:&apos;FOR&apos;, &apos;DEVSTAGE&apos;:&apos;FTGNAT&apos; }   ">
<icon BUILTIN="full-5"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ST2: ${name} should be &gt; 0.4 when DEVSTAGE is FTG* when POLYTYPE is FOR."/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="{ &apos;STKG&apos;:0.0 , &apos;POLYTYPE&apos;:&apos;FOR&apos;, &apos;DEVSTAGE&apos;:&apos;FTGNAT&apos; }   ">
<icon BUILTIN="full-9"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ST1: ${name} must be &gt; 0.0 when DEVSTAGE is FTG* when POLYTYPE is FOR."/>
</node>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="mandatory">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" ID="ID_255776738" MODIFIED="1562265931188" TEXT="SC:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="alias">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Site Class"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="length">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="2"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="type">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Integer"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="SC"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="domain">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="SC"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="definition">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="The site class attribute indicates a site quality estimate for a stand.  Site class is an indicator of site productivity and is determined using the average height, age, and working group, based on the dominant tree species in a forest stand. These attributes are compared against height and age growth curves in Plonski&apos;s Normal Yield Tables for different species to determine the relative growth rate for a forest stand.  "/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="description">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement. Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a timber management decision (FORMOD) that is based on more than just site class. "/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Validation:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="english">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="If the FORMOD attribute equals PF, then the SC attribute should equal 4  when POLYTYPE is equal to FOR and the VERT is not TU or MU"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PROPOSED BY LARRY WATKINS...  (previously foresters would change areas unavailable to SC4 so they would be considered PFR rather than making it unavailable)  applies to USC as well.  Need to check when PCM come in that changes to SC as presented in the eFRI have been altered.  Larry says this was common in the past.  LARRYS COMMENT on Site Class:  For some reason the old site class 4 sort from FORTRAN keeps creeping into the FRI spec inappropriately. SC4 should me SC4 - the productivity of the site. Don&apos;t call site class 1 trees on an island or on a rocky site 4 to match a  1970s coding issue, but rather flag the other appropriate fields.  ">
<icon BUILTIN="info"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="python">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="[ &apos;${name} must follow coding&apos; for row in [row] if row[&apos;${name}&apos;] not in [ 0,1,2,3,4 ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="[ &apos;${name} must be 4 when FORMOD is PF&apos; for row in [row] if row[&apos;${name}&apos;] not in [ 4 ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; and row[&apos;FORMOD&apos;] == &apos;PF&apos; ]   "/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="sql"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="test:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="{}">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="values">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="0:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="0"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Best"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="1:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="1"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Better"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="2:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="2"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Good"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="3:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="3"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Poor"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="4:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="4"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock."/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Protection Forest"/>
</node>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="mandatory">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ECOSRC:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="alias">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Ecosite Source of Update"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="length">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="8"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="type">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="String"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ECOSRC"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="domain">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ECOSRC"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="values">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ALGO:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ALGO"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="algorithm / model"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="DIGITALA:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="DIGITALA"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="multispectral scanning (digital image) - automated process Digital analysis, automated processing. (e.g., Ecognition)"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="DIGITALP:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="DIGITALP"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="multispectral scanning (digital image) - manual process photo-interpreted by softcopy systems"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="FOC:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="FOC"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="forest operations compliance inspection"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="FRICNVRT:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="FRICNVRT"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="forest resources inventory conversion"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="MARKING:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="MARKING"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="pre-harvest site inspection / marking"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="OCULARA:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="OCULARA"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="aerial survey/reconnaissance"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="OCULARG:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="OCULARG"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="ocular estimate (ground)"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="OPC:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="OPC"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="operational cruise"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PHOTO:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PHOTO"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="air photo interpretation"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PHOTOLS:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PHOTOLS"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="large scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PHOTOPLS:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PHOTOPLS"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="air photo interpretation"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PLUS:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PLUS"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="soils data"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PHOTOSS:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PHOTOSS"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="small scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PLOTFIXD:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PLOTFIXD"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="fixed area plot"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PLOTVAR:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="PLOTVAR"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="variable area (radius) plot"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="REGENASS:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="REGENASS"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="regeneration assessment"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="SEMEXTEN:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="SEMEXTEN"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="extensive silvicultural effectiveness monitoring survey"/>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="SEMINTEN:">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="name">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="SEMINTEN"/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="desctext">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT=""/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="option">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="intensive silvicultural effectiveness monitoring survey"/>
</node>
</node>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="definition">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="The ecosite source of update attribute identifies the methodology by which the ecosite information associated with the polygon was determined (i.e., how the polygon&apos;s ecosite description was determined). "/>
</node>
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Description">
<node CREATED="1562265931188" FOLDED="true" MODIFIED="1562265931188" TEXT="Note that this attribute is similar to the SOURCE attribute which indicates the methodology by which the overall stand description was determined. The main difference between the SOURCE attribute and this ECOSRC attribute is that there are two additional methodologies that are commonly used when determining ecosite information. These additional methodologies are: determination of ecosite by algorithm and determination by supplementing air photo interpretation with soils data. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Validation:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="english">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="python"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="sql"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="test:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="{&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;, &apos;POLYTYPE&apos;:&apos;FOR&apos;}">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="{&apos;${name}&apos;:None, &apos;POLYTYPE&apos;:&apos;FOR&apos;}">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="values">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ALGO:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ALGO"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT=" "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="algorithm / model "/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="DIGITALA:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="DIGITALA"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Digital analysis, automated processing. (e.g., Ecognition)"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="multispectral scanning (digital image) - automated process"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="DIGITALP:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="DIGITALP"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="photo-interpreted by softcopy systems"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="multispectral scanning (digital image) - manual process "/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="FOC:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="FOC"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Inspection of a site after silvicultural treatment was applied to determine whether an operator / operations conforms to the approved plan or permit. The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="forest operations compliance inspection"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="FRICNVRT:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="FRICNVRT"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="The current polygon description is based on a data conversion from traditional FRI. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="forest resources inventory conversion"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="MARKING:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="MARKING"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription. Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="pre-harvest site inspection / marking"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="OCULARA:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="OCULARA"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="aerial survey/reconnaissance"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="OCULARG:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="OCULARG"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements). "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ocular estimate (ground)"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="OPC:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="OPC"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Measuring standing trees to determine the volume of wood on a given tract of land. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="operational cruise"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PHOTO:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PHOTO"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000 "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="air photo interpretation "/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PHOTOLS:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PHOTOLS"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Photography at a scale larger than 1:10,000 (e.g., 1:500, 1:1000). "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="large scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PHOTOPLS:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PHOTOPLS"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT=""/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="air photo interpretation PLUS soils data"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PHOTOSS:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PHOTOSS"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Photography at a scale smaller than 1:20,000 (e.g., 1:100,000). "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="small scale aerial photography "/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PLOTFIXD:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PLOTFIXD"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT=""/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="fixed area plot "/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PLOTVAR:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PLOTVAR"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT=""/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="variable area (radius) plot"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="REGENASS:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="REGENASS"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Survey of a regenerated area to determine how well the new stand is growing. This includes seeding, survival, and stocking assessments. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="regeneration assessment "/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="SEMEXTEN:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="SEMEXTEN"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="An appraisal of a forest stand&apos;s structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved). Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="extensive silvicultural effectiveness monitoring survey "/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="SEMINTEN:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="SEMINTEN"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="An appraisal of a forest stand&apos;s structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved). Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="intensive silvicultural effectiveness monitoring survey "/>
</node>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="mandatory">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ECOSITE1:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="alias">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Ecosite 1"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="length">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="10"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="type">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="String"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ECOSITE1"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="definition">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="The ecosite attribute identifies an ecological landscape type that is present in the polygon and the regional ecological land classification system being used to define the ecosite code. Soil depth modifier information may also be included, if relevant. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Description">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Refer to the appropriate ecosystem field guide for a list of valid ecosite codes and their definitions. Ecosite is also referred to as site type. "/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="When recording the ecosite information in this attribute, the regional land classification identifier code value (from table below) replaces the first 2 characters of the ecosite code (either &quot;ES&quot; or &quot;ST&quot;). For example, if the northwest regional ecosite code of ES11 was determined to be appropriate for the stand, it would be recorded as NW11."/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Additionally, some ecosites, especially in the northwest region, can have soil depth modifier information associated with them. To record soil depth modifier information, add one of the codes from the table below to the end of the ecosite code. For example, northwest ecosite code of ES13 with a soil depth modifier of &quot;shallow&quot; would be recorded as:"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Ecosite information which is collected or created in accordance with an ecosystem classification (e.g., wetlands ecosystem classification) may also be entered for non-productive forest lands, if desired, but it is not required. Determination of ecosite is a prerequisite of scheduling a forest stand for silvicultural treatment and is required for verifying the selected forest operations prescription in accordance with the silvicultural ground rules of an approved forest management plan. Ecosite must be identified for all free-growing, (free-to-grow) forest stands based on the pre-harvest condition, and must reflect the characteristics of the site in terms of vegetation, soils, drainage, and tree species in accordance with a forest ecosystem classification. Ecosite should not be determined after a forest stand has been subjected to forest management operations. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Validation:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="english">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ECOSITE1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="The population of this attribute is only mandatory for ECOSITE1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="python">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) ${:F} row[&apos;${name}&apos;][0:2] not in (&apos;CE&apos;, &apos;NE&apos;, &apos;NW&apos;)  ]   "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="sql"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="test:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="{}">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="mandatory">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" ID="ID_1989237206" MODIFIED="1562265931189" TEXT="ECOSITE2:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="alias">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Ecosite 2"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="length">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="10"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="type">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="String"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ECOSITE2"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="definition">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="The ecosite attribute identifies an ecological landscape type that is present in the polygon and the regional ecological land classification system being used to define the ecosite code. Soil depth modifier information may also be included, if relevant. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Description">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Refer to the appropriate ecosystem field guide for a list of valid ecosite codes and their definitions. Ecosite is also referred to as site type. "/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="When recording the ecosite information in this attribute, the regional land classification identifier code value (from table below) replaces the first 2 characters of the ecosite code (either &quot;ES&quot; or &quot;ST&quot;). For example, if the northwest regional ecosite code of ES11 was determined to be appropriate for the stand, it would be recorded as NW11."/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Additionally, some ecosites, especially in the northwest region, can have soil depth modifier information associated with them. To record soil depth modifier information, add one of the codes from the table below to the end of the ecosite code. For example, northwest ecosite code of ES13 with a soil depth modifier of &quot;shallow&quot; would be recorded as:"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Ecosite information which is collected or created in accordance with an ecosystem classification (e.g., wetlands ecosystem classification) may also be entered for non-productive forest lands, if desired, but it is not required. Determination of ecosite is a prerequisite of scheduling a forest stand for silvicultural treatment and is required for verifying the selected forest operations prescription in accordance with the silvicultural ground rules of an approved forest management plan. Ecosite must be identified for all free-growing, (free-to-grow) forest stands based on the pre-harvest condition, and must reflect the characteristics of the site in terms of vegetation, soils, drainage, and tree species in accordance with a forest ecosystem classification. Ecosite should not be determined after a forest stand has been subjected to forest management operations. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Validation:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="english">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Blank or null values ARE valid"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="python"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="sql"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="test:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="{}">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="mandatory">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ECOPCT1:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="alias">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Ecosite 1 Percentage"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="length">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="2"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="type">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Integer"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ECOPCT1"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="definition">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="The ecosite percentage attribute identifies the percentage of an ecological landscape type that is present in the polygon. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Validation:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="english">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ECOPCT1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="The population of this attribute is only mandatory for ECOPCT1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: A zero or null value is not a valid code when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Adding all of ECOPCT attributes should equal 100 when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="python">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="[&apos;ST2: Sum of &lt;mark&gt;ECOPCT&lt;/mark&gt; attributes should equal 100&apos; ${:NF} row[&apos;${name}&apos;] + ( 0 if row[&apos;ECOPCT2&apos;] in ${list_NULL} else row[&apos;ECOPCT2&apos;] ) != 100 ]"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="sql"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="test:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;, &apos;${name}&apos;:99, &apos;ECOPCT2&apos;:None }">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST2: Sum of &lt;mark&gt;ECOPCT&lt;/mark&gt; attributes should equal 100"/>
</node>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="mandatory">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ECOPCT2:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="alias">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Ecosite 2 Percentage"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="length">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="2"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="type">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Integer"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ECOPCT2"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="definition">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="The ecosite percentage attribute identifies the percentage of an ecological landscape type that is present in the polygon. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Validation:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="english">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ECOPCT2 is NOT mandatory"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="python"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="sql"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="test:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="{}">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ACCESS1:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="alias">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Accessibility Indicator"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="length">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="3"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="type">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="String"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ACCESS1"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="domain">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="FMP_ACCESS"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="definition">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="The accessibility indicator attributes specifies whether or not there are any restrictions to accessing a productive forest stand. These restrictions may be legal (i.e., ownership), political / land use policy (i.e., land use designation, road closures), and/or a natural barrier. The focus of this field is identification of Crown productive forest stands that are inaccessible and therefore are not considered as part of the managed landbase for forest management planning purposes, but the principle may be applied to any area."/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT=" NOTE:   These attributes are not completed by the photo interpreters during inventory production. Access restriction information is to be determined by the forest management planning team. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="values">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="GEO:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="GEO"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Area is not accessible due to geographic reasons. "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="geography"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="LUD:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="LUD"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands). "/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="land use designation"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="NON:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="NON"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="The area is accessible/reachable."/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="no accessibility considerations"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="OWN:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="OWN"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)."/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ownership"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PRC:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="PRC"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area."/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="road closure"/>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="STO:">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="name">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="STO"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="desctext">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time."/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="option">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="subject to ownership"/>
</node>
</node>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="mandatory">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="Validation:">
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="english">
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR ( for ACCESS1)">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR (ACCESS1)">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931189" FOLDED="true" MODIFIED="1562265931189" TEXT="ST1: If ACCESS2 is not equal to NON then ACCESS1 must not be NON when POLYTYPE is equal to FOR">
<icon BUILTIN="full-6"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="python">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[&apos;ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON&apos;  ${:F} row[&apos;${name}&apos;] == &apos;NON&apos; and row[&apos;ACCESS2&apos;] != &apos;NON&apos; and row[&apos;ACCESS2&apos;] not in ${list_NULL}  ]">
<icon BUILTIN="full-6"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[&apos;ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON&apos;  for row in [row] if row[&apos;${name}&apos;] == &apos;NON&apos;       and (             row[&apos;ACCESS2&apos;] != &apos;NON&apos; and             str(row[&apos;ACCESS2&apos;]).strip() != &apos;&apos;              and row[&apos;ACCESS2&apos;] is not None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;             ) ]">
<icon BUILTIN="full-6"/>
</node>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="sql"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="test:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="{&apos;${name}&apos;:None, &apos;POLYTYPE&apos;:&apos;FOR&apos;}">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="{&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;, &apos;POLYTYPE&apos;:&apos;FOR&apos;}">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="{&apos;${name}&apos;:&apos;LUD&apos;, &apos;ACCESS2&apos;:&apos;LUD&apos;, &apos;POLYTYPE&apos;:&apos;FOR&apos;}">
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="{&apos;${name}&apos;:&apos;NON&apos;, &apos;ACCESS2&apos;:&apos;LUD&apos;, &apos;POLYTYPE&apos;:&apos;FOR&apos;}">
<icon BUILTIN="full-6"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: if ACCESS2 != NON then &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not = NON"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="{&apos;${name}&apos;:&apos;NON&apos;, &apos;POLYTYPE&apos;:&apos;FOR&apos;, &apos;ACCESS2&apos;:None}"/>
</node>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ACCESS2:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="alias">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="Accessibility Indicator 2"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="length">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="3"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="type">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="String"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ACCESS2"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="domain">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="FMP_ACCESS"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="Validation:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="english">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="Note: The ACCESS2 attribute may not exist in the submitted coverage as this attribute is not mandatory.  If this attribute exists, it may not be populated.  Therefore, this validation will be performed on the ACCESS2 attribute for the application of the correct coding scheme.  "/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1 when POLYTYPE is equal to FOR "/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT=" ST1: The population of this attribute is mandatory for ACCESS1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT=" ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="If ACCESS1 or ACCESS2 do not equal NON then AVAIL should be U when POLYTYPE is equal to FOR. Tested by the AVAIL attribute"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="python">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;GEO&apos;,&apos;LUD&apos;,&apos;NON&apos;,&apos;OWN&apos;,&apos;PRC&apos;,&apos;STO&apos;, &apos;&apos;, &apos; &apos;, None ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="sql"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="test:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="{}">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="mandatory">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="values">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="GEO:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="GEO"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="Area is not accessible due to geographic reasons. "/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="geography"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="LUD:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="LUD"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands). "/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="land use designation"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="NON:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="NON"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="The area is accessible/reachable."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="no accessibility considerations"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="OWN:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="OWN"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ownership"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="PRC:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="PRC"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="road closure"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="STO:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="STO"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="subject to ownership"/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="MGMTCON1:">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="length">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="4"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="type">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="String"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="MGMTCON1"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="domain">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="MGMTCON"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="alias">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="Management Consideration"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="definition">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. "/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="description">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="values">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="COLD:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="COLD"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="permafrost"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="DAMG:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="DAMG"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="physical/natural damage  "/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ISLD:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ISLD"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="island"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="NATB:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="NATB"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="natural barrier"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="NONE:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="NONE"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="There are no physical or ecological &apos;restrictions&apos; in the site that need to be considered when determining management of the stand."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="no management consideration"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="PENA:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="PENA"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="An area of land that is nearly surrounded by water and is connected to the mainland."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="peninsula"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="POOR:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="POOR"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="stagnated, poor tree growth - no indicator"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ROCK:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ROCK"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="exposed bedrock / rocky outcrops"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="SAND:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="SAND"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A site where forest regeneration will be difficult due to the potential for erosion."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="blow sand / exposed fine sand, shallow or no humus"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="SHRB:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="SHRB"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="heavy shrub / brush"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="SOIL:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="SOIL"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="shallow soils"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="STEP:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="STEP"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. "/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="steep slopes"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="WATR:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="WATR"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="telluric / highly fluctuating, moving ground water"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="WETT:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="name">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="WETT"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="desctext">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables."/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="option">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="poorly drained - high water table"/>
</node>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="Validation:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="english">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: The population of this attribute is mandatory for MGMTCON1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: A blank or null value is not a valid code for MGMTCON1"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: IF ACCESS1 or ACCESS2 is equal to GEO then MGMTCON1 must not be NONE">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="ST1: If FORMOD equals PF, then the MGMTCON1 attribute should not equal NONE">
<icon BUILTIN="full-9"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="If the SC attribute equals 4, then the MGMTCON1 attribute should not equal NONE">
<icon BUILTIN="full-1"/>
<icon BUILTIN="full-0"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="If MGMTCON2 is not NONE then MGMTCON1 must not be NONE"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="If MGMTCON3 is not NONE then the MGMTCON2 and MGMTCON1 must not be NONE"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="python">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[ &apos;${name} must not be blank or null&apos; for row in [row] if row[&apos;${name}&apos;] in [ None, &apos;&apos;, &apos; &apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;   ]"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;COLD&apos;,&apos;DAMG&apos;,&apos;ISLD&apos;,&apos;NATB&apos;,&apos;NONE&apos;,&apos;PENA&apos;,&apos;POOR&apos;,&apos;&apos;,&apos;ROCK&apos;,&apos;SAND&apos;,&apos;SHRB&apos;,&apos;SOIL&apos;,&apos;STEP&apos;,&apos;WATR&apos;,&apos;WETT&apos;] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[ &apos;${name} must not be none when MGMTCON2 is not None&apos; for row in [row] if row[&apos;${name}&apos;] in [ &apos;NONE&apos; ] and row[&apos;MGMTCON2&apos;] not in [ &apos;NONE&apos; , &apos;&apos; ,&apos; &apos;] ]"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[ &apos;${name} must not be none when MGMTCON3 is not None&apos; for row in [row] if row[&apos;${name}&apos;] in [ &apos;NONE&apos; ] and row[&apos;MGMTCON3&apos;] not in [ &apos;NONE&apos; , &apos;&apos; ,&apos; &apos;] ]"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[&apos;ST1: If ACCESS1 or ACCESS2 are GEO, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE&apos; ${:F} row[&apos;${name}&apos;] == &apos;NONE&apos; and row[&apos;ACCESS1&apos;] == &apos;GEO&apos; ]">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[&apos;ST1: If ACCESS1 or ACCESS2 are GEO, &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be NONE&apos; ${:F} row[&apos;${name}&apos;] == &apos;NONE&apos; and row[&apos;ACCESS2&apos;] == &apos;GEO&apos;  ]">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[&apos;ST1: If FORMOD equals PF, &lt;b&gt;&lt;mark&gt;MGMTCON1&lt;/mark&gt;&lt;/b&gt; should not equal NONE when POLYTYPE equals FOR&apos; ${:F} row[&apos;${name}&apos;] == &apos;NONE&apos; and row[&apos;FORMOD&apos;] == &apos;PF&apos; ]">
<icon BUILTIN="full-9"/>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="[&apos;ST1: If SC equals 4, &lt;b&gt;&lt;mark&gt;MGMTCON1&lt;/mark&gt;&lt;/b&gt; should not equal NONE when POLYTYPE equals FOR&apos; ${:F} row[&apos;${name}&apos;] == &apos;NONE&apos; and row[&apos;SC&apos;] == 4 ]">
<icon BUILTIN="full-1"/>
<icon BUILTIN="full-0"/>
</node>
</node>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="sql"/>
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="test:">
<node CREATED="1562265931190" FOLDED="true" MODIFIED="1562265931190" TEXT="{}">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="mandatory">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="MGMTCON2:">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="length">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="4"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="type">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="String"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="MGMTCON2"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="domain">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="MGMTCON"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="alias">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="Management Consideration"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="definition">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. "/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="description">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="values">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="COLD:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="COLD"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="permafrost"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="DAMG:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="DAMG"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="physical/natural damage  "/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ISLD:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ISLD"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="island"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="NATB:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="NATB"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="natural barrier"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="NONE:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="NONE"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="There are no physical or ecological &apos;restrictions&apos; in the site that need to be considered when determining management of the stand."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="no management consideration"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="PENA:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="PENA"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="An area of land that is nearly surrounded by water and is connected to the mainland."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="peninsula"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="POOR:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="POOR"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="stagnated, poor tree growth - no indicator"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ROCK:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ROCK"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="exposed bedrock / rocky outcrops"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SAND:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SAND"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where forest regeneration will be difficult due to the potential for erosion."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="blow sand / exposed fine sand, shallow or no humus"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SHRB:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SHRB"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="heavy shrub / brush"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SOIL:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SOIL"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="shallow soils"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="STEP:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="STEP"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. "/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="steep slopes"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="WATR:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="WATR"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="telluric / highly fluctuating, moving ground water"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="WETT:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="WETT"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="poorly drained - high water table"/>
</node>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="Validation:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="english">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT=" The attribute population must follow the correct format, or empty when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="python">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;COLD&apos;,&apos;DAMG&apos;,&apos;ISLD&apos;,&apos;NATB&apos;,&apos;NONE&apos;,&apos;PENA&apos;,&apos;POOR&apos;,&apos;&apos;,&apos;ROCK&apos;,&apos;SAND&apos;,&apos;SHRB&apos;,&apos;SOIL&apos;,&apos;STEP&apos;,&apos;WATR&apos;,&apos;WETT&apos;,&apos;&apos;,None] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="[ &apos;${name} must not be none when MGMTCON3 is not None&apos; for row in [row] if row[&apos;${name}&apos;] in [ &apos;NONE&apos; ] and row[&apos;MGMTCON3&apos;] not in [ &apos;NONE&apos; , &apos;&apos; ,&apos; &apos; ] ]"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="sql"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="test:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="{}">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="mandatory">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="MGMTCON3:">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="length">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="4"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="type">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="String"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="MGMTCON3"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="domain">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="MGMTCON"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="alias">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="Management Consideration"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="definition">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. "/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="description">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="values">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="COLD:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="COLD"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="permafrost"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="DAMG:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="DAMG"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="physical/natural damage  "/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ISLD:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ISLD"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="island"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="NATB:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="NATB"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="natural barrier"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="NONE:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="NONE"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="There are no physical or ecological &apos;restrictions&apos; in the site that need to be considered when determining management of the stand."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="no management consideration"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="PENA:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="PENA"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="An area of land that is nearly surrounded by water and is connected to the mainland."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="peninsula"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="POOR:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="POOR"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="stagnated, poor tree growth - no indicator"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ROCK:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ROCK"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="exposed bedrock / rocky outcrops"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SAND:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SAND"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where forest regeneration will be difficult due to the potential for erosion."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="blow sand / exposed fine sand, shallow or no humus"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SHRB:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SHRB"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="heavy shrub / brush"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SOIL:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="SOIL"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="shallow soils"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="STEP:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="STEP"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. "/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="steep slopes"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="WATR:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="WATR"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="telluric / highly fluctuating, moving ground water"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="WETT:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="name">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="WETT"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="desctext">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables."/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="option">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="poorly drained - high water table"/>
</node>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="Validation:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="english">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT=" The attribute population must follow the correct format, or empty when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="python">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;COLD&apos;,&apos;DAMG&apos;,&apos;ISLD&apos;,&apos;NATB&apos;,&apos;NONE&apos;,&apos;PENA&apos;,&apos;POOR&apos;,&apos;&apos;,&apos;ROCK&apos;,&apos;SAND&apos;,&apos;SHRB&apos;,&apos;SOIL&apos;,&apos;STEP&apos;,&apos;WATR&apos;,&apos;WETT&apos;,&apos;&apos;,None] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="sql"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="test:">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="{}">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="mandatory">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="AGS_POLE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="alias">
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="Polewood Acceptable Growing Stock"/>
</node>
<node CREATED="1562265931191" FOLDED="true" MODIFIED="1562265931191" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="4"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Float"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="AGS_POLE"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="AGS_SML:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="alias">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Sm Sawlog Acceptable Growing Stock"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="4"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Float"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="AGS_SML"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="AGS_MED:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="alias">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Med Sawlog Acceptable Growing Stock"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="4"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Float"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="AGS_MED"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" ID="ID_851804329" MODIFIED="1562265931192" TEXT="AGS_LGE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="alias">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Lg Sawlog Acceptable Growing Stock"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="4"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Float"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="AGS_LGE"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UGS_POLE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="alias">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Polewood Unacceptable Growing Stock"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="4"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Float"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UGS_POLE"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UGS_SML:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="alias">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Sm Sawlog Unacceptable Growing Stock"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="4"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Float"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UGS_SML"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UGS_MED:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="alias">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Med Sawlog Unacceptable Growing Stock"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="4"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Float"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UGS_MED"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UGS_LGE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="alias">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Lg Sawlog Unacceptable Growing Stock"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="4"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Float"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UGS_LGE"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=" If the DEVSTAGE attribute is IMPROVE, SELECT, SNGLTREE or GROUPSE, then one of  these attributes must be greater than zero"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" ID="ID_1655388715" MODIFIED="1562265931192" TEXT="USPCOMP:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="alias">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Understorey Species Composition"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="120"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="String"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="USPCOMP"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="definition">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="This understorey species composition attribute indicates the tree species that are present in the understorey portion of the forest stand canopy and the proportion of cover that each species occupies within the understorey. "/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Description">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="See SPCOMP list in eFRI tech spec for list of acceptable species"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) "/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=" No duplicate species codes allowed in the string"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Proportion values in the string must sum to 100 "/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="The tree species in the composition are to be coded using the scheme listed here.   "/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="*each species code is 3 characters (including blanks) and is left justified"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="*each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified. Note that initial inventory values determined by photo interpretation will not go below 10% and are rounded to the nearest 10%  (e.g., 10, 20, 30). During inventory update, proportions based on subsequent field survey sampling that are of a finer resolution can be entered (e.g., go below 10% or be a value in between the initial rounded proportions such as 15). "/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="*maximum of 10 species and proportions pairs in the string"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="If SOURCE is NOT plot based then allow specific species in SPCOMP (eFRI tech spec)"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Tree species in the list [     &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,     &apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,     &apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,     &apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;,     &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos;     ]"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="If SOURCE is plot based (PLOTVAR, PLOTFIXD) then allow other species in SPCOMP (eFRI tech spec)"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Plot Species are [     &apos;AL&apos;,&apos;Aq&apos;,&apos;Ap&apos;,&apos;Ag&apos;,&apos;Bc&apos;,&apos;Bp&apos;,&apos;Gb&apos;,&apos;Bb&apos;,&apos;Cat&apos;,&apos;Cc&apos;,&apos;Cm&apos;,     &apos;Cp&apos;,&apos;Cs&apos;,&apos;Ct&apos;,&apos;Er&apos;,&apos;Eu&apos;,&apos;Hk&apos;,&apos;Ht&apos;,&apos;Hl&apos;,&apos;Hb&apos;,&apos;Hm&apos;,&apos;Hp&apos;,     &apos;Hs&apos;,&apos;Hc&apos;,&apos;Kk&apos;,&apos;Le&apos;,&apos;Lj&apos;,&apos;Bl&apos;,&apos;Ll&apos;,&apos;Lb&apos;,&apos;Gt&apos;,&apos;Mb&apos;,&apos;Mf&apos;,     &apos;Mm&apos;,&apos;Mt&apos;,&apos;Mn&apos;,&apos;Mp&apos;,&apos;AM&apos;,&apos;Ema&apos;,&apos;Mo&apos;,&apos;Obl&apos;,&apos;Ob&apos;,&apos;Och&apos;,&apos;Op&apos;,     &apos;Os&apos;,&apos;Osw&apos;,&apos;Pa&apos;,&apos;Pn&apos;,&apos;Pp&apos;,&apos;Pc&apos;,&apos;Ph&apos;,&apos;Pe&apos;,&apos;Red&apos;,&apos;Ss&apos;,&apos;Sc&apos;,     &apos;Sk&apos;,&apos;Sn&apos;,&apos;Sr&apos;,&apos;Sy&apos;,&apos;Tp&apos;,&apos;Haz&apos;     ]"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="If SPCOMP percentages are not in 10&apos;s then SOURCE must be plot based. 1&apos;s not 10&apos;s"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="[&apos;ST%s: &lt;b&gt;&lt;mark&gt;${name} formatting error&lt;/mark&gt;&lt;/b&gt; %s&apos;%( &apos;2&apos; if &apos;WARNING&apos; in i else &apos;1&apos;   ,i)    for i in test_spcomp(row[&apos;${name}&apos;]) if ${NINN} if i not in  [ None, [ None ] , []  ] ]">
<icon BUILTIN="full-4"/>
<icon BUILTIN="full-5"/>
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="[ &apos;%s is not a valid species code&apos;%(s) for s in test_SpeciesInList(row[&apos;${name}&apos;], [ &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,&apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,&apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;, &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos; ]        ) if row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;SOURCE&apos;][:4] != &apos;PLOT&apos; and row[&apos;VERT&apos;] in [&apos;TO&apos;,&apos;TU&apos;,&apos;MO&apos;,&apos;MU&apos;]  and row[&apos;${name}&apos;] is not None ]"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="[ &apos;%s is not a valid species code&apos;%(s) for s in test_SpeciesInList(row[&apos;${name}&apos;],    [ &apos;AL&apos;,&apos;Aq&apos;,&apos;Ap&apos;,&apos;Ag&apos;,&apos;Bc&apos;,&apos;Bp&apos;,&apos;Gb&apos;,&apos;Bb&apos;,&apos;Cat&apos;,&apos;Cc&apos;,&apos;Cm&apos;, &apos;Cp&apos;,&apos;Cs&apos;,&apos;Ct&apos;,&apos;Er&apos;,&apos;Eu&apos;,&apos;Hk&apos;,&apos;Ht&apos;,&apos;Hl&apos;,&apos;Hb&apos;,&apos;Hm&apos;,&apos;Hp&apos;, &apos;Hs&apos;,&apos;Hc&apos;,&apos;Kk&apos;,&apos;Le&apos;,&apos;Lj&apos;,&apos;Bl&apos;,&apos;Ll&apos;,&apos;Lb&apos;,&apos;Gt&apos;,&apos;Mb&apos;,&apos;Mf&apos;,&apos;Mm&apos;,&apos;Mt&apos;,&apos;Mn&apos;,&apos;Mp&apos;,&apos;AM&apos;,&apos;Ema&apos;,&apos;Mo&apos;,&apos;Obl&apos;,&apos;Ob&apos;,&apos;Och&apos;,&apos;Op&apos;, &apos;Os&apos;,&apos;Osw&apos;,&apos;Pa&apos;,&apos;Pn&apos;,&apos;Pp&apos;,&apos;Pc&apos;,&apos;Ph&apos;,&apos;Pe&apos;,&apos;Red&apos;,&apos;Ss&apos;,&apos;Sc&apos;,&apos;Sk&apos;,&apos;Sn&apos;,&apos;Sr&apos;,&apos;Sy&apos;,&apos;Tp&apos;,&apos;Haz&apos; ,                               &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,&apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,&apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;, &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos;    ]     ) if row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; and row[&apos;SOURCE&apos;][:4] == &apos;PLOT&apos; and row[&apos;VERT&apos;] in [&apos;TO&apos;,&apos;TU&apos;,&apos;MO&apos;,&apos;MU&apos;] and row[&apos;${name}&apos;] is not None ]"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="mandatory">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UWG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="alias">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Understory Working Group"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="length">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="2"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="type">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="String"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="UWG"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="Validation:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="english">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="python"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="sql"/>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="test:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="{&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;, &apos;POLYTYPE&apos;:&apos;FOR&apos;}">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
</node>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="values">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="BF:">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="name">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="BF"/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="desctext">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT=""/>
</node>
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="option">
<node CREATED="1562265931192" FOLDED="true" MODIFIED="1562265931192" TEXT="balsam fir"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="CE:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="CE"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="cedar"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="HE:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="HE"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="hemlock"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="LA:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="LA"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="larch"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PJ:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PJ"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="jack pine"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PR:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PR"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="red pine"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PS:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PS"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="scots pine"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PW:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PW"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="white pine"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="SB:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="SB"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="black spruce"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="SW:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="SW"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="white spruce"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="SX:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="SX"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="spruce mix"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="OC:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="OC"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="other conifer"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="AX:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="AX"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="ash mix"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="BG:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="BG"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="gray birch"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="BW:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="BW"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="white birch"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="BY:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="BY"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="yellow birch"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="MH:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="MH"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="hard / sugar maple"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="MR:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="MR"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="soft / red maple"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="MX:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="MX"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="maple mix"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="OX:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="OX"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="oak mix"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PO:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PO"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Poplar"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PB:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="PB"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="balsam poplar"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="OH:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="OH"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="desctext">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="option">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="other hardwoods"/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="UYRORG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="alias">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Understory Year of Origin"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="length">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="4"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="type">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Integer"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="UYRORG"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="definition">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="The understorey year of origin attribute indicates the average year that the species having the greatest relative abundance (in terms of basal area) within the understorey came into existence."/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Description">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="The year of origin is used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration.   Once a forest stand has been assessed as free-to-grow, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site.   In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered.  For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach free-growing conditions), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip has achieved free-to-grow status, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required.  Normally, updates to the year of origin have a corresponding change to the year of update attribute as this indicates the currency/vintage of the information and when the year of origin value was determined.   Year of origin information is not required for non-forested and non-productive forest land types. "/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="This is a calculated value. Understorey year of origin is calculated as the year of the data source from which age is determined  minus  the understorey age as determined using the source  (i.e.,  UYRORG  =  year of age source - UAGE).  For example, using imagery taken in 2007, the age of the stand is interpreted to be 50 years at that time (in 2007).  Thus the year of origin for the stand is 2007 - 50 = 1957.  Note that the source used to determine age may not be the data source listed in the SOURCE attribute if multiple data sources were used to generate the stand description.  ">
<icon BUILTIN="help"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Validation:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="english">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="ST1: The UYRORG attribute value must be greater than or equal to 1600 when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="python">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="[ &apos;ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;= 1600&apos; ${:NF} row[&apos;${name}&apos;]  &lt;1600  ]   "/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="[ &apos;ST1: &lt;mark&gt;${name}&lt;/mark&gt; should be &gt; YRORG&apos; ${:NF} row[&apos;${name}&apos;]  &lt;= row[&apos;YRORG&apos;]  ]   "/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="sql"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="test:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="{&apos;${name}&apos;:1599, &apos;POLYTYPE&apos;:&apos;FOR&apos;}">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be &gt;= 1600"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="{&apos;${name}&apos;:1899, &apos;POLYTYPE&apos;:&apos;FOR&apos;, &apos;YRORG&apos;:1930 }">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; should be &gt; YRORG"/>
</node>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="mandatory">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="UHT:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="alias">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Understory Height"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="length">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="4"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="type">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Float"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="UHT"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="definition">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="The understorey height attribute indicates the estimated average tree height (in meters) of the species that has the most basal area as inventoried in the Year of Update. Estimates can be made from interpreted crown closure or field samples, or from growth algorithms."/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Validation:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="english">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Value should be less than HT when POLYTYPE is equal to FOR."/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="python">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 40) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  ]   "/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="[ &apos;${name} should be &lt;= HT&apos; for row in [row] if row[&apos;${name}&apos;]  &gt; row[&apos;HT&apos;]  and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]   "/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="sql"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="test:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="{}">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="DomainValidation:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="english">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Height / Age relationships should be in the range of Plonski and others."/>
</node>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="USTKG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="alias">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Understory Stocking"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="length">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="4"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="type">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Float"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="name">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="USTKG"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="definition">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="The understorey stocking attribute indicates a qualitative measure of the density of tree cover within the understorey.  It is expressed as a percentage value ranging from zero to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field."/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="Validation:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="english">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="ST1: The attribute population must follow the correct format when the POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="python">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 4.00 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 4.01) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  ]   "/>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="sql"/>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="test:">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="{}">
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931193" FOLDED="true" MODIFIED="1562265931193" TEXT="USC:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="length">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="1"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="type">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Integer"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="USC"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="alias">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Understorey Site Class"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="definition">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="The understorey site class attribute indicates a site quality estimate for the understorey of a forest stand. It is determined using the average height, age, and working group, based on the dominant tree species of the understorey. These attributes are compared against height and age growth curves in Plonski&apos;s Normal Yield Tables for different species to determine the relative growth rate for a forest stand.   "/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="values">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="0:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="0"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="desctext">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=""/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="option">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Best"/>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="1:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="1"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="desctext">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=""/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="option">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Better"/>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="2:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="2"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="desctext">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=""/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="option">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Good"/>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="3:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="3"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="desctext">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=""/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="option">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Poor"/>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="4:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="4"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="desctext">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock."/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="option">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Protection Forest"/>
</node>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Validation:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="english">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="python">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="[ &apos;${name} must follow coding&apos; for row in [row] if row[&apos;${name}&apos;] not in [ 0,1,2,3,4 ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  ]   "/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="sql"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="test:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="{}">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="VERDATE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="length">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="8"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="type">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Date"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="VERDATE"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="alias">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Verification Status Date"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="definition">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="The verification status date attribute contains the date that the geographic unit was verified/validated.  "/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Validation:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="english">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="python"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="sql"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="test:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="{}">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" ID="ID_530587401" MODIFIED="1562265931194" TEXT="SENSITIV:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="length">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="3"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="type">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="String"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="SENSITIV"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="alias">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Data Sensitivity Indicator"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="definition">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="The data sensitivity indicator attribute contains an indication of whether the geographic unit is classified as sensitive or not."/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Validation:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="english">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="The attribute population must follow the correct format"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Contains yes or no"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="python"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="sql"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="test:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="{}">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="values">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Y:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Y"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="desctext">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=""/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="option">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="yes"/>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="N:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="N"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="desctext">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT=""/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="option">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="No"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265930023" FOLDED="true" ID="ID_799740481" MODIFIED="1562265930023" TEXT="SGR:">
<icon BUILTIN="flag"/>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="length">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="25"/>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="type">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="String"/>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="name">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="SGR"/>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="alias">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="Silviculture Ground Rule"/>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="definition">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT=""/>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="description">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT=""/>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="Validation:">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="english">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR">
<icon BUILTIN="full-2"/>
</node>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="python"/>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="test:">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="{ &apos;POLYTYPE&apos;:&apos;GRS&apos;, &apos;FORMOD&apos;:None, &apos;DEVSTAGE&apos;:None, &apos;OYRORG&apos;:None, &apos;OSPCOMP&apos;:None, &apos;OLEADSPC&apos;:None, &apos;OAGE&apos;:None, &apos;OHT&apos;:None, &apos;OCCLO&apos;:None, &apos;OSTKG&apos;:None, &apos;OSC&apos;:None, &apos;UYRORG&apos;:None, &apos;USPCOMP&apos;:None, &apos;ULEADSPC&apos;:None, &apos;UAGE&apos;:None, &apos;UHT&apos;:None, &apos;UCCLO&apos;:None, &apos;USTKG&apos;:None, &apos;USC&apos;:None, &apos;INCIDSPC&apos;:None, &apos;ACCESS1&apos;:None, &apos;ACCESS2&apos;:None, &apos;MGMTCON1&apos;:None, &apos;MGMTCON2&apos;:None, &apos;MGMTCON3&apos;:None, &apos;YRORG&apos;:None, &apos;SPCOMP&apos;:None, &apos;LEADSPC&apos;:None, &apos;AGE&apos;:None, &apos;HT&apos;:None, &apos;CCLO&apos;:None, &apos;STKG&apos;:None, &apos;SC&apos;:None, &apos;MANAGED&apos;:None, &apos;PLANFU&apos;:None, &apos;AU&apos;:None, &apos;AGESTR&apos;:None, &apos;AVAIL&apos;:None, &apos;SILVSYS&apos;:None, &apos;NEXTSTG&apos;:None, &apos;YIELD&apos;:None, &apos;SGR&apos;:None, &apos;${name}&apos;:&apos;COLD&apos; }">
<icon BUILTIN="full-2"/>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be null if POLYTYPE is not FOR..."/>
</node>
</node>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="mandatory">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="default">
<node CREATED="1562265930023" FOLDED="true" MODIFIED="1562265930023" TEXT="default"/>
</node>
</node>
</node>
<node CREATED="1562266852872" ID="ID_1341491854" MODIFIED="1562266859123" TEXT="non-fields">
<node CREATED="1562265931186" FOLDED="true" ID="ID_1449480991" MODIFIED="1562265931186" TEXT="DEPTYPE:">
<icon BUILTIN="flag"/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="length">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="8"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="type">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Character"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="DEPTYPE"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="alias">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Type of Disturbance"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="definition">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The type of disturbance attribute identifies the disturbance that occurred in the year recorded in the companion attribute YRDEP (year of disturbance).  The disturbance may have affected the entire stand or only a portion of it."/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="mandatory">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="The attribute population must follow the correct coding scheme"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Validation:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="english">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The attribute must be Null when POLYTYPE is not equal to FOR">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: The attribute population must follow the correct coding scheme where YRDEP &lt;&gt; 0">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST1: A null or blank value is not a valid code where YRDEP &lt;&gt; 0">
<icon BUILTIN="full-4"/>
</node>
<node BACKGROUND_COLOR="#ffffff" CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST2: If the development stage is depleted, new or established (DEVSTAGE = DEP, NEW or EST) then the disturbance type should not have a value of unknown (DEPTYPE = UNKNOWN)">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ST2: If the disturbance type is not null (DEPTYPE &lt;&gt; null) then the disturbance year should not be equal to zero (YRDEP = 0). Should actually be &gt;= 1900">
<icon BUILTIN="full-6"/>
<icon BUILTIN="password"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="python">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="[&apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be UNKNOWN if DEVSTAGE are DEP, NEW or EST&apos; ${:} row[&apos;${name}&apos;] == &apos;UNKNOWN&apos; and row[&apos;DEVSTAGE&apos;] in [&apos;DEPHARV&apos;, &apos;DEPNAT&apos;, &apos;NEWPLANT&apos;, &apos;NEWSEED&apos;, &apos;NEWNAT&apos;, &apos;ESTPLANT&apos;, &apos;ESTSEED&apos;, &apos;ESTNAT&apos;] ]">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="[&apos;ST2: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; should be populated when YRDEP is populated&apos;  ${:F} ${NIN} and row[&apos;YRDEP&apos;] not in ${list_NULL} ]">
<icon BUILTIN="full-6"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="sql"/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="test:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="{}">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="values">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="BLOWDOWN:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="BLOWDOWN"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="wind / blowdown"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="DISEASE:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="DISEASE"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="disease"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="DROUGHT:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="DROUGHT"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="drought"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="FIRE:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="FIRE"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="fire"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="FLOOD:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="FLOOD"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="flood"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="HARVEST:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="HARVEST"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Partial or full stand removal of timber.  This includes mid-rotation or stand improvement operations where merchantable timber is removed."/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="harvest"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ICE:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ICE"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="ice damage"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="INSECTS:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="INSECTS"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="insects"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="SNOW:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="INSECTS"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT=""/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="insects"/>
</node>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="UNKOWN:">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="name">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="UNKNOWN"/>
</node>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="desctext"/>
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="option">
<node CREATED="1562265931186" FOLDED="true" MODIFIED="1562265931186" TEXT="Unknown"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="geometry_type">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Polygon"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="alias">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Planning Composite (PCM)"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="path">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Plan"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Validation:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="english">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="This table must contain all mandatory fields"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="POLYID must contain unique values"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="python">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="[ &apos;POLYID Field values must be unique&apos; for f in [&apos;POLYID&apos; ] if not self.uniqueValueTest(f) ]"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="sql"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="symbology:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="type">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="categorized"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="field">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="POLYTYPE"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="stroke color">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="#777777"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="fill color">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="#FFFFFF"/>
</node>
</node>
</node>
<node CREATED="1562265931194" ID="ID_987035942" MODIFIED="1562265931194" POSITION="right" TEXT="BaseModelInventory:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="BaseModelInventory"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="shortname">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="BMI"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="spatial_reference">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="26916"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="fields_oldversion">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="FMFOBJID:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="alias">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="FMFOBJID"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="length">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="13"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="type">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Integer"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="FMFOBJID"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Definition:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="The FMF Object Identifier attribute information is supplied by MNR and is intended to be used to identify / track change data in the future. This is a numeric value supplied by MNR. "/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Validation:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="english">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="This field is optional and will be blank for new SFL maintained polygons"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="python"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="sql"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="shortname">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="FMFOBJID"/>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="GEOGNUM:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="alias">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="GEOGNUM"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="length">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="7"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="type">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Integer"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="GEOGNUM"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Definition">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="The geographic unit type number attribute information is supplied by MNR. This is a numeric value supplied by MNR. "/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="Validation:">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="english">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="This field is optional and will be blank for new SFL maintained polygons"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="python"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="sql"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="shortname">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="POLYID"/>
</node>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="POLYID:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="length">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="25"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="type">
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="String"/>
</node>
<node CREATED="1562265931194" FOLDED="true" MODIFIED="1562265931194" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="POLYID"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="alias">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Polygon Identifier"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Definition">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="The polygon identifier attribute is a unique identifier / label for the polygon. "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Validation:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="english">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: A blank or null value is not a valid code"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Attribute must contain a unique value"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="python">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None    ]"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos;    ]"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="sql"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="shortname">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="POLYID"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="POLYTYPE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="length">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="3"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="type">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="String"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="POLYTYPE"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="domain">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="POLYTYPE"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="alias">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Polygon Type"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="definition">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="The polygon type attribute indicates the classification of the area within the polygon boundaries into one of several generalized water and land types. "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Description">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="The polygon type is determined from the classification of area on a forest management unit into different water and land types. Some polygon types are derived from inventory base features, while other polygons must be created from a classification process.  All remaining land areas within a designated forest management unit are classified into various non-forested or forested lands. "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Validation:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="english">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: A blank or null value is not a valid code"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="If POLYTYPE attribute does not equal FOR, then FORMOD, DEVSTAGE, YRDEP, SPCOMP, WG, YRORG, HT, STKG, SC, ECOSRC, ECOSITE1, ECOPCT1, ECOSITE2, ECOPCT2, ACCESS1, ACCESS2, MGMTCON1, MGMTCON2, MGMTCON3, AGS_POLE, AGS_SML, AGS_MED, AGS_LGE, UGS_POLE, UGS_SML, UGS_MED, UGS_LGE, USPCOMP, UWG, UYRORG, UHT, USTKG, USC, MANAGED, TYPE, MNRCODE, SMZ, OMZ, PLANFU, AGESTR, AGE, AVAIL, SILVSYS, NEXTSTG, SI, and SISRC attributes should be empty">
<icon BUILTIN="stop-sign"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="If a delineated polygon is an island or is located on an island, the management consideration attribute is set to island (MGMTCON1 = ISLD). This applies to all polygon types. This apparent redundancy in classification for polygons having a type of island (i.e., POLYTYPE = ISL   and   MGMTCON1 = ISLD) allow resource managers to easily identify:   1.) all polygons located on islands regardless of polygon type (by querying on MGMTCON1 = ISLD)  2.) just the small uninterpreted islands (by querying on POLYTYPE = ISL) depending upon the desired analysis. ">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="python">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;WAT&apos;, &apos;DAL&apos;, &apos;GRS&apos;, &apos;ISL&apos;, &apos;UCL&apos;, &apos;BSH&apos;, &apos;RCK&apos;, &apos;TMS&apos;, &apos;OMS&apos;, &apos;FOR&apos;] ]   "/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None    ]"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos;    ]"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &quot;POLYTYPE ISL implies MGMTCON1 ISLD&quot; for row in [ row ] if  row[&apos;POLYTYPE&apos;] == &apos;ISL&apos; and row[&apos;MGMTCON1&apos;] != &apos;ISLD&apos; ]"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="sql"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="values">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="WAT:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="WAT"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" STYLE="fork" TEXT="All water areas.  Includes lakes, ponds, reservoirs (i.e., inland basin areas containing water) and wide (two sided) rivers.  These are rivers that can be defined by area.  Generally, these rivers/streams are consistently wider than 20 meters when portrayed at a mapping scale of 1:10,000 or consistently wider than 40 meters when portrayed at a mapping scale of 1:20,000.  Smaller/narrower rivers and streams are maintained as linear features in a centre-line layer(s)."/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Water"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="DAL:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="DAL"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Lands which are cultivated for growing crops, orchards, floral gardens, etc. These areas may include abandoned agricultural lands. "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Developed agricultural land"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="GRS:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="GRS"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Farm areas devoted to pasture for domesticated animals. These areas may also include abandoned grass and meadows, but are not part of the productive forest land base and do not include &quot;barren and scattered&quot; areas. These areas are similar to barren and scattered, but are located near developed agriculture land or unclassified areas and are usually fenced. "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Grass and meadow"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ISL:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ISL"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Islands less than 8 hectares in size, down to a lower limit of 0.0025 hectares or 25 square meters in size (e.g., 5 meters X 5 meters) are recorded during the inventory production process, but are not interpreted/typed for practicality and cost considerations. Only islands 8 hectares and larger are interpreted and assigned an appropriate POLYTYPE code, such as FOR or BSH. * "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Small island"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="UCL:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="UCL"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="None"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Unclassified"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="BSH:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="BSH"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Areas covered with &apos;non-commercial&apos; tree species or shrubs.  These areas are normally associated with wetlands or water features."/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Brush and alder"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="OMS:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="OMS"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Wet areas of mosses, grasses, sedges, and small herbaceous plants, often interspersed with small areas of open water."/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Open wetland"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="RCK:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="RCK"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Areas of barren or exposed rock (e.g., bedrock, cliff face, talus slope) which may support a few scattered trees, but is less that 25% stocked."/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Rock"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="TMS:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="TMS"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Areas of dry or wet muskeg on which stunted trees occur as widely spaced individuals or in small groups."/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Treed wetland"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="FOR:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="FOR"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Areas that are capable of producing trees and can support tree growth.  These areas may or may not be capable of supporting the harvesting of timber on a sustained yield basis.  Some areas may have physical and/or biological characteristics which effect land use.  Thus this polygon type includes both production and protection forest areas. "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Productive forest"/>
</node>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="mandatory">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="OWNER:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="OWNER"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="length">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="1"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="type">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="string"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="domain">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="OWNER"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="alias">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Ownership"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="definition">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="This ownership attribute contains the traditional FRI ownership information as assigned by the Office of the Surveyor General."/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Description">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="The ownership designation attribute is derived from the ownership and land tenure, and parks and reserves layers which are maintained in the MNR&apos;s values information system. This attribute also identifies the managed Crown area in a forest management unit.  Any discrepancies regarding the information contained in the ownership designation attribute should be reported to the appropriate MNR district office. The most accurate source for ownership information is located in the appropriate regional Land Registry Office. "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Validation:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="english">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="python">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None    ]"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9] ]   "/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos;    ]"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="sql"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="values">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="0:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="0"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Unknown / unassigned ownership"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="1:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="1"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Crown land"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="2:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="2"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Patent land - with timber rights reserved to the Crown"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="3:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="3"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Patent land - fee simple (private)"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="4:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="4"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Patent land- Company Freehold"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="5:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="5"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Provincial Park"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="6:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="6"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Indian Reserve"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="7:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="7"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Recreation Reserve"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="8:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="8"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Agreement Forest"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="9:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="9"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Federal Reserve"/>
</node>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="mandatory">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="AUTHORTY:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="AUTHORTY"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="length">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="3"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="type">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="string"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="domain">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="AUTHORTY"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="alias">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Ownership Designation"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="definition">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="The ownership designation attribute indicates the custodian responsible for providing the inventory information about the polygon.  (The party responsible for maintaining and providing the forest resources inventory information in accordance with the Forest Information Manual; that is, for determining the polygon description.) "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Description">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="As of 2006 the forest resource inventory is the responsibility of MNR and therefore the default value for the inventory is MNR.  "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Validation:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="english">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="python">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;SFL&apos;, &apos;MNR&apos;] ]   "/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="sql"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="values">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="SFL:">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="name">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="SFL"/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="desctext">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT=""/>
</node>
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="option">
<node CREATED="1562265931195" FOLDED="true" MODIFIED="1562265931195" TEXT="Licensee"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="MNR:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="MNR"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT=""/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Ministry of Natural Resources"/>
</node>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="mandatory">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="YRUPD:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="alias">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Year of Update"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="length">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="4"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="type">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Integer"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="YRUPD"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="definition">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="The year of update attribute contains a four-digit number representing the calendar year (January 1 to December 31) of the source data used to determine / update the polygon description, in particular the height attribute (A1.1.15). "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Description">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="The year of update identifies the calendar year that information about a polygon was last confirmed or modified based on field inspection, photo interpretation, analysis of satellite imagery, or conversion or update to spatial or tabular data. The year of update should not be changed to reflect error corrections to tabular attributes. "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Validation:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="english">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="The attribute population must follow the correct format"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="A zero value is not a valid code"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="python">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="  [ &apos;${name} must be &gt;= 1975 and &lt; %[year[&apos; for row in [row] if row[&apos;${name}&apos;]  &lt;1975 ]   "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="sql"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="values">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SFL:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SFL"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT=""/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Licensee"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="MNR:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="MNR"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT=""/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Ministry of Natural Resources"/>
</node>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="mandatory">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SOURCE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="length">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="8"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="type">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="String"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SOURCE"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="domain">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SOURCE"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="alias">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Source of Update Data "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="definition">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="The source of update data  attribute identifies the methodology by which the information stored in the other tabular attributes that are associated with the same polygon was determined (i.e., how the polygon description was determined). "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Description">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="The source of data update attribute does not apply to the changes or updates made to the ecosite attribute."/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FIXME: Is this value list correct? What is SPECTRAL? What is INFRARED?">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Validation:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="english">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: A blank or null value is not a valid code"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="A value of FORECAST is only valid for the SOURCE attribute in the Base Model Inventory"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="If the DEVSTAGE attribute starts with FTG, then the SOURCE attribute should not equal ESTIMATE"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="If the DEVSTAGE attribute starts with NEW, then the SOURCE attribute should be ESTIMATE. **** FIM Difference - Allow ESTIMATE in EPC product to allow new treatment estimates."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="python">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None    ]"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos;    ]"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="[ &apos;${name} must not be ESTIMATE when DEVSTAGE LIKE FTG% &apos; for row in [row] if row[&apos;${name}&apos;]  == &apos;ESTIMATE&apos; and row[&apos;DEVSTAGE&apos;] in [ &apos;FTGNAT&apos;,&apos;FTGPLANT&apos;,&apos;FTGSEED&apos; ] ]   "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="sql"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="mandatory">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="values">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="BASECOVR:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="BASECOVR"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Information that is provided by the MNR  (e.g., water or evaluated wetlands)."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="planimetric base layer"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="DIGITALA:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="DIGITALA"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Digital analysis, automated processing. (e.g., Ecognition)"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="multispectral scanning (digital image) - automated process"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="DIGITALP:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="DIGITALP"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Photo-interpreted by softcopy systems"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="multispectral scanning (digital image) - manual process"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ESTIMATE:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ESTIMATE"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="This source option is only for use in areas that have been recently renewed and have not been revisited since the renewal work was performed.  That is, where a follow-up survey has not yet been performed (e.g., regeneration survey, FTG survey).  Therefore, the description of the newly regenerated stand is a &apos;best estimate&apos; of the expected outcome / result of the renewal treatment that was applied to the area based on past silvicultural successes."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="expected / estimated outcome / result"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FOC:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FOC"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Inspection of a site after silvicultural treatment to determine whether an operator / operations conforms to the approved plan or permit.   The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="forest operations compliance inspection"/>
</node>
</node>
<node BACKGROUND_COLOR="#99ff99" CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FORECAST:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FORECAST"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="This indicates a polygon description that was updated based on expected outcomes of planned operations (which have not yet been implemented) for the remainder of the current plan term.  This code is only valid in the Base Model Inventory. "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="forecasted description"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FRICNVRT:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FRICNVRT"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Current polygon description based on data conversion from traditional FRI  "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="forest resources inventory conversion"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="INFRARED:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="INFRARED"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT=""/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Infrared satellite imagery"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="MARKING:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="MARKING"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription.  Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="pre-harvest site inspection / marking"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="OCULARA:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="OCULARA"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft. "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="aerial survey / reconnaissance"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="OCULARG:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="OCULARG"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements)."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="ocular estimate (ground)"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="OPC:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="OPC"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Measuring standing trees to determine the volume of wood on a given tract of land."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="operational cruise"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PHOTO:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PHOTO"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="air photo interpretation"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PHOTOLS:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PHOTOLS"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Photography at a scale larger than 1:10,000  (e.g., 1:500, 1:1000)."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="large scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PHOTOSS:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PHOTOSS"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Photography at a scale smaller than 1:20,000  (e.g., 1:100,000)."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="small scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PLOTFIXD:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PLOTFIXD"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT=""/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="fixed area plot"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PLOTVAR:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PLOTVAR"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT=""/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="variable area (radius) plot"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="RADAR:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="RADAR"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT=""/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="radar satellite imagery"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="REGENASS:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="REGENASS"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Survey of a regenerated area to determine how well the new stand is growing.  This includes seeding, survival, and stocking assessments."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="regeneration assessment"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SEMEXTEN:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SEMEXTEN"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="An appraisal of a forest stand&apos;s structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).   Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="extensive silvicultural effectiveness monitoring survey"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SEMINTEN:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SEMINTEN"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="An appraisal of a forest stand&apos;s structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved).  Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments. "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="intensive silvicultural effectiveness monitoring survey"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SPECTRAL:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SPECTRAL"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT=""/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="spectral satellite imagery"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SUPINFO:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="SUPINFO"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Stand update information from a source other than those listed/defined in the other coding options that is provided by either MNR or Licensee. For example, disturbance records. The date of information capture/acquisition and the age of the stand as of the date of information was acquired must be supplied for the information to be incorporated into the inventory."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Supplied information"/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FORMOD:">
<icon BUILTIN="full-1"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="length">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="2"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="type">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="String"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FORMOD"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="domain">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="FORMOD"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="values">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="RP:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="RP"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT=""/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Production Forest - Regular"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="MR:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="MR"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Productive forest areas which are considered to be production forest, but that are unavailable for timber production as determined through the forest management planning process. That is, these areas have been identified as no-cut areas through area of concern planning for the purpose of protecting values (e.g., reserve buffers applied to protect tourism values)."/>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Designated management reserves are areas that were managed as an operational reserve during previous forest management plan terms. That is, they are actual operational or management reserve areas created by the implementation of forest management operations around them. As guides are updated, these areas should be reviewed as some may return to production forest. "/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Production Forest - Designated Management Reserve"/>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PF:">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="name">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="PF"/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="desctext">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock."/>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="option">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="Protection Forest"/>
</node>
</node>
</node>
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="definition">
<node CREATED="1562265931196" FOLDED="true" MODIFIED="1562265931196" TEXT="The productive forest modifier attribute represents a further classification (sub-division) of productive forest areas based on the presence or absence of physical or biological factors which may influence the ability to practice timber management."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Description">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="The productive forest modifier must be identified for every productive forest stand and must be used in conjunction with the management consideration attribute.   There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement.  Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a timber management decision (FORMOD) that is based on more than just site class.  "/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="If the productive forest modifier is set to protection forest (FORMOD = PF), then the reason for the designation must be entered in the management consideration attribute (MGMTCON1 &lt;&gt; NONE).  "/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="There is a relationship between the site class (OSC, USC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement. Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest. The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (OSC, USC) versus a timber management decision (FORMOD) that is based on more than just site class.  Our validation has changed from FIM Tech Specs."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="mandatory">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Validation:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="english">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A value of MR is only valid for the FORMOD attribute in the Base Model Inventory">
<icon BUILTIN="full-1"/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="python">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos;  and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must follow coding&apos; for row in [row] if row[&apos;${name}&apos;] not in [  &apos;RP&apos;,&apos;PF&apos;,&apos;MR&apos;  ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
</node>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="DEVSTAGE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="length">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="8"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="type">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="String"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="DEVSTAGE"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="alias">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Stage of Development"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="domain">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="DEVSTAGE"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="values">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="LOWMGMT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="LOWMGMT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest stands which were previously harvested and have not reached the regeneration standards as described in an approved forest management plan within the estimated timeframe.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., stocking less than 25%).  "/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Below regeneration standards due to past management "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="LOWNAT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="LOWNAT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest stands which were previously disturbed by natural causes and have not reached the regeneration standards as described in an approved forest management plan within the estimated timeframe.  Further, these areas require additional silvicultural treatment to bring them up to regeneration standards.  This does not include areas that have been recently disturbed or recently renewed.  However, it may include areas which have received renewal treatments in the past that have failed to produce a regenerated forest to the applicable regeneration standards.  This option may also include those areas which have traditionally been designated as barren and scattered (i.e., stocking less than 25%).  "/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Below regeneration standards due to natural causes / succession "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="DEPHARV:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="DEPHARV"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest area that was recently disturbed by clearcut harvesting and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer that would be released and/or protected as part of the depletion operation."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="recently disturbed by harvest and has received no regeneration/renewal treatment "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="DEPNAT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="DEPNAT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest area that was recently disturbed by natural causes (i.e., fire, blowdown, ice damage, insect and disease) and has not received a silvicultural treatment such as natural regeneration, seeding or planting.  These areas do not have advanced regeneration, or a distinct or established regeneration layer."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="recently disturbed by natural causes and has received no regeneration / renewal treatment "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="NEWPLANT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="NEWPLANT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest areas which have been regenerated predominantly by planting, but have not been assessed as free-to-grow"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="recently renewed : mainly planted"/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="NEWSEED:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="NEWSEED"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest areas which have been regenerated predominantly by seeding, but have not been assessed as free-to-grow"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="recently renewed : mainly seeded "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="NEWNAT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="NEWNAT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest areas which have been regenerated predominantly by natural means, but have not been assessed as free-to-grow"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="recently renewed : mainly natural regeneration "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FTGPLANT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FTGPLANT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest areas which were regenerated predominantly from planted stock and which have been assessed as free-to-grow.  "/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="free-to-grow mainly planted"/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FTGSEED:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FTGSEED"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest areas which were regenerated predominantly by seeding and which have been assessed as free-to-grow.  "/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="free-to-grow mainly seeded"/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FTGNAT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FTGNAT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Productive forest areas which were regenerated predominantly by natural means and which have been assessed as free-to-grow. This classification will also be used to describe the forested areas that have never been treated to date (original forest) that would be considered as free growing.  "/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="free-to-grow mainly natural regeneration "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="THINPRE:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="THINPRE"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Free-growing productive forest areas which have received a mid-rotation thinning/spacing to promote the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume. "/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="received pre-commercial thinning/spacing treatment"/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="THINCOM:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="THINCOM"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Free-growing productive forest areas which have received a mid-rotation partial harvest designed to promote the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes. "/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="received commercial thinning/spacing treatment "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="STRIPCUT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="STRIPCUT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="The removal of a portion of the existing trees in a stand in progressive strips in more than one operation so that the non-depleted portion of the stand is left primarily to provide a natural seed source for regeneration of the depleted area. Several cutting patterns are available to achieve same goal."/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters (5 chains) wide. It is designed to encourage regeneration on difficult and/or fragile sites."/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Note: Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="modified cut: strip"/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FRSTPASS:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FRSTPASS"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A partial harvest where the first harvest operation removes target/specific merchantable tree species from a forest stand. The remaining species are merchantable and are intended to be harvested by another logger/contractor/forest resource licence holder in the next pass.  A first pass should be recorded if merchantable tree species remain in the forest stand which have been allocated for harvest - but have not yet been harvested."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="modified cut: first pass "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="SEEDTREE:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="SEEDTREE"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="An even-aged, silvicultural system that retains mature standing trees scattered throughout the cutblock to provide seed sources for natural regeneration."/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A method of harvesting and regenerating a forest stand in which all trees are removed from the area except for a small number of seed-bearing trees that are left singly or in small groups. The objective is to create an even-aged stand."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="modified cut: seed tree"/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="PREPCUT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="PREPCUT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source and to improve conditions for seed production and natural regeneration."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="received a preparatory cut "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="SEEDCUT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="SEEDCUT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="received a seed cut"/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FIRSTCUT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="FIRSTCUT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="received a first removal harvest "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="LASTCUT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="LASTCUT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A shelterwood silvicultural system stage of management where all of the remaining trees in the overstorey are removed. This is the removal of the seed or shelter trees after the regeneration has been effective."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="received a final removal harvest "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="IMPROVE:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="IMPROVE"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition, distribution and quality by removing less desirable trees of any species."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="received an improvement cut "/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="SELECT:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="SELECT"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="desctext">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment. "/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="option">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="received a selection harvest "/>
</node>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="definition">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="The stage of development attribute indicates the current state of growth and development for a productive forest stand.  Note that some states are best described based on the last major silvicultural treatment that was applied to a stand, if the stand is being managed for timber production."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Description">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="The stage of development will be used to populate the stage of management in the landbase tables in the forest management plan (i.e., FMP-3 and FMP-5).  The stage of development represents the current development state, and/or the current stage of silvicultural management of a productive forest stand. Productive forest polygons can be described by the following ranges of forest stand development or management: a) Recent Disturbances (harvest or natural disturbance, complete or partial)  b) Below Regeneration Standards  * Not Satisfactorily Regenerated * Renewed (artificial or natural) c) Forest Stands  * Free-growing (not disturbed)  * Free-growing (disturbed by silviculture system)  Forest stands identified by either IMPROVE, SELECT, SNGLTREE or GROUPSE - stage of development must include a complete description for the acceptable growing stock and unacceptable growing stock attributes. "/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Validation:">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="english">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="If the YRDEP attribute equals 0 (unknown), and DEPTYPE is not empty, then the DEVSTAGE attribute should only contain LOWNAT FTGNAT values when POLYTYPE is equal to FOR    *** FIM DIFFERENCE - added DEPTYPE clause for unknown deptypes"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="If the STKG attribute equals 0.00, then the DEVSTAGE attribute should be either DEPHARV or DEPNAT when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="If the DEVSTAGE attribute is either DEPHARV or DEPNAT, then the HT attribute should be 0.0 when POLYTYPE is equal to FOR.  This validation is taken care of in OHT"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="If the OSC attribute equals 4, then the DEVSTAGE attribute should not be DEPHARV when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="If the USC attribute equals 4, then the DEVSTAGE attribute should not be DEPHARV when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Must not have two canopy stand driven by the overstory in depletion or new state when older than 20. If DEVSTAGE is in [&apos;DEPHARV&apos; , &apos;DEPNAT&apos; , &apos;NEWNAT&apos; , &apos;NEWPLANT&apos; , &apos;NEWSEED&apos;] AND VERT = &apos;TO&apos; AND OAGE &gt;20."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="python">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos;  and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]"/>
<node COLOR="#338800" CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;])  for row in [row] if row[&apos;${name}&apos;] not in [ &apos;LOWMGMT&apos;,&apos;LOWNAT&apos;,&apos;DEPHARV&apos;,&apos;DEPNAT&apos;,&apos;NEWNAT&apos;,&apos;NEWPLANT&apos;,&apos;NEWSEED&apos;,&apos;FTGNAT&apos;,&apos;FTGPLANT&apos;,&apos;FTGSEED&apos;,&apos;THINCOM&apos;,&apos;THINPRE&apos;,&apos;STRIPCUT&apos;,&apos;FRSTPASS&apos;,&apos;SEEDTREE&apos;,&apos;PREPCUT&apos;,&apos;SEEDCUT&apos;,&apos;FIRSTCUT&apos;,&apos;LASTCUT&apos;,&apos;IMPROVE&apos;,&apos;SELECT&apos;  ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must be LOWNAT or FTGNAT when POLYTYPE is FOR and ( YRDEP = 0 and DEPTYPE is not null) &apos; for row in [row] if row[&apos;${name}&apos;] not in [ &apos;LOWNAT&apos;,&apos;FTGNAT&apos;  ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;YRDEP&apos;] == 0 and ( row[&apos;DEPTYPE&apos;] != &apos;&apos; and row[&apos;DEPTYPE&apos;] is not None )  ]   "/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must be DEPHARV or DEPNAT when POLYTYPE is FOR and STKG = 0.00&apos; for row in [row] if row[&apos;${name}&apos;] not in [ &apos;DEPHARV&apos;,&apos;DEPNAT&apos;  ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;STKG&apos;] == 0.0  ]   "/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must not be DEPHARV when POLYTYPE is FOR and OSC = 4&apos; for row in [row] if row[&apos;${name}&apos;] == &apos;DEPHARV&apos; and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;OSC&apos;] == 4  ]   "/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;${name} must not be DEPHARV when POLYTYPE is FOR and USC = 4&apos; for row in [row] if row[&apos;${name}&apos;] == &apos;DEPHARV&apos; and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;USC&apos;] == 4  ]   "/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="[ &apos;Must not have two canopy stand (NEW/DEP) where VERT is TO older than 20...${name} in [DEPHARV , DEPNAT , NEWNAT , NEWPLANT , NEWSEED] AND VERT = TO AND OAGE &gt;20&apos; for row in [row] if row[&apos;${name}&apos;]  in [&apos;DEPHARV&apos; , &apos;DEPNAT&apos; , &apos;NEWNAT&apos; , &apos;NEWPLANT&apos; , &apos;NEWSEED&apos;]  and row[&apos;VERT&apos;] == &apos;TO&apos;  and row[&apos;OAGE&apos;] &gt;20  ]"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="sql"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="mandatory">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="YRDEP:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="length">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="4"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="type">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Integer"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="name">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="YRDEP"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="alias">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Year of Last Depletion or Disturbance"/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="definition">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="The year of last disturbance attribute indicates a four digit number of the most recent (or latest) fiscal year (April 1 to March 31) that a productive forest area was disturbed, completely or partially, by harvest or by natural causes. This includes mid-rotation or stand improvement operations where merchantable timber is removed. This is actual and known disturbances and not calculated from year of origin."/>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="A special value (0) is used to indicate FTGNAT or LOWNAT where YRDEP is unknown. The validation rules have been updated in the EPC revision to indicate valid scenarios for this flag case, and are different from the FIM 2009 PCM Spec."/>
</node>
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="Description">
<node CREATED="1562265931197" FOLDED="true" MODIFIED="1562265931197" TEXT="The planning composite inventory must be updated with all disturbances (harvest and natural) which are identified in annual reports. The year of last disturbance for each productive forest stand must correspond to the year that the disturbance occurred, as recorded in the applicable annual reports.  Forest stands that are managed under the selection or shelterwood silvicultural systems must also have a year of last disturbance. The year of last disturbance provides an estimate of the stage of forest stand development within a cutting cycle for selection stands or within the current stage of a shelterwood system.  In the case of a partial harvest where certain tree species have been removed, such as a first pass where merchantable trees remain in a forest stand, the year that the first pass was carried out must be entered as the year of last disturbance. If an additional harvest or pass is conducted, the year of last disturbance is changed to the year in which the most recent harvest/pass operation was carried out.  Commercial thinning is a mid-rotation treatment that is recorded as a disturbance and the stand area is considered as disturbed area in forest management planning. Forest stands that have received a commercial thinning must show the applicable year of last disturbance in the forest polygon coverage.  The year of last disturbance is also used to identify other stand improvement operations. A stand improvement operation is normally associated with the selection system.   Year of last disturbance must not be used to indicate tending operations, such as a chemical or manual release which are required to bring a forest stand to free-to-grow standards. "/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="The year of depletion attribute (YRDEP) and the associated depletion type attribute (DEPTYPE) are only completed during the photo interpretation process when the stage of development attribute is set to one of the &apos;newly depleted&apos; options (i.e., DEVSTAGE = DEPHARV or DEPNAT).  The fields are also generally completed for newly regenerated stages of development (i.e., DEVSTAGE = NEWNAT, NEWPLANT, or NEWSEED).  For other stages of development, the fields may be completed if the interpreter has access to additional information pertaining to past disturbances."/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Validation:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="english">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="The value must be greater than or equal to 1900 when POLYTYPE is equal to FOR, unless the special flag value 0 is used, as below."/>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="If YRDEP attribute equals 0, then DEVSTAGE attribute should only contain LOWNAT or FTGNAT values when POLYTYPE is equal to FOR.  Tested by DEVSTAGE attribute"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="The YRDEP attribute value should not be less than the YRORG attribute value when POLYTYPE is equal to FOR, unless the flag value 0 is used.***** FIM DIFFERENCE - removed this rule. Counterexamples abound..."/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="python">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="[ &apos;${name} must be &gt;= 1900 unless it is 0&apos; for row in [row] if row[&apos;${name}&apos;]  &lt;1900 and row[&apos;${name}&apos;] != 0 ]   "/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="sql"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="mandatory">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="SPCOMP:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="alias">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Species Composition"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="length">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="120"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="type">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="String"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="SPCOMP"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Definition">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=" This species composition attribute indicates the tree species that are present in the stand canopy and the relative proportion of the canopy that each species occupies."/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Description">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=" "/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Validation:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="english">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The population of this attribute is mandatory "/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) "/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=" No duplicate species codes allowed in the string"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Proportion values in the string must sum to 100 "/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="The tree species in the composition are to be coded using the scheme listed here.   "/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="*each species code is 3 characters (including blanks) and is left justified"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="*each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified. Note that initial inventory values determined by photo interpretation will not go below 10% and are rounded to the nearest 10%  (e.g., 10, 20, 30). During inventory update, proportions based on subsequent field survey sampling that are of a finer resolution can be entered (e.g., go below 10% or be a value in between the initial rounded proportions such as 15). "/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="*maximum of 10 species and proportions pairs in the string"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Plot Species are [     &apos;AL&apos;,&apos;Aq&apos;,&apos;Ap&apos;,&apos;Ag&apos;,&apos;Bc&apos;,&apos;Bp&apos;,&apos;Gb&apos;,&apos;Bb&apos;,&apos;Cat&apos;,&apos;Cc&apos;,&apos;Cm&apos;,     &apos;Cp&apos;,&apos;Cs&apos;,&apos;Ct&apos;,&apos;Er&apos;,&apos;Eu&apos;,&apos;Hk&apos;,&apos;Ht&apos;,&apos;Hl&apos;,&apos;Hb&apos;,&apos;Hm&apos;,&apos;Hp&apos;,     &apos;Hs&apos;,&apos;Hc&apos;,&apos;Kk&apos;,&apos;Le&apos;,&apos;Lj&apos;,&apos;Bl&apos;,&apos;Ll&apos;,&apos;Lb&apos;,&apos;Gt&apos;,&apos;Mb&apos;,&apos;Mf&apos;,     &apos;Mm&apos;,&apos;Mt&apos;,&apos;Mn&apos;,&apos;Mp&apos;,&apos;AM&apos;,&apos;Ema&apos;,&apos;Mo&apos;,&apos;Obl&apos;,&apos;Ob&apos;,&apos;Och&apos;,&apos;Op&apos;,     &apos;Os&apos;,&apos;Osw&apos;,&apos;Pa&apos;,&apos;Pn&apos;,&apos;Pp&apos;,&apos;Pc&apos;,&apos;Ph&apos;,&apos;Pe&apos;,&apos;Red&apos;,&apos;Ss&apos;,&apos;Sc&apos;,     &apos;Sk&apos;,&apos;Sn&apos;,&apos;Sr&apos;,&apos;Sy&apos;,&apos;Tp&apos;,&apos;Haz&apos;     ]"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="python">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None ]"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos; ]"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="[ i for i in [ test_spcomp(row[&apos;${name}&apos;]) for r in [row] ] if i is not None ]"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="[&quot;${name} should not be %s: %s&quot;%(i, r[&apos;${name}&apos;] )  for i in [  test_spcomp(row[&apos;${name}&apos;])   for r in [row]    if row[&apos;${name}&apos;] is not None  ]  if i is not None ]"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="[ &apos;%s is not a valid species code&apos;%(s) for s in test_SpeciesInList(row[&apos;${name}&apos;],    [ &apos;AL&apos;,&apos;Aq&apos;,&apos;Ap&apos;,&apos;Ag&apos;,&apos;Bc&apos;,&apos;Bp&apos;,&apos;Gb&apos;,&apos;Bb&apos;,&apos;Cat&apos;,&apos;Cc&apos;,&apos;Cm&apos;, &apos;Cp&apos;,&apos;Cs&apos;,&apos;Ct&apos;,&apos;Er&apos;,&apos;Eu&apos;,&apos;Hk&apos;,&apos;Ht&apos;,&apos;Hl&apos;,&apos;Hb&apos;,&apos;Hm&apos;,&apos;Hp&apos;, &apos;Hs&apos;,&apos;Hc&apos;,&apos;Kk&apos;,&apos;Le&apos;,&apos;Lj&apos;,&apos;Bl&apos;,&apos;Ll&apos;,&apos;Lb&apos;,&apos;Gt&apos;,&apos;Mb&apos;,&apos;Mf&apos;,&apos;Mm&apos;,&apos;Mt&apos;,&apos;Mn&apos;,&apos;Mp&apos;,&apos;AM&apos;,&apos;Ema&apos;,&apos;Mo&apos;,&apos;Obl&apos;,&apos;Ob&apos;,&apos;Och&apos;,&apos;Op&apos;, &apos;Os&apos;,&apos;Osw&apos;,&apos;Pa&apos;,&apos;Pn&apos;,&apos;Pp&apos;,&apos;Pc&apos;,&apos;Ph&apos;,&apos;Pe&apos;,&apos;Red&apos;,&apos;Ss&apos;,&apos;Sc&apos;,&apos;Sk&apos;,&apos;Sn&apos;,&apos;Sr&apos;,&apos;Sy&apos;,&apos;Tp&apos;,&apos;Haz&apos; ,                               &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,&apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,&apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;, &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos;    ]     ) if row[&apos;${name}&apos;] is not None ]"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="sql"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="DomainValidation:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="english">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=" "/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="mandatory">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="The attribute population must follow the correct format"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="WG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="alias">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Working Group"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="length">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="2"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="type">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="String"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="WG"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="domain">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="WG"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="values">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="BF:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="BF"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="balsam fir"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="CE:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="CE"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="cedar"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="HE:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="HE"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="hemlock"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="LA:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="LA"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="larch"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PJ:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PJ"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="jack pine"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PR:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PR"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="red pine"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PS:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PS"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="scots pine"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PW:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PW"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="white pine"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="SB:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="SB"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="black spruce"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="SW:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="SW"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="white spruce"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="SX:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="SX"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="spruce mix"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="OC:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="OC"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="other conifer"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="AX:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="AX"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ash mix"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="BG:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="BG"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="gray birch"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="BW:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="BW"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="white birch"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="BY:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="BY"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="yellow birch"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="MH:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="MH"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="hard / sugar maple"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="MR:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="MR"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="soft / red maple"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="MX:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="MX"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="maple mix"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="OX:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="OX"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="oak mix"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PO:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PO"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Poplar"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PB:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="PB"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="balsam poplar"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="OH:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="OH"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="desctext">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="option">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="other hardwoods"/>
</node>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="definition">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="The working group attribute indicates a grouping of productive forest stands for timber management purposes. Stands which are grouped together have the same predominant species composition and are being managed under the same silvicultural system. The grouping of stands is generally labelled based on the species which dictates the management regime to be applied. Therefore, the working group normally indicates the dominant species in a forest stand based on the tree species that occupies the greatest canopy closure or the greatest amount of basal area in the stand as indicated by the species composition attribute. "/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Description">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=""/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Validation:">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="english">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="python">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="[ &apos;${name} must follow coding&apos; for row in [row] if row[&apos;${name}&apos;] not in [  &apos;BF&apos;,&apos;CE&apos;,&apos;HE&apos;, &apos;LA&apos;, &apos;PJ&apos;, &apos;PR&apos;, &apos;PS&apos;, &apos;PW&apos;, &apos;SB&apos;, &apos;SW&apos;, &apos;SX&apos;, &apos;OC&apos;, &apos;AX&apos;, &apos;BG&apos;, &apos;BW&apos;, &apos;BY&apos;, &apos;MH&apos;, &apos;MR&apos;, &apos;MX&apos;, &apos;OX&apos;, &apos;PO&apos;, &apos;PB&apos;, &apos;OH&apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]    "/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="YRORG:">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="alias">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Year of Origin"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="length">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="2"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="type">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Integer"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="name">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="YRORG"/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Description">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="The year of origin attribute contains a four digit number representing the average year that the predominant species (i.e., the species within the stand having the greatest relative abundance in terms of basal area), came into existence. "/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Description">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT=" The year of origin is used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or  Appendix 1 Tabular Attribute Descriptions artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration. Once a forest stand has been assessed as free-to-grow, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site. In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered. For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach free-growing conditions), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip has achieved free-to-grow status, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required. Normally, updates to the year of origin have a corresponding change to the year of update attribute as this indicates the currency/vintage of the information and when the year of origin value was determined. Year of origin information is not required for non-forested and non-productive forest land types."/>
</node>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="Validation:">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="english">
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931198" FOLDED="true" MODIFIED="1562265931198" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="A zero value is not a valid code when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="The YRORG attribute value must be greater than or equal to 1600 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="The YRORG attribute must be less than the plan period start year when POLYTYPE is equal to FOR">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="The YRDEP attribute value should not be less than the YRORG attribute value (addressed in YRDEP)"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="The YRORG attribute value should not be greater than YRUPD attribute value"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="In the Base Model Inventory, the YRORG attribute value should not be greater than the YRUPD attribute value + 5 (Addressed in BMI/eBMI)"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="python">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must not be = 0&apos; for row in [row] if row[&apos;${name}&apos;]  == 0 and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must be &gt;= 1600 &apos; for row in [row] if row[&apos;${name}&apos;]  &lt;1600 and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must be &lt;= YRSOURCE&apos; for row in [row] if row[&apos;${name}&apos;]  &gt; row[&apos;YRSOURCE&apos;]  and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]   ">
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must be &lt;= YRUPD&apos; for row in [row] if row[&apos;${name}&apos;]  &gt; row[&apos;YRUPD&apos;]  and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]   "/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="HT:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="alias">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Height"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="length">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="4"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="type">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Float"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="HT"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="definition">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="The height attribute indicates the estimated average tree height (in meters) of the predominant species as inventoried in the year of update. Estimates can be made from interpreted crown closure or field samples, or from growth algorithms. "/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Validation:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="english">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="If the DEVSTAGE attribute does not start with DEP, NEW or LOW then the HT attribute must be greater than zero when POLYTYPE is equal to FOR.">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="The value of HT should not be greater than 0.5 when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=" If the DEVSTAGE attribute starts with FTG, then the HT attribute should be greater than or equal to 0.8 when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="python">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos; and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must not be 0 if DEVSTAGE is not DEPHARV or DEPNAT or NEWNAT or NEWPLANT or NEWSEED, LOWNAT, LOWMGMT when POLYTYPE is FOR. Consider updating DEVSTAGE?&apos; for row in [row] if row[&apos;DEVSTAGE&apos;]  not in [ &apos;DEPHARV&apos;,&apos;DEPNAT&apos;,&apos;NEWNAT&apos;,&apos;NEWPLANT&apos;,&apos;NEWSEED&apos;,&apos;LOWNAT&apos;,&apos; LOWMGMT&apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;${name}&apos;] == 0.0 ]   ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;WARNING: ${name} / (${year} - YRORG) should not be &gt; 0.5 when POLYTYPE is FOR&apos; for row in [row] if (row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; and row[&apos;YRORG&apos;] &gt; 0) if row[&apos;${name}&apos;] / (${year} - row[&apos;YRORG&apos;]) &gt; 0.5  ]   ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must be &gt;= 0.8 when DEVSTAGE like FTG% and POLYTYPE is FOR&apos; for row in [row] if row[&apos;${name}&apos;] &lt; 0.8 and row[&apos;DEVSTAGE&apos;] in [ &apos;FTGNAT&apos;,&apos;FTGPLANT&apos;,&apos;FTGSEED&apos;] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 40) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 40) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   ">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="sql"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="DomainValidation:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="english">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Height / Age relationships should be in the range of Plonski and others."/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="mandatory">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="STKG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="type">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="double"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="STKG"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="alias">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Stocking"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="definition">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="The stocking attribute indicates a qualitative measure of the density of tree cover in a forest stand.  It is expressed as a percentage value ranging from zero, for recently disturbed stands, to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field.  Stocking of a forest stand refers to all species that make up the stand&apos;s canopy, but it is generally based on the species with the most basal area. "/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="description">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Stocking is determined by comparing the actual basal area measured from field sampling to the relative basal area of a fully-stocked stand using Plonski&apos;s Normal Yield Tables. (Plonski&apos;s Normal Yield Tables were developed from permanent sample plots established for several of the major tree species in Ontario.) Stocking can also be determined from aerial photography based on the degree of canopy closure, average age, height, and species composition. Actual basal area collected from field sampling is used to calibrate stocking assessments made from photo-interpretation.  The silvicultural ground rules in a forest management plan describe the standards for assessing the regeneration of forest stands, based on forest unit, desired species composition, age, height, and stocking. If the stocking of a productive forest stand does not meet the regeneration standards in the silvicultural ground rules of an approved forest management plan, the forest stand will be considered as NSR and must be classified into the appropriate below regeneration standards stage of development.  In some cases, the regeneration and/or management standards of a silvicultural ground rule may be expressed as a density, which usually describes the frequency or number of stems per hectare. Where density information (stems/hectare) has been collected or determined from a regeneration survey, this information must be converted to a stocking value for the purpose of updating the forest stand description information. "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="This definition needs to be brought up to date using the current eFRI specification. There may (or may not) be a difference between traditional FRI and eFRI stocking calculations."/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Validation:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="english">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="* Valid numeric values are from 0 through 4.00"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="If the DEVSTAGE attribute starts with FTG, then the STKG attribute should be greater than or equal to 0.40   when POLYTYPE is equal to FOR."/>
<node COLOR="#999900" CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should equal 0.00   when POLYTYPE is equal to FOR and VERT is not SV (Veterans contribute to stocking) "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should not exceed 0.1   when POLYTYPE is equal to FOR and VERT is SV (Veterans contribute to stocking) "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="If DEVSTAGE attribute starts with DEP, then STKG attribute should not exceed 0.3 when POLYTYPE is equal to FOR and VERT is TU or MU "/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="python">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 4.0 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 4.0) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  ]   "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;WARNING: ${name} should be &gt; 0.4 when DEVSTAGE is FTG* when POLYTYPE is FOR.&apos; for row in [row] if row[&apos;DEVSTAGE&apos;]  in [ &apos;FTGNAT&apos;,&apos;FTGPLANT&apos;,&apos;FTGSEED&apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;${name}&apos;] &lt; 0.4 ]   "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must = 0.0 when DEVSTAGE is DEP* when POLYTYPE is FOR in single-canopy stands&apos; for row in [row] if row[&apos;DEVSTAGE&apos;]  in [ &apos;DEPNAT&apos;,&apos;DEPHARV&apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;${name}&apos;] &gt; 0 and row[&apos;VERT&apos;] not in [ &apos;SV&apos;,&apos;TU&apos;,&apos;MU&apos;]  ]   "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;WARNING: ${name} should not exceed 0.1 when DEVSTAGE is DEP* when POLYTYPE is FOR and VERT is SV.&apos; for row in [row] if row[&apos;DEVSTAGE&apos;]  in [ &apos;DEPNAT&apos;,&apos;DEPHARV&apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;${name}&apos;] &gt; 0.1 and row[&apos;VERT&apos;] == &apos;SV&apos; ]   "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;WARNING: ${name} should not exceed 0.3 when DEVSTAGE is DEP* when POLYTYPE is FOR and VERT is TU or MU.&apos; for row in [row] if row[&apos;DEVSTAGE&apos;]  in [ &apos;DEPNAT&apos;,&apos;DEPHARV&apos; ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;${name}&apos;] &gt; 0.3 and row[&apos;VERT&apos;] in [&apos;TU&apos;,&apos;MU&apos;] ]   "/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="sql"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="mandatory">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="SC:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="alias">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Site Class"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="length">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="2"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="type">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Integer"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="SC"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="domain">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="SC"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="definition">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="The site class attribute indicates a site quality estimate for a stand.  Site class is an indicator of site productivity and is determined using the average height, age, and working group, based on the dominant tree species in a forest stand. These attributes are compared against height and age growth curves in Plonski&apos;s Normal Yield Tables for different species to determine the relative growth rate for a forest stand.  "/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="description">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="There is a relationship between the site class (SC) and the productive forest modifier (FORMOD) values assigned to a forested area. Generally, areas assigned a site class value of 4 (protection forest) are also assigned a productive forest modifier value of PF (protection forest), but it is not a requirement. Areas assigned a site class value other than 4 (e.g., 3) can also be assigned the productive forest modifier value of PF. Conversely areas assigned a site class value of 4 may be assigned a productive forest modifier value of production forest (RP) instead of protection forest.  The apparent discrepancy in the protection forest assignment between the site class attribute and the productive forest modifier attribute reflects the difference between a calculated assessment of site conditions (SC) versus a timber management decision (FORMOD) that is based on more than just site class. "/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Validation:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="english">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="If the FORMOD attribute equals PF, then the SC attribute should equal 4  when POLYTYPE is equal to FOR and the VERT is not TU or MU"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="PROPOSED BY LARRY WATKINS...  (previously foresters would change areas unavailable to SC4 so they would be considered PFR rather than making it unavailable)  applies to USC as well.  Need to check when PCM come in that changes to SC as presented in the eFRI have been altered.  Larry says this was common in the past.  LARRYS COMMENT on Site Class:  For some reason the old site class 4 sort from FORTRAN keeps creeping into the FRI spec inappropriately. SC4 should me SC4 - the productivity of the site. Don&apos;t call site class 1 trees on an island or on a rocky site 4 to match a  1970s coding issue, but rather flag the other appropriate fields.  ">
<icon BUILTIN="info"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="python">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must follow coding&apos; for row in [row] if row[&apos;${name}&apos;] not in [ 0,1,2,3,4 ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="[ &apos;${name} must be 4 when FORMOD is PF&apos; for row in [row] if row[&apos;${name}&apos;] not in [ 4 ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; and row[&apos;FORMOD&apos;] == &apos;PF&apos; and row[&apos;VERT&apos;] not in [&apos;TU&apos;,&apos;MU&apos;] ]   "/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="sql"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="values">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="0:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="0"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Best"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="1:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="1"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Better"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="2:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="2"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Good"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="3:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="3"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Poor"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="4:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="4"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock."/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Protection Forest"/>
</node>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="mandatory">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ECOSRC:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="alias">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="Ecosite Source of Update"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="length">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="8"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="type">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="String"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ECOSRC"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="domain">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ECOSRC"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="values">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ALGO:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ALGO"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="algorithm / model"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="DIGITALA:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="DIGITALA"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="multispectral scanning (digital image) - automated process Digital analysis, automated processing. (e.g., Ecognition)"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="DIGITALP:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="DIGITALP"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="multispectral scanning (digital image) - manual process photo-interpreted by softcopy systems"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="FOC:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="FOC"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="forest operations compliance inspection"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="FRICNVRT:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="FRICNVRT"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="forest resources inventory conversion"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="MARKING:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="MARKING"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="pre-harvest site inspection / marking"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="OCULARA:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="OCULARA"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="aerial survey/reconnaissance"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="OCULARG:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="OCULARG"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="ocular estimate (ground)"/>
</node>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="OPC:">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="name">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="OPC"/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="desctext">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT=""/>
</node>
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="option">
<node CREATED="1562265931199" FOLDED="true" MODIFIED="1562265931199" TEXT="operational cruise"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTO:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTO"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="air photo interpretation"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOLS:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOLS"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="large scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOPLS:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOPLS"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="air photo interpretation"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLUS:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLUS"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="soils data"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOSS:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOSS"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="small scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLOTFIXD:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLOTFIXD"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="fixed area plot"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLOTVAR:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLOTVAR"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="variable area (radius) plot"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="REGENASS:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="REGENASS"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="regeneration assessment"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="SEMEXTEN:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="SEMEXTEN"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="extensive silvicultural effectiveness monitoring survey"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="SEMINTEN:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="SEMINTEN"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="intensive silvicultural effectiveness monitoring survey"/>
</node>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="definition">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="The ecosite source of update attribute identifies the methodology by which the ecosite information associated with the polygon was determined (i.e., how the polygon&apos;s ecosite description was determined). "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Description">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Note that this attribute is similar to the SOURCE attribute which indicates the methodology by which the overall stand description was determined. The main difference between the SOURCE attribute and this ECOSRC attribute is that there are two additional methodologies that are commonly used when determining ecosite information. These additional methodologies are: determination of ecosite by algorithm and determination by supplementing air photo interpretation with soils data. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Validation:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="english">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="python">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;   ]"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;ALGO&apos;, &apos;DIGITALA&apos;, &apos;DIGITALP&apos;, &apos;FOC&apos;, &apos;FRICNVRT&apos;, &apos;MARKING&apos;, &apos;OCULARA&apos;, &apos;OCULARG&apos;, &apos;OPC&apos;, &apos;PHOTO&apos;, &apos;PHOTOLS&apos;, &apos;PHOTOPLS&apos;, &apos;PHOTOSS&apos;, &apos;PLOTFIXD&apos;, &apos;PLOTVAR&apos;, &apos;REGENASS&apos;, &apos;SEMEXTEN&apos;, &apos;SEMINTEN&apos;] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos; and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;   ]"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="sql"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="values">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ALGO:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ALGO"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=" "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="algorithm / model "/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="DIGITALA:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="DIGITALA"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Digital analysis, automated processing. (e.g., Ecognition)"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="multispectral scanning (digital image) - automated process"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="DIGITALP:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="DIGITALP"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="photo-interpreted by softcopy systems"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="multispectral scanning (digital image) - manual process "/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="FOC:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="FOC"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Inspection of a site after silvicultural treatment was applied to determine whether an operator / operations conforms to the approved plan or permit. The evaluation of any harvest, renewal, maintenance, or access forest management activity (e.g., post-harvest site inspection) can be included here. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="forest operations compliance inspection"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="FRICNVRT:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="FRICNVRT"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="The current polygon description is based on a data conversion from traditional FRI. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="forest resources inventory conversion"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="MARKING:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="MARKING"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Assessment of the trees in a stand for purpose of establishing a silvicultural or operational prescription. Selecting and marking the trees to be harvested and/or the trees to be left to grow; to sustain and enhance the stand for timber management, wildlife habitat management, aesthetics, recreation, biodiversity and other environmental and heritage concerns. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="pre-harvest site inspection / marking"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="OCULARA:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="OCULARA"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Visual assessment of a stand from a helicopter or fixed wing aircraft. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="aerial survey/reconnaissance"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="OCULARG:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="OCULARG"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Visual assessment of a stand using extensive ground survey methodologies (i.e., no detailed measurements). "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ocular estimate (ground)"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="OPC:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="OPC"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Measuring standing trees to determine the volume of wood on a given tract of land. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="operational cruise"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTO:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTO"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Photography at a conventional scale of 1:10,000, 1:15,840, or 1:20,000 "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="air photo interpretation "/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOLS:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOLS"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Photography at a scale larger than 1:10,000 (e.g., 1:500, 1:1000). "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="large scale aerial photography"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOPLS:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOPLS"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="air photo interpretation PLUS soils data"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOSS:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PHOTOSS"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Photography at a scale smaller than 1:20,000 (e.g., 1:100,000). "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="small scale aerial photography "/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLOTFIXD:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLOTFIXD"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="fixed area plot "/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLOTVAR:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="PLOTVAR"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT=""/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="variable area (radius) plot"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="REGENASS:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="REGENASS"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Survey of a regenerated area to determine how well the new stand is growing. This includes seeding, survival, and stocking assessments. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="regeneration assessment "/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="SEMEXTEN:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="SEMEXTEN"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="An appraisal of a forest stand&apos;s structure and composition using generalized survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved). Extensive survey methods are generally used where there are obvious successes or failure, or to identify problem areas requiring more intensive assessment. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="extensive silvicultural effectiveness monitoring survey "/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="SEMINTEN:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="SEMINTEN"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="desctext">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="An appraisal of a forest stand&apos;s structure and composition using rigorous survey sampling methodologies to determine if regeneration or management objectives have been met (i.e., determine if the expected results were achieved). Intensive survey methods are intended for stands where the status of regeneration is uncertain or specific quantitative data is required to determine the silvicultural effectiveness for operational treatments. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="option">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="intensive silvicultural effectiveness monitoring survey "/>
</node>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="mandatory">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ECOSITE1:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="alias">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Ecosite 1"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="length">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="10"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="type">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="String"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ECOSITE1"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="definition">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="The ecosite attribute identifies an ecological landscape type that is present in the polygon and the regional ecological land classification system being used to define the ecosite code. Soil depth modifier information may also be included, if relevant. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Description">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Refer to the appropriate ecosystem field guide for a list of valid ecosite codes and their definitions. Ecosite is also referred to as site type. "/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="When recording the ecosite information in this attribute, the regional land classification identifier code value (from table below) replaces the first 2 characters of the ecosite code (either &quot;ES&quot; or &quot;ST&quot;). For example, if the northwest regional ecosite code of ES11 was determined to be appropriate for the stand, it would be recorded as NW11."/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Additionally, some ecosites, especially in the northwest region, can have soil depth modifier information associated with them. To record soil depth modifier information, add one of the codes from the table below to the end of the ecosite code. For example, northwest ecosite code of ES13 with a soil depth modifier of &quot;shallow&quot; would be recorded as:"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Ecosite information which is collected or created in accordance with an ecosystem classification (e.g., wetlands ecosystem classification) may also be entered for non-productive forest lands, if desired, but it is not required. Determination of ecosite is a prerequisite of scheduling a forest stand for silvicultural treatment and is required for verifying the selected forest operations prescription in accordance with the silvicultural ground rules of an approved forest management plan. Ecosite must be identified for all free-growing, (free-to-grow) forest stands based on the pre-harvest condition, and must reflect the characteristics of the site in terms of vegetation, soils, drainage, and tree species in accordance with a forest ecosystem classification. Ecosite should not be determined after a forest stand has been subjected to forest management operations. "/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Validation:">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="english">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ECOSITE1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="The population of this attribute is only mandatory for ECOSITE1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="A blank or null value is not valid for ECOSITE1"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="python">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;   ]"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;][0:2] not in (&apos;CE&apos;, &apos;NE&apos;, &apos;NW&apos;) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos; and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;   ]"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="sql"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="mandatory">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ECOSITE2:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="alias">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="Ecosite 2"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="length">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="10"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="type">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="String"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="name">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="ECOSITE2"/>
</node>
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="definition">
<node CREATED="1562265931200" FOLDED="true" MODIFIED="1562265931200" TEXT="The ecosite attribute identifies an ecological landscape type that is present in the polygon and the regional ecological land classification system being used to define the ecosite code. Soil depth modifier information may also be included, if relevant. "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Description">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Refer to the appropriate ecosystem field guide for a list of valid ecosite codes and their definitions. Ecosite is also referred to as site type. "/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="When recording the ecosite information in this attribute, the regional land classification identifier code value (from table below) replaces the first 2 characters of the ecosite code (either &quot;ES&quot; or &quot;ST&quot;). For example, if the northwest regional ecosite code of ES11 was determined to be appropriate for the stand, it would be recorded as NW11."/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Additionally, some ecosites, especially in the northwest region, can have soil depth modifier information associated with them. To record soil depth modifier information, add one of the codes from the table below to the end of the ecosite code. For example, northwest ecosite code of ES13 with a soil depth modifier of &quot;shallow&quot; would be recorded as:"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Ecosite information which is collected or created in accordance with an ecosystem classification (e.g., wetlands ecosystem classification) may also be entered for non-productive forest lands, if desired, but it is not required. Determination of ecosite is a prerequisite of scheduling a forest stand for silvicultural treatment and is required for verifying the selected forest operations prescription in accordance with the silvicultural ground rules of an approved forest management plan. Ecosite must be identified for all free-growing, (free-to-grow) forest stands based on the pre-harvest condition, and must reflect the characteristics of the site in terms of vegetation, soils, drainage, and tree species in accordance with a forest ecosystem classification. Ecosite should not be determined after a forest stand has been subjected to forest management operations. "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Validation:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="english">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Blank or null values ARE valid"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="python"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="sql"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="mandatory">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ECOPCT1:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="alias">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Ecosite 1 Percentage"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="length">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="2"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="type">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Integer"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ECOPCT1"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="definition">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="The ecosite percentage attribute identifies the percentage of an ecological landscape type that is present in the polygon. "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Validation:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="english">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ECOPCT1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="The population of this attribute is only mandatory for ECOPCT1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="A blank or null value is not valid for ECOSITE1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="A zero value is not a valid code for ECOPCT1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Adding all of ECOPCT1 attributes should equal 100 when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="python">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;   ]"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos; and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;   ]"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;${name} must be &gt;= 0 and POLYTYPE is FOR&apos; for row in [row] if  row[&apos;${name}&apos;] &lt; 0 and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ECOPCT2:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="alias">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Ecosite 2 Percentage"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="length">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="2"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="type">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Integer"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ECOPCT2"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="definition">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="The ecosite percentage attribute identifies the percentage of an ecological landscape type that is present in the polygon. "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Validation:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="english">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ECOPCT2 is NOT mandatory"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="python"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ACCESS1:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="alias">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Accessibility Indicator"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="length">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="3"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="type">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="String"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ACCESS1"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="domain">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="FMP_ACCESS"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="definition">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="The accessibility indicator attributes specifies whether or not there are any restrictions to accessing a productive forest stand. These restrictions may be legal (i.e., ownership), political / land use policy (i.e., land use designation, road closures), and/or a natural barrier. The focus of this field is identification of Crown productive forest stands that are inaccessible and therefore are not considered as part of the managed landbase for forest management planning purposes, but the principle may be applied to any area."/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT=" NOTE:   These attributes are not completed by the photo interpreters during inventory production. Access restriction information is to be determined by the forest management planning team. "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Validation:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="english">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Note: The ACCESS2 attribute may not exist in the submitted coverage as this attribute is not mandatory.  If this attribute exists, it may not be populated.  Therefore, this validation will be performed on the ACCESS2 attribute for the application of the correct coding scheme.  "/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="*   If the code of GEO is entered, then a management consideration attribute (MGMTCON) must be completed with the appropriate associated explanation/details, such as island or natural barrier. Refer to the MGMTCON attribute description."/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1 when POLYTYPE is equal to FOR "/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT=" ST1: The population of this attribute is mandatory for ACCESS1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: A blank or null value is not a valid code for ACCESS1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT=" ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR and is not blank or null"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="If ACCESS1 or ACCESS2 do not equal NON then AVAIL should be U when POLYTYPE is equal to FOR. Tested by the AVAIL attribute"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="If ACCESS2 is not equal to NON then ACCESS1 must not be NON when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT=" ACCESS1 and ACCESS2 can&apos;t contain the same value "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="python">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;${name} must not be null when POLYTYPE is FOR&apos; for row in [row] if row[&apos;${name}&apos;] is None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;${name} must not be blank, when POLYTYPE is FOR&apos; for row in [row] if str(row[&apos;${name}&apos;]) in  [ &apos; &apos;, &apos;&apos;  ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;GEO&apos;,&apos;LUD&apos;,&apos;NON&apos;,&apos;OWN&apos;,&apos;PRC&apos;,&apos;STO&apos;, &apos;&apos;, &apos; &apos;, None ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;If ${name} is GEO, MGMTCON1 must not be blank&apos; for row in [row] if row[&apos;${name}&apos;] == &apos;GEO&apos; if str(row[&apos;MGMTCON1&apos;]).strip() == &apos;&apos;  or row[&apos;MGMTCON1&apos;] is None ]"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;${name} must != ACCESS2 &apos; for row in [row] if row[&apos;${name}&apos;] == row[&apos;ACCESS2&apos;] and str(row[&apos;${name}&apos;]).strip() != &apos;&apos;  and row[&apos;${name}&apos;] is not None ]"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;if ACCESS2 != NON then ${name} must not = NON&apos;  for row in [row]  if row[&apos;${name}&apos;] == &apos;NON&apos;       and (             row[&apos;ACCESS2&apos;] != &apos;NON&apos; and             str(row[&apos;ACCESS2&apos;]).strip() != &apos;&apos;              and row[&apos;ACCESS2&apos;] is not None             ) ]"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="sql"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="values">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="GEO:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="GEO"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Area is not accessible due to geographic reasons. "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="geography"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="LUD:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="LUD"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands). "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="land use designation"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="NON:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="NON"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="The area is accessible/reachable."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="no accessibility considerations"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="OWN:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="OWN"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ownership"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="PRC:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="PRC"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="road closure"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="STO:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="STO"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="subject to ownership"/>
</node>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="mandatory">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ACCESS2:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="alias">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Accessibility Indicator 2"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="length">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="3"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="type">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="String"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ACCESS2"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="domain">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="FMP_ACCESS"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Validation:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="english">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Note: The ACCESS2 attribute may not exist in the submitted coverage as this attribute is not mandatory.  If this attribute exists, it may not be populated.  Therefore, this validation will be performed on the ACCESS2 attribute for the application of the correct coding scheme.  "/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory for ACCESS1 when POLYTYPE is equal to FOR "/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT=" ST1: The population of this attribute is mandatory for ACCESS1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT=" ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="If ACCESS1 or ACCESS2 do not equal NON then AVAIL should be U when POLYTYPE is equal to FOR. Tested by the AVAIL attribute"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="python">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;GEO&apos;,&apos;LUD&apos;,&apos;NON&apos;,&apos;OWN&apos;,&apos;PRC&apos;,&apos;STO&apos;, &apos;&apos;, &apos; &apos;, None ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="sql"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="mandatory">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="values">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="GEO:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="GEO"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Area is not accessible due to geographic reasons. "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="geography"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="LUD:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="LUD"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="An area is not accessible for forest management purposes due to land use designation (e.g., a provincial or federal park, agreement forest, mining claim, native lands, federal lands). "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="land use designation"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="NON:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="NON"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="The area is accessible/reachable."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="no accessibility considerations"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="OWN:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="OWN"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="An area of Crown land that is unreachable because it is surrounded by lands owned by an other party/parties (e.g., an area of Crown productive forest land that is not accessible for forest management because it is surrounded by private land)."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ownership"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="PRC:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="PRC"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="An area that is no longer accessible due to the permanent closure of the only road leading into the area."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="road closure"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="STO:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="STO"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="An area of land that is owned by a party/parties other than the Crown (e.g., a parcel of private land) and where the access conditions are applied by the land owner. Note that ownership and access conditions can change over time."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="subject to ownership"/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="MGMTCON1:">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="length">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="4"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="type">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="String"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="MGMTCON1"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="domain">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="MGMTCON"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="alias">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="Management Consideration"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="definition">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. "/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="description">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="values">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="COLD:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="COLD"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="permafrost"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="DAMG:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="DAMG"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="physical/natural damage  "/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ISLD:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="ISLD"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="island"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="NATB:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="NATB"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="natural barrier"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="NONE:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="NONE"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="There are no physical or ecological &apos;restrictions&apos; in the site that need to be considered when determining management of the stand."/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="option">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="no management consideration"/>
</node>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="PENA:">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="name">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="PENA"/>
</node>
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="desctext">
<node CREATED="1562265931201" FOLDED="true" MODIFIED="1562265931201" TEXT="An area of land that is nearly surrounded by water and is connected to the mainland."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="peninsula"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="POOR:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="POOR"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="stagnated, poor tree growth - no indicator"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ROCK:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ROCK"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="exposed bedrock / rocky outcrops"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="SAND:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="SAND"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A site where forest regeneration will be difficult due to the potential for erosion."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="blow sand / exposed fine sand, shallow or no humus"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="SHRB:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="SHRB"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="heavy shrub / brush"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="SOIL:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="SOIL"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="shallow soils"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="STEP:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="STEP"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. "/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="steep slopes"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="WATR:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="WATR"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="telluric / highly fluctuating, moving ground water"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="WETT:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="WETT"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="poorly drained - high water table"/>
</node>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="Validation:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="english">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ST1: The population of this attribute is mandatory for MGMTCON1 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ST1: A blank or null value is not a valid code for MGMTCON1"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="If ACCESS1 or ACCESS2 is equal to GEO then MGMTCON1 must be NONE"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="IF the FORMOD attribute equals PF, then the MGMTCON1 attribute should not equal NONE when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="If the SC attribute equals 4, then the MGMTCON1 attribute should not equal NONE"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="If MGMTCON2 is not NONE then MGMTCON1 must not be NONE"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="If MGMTCON3 is not NONE then the MGMTCON2 and MGMTCON1 must not be NONE"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="python">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;COLD&apos;,&apos;DAMG&apos;,&apos;ISLD&apos;,&apos;NATB&apos;,&apos;NONE&apos;,&apos;PENA&apos;,&apos;POOR&apos;,&apos;&apos;,&apos;ROCK&apos;,&apos;SAND&apos;,&apos;SHRB&apos;,&apos;SOIL&apos;,&apos;STEP&apos;,&apos;WATR&apos;,&apos;WETT&apos;] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="sql"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="mandatory">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="MGMTCON2:">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="length">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="4"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="type">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="String"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="MGMTCON2"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="domain">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="MGMTCON"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="alias">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="Management Consideration"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="definition">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. "/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="description">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="values">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="COLD:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="COLD"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="permafrost"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="DAMG:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="DAMG"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="physical/natural damage  "/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ISLD:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="ISLD"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="island"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="NATB:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="NATB"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)."/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="option">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="natural barrier"/>
</node>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="NONE:">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="name">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="NONE"/>
</node>
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="desctext">
<node CREATED="1562265931202" FOLDED="true" MODIFIED="1562265931202" TEXT="There are no physical or ecological &apos;restrictions&apos; in the site that need to be considered when determining management of the stand."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="no management consideration"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="PENA:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="PENA"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="An area of land that is nearly surrounded by water and is connected to the mainland."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="peninsula"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="POOR:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="POOR"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="stagnated, poor tree growth - no indicator"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ROCK:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ROCK"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="exposed bedrock / rocky outcrops"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SAND:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SAND"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where forest regeneration will be difficult due to the potential for erosion."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="blow sand / exposed fine sand, shallow or no humus"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SHRB:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SHRB"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="heavy shrub / brush"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SOIL:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SOIL"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="shallow soils"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="STEP:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="STEP"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. "/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="steep slopes"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="WATR:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="WATR"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="telluric / highly fluctuating, moving ground water"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="WETT:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="WETT"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="poorly drained - high water table"/>
</node>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT=" The attribute population must follow the correct format, or empty when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;COLD&apos;,&apos;DAMG&apos;,&apos;ISLD&apos;,&apos;NATB&apos;,&apos;NONE&apos;,&apos;PENA&apos;,&apos;POOR&apos;,&apos;&apos;,&apos;ROCK&apos;,&apos;SAND&apos;,&apos;SHRB&apos;,&apos;SOIL&apos;,&apos;STEP&apos;,&apos;WATR&apos;,&apos;WETT&apos;,&apos;&apos;,None] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="mandatory">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="MGMTCON3:">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="String"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="MGMTCON3"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="domain">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="MGMTCON"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Management Consideration"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="definition">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="The management consideration attributes indicates whether or not ecological/landscape features or site conditions are present within the polygon. These features and conditions may require special consideration during resource management planning. "/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="description">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="The photo interpreters will only use the MGMTCON1 and MGMTCON2 attributes during inventory production. Use of the MGMTCON3 attribute is reserved for the forest management planning team."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="values">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="COLD:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="COLD"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site with poor or unstable growing conditions due to the soil being frozen year-round."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="permafrost"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="DAMG:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="DAMG"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A stand which contains trees that are damaged, dead and/or dying due to natural causes (e.g., ice damage, blowdown, insect/disease damage)."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="physical/natural damage  "/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ISLD:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ISLD"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="The area is or is located on an island  (i.e., an area of land that is totally surrounded by water)."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="island"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="NATB:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="NATB"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A productive stand that is unreachable due to the physical features of the surrounding area (e.g., the area is a mesa or is productive forest surrounded by non-forested wetland)."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="natural barrier"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="NONE:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="NONE"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="There are no physical or ecological &apos;restrictions&apos; in the site that need to be considered when determining management of the stand."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="no management consideration"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="PENA:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="PENA"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="An area of land that is nearly surrounded by water and is connected to the mainland."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="peninsula"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="POOR:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="POOR"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A stand exhibiting stagnated growth with no discernible cause for the poor growing condition."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="stagnated, poor tree growth - no indicator"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ROCK:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ROCK"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where the rocky conditions limit accessibility by forest management equipment and/or present a potential for soil erosion due to operations."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="exposed bedrock / rocky outcrops"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SAND:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SAND"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where forest regeneration will be difficult due to the potential for erosion."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="blow sand / exposed fine sand, shallow or no humus"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SHRB:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SHRB"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where forest regeneration will be difficult without major silvicultural intervention due to shrub or brush competition."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="heavy shrub / brush"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SOIL:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="SOIL"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="An area with minimal soil depth where forest regeneration will be limited as the site could be damaged or experience degradation due to operations (e.g., potential for root damage or erosion)."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="shallow soils"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="STEP:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="STEP"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where the degree of incline is dangerous for equipment operation and presents a potential for erosion. "/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="steep slopes"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="WATR:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="WATR"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where forest regeneration will be difficult due to the potential of rising surface water."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="telluric / highly fluctuating, moving ground water"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="WETT:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="WETT"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="desctext">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A site where forest regeneration will be difficult due to the potential of rising water tables."/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="option">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="poorly drained - high water table"/>
</node>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory  when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT=" The attribute population must follow the correct format, or empty when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;COLD&apos;,&apos;DAMG&apos;,&apos;ISLD&apos;,&apos;NATB&apos;,&apos;NONE&apos;,&apos;PENA&apos;,&apos;POOR&apos;,&apos;&apos;,&apos;ROCK&apos;,&apos;SAND&apos;,&apos;SHRB&apos;,&apos;SOIL&apos;,&apos;STEP&apos;,&apos;WATR&apos;,&apos;WETT&apos;,&apos;&apos;,None] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="mandatory">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="AGS_POLE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Polewood Acceptable Growing Stock"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Float"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="AGS_POLE"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="AGS_SML:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Sm Sawlog Acceptable Growing Stock"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Float"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="AGS_SML"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="AGS_MED:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Med Sawlog Acceptable Growing Stock"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Float"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="AGS_MED"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="AGS_LGE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Lg Sawlog Acceptable Growing Stock"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Float"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="AGS_LGE"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UGS_POLE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Polewood Unacceptable Growing Stock"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Float"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UGS_POLE"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UGS_SML:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Sm Sawlog Unacceptable Growing Stock"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Float"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UGS_SML"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UGS_MED:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Med Sawlog Unacceptable Growing Stock"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Float"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UGS_MED"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UGS_LGE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Lg Sawlog Unacceptable Growing Stock"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Float"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UGS_LGE"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="USPCOMP:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Understorey Species Composition"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="120"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="String"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="USPCOMP"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="definition">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="This understorey species composition attribute indicates the tree species that are present in the understorey portion of the forest stand canopy and the proportion of cover that each species occupies within the understorey. "/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Description">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="See SPCOMP list in eFRI tech spec for list of acceptable species"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="When POLYTYPE is equal to FOR:"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ST1: The population of this attribute is mandatory when VERT is two-tiered or multitiered (T or M)"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Pattern is SSSPPPSSSPPP   example: PJ  80PO  20  (there are two blanks between the species and the proportion) "/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT=" No duplicate species codes allowed in the string"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Proportion values in the string must sum to 100 "/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="The tree species in the composition are to be coded using the scheme listed here.   "/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="*each species code is 3 characters (including blanks) and is left justified"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="*each proportion is 3 characters which represents an integer value from 1 to 100 and is right justified. Note that initial inventory values determined by photo interpretation will not go below 10% and are rounded to the nearest 10%  (e.g., 10, 20, 30). During inventory update, proportions based on subsequent field survey sampling that are of a finer resolution can be entered (e.g., go below 10% or be a value in between the initial rounded proportions such as 15). "/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="*maximum of 10 species and proportions pairs in the string"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="If SOURCE is NOT plot based then allow specific species in SPCOMP (eFRI tech spec)"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Tree species in the list [     &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,     &apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,     &apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,     &apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;,     &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos;     ]"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="If SOURCE is plot based (PLOTVAR, PLOTFIXD) then allow other species in SPCOMP (eFRI tech spec)"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Plot Species are [     &apos;AL&apos;,&apos;Aq&apos;,&apos;Ap&apos;,&apos;Ag&apos;,&apos;Bc&apos;,&apos;Bp&apos;,&apos;Gb&apos;,&apos;Bb&apos;,&apos;Cat&apos;,&apos;Cc&apos;,&apos;Cm&apos;,     &apos;Cp&apos;,&apos;Cs&apos;,&apos;Ct&apos;,&apos;Er&apos;,&apos;Eu&apos;,&apos;Hk&apos;,&apos;Ht&apos;,&apos;Hl&apos;,&apos;Hb&apos;,&apos;Hm&apos;,&apos;Hp&apos;,     &apos;Hs&apos;,&apos;Hc&apos;,&apos;Kk&apos;,&apos;Le&apos;,&apos;Lj&apos;,&apos;Bl&apos;,&apos;Ll&apos;,&apos;Lb&apos;,&apos;Gt&apos;,&apos;Mb&apos;,&apos;Mf&apos;,     &apos;Mm&apos;,&apos;Mt&apos;,&apos;Mn&apos;,&apos;Mp&apos;,&apos;AM&apos;,&apos;Ema&apos;,&apos;Mo&apos;,&apos;Obl&apos;,&apos;Ob&apos;,&apos;Och&apos;,&apos;Op&apos;,     &apos;Os&apos;,&apos;Osw&apos;,&apos;Pa&apos;,&apos;Pn&apos;,&apos;Pp&apos;,&apos;Pc&apos;,&apos;Ph&apos;,&apos;Pe&apos;,&apos;Red&apos;,&apos;Ss&apos;,&apos;Sc&apos;,     &apos;Sk&apos;,&apos;Sn&apos;,&apos;Sr&apos;,&apos;Sy&apos;,&apos;Tp&apos;,&apos;Haz&apos;     ]"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="If SPCOMP percentages are not in 10&apos;s then SOURCE must be plot based. 1&apos;s not 10&apos;s"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="[ &apos;${name} must not be null in two- or multi-canopy stands&apos; for row in [row] if row[&apos;${name}&apos;] is None   and row[&apos;VERT&apos;] in [&apos;TO&apos;,&apos;TU&apos;,&apos;MO&apos;,&apos;MU&apos;] ]"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="[ &apos;${name} must not be blank in two- or multi-canopy stands&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos;  and row[&apos;VERT&apos;] in [&apos;TO&apos;,&apos;TU&apos;,&apos;MO&apos;,&apos;MU&apos;]  ]"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="[ i for i in [ test_spcomp(row[&apos;${name}&apos;]) for r in [row] if r[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;VERT&apos;] in [&apos;TO&apos;,&apos;TU&apos;,&apos;MO&apos;,&apos;MU&apos;] and r[&apos;${name}&apos;] is not None ] if i is not None  ]"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="[ &apos;%s is not a valid species code&apos;%(s) for s in test_SpeciesInList(row[&apos;${name}&apos;], [ &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,&apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,&apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;, &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos; ]        ) if row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;SOURCE&apos;][:4] != &apos;PLOT&apos; and row[&apos;VERT&apos;] in [&apos;TO&apos;,&apos;TU&apos;,&apos;MO&apos;,&apos;MU&apos;]  and row[&apos;${name}&apos;] is not None ]"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="[ &apos;%s is not a valid species code&apos;%(s) for s in test_SpeciesInList(row[&apos;${name}&apos;],    [ &apos;AL&apos;,&apos;Aq&apos;,&apos;Ap&apos;,&apos;Ag&apos;,&apos;Bc&apos;,&apos;Bp&apos;,&apos;Gb&apos;,&apos;Bb&apos;,&apos;Cat&apos;,&apos;Cc&apos;,&apos;Cm&apos;, &apos;Cp&apos;,&apos;Cs&apos;,&apos;Ct&apos;,&apos;Er&apos;,&apos;Eu&apos;,&apos;Hk&apos;,&apos;Ht&apos;,&apos;Hl&apos;,&apos;Hb&apos;,&apos;Hm&apos;,&apos;Hp&apos;, &apos;Hs&apos;,&apos;Hc&apos;,&apos;Kk&apos;,&apos;Le&apos;,&apos;Lj&apos;,&apos;Bl&apos;,&apos;Ll&apos;,&apos;Lb&apos;,&apos;Gt&apos;,&apos;Mb&apos;,&apos;Mf&apos;,&apos;Mm&apos;,&apos;Mt&apos;,&apos;Mn&apos;,&apos;Mp&apos;,&apos;AM&apos;,&apos;Ema&apos;,&apos;Mo&apos;,&apos;Obl&apos;,&apos;Ob&apos;,&apos;Och&apos;,&apos;Op&apos;, &apos;Os&apos;,&apos;Osw&apos;,&apos;Pa&apos;,&apos;Pn&apos;,&apos;Pp&apos;,&apos;Pc&apos;,&apos;Ph&apos;,&apos;Pe&apos;,&apos;Red&apos;,&apos;Ss&apos;,&apos;Sc&apos;,&apos;Sk&apos;,&apos;Sn&apos;,&apos;Sr&apos;,&apos;Sy&apos;,&apos;Tp&apos;,&apos;Haz&apos; ,                               &apos;AX&apos;,&apos;Ab&apos;,&apos;Aw&apos;,&apos;Pl&apos;,&apos;Pt&apos;,&apos;Bd&apos;,&apos;Be&apos;,&apos;Bg&apos;,&apos;Bw&apos;,&apos;By&apos;,&apos;Bn&apos;,&apos;CE&apos;,&apos;Cr&apos;,&apos;Cw&apos;,&apos;CH&apos;,&apos;Cb&apos;,&apos;Cd&apos;,&apos;OC&apos;,&apos;Pd&apos;,&apos;EX&apos;,&apos;Ew&apos;,&apos;Bf&apos;,&apos;OH&apos;,&apos;He&apos;,&apos;Hi&apos;,&apos;Iw&apos;,&apos;La&apos;,&apos;LO&apos;,&apos;MX&apos;,&apos;Mh&apos;,&apos;Mr&apos;,&apos;Ms&apos;,&apos;OX&apos;,&apos;Or&apos;,&apos;Ow&apos;,&apos;PX&apos;,&apos;Pj&apos;,&apos;Pr&apos;,&apos;Ps&apos;,&apos;Pw&apos;,&apos;PO&apos;,&apos;Pb&apos;,&apos;SX&apos;, &apos;Sb&apos;,&apos;Sw&apos;,&apos;la&apos;,&apos;Wb&apos;,&apos;Wi&apos;    ]     ) if row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; and row[&apos;SOURCE&apos;][:4] == &apos;PLOT&apos; and row[&apos;VERT&apos;] in [&apos;TO&apos;,&apos;TU&apos;,&apos;MO&apos;,&apos;MU&apos;] and row[&apos;${name}&apos;] is not None ]"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="mandatory">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UWG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Understory Working Group"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="2"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="String"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UWG"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Validation:">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="english">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="python"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UYRORG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="alias">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Understory Year of Origin"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="length">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="4"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="type">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="Integer"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="name">
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="UYRORG"/>
</node>
<node CREATED="1562265931203" FOLDED="true" MODIFIED="1562265931203" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The understorey year of origin attribute indicates the average year that the species having the greatest relative abundance (in terms of basal area) within the understorey came into existence."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Description">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The year of origin is used to calculate the average age of a productive forest stand. The year of origin is determined in relation to the age of the trees and the year that regeneration (natural or artificial) was established on a site. The year of origin is synonymous to the year of germination. If regeneration has not been established, then the year of origin will be the same as the year of last disturbance. Once trees are established on the site, the year of origin must be adjusted to reflect the average age of the established regeneration.   Once a forest stand has been assessed as free-to-grow, based on attaining the regeneration/management standards, the year of origin should be adjusted to reflect the average age of the growing stock on that site.   In determining the year of origin, age differences that result from natural and artificial growing stock, or age differences that occur due to modified clear-cut harvesting or shelterwood management, must be considered.  For example, if a productive forest stand is harvested in two strips/passes which occur in different years (e.g., a type of modified clear-cut), the forest stand will technically have two ages for a period of time; one age for the recently disturbed strip and another age for the non-disturbed strip. The eventual strategy is to produce an even-aged forest stand. Therefore, the year of origin will eventually be determined as one average value for the forest stand. If the remaining strip is not harvested for ten-years (assuming that the harvested strip requires the same time period to reach free-growing conditions), the harvested and regenerated strip will be described by the understorey forest stand characteristics and, therefore, have a separate year of origin. The year of origin and the forest stand characteristics for the overstorey will likely not change. Once the remaining strip has been harvested and the regeneration in that strip has achieved free-to-grow status, the average age of the dominant tree species in each of the strips should be determined and then used to calculate the appropriate year of origin for the entire stand. At this point, understorey forest stand characteristics are no longer required.  Normally, updates to the year of origin have a corresponding change to the year of update attribute as this indicates the currency/vintage of the information and when the year of origin value was determined.   Year of origin information is not required for non-forested and non-productive forest land types. "/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="This is a calculated value. Understorey year of origin is calculated as the year of the data source from which age is determined  minus  the understorey age as determined using the source  (i.e.,  UYRORG  =  year of age source - UAGE).  For example, using imagery taken in 2007, the age of the stand is interpreted to be 50 years at that time (in 2007).  Thus the year of origin for the stand is 2007 - 50 = 1957.  Note that the source used to determine age may not be the data source listed in the SOURCE attribute if multiple data sources were used to generate the stand description.  ">
<icon BUILTIN="help"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Validation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="A zero value is not a valid code when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The UYRORG attribute value must be greater than or equal to 1600 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The UYRORG attribute must be less than the plan period start year when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="python">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="[ &apos;${name} must not be = 0  in two- or multi-canopy stands&apos; for row in [row] if row[&apos;${name}&apos;]  == 0 and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="[ &apos;${name} must be &gt;= 1600&apos; for row in [row] if row[&apos;${name}&apos;]  &lt;1600 and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="[ &apos;${name} must be &lt;= YRORG  in two- or multi-canopy stands&apos; for row in [row] if row[&apos;${name}&apos;]  &gt; row[&apos;YRORG&apos;]  and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="sql"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="mandatory">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="UHT:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="alias">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Understory Height"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="length">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="4"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="type">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Float"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="UHT"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The understorey height attribute indicates the estimated average tree height (in meters) of the species that has the most basal area as inventoried in the Year of Update. Estimates can be made from interpreted crown closure or field samples, or from growth algorithms."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Validation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Valid numeric values are from 0 through 40.0 when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Value should be less than HT when POLYTYPE is equal to FOR."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="python">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 40 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 40) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  ]   "/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="[ &apos;${name} must be &lt;= HT&apos; for row in [row] if row[&apos;${name}&apos;]  &gt; row[&apos;HT&apos;]  and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;]   "/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="sql"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="DomainValidation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Height / Age relationships should be in the range of Plonski and others."/>
</node>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="USTKG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="alias">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Understory Stocking"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="length">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="4"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="type">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Float"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="USTKG"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The understorey stocking attribute indicates a qualitative measure of the density of tree cover within the understorey.  It is expressed as a percentage value ranging from zero to a maximum of 4.00, although 2.50 is the typical maximum value encountered in the field."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Validation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: The attribute population must follow the correct format when the POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="python">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="[ &apos;${name} must be &gt;= 0 and  ${name} &lt;= 4.00 and POLYTYPE is FOR&apos; for row in [row] if (row[&apos;${name}&apos;] &lt; 0 or row[&apos;${name}&apos;] &gt; 4.01) and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  ]   "/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="USC:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="length">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="1"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="type">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Integer"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="USC"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="alias">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Understorey Site Class"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The understorey site class attribute indicates a site quality estimate for the understorey of a forest stand. It is determined using the average height, age, and working group, based on the dominant tree species of the understorey. These attributes are compared against height and age growth curves in Plonski&apos;s Normal Yield Tables for different species to determine the relative growth rate for a forest stand.   "/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="values">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="0:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="0"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="desctext">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="option">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Best"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="1:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="1"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="desctext">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="option">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Better"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="2:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="2"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="desctext">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="option">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Good"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="3:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="3"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="desctext">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="option">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Poor"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="4:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="4"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="desctext">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Productive forest land on which forest management activities cannot normally be practiced without incurring deleterious environmental effects because of obvious physical limitation such as steep slopes or shallow soils over bedrock."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="option">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Protection Forest"/>
</node>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Validation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=" ST1: The attribute population must follow the correct format  when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="python">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="[ &apos;${name} must follow coding&apos; for row in [row] if row[&apos;${name}&apos;] not in [ 0,1,2,3,4 ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  ]   "/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="VERDATE:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="length">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="8"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="type">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Date"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="VERDATE"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="alias">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Verification Status Date"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The verification status date attribute contains the date that the geographic unit was verified/validated.  "/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Validation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="A blank or null value is a valid code"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="python"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="SENSITIV:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="length">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="3"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="type">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="String"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="SENSITIV"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="alias">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Data Sensitivity Indicator"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The data sensitivity indicator attribute contains an indication of whether the geographic unit is classified as sensitive or not."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Validation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The attribute population must follow the correct format"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="A blank or null value is a valid code"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Contains yes or no"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="python"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="sql"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="values">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Y:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Y"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="desctext">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="option">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="yes"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="N:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="N"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="desctext">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="option">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="No"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1562265931204" ID="ID_1328293766" MODIFIED="1562265931204" TEXT="fields">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="MANAGED:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="alias">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Managed / Unmanaged Indicator"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="length">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="1"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="type">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="String"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="MANAGED"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="domain">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="MANAGED"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="values">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="M:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="M"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="desctext">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The Crown forest area can be managed for timber production."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="option">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Managed"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="U:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="U"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="desctext">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="There exists a legal or land use planning decision which prevents the Crown forest from being managed for timber production."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="option">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Unmanaged"/>
</node>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The managed / unmanaged indicator attribute applies to Crown forest areas only.  The attribute indicates whether or not there is a legal or land use planning decision which prevents the land from being managed for timber."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Validation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="python"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="sql"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="test:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;}">
<icon BUILTIN="full-4"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:None}">
<icon BUILTIN="full-5"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="mandatory">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="TYPE:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="alias">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="TYPE"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="length">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="2"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="type">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="integer"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="TYPE"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="domain">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="TYPE"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The stand type attribute is traditional/historic forest resources inventory attribute. This  attribute contains a unique code which is used to classify and describe the area within  a polygon."/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Validation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="python"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="sql"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="test:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="{}">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931204" FOLDED="true" ID="ID_251989612" MODIFIED="1562265931204" TEXT="MNRCODE:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="alias">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="MNRCODE"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="length">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="3"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="type">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="integer"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="MNRCODE"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="domain">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="MNRCODE"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The MNR code attribute is traditional/historic forest resources inventory (FRI) attribute.  This attribute provides the basic identity for all topologic features by describing the  spatial feature type, such as a polygon, point, or line, in combination with identifying  the kind of geographic feature, such as lake polygon or a shoreline - line segment.   MNR code was created to identify spatial and geographic feature types (topologic  identity) for all spatial features maintained in the Ontario Geographic Database System  or former Ontario Basic Mapping (OBM) that were utilized in the creation of a traditional  FRI (PFOREST, ALLCL, OWNER and other former base layers), which in some cases  have been maintained and incorporated into several of the forest polygon coverages. "/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Validation:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="english">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="python"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="sql"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="test:">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="{}">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931204" ID="ID_11005501" MODIFIED="1562265931204" TEXT="SMZ:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="alias">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="Strategic Management Zone"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="length">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="5"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="type">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="String"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="name">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="SMZ"/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="definition">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="The strategic management zone attribute indicates the unique short form identifier given to a strategic management zone.  "/>
</node>
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="description">
<node CREATED="1562265931204" FOLDED="true" MODIFIED="1562265931204" TEXT="A strategic management zone is a geographical separation or sub-division of the area within a forest management unit.  In relation to forest diversity and landscape classes, the strategic management zone refers to similar areas within a forest management unit which are physically or geographically separated from each other but are grouped for management purposes.     Examples of strategic management zones are ecological zones (eco-regions or ecodistricts), watershed zones, large landscape patches (LLP), wilderness zones, industrial working circles/operating units, and intensive forest management areas.  Strategic management zone information may be used in the preparation of forest management plan tables. Strategic management zone information may also influence forest modelling and landscape diversity analysis. If a forest management unit is divided into strategic management zones, Sustainable Forest Licence holders (plan holders) must provide the strategic management zone identification code and a name that describes the strategic management zone for all licenced Crown lands within a designated forest management unit. "/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Validation:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="english">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="python"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="sql"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="test:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{}">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="mandatory">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" ID="ID_365895033" MODIFIED="1562265931205" TEXT="PLANFU:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="alias">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Plan Forest Unit"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="length">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="10"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="type">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="String"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="name">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="PLANFU"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="definition">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The plan forest unit attribute indicates the unique short form label / ID given to an aggregation of forest stands for management purposes which have similar species composition, develop in a similar manner (both naturally and in response to silvicultural treatments), and are managed under the same silvicultural system and is the primary regulated forest unit for which the available harvest area is determined."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="description">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Licensees must identify a forest unit for all productive forest areas on Crown lands within a forest management unit."/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The classification of the plan forest unit attribute must be applied as a set of inventory based SQL statements and must not be manually assigned.  This is to ensure repeatability and ease of documentation. The classification must be identified in the Analysis Package."/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The technical specifications allow FU codes of up to 10 characters but the Modelling and Inventory Support Tool (MIST) can only handle forest unit codes that are a maximum of 5 characters. This field will be truncated to 5 characters on load to MIST. Therefore, the codes must be unique in the first 5 characters to maintain proper forest unit groupings.  "/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Forest unit information is used to support the preparation of several other forest management plan tables, schedules, and reports as well as to support forest modeling, landscape diversity analysis, and the development of a management strategy."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Validation:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="english">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="python"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="test:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:None}">
<icon BUILTIN="full-4"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="mandatory">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="default">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="default"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" ID="ID_1506841259" MODIFIED="1562265931205" TEXT="AGE:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="alias">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Age"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="length">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="3"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="type">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Integer"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="name">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="AGE"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="definition">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The age attribute indicates the average age of the dominant and co-dominant trees based on the working group species in a forest stand as of the start date of the new plan period. This is a numeric value calculated on the difference between the plan start year and the YRORG value."/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The age attribute must be calculated based on the difference between the plan period start date and the forest stand year of origin. For example, if the start date of the plan period is April 1, 2013 and the year of origin for a forest stand is 1933, then the average age of the forest stand is 80 years. "/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="description">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Age must be determined for all productive forest areas on the forest management unit and is used to determine the age class information which is required in the preparation of several forest management plan tables, schedules, and reports. Age class, similar to age, must also be determined based on the start date of the plan period."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Validation:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="english">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory ">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The attribute population must follow the correct format when POLYTYPE is equal to FOR">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: A zero or null value is not a valid code when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1:  The plan period start year minus the YRORG attribute value should equal the AGE attribute value.">
<icon BUILTIN="full-5"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="python">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="[ &apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must equal plan period start year minus YRORG when POLYTYPE equals FOR&apos; for row in [row] if ( ${planyear} - row[&apos;YRORG&apos;] != row[&apos;${name}&apos;])]">
<icon BUILTIN="full-5"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="sql"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="test:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:10, &apos;YRORG&apos;:1950 } ">
<icon BUILTIN="full-6"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must equal plan period start year minus YRORG when POLYTYPE equals FOR"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:0, &apos;DEVSTAGE&apos;:&apos;NAT&apos; } ">
<icon BUILTIN="full-5"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" STYLE="fork" TEXT="mandatory">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="default">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="70"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" ID="ID_1379870646" MODIFIED="1562265931205" TEXT="AVAIL:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="alias">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Availability Indicator"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="length">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="1"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="type">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="String"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="name">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="AVAIL"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="domain">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="AVAIL"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="definition">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The availability indicator attribute identifies which portions of the managed Crown production forest are available for timber production or not. "/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="description">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Licensees must identify the areas of managed Crown production forest, by forest stand, which are available or unavailable for timber production. The determination of availability is a management planning decision based on considering the productive forest modifier, recent changes to any operational guidelines since the last plan, and reserve areas that were identified during area of concern planning (both past and present).  The productive forest modifier attribute identifies whether a forest stand is designated as production forest, production forest - designated management reserve, or protection forest. Normally, productive forest areas which are designated as production forest are considered as forest stands which are available for timber production. Productive forest areas that are designated as protection forest are usually considered as forest stands which are not available for timber production. Productive forest areas which are designated as production forest - designated management reserve are also normally considered as forest stands which are not available for timber production.  The decision regarding the availability of a forest stand area for timber management must be identified in the availability indicator attribute as either available or unavailable. The sum of the available production forest area, by forest stand and age class, as determined from the age attribute, should correspond to the forest unit and age class subtotals in forest management plan table FMP-3, Summary of Managed Crown Productive Forest by Forest Unit. "/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="values">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="A:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="name">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="A"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="desctext">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Crown production forest that can be managed for timber production."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="option">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Available"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="U:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="name">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="U"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="desctext">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Managed Crown production forest that is not available for timber production as a result of reserve prescriptions developed in a forest management plan (e.g., area of concern reserve)."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="option">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Unavailable"/>
</node>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Validation:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="english">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: If the MANAGED attribute equals U, then the AVAIL attribute should be U when POLYTYPE is equal to FOR">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST2: If the FORMOD attribute equals PF, then AVAIL attribute should be equal to U when POLYTYPE equals FOR and OWNER equals 1">
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST2: If the SC attribute equals 4, then the AVAIL attribute should be U when POLYTYPE is equal to FOR and OWNER equals 1">
<icon BUILTIN="full-8"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST2: If the ACCESS1 and ACCESS2 attributes are not equal to NON, then the AVAIL attribute should be U when POLYTYPE is equal to FOR and OWNER equals 1">
<icon BUILTIN="full-9"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="python">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="[ &apos;ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and MANAGED IS U&apos; ${:F} row[&apos;${name}&apos;] != &apos;U&apos;  and row[&apos;MANAGED&apos;] == &apos;U&apos;  ]   ">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="[ &apos;ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when FORMOD is PF and OWNER is 1&apos; ${:F} row[&apos;${name}&apos;] != &apos;U&apos; and row[&apos;FORMOD&apos;] == &apos;PF&apos; and row[&apos;OWNER&apos;] == &apos;1&apos;  ]   ">
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="[ &apos;ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when POLYTYPE is FOR and SC equals 4 ( and OWNER 1)&apos; ${:F} row[&apos;${name}&apos;] != &apos;U&apos; and row[&apos;SC&apos;] == 4  and row[&apos;OWNER&apos;] == &apos;1&apos;]   ">
<icon BUILTIN="full-8"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="[ &apos;ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when ACCESS1 and ACCESS2 are not NON (and OWNER 1)&apos; ${:F} row[&apos;${name}&apos;] !=&apos;U&apos; and row[&apos;ACCESS1&apos;] not in [  &apos;NON&apos; ]  and row[&apos;ACCESS2&apos;] not in [ None, &apos;NON&apos; ] and row[&apos;OWNER&apos;] == &apos;1&apos;  ]   ">
<icon BUILTIN="full-9"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="sql"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="test:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;}">
<icon BUILTIN="full-4"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:None}">
<icon BUILTIN="full-5"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A&apos;, &apos;MANAGED&apos;:&apos;U&apos; }">
<icon BUILTIN="full-6"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and MANAGED IS U"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A&apos;, &apos;OWNER&apos;:&apos;1&apos;, &apos;FORMOD&apos;:&apos;PF&apos; , &apos;MGMTCON1&apos;:&apos;ROCK&apos; }    ">
<icon BUILTIN="full-7"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when FORMOD is PF and OWNER is 1"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A&apos;, &apos;OWNER&apos;:&apos;1&apos;, &apos;SC&apos;:4 }">
<icon BUILTIN="full-8"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when POLYTYPE is FOR and SC equals 4 ( and OWNER 1)"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A&apos;, &apos;OWNER&apos;:&apos;1&apos;, &apos;ACCESS1&apos;:&apos;LUD&apos;, &apos;ACCESS2&apos;:&apos;OWN&apos; }  ">
<icon BUILTIN="full-9"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST2: &lt;mark&gt;${name}&lt;/mark&gt; should be U when ACCESS1 and ACCESS2 are not NON (and OWNER 1)"/>
</node>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="mandatory">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" ID="ID_1760924162" MODIFIED="1562265931205" TEXT="SILVSYS:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="alias">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Silvicultural System"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="length">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="2"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="type">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="String"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SILVSYS"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="domain">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SILVSYS"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="definition">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="The silviculture system attribute indicates the process by which a productive forest stand will be managed for timber production purposes.  The process/system is classified according to the method of harvesting that will be used. "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="description">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="There are three basic silvicultural systems clear-cut, shelterwood, and selection. Licensees must identify the applicable silvicultural system for those forest stands that have been identified as available for timber production in the availability indicator attribute.  The stage of development attribute provides an indication of the most recent silvicultural system that was applied to each forest stand. Therefore, the silvicultural system management decision attribute should normally correspond to the silvicultural system that is associated with the stage of development attribute. However, Licensees may identify a more appropriate silvicultural system based on the forest unit for the purposes of the new forest management plan. Therefore, the silvicultural system attribute represents the silvicultural system that will be applied to a forest stand. In some cases, this may differ from the silvicultural system that was employed in the past. The silvicultural system must be identified for each stand, although it is normally assigned on a forest unit basis.  "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="values">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="CC:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="CC"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="A silvicultural system of regenerating an even-aged forest stand in which new seedlings become established in fully exposed micro-environments after most or all of the existing trees have been removed. Regeneration can originate naturally or can be applied artificially. Clearcutting may be done in blocks, strips or patches."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Clearcut"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SE:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SE"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="An uneven-aged silvicultural system where mature and/or undesirable trees are removed individually or in small groups over the whole area, usually in the course of a cutting cycle. Regeneration is generally natural."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Selection"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SH:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SH"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="An even-aged silvicultural system where mature trees are harvested in a series of two or more cuts (preparatory, seed, first removal, final removal) for the purpose of obtaining natural regeneration under shelter of the residual trees, either by cutting uniformly over the entire stand area or in narrow strips. Regeneration is natural or artificial. The regeneration interval determines the degree of even-aged uniformity."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Shelterwood"/>
</node>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Validation:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="english">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR">
<icon BUILTIN="full-5"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="python"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Rule we aren&apos;t testiing - policy compliance but not a sensible error">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: A null or blank value is not a valid code when AVAIL is equal to A">
<icon BUILTIN="full-5"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="[&apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null if POLYTYPE equals FOR and AVAIL is A&apos; ${:F} ${NIN} and row [&apos;AVAIL&apos;] == &apos;A&apos;    ]">
<icon BUILTIN="full-3"/>
<icon BUILTIN="full-5"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="test:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;}">
<icon BUILTIN="full-4"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:None}">
<icon BUILTIN="full-5"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="mandatory">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931206" ID="ID_1410425515" MODIFIED="1562265931206" TEXT="NEXTSTG:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="alias">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="NEXTSTG"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="length">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="8"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="type">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="String"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="NEXTSTG"/>
</node>
<node CREATED="1562265931206" ID="ID_227117135" MODIFIED="1562265931206" TEXT="definition">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="The next stage attribute indicates the next harvest or cut treatment that is planned to occur in an available productive forest stand being managed for timber production. "/>
</node>
<node CREATED="1562265931206" ID="ID_53974377" MODIFIED="1562265931206" TEXT="description">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="The next stage attribute indicates the next harvest or cut treatment that is planned to occur for an available productive forest stand. The next stage depends on the silvicultural system employed. Licensees must identify the next harvest treatment that will occur for each forest stand which is available for timber production based on the availability indicator attribute.   The next stage often corresponds to the stage of development attribute. The stage of development attribute represents the current development state, and/or the current stage of silvicultural management for each productive forest stand.   The next stage is most applicable to the forest stands that have been selected for planned operations (harvest) within the new plan term. The next stage will be used to populate the stage of management in the operational tables in the forest management plan (i.e., FMP-11 and FMP-16). "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Validation:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="english">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Validation when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory "/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: The population of this attribute is mandatory "/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: The attribute population must follow the correct coding scheme "/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT=" ST1: A blank or null value is not a valid code "/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="If the AGESTR attribute equals E, then the NEXTSTG attribute cannot be IMPROVE or SELECT "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="python">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="[ &apos;${name} must not be null&apos; for row in [row] if row[&apos;${name}&apos;] is None and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="[ &apos;${name} must not be blank&apos; for row in [row] if str(row[&apos;${name}&apos;]).strip() == &apos;&apos; and row[&apos;POLYTYPE&apos;]==&apos;FOR&apos; ]"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="[ &apos;${name} must follow coding: %s&apos;%(row[&apos;${name}&apos;]) for row in [row] if row[&apos;${name}&apos;] not in [ &apos;THINPRE&apos;,&apos;THINCOM&apos;,&apos;CONVENTN&apos;,&apos;BLKSTRIP&apos;,&apos;SEEDTREE&apos;,&apos;SCNDPASS&apos;,&apos;HARP&apos;,&apos;CLAAG&apos;,&apos;PREPCUT&apos;,&apos;SEEDCUT&apos;,&apos;FIRSTCUT&apos;,&apos;LASTCUT&apos;,&apos;IMPROVE&apos;,&apos;SELECT&apos;,&apos;SNGLTREE&apos;,&apos;GROUPSE&apos;] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos; ]   "/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="[ &apos;${name} must not be IMPROVE or SELECT when POLYTYPE is FOR and AGESTR IS E &apos; for row in [row] if row[&apos;${name}&apos;] in [ &apos;IMPROVE&apos;,&apos;SELECT&apos;  ] and row[&apos;POLYTYPE&apos;] == &apos;FOR&apos;  and row[&apos;AGESTR&apos;] == &apos;E&apos;  ]   "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="sql"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="test:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="{}">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="comment">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT=" (MGMTSTG)"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="values">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="THINPRE:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="THINPRE"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Free-growing productive forest areas which receive a mid-rotation partial harvest (reduction in the growing stock)  that is designed to meet various objectives such as improving tree spacing, removing trees not suited to the site, and promoting the growth of the best quality trees.  The trees selected for removal do not result in a harvest of merchantable volume. "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a pre-commercial thinning/spacing treatment"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="THINCOM:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="THINCOM"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Free-growing productive forest areas which receive a mid-rotation partial harvest (reduction in the growing stock)  that is designed to meet various objectives such as improving tree spacing, removing trees not suited to the site, and promoting the growth of the best quality trees.  The harvested trees are removed from the site and used for commercial purposes. "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a commercial thinning/spacing treatment"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="CONVENTN:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="CONVENTN"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="The removal of most or all of the existing trees in a stand (or a number of adjacent stands) in one operation, so that new seedlings become established in a fully exposed micro-environments.  Harvesting patterns include conventional clearcuts, block cuts and patch cuts.  "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a conventional clearcut"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="BLKSTRIP:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="BLKSTRIP"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="The removal of a portion of the existing trees in a stand in progressive strips in more than one operation so that the non-disturbed portion of the stand is left primarily to provide a natural seed source for regeneration of the disturbed area.  Several cutting patterns are available to achieve same goal.  The removal of trees in one or more passes in a system of strips of various widths; where each strip is less than or equal to 100 meters (5 chains) wide.  It is designed to encourage regeneration on difficult and/or fragile sites.  Note:  Harvesting where the cut strips are greater than 100 meters wide (&gt; 5 chains) should be recorded as clearcut. "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a modified cut: block or strip"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SEEDTREE:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SEEDTREE"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="An even-aged, silvicultural system that retains mature standing trees scattered throughout the cutblock to provide seed sources for natural regeneration.  A method of harvesting and regenerating a forest stand in which all trees are removed from the area except for a small number of seed-bearing trees that are left singly or in small groups.  The objective is to create an even-aged stand. "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a modified cut: seed tree"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SCNDPASS:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SE"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="For areas managed using the clearcut silvicultural system, harvest may be planned in two passes.  This is normally when species within the stand are harvested and utilized by different logger/contractor/forest resource Licensee in different years (e.g., first pass is conifer and second-pass is hardwood).  A first pass should have been recorded in the annual report if merchantable tree species remained in the forest stand which have been allocated for harvest - but not yet harvested.  The second-pass option should be denoted when merchantable tree species are selected to be harvested from forest stands which have been previously recorded as harvested in a first pass."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a modified cut: next / second-pass"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="HARP:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="HARP"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="HARP is the removal of the dominant canopy layer in uneven-aged lowland black spruce ecosystems.  HARP protects and retains stems below a set diameter limit, leaving a significant component of the overstorey.  The resulting stand is uneven-aged and uneven-sized."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a harvest with regeneration protection  "/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="CLAAG:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="CLAAG"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="An operational practice that can be applied with any harvest method under the clearcut silvicultural system, where the objective is to remove the overstorey, protect understorey advance growth, and regenerate an even-aged stand.  The resulting stand develops under full light conditions, generally with a reduced rotation length."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a careful logging around advance growth / regeneration cut"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="PREPCUT:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="PREPCUT"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="A shelterwood silvicultural system stage of management designed to remove undesirable species of any species from the stand and to select trees to remain that will provide the best seed source.  The removal of undesirable trees opens the canopy and enables the crowns of remaining seed-bearing trees to enlarge; to improve conditions for seed production and natural regeneration."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a preparatory cut"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SEEDCUT:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SEEDCUT"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="A shelterwood silvicultural system stage of management where trees are removed from a mature stand in order to create openings in the canopy / create spaces and to prepare sites for natural regeneration while maintaining the seed-bearing trees and protecting any existing advance regeneration."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a seed cut"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="FIRSTCUT:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="FIRSTCUT"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="A shelterwood silvicultural system stage of management where overstorey trees are removed in one or more harvests in order to release the established seedlings from competition."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a first removal harvest"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="LASTCUT:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="LASTCUT"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="A shelterwood silvicultural system stage of management where all of the remaining trees in the overstorey are removed.  This is the removal of the seed or shelter trees after the regeneration has been effective."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a last removal harvest"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="IMPROVE:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="IMPROVE"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="A selection silvicultural system stage of management where a cut is made in an uneven-aged stand primarily to improve stand composition. distribution and quality by removing less desirable trees of any species."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive an improvement cut"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SELECT:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SELECT"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="A selection silvicultural system stage of management where individual trees or groups of trees are selected for cutting in order to recover the yield and develop a balanced uneven-aged structure, while providing the cultural measures required for tree growth and seeding establishment. "/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Will receive a selection harvest"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SNGLTREE:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SNGLTREE"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="The stand canopy is (periodically) opened uniformly throughout the entire stand to achieve a post-harvest, basal area target."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Selection: single-tree "/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="GROUPSE:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="GROUPSE"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="desctext">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="The stand canopy is (periodically) opened by harvesting trees in small groups. The resulting canopy opening usually occupies a fraction of a hectare."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="option">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Selection: group "/>
</node>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Validation:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="english">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR and AVAIL is equal to A">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: If the AGESTR attribute equals E, then the NEXTSTG attribute cannot be IMPROVE or SELECT ">
<icon BUILTIN="full-6"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="python">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="[ &apos;ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be IMPROVE or SELECT when POLYTYPE is FOR and AGESTR IS E&apos; ${:NF} row[&apos;${name}&apos;] in [ &apos;IMPROVE&apos;,&apos;SELECT&apos;  ] and row[&apos;AGESTR&apos;] == &apos;E&apos;  ]   ">
<icon BUILTIN="full-6"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="sql"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="test:">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;}">
<icon BUILTIN="full-4"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:None }">
<icon BUILTIN="full-5"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;IMPROVE&apos;, &apos;AGESTR&apos;:&apos;E&apos; }">
<icon BUILTIN="full-5"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be IMPROVE or SELECT when POLYTYPE is FOR and AGESTR IS E"/>
</node>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="mandatory">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="default">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="CONVENT"/>
</node>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SI:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="alias">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Silvicultural Intensity"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="length">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="5"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="type">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="String"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="name">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SI"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="domain">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="SI"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="definition">
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="Growth class upon which the stand is growing (or is projected to be growing) independent of silvicultural treatment"/>
</node>
<node CREATED="1562265931206" FOLDED="true" MODIFIED="1562265931206" TEXT="description">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="The FMPM says Silvicultural Intensity means the projected yield and not the treatments to be implemented"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Validation:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="english">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR,  and SILVSYS is CC or SH"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: A null or blank value is not a valid code when POLYTYPE is equal to FOR, AVAIL is A and SILVSYS is CC or SH"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="python">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="[&apos;ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when POLYTYPE is FOR, AVAIL is A and SILVSYS is CC or SH&apos; ${:F} ${NIN} and row [&apos;AVAIL&apos;] == &apos;A&apos; and row[&apos;SILVSYS&apos;] in [&apos;CC&apos;,&apos;SH&apos;]  ]"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="sql"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="test:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:None, &apos;AVAIL&apos;:&apos;A&apos;, &apos;SILVSYS&apos;:&apos;CC&apos; }">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when POLYTYPE is FOR, AVAIL is A and SILVSYS is CC or SH"/>
</node>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="mandatory">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SISRC:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="alias">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Source of Silvi. Intensity"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="length">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="8"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="type">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="String"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SISRC"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="domain">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SISRC"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="definition">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Source of Silvicultural Intensity (SI)"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Validation:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="english">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: A null or blank value is not a valid code when POLYTYPE is equal to FOR, AVAIL is A and SILVSYS is CC or SH"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="python">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="[&apos;ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when POLYTYPE is FOR, AVAIL is A and SILVSYS is CC or SH&apos; ${:F} ${NIN} and row [&apos;AVAIL&apos;] == &apos;A&apos; and row[&apos;SILVSYS&apos;] in [&apos;CC&apos;,&apos;SH&apos;]  ]"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="sql"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="test:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:None, &apos;AVAIL&apos;:&apos;A&apos;, &apos;SILVSYS&apos;:&apos;CC&apos; }">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must not be blank or null when POLYTYPE is FOR, AVAIL is A and SILVSYS is CC or SH"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;}">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="values">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ACTUAL:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ACTUAL"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Information from a survey."/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT=""/>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ASSIGNED:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ASSIGNED"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Assigned by the planning team, based on projections or other data modelling exercise."/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="comment">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Should we refine this to include the 2 survey stages from SEI? Regen Assessment and Performance Survey"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="mandatory">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562267067318" ID="ID_37520364" MODIFIED="1562267072282" TEXT="Non-fields">
<node CREATED="1562265931205" FOLDED="true" ID="ID_1378051630" MODIFIED="1562265931205" TEXT="OMZ:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="alias">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Operational Management Zone"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="length">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="8"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="type">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="String"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="name">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="OMZ"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="definition">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The operational management zone attribute indicates the unique short form identifier given to an operational management zone.  "/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="description">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="An operational management zone represents areas with distinct operational constraints (e.g., accessibility, other constraints on harvest operations, moose emphasis areas, deer yards). Operational management zones may be used on management units with significant variation in forest-level operational characteristics.     Operational management zone information is less likely to influence forest modelling and landscape diversity analysis at a strategic scale. The operational management zone information is more likely to be used during the operational planning of the forest management plan. If a forest management unit is divided into operational management zones, Sustainable Forest Licence holders (plan holders) must provide the operational management zone identification code or a name that describes the operational management zone for all licenced Crown lands within a designated forest management unit. "/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Validation:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="english">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="python"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="sql"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="test:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{}">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="mandatory">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node COLOR="#000000" CREATED="1562265931205" FOLDED="true" ID="ID_502940734" MODIFIED="1562265931205" TEXT="AGESTR:">
<icon BUILTIN="button_ok"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="alias">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Age Structure Indicator"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="length">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="1"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="type">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="String"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="name">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="AGESTR"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="definition">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="The age structure indicator attribute designates whether the range of ages of the trees in a forest stand is narrow or wide spread."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Validation:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="english">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The population of this attribute is mandatory when POLYTYPE is equal to FOR"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: The attribute population must follow the correct coding scheme when POLYTYPE is equal to FOR">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT=" ST1: A blank or null value is not a valid code when POLYTYPE is equal to FOR">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="If the SILVSYS attribute equals CC, then the AGESTR attribute should be E when POLYTYPE is equal to FOR">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="If the SILVSYS attribute equals SE, then the AGESTR attribute should be U when POLYTYPE is equal to FOR">
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT=" If the SILVSYS attribute equals SH, then the AGESTR attribute should be E when POLYTYPE is equal to FOR">
<icon BUILTIN="full-8"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="python">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="[ &apos;ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be E when POLYTYPE is FOR and SILVSYS IS CC&apos; ${:NF} row[&apos;${name}&apos;] not in [ &apos;E&apos;  ]   and row[&apos;SILVSYS&apos;] == &apos;CC&apos;  ]   ">
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="[ &apos;ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and SILVSYS IS SE&apos; ${:NF} row[&apos;${name}&apos;] not in [ &apos;U&apos;  ]   and row[&apos;SILVSYS&apos;] == &apos;SE&apos;  ]   ">
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="[ &apos;ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be E when POLYTYPE is FOR and SILVSYS IS SH&apos; ${:NF} row[&apos;${name}&apos;] not in [ &apos;E&apos;  ]   and row[&apos;SILVSYS&apos;] == &apos;SH&apos;  ]   ">
<icon BUILTIN="full-8"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="sql"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="test:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:None}">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must not be blank or null...FOR..."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;A Decidedly Invalid Code&apos;}">
<icon BUILTIN="full-4"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must follow the correct coding scheme...FOR..."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;U&apos;, &apos;SILVSYS&apos;:&apos;CC&apos;}">
<icon BUILTIN="full-6"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be E when POLYTYPE is FOR and SILVSYS IS CC"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;E&apos;, &apos;SILVSYS&apos;:&apos;SE&apos;}">
<icon BUILTIN="full-7"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be U when POLYTYPE is FOR and SILVSYS IS SE"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="{&apos;POLYTYPE&apos;:&apos;FOR&apos;,&apos;${name}&apos;:&apos;U&apos;, &apos;SILVSYS&apos;:&apos;SH&apos;}">
<icon BUILTIN="full-8"/>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="ST1: &lt;mark&gt;${name}&lt;/mark&gt; must be E when POLYTYPE is FOR and SILVSYS IS SH"/>
</node>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="values">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="E:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="name">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="E"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="desctext">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="An even-aged forest stand is composed of trees having no, or relatively small, differences in age, usually less than10 to 20 years.  Since the trees are approximately the same age, they generally represent one age class, however two distinct age classes may develop (be present) for a limited time, as a result of timber management techniques employed (e.g., shelterwood or modified clear-cut harvesting) or natural events. "/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="option">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Even-aged"/>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="U:">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="name">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="U"/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="desctext">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="A forest stand is considered to be uneven-aged if there are considerable differences in the age of the trees in the stand, usually an age range of more than 10 to 20 years.  Uneven-aged stands may contain trees representing three or more age classes, or a continuous range of ages that are spatially intermixed within the stand.  These stands are also referred to as &quot;all-aged&quot; stands.  Uneven-aged stands being managed for timber production purposes are normally managed under the selection silvicultural system."/>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="option">
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="Uneven-aged"/>
</node>
</node>
</node>
<node CREATED="1562265931205" FOLDED="true" MODIFIED="1562265931205" TEXT="default"/>
</node>
</node>
<node CREATED="1562265931207" ID="ID_1697497610" MODIFIED="1562265931207" TEXT="fields_include:">
<node CREATED="1562265931207" ID="ID_1778630601" MODIFIED="1562265931207" TEXT="PCM">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FMFOBJID:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="GEOGNUM:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="POLYID:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="POLYTYPE:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="OWNER:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="AUTHORTY:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="YRUPD:">
<icon BUILTIN="button_ok"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SOURCE:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FORMOD:">
<icon BUILTIN="full-1"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="DEVSTAGE:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" ID="ID_640857929" MODIFIED="1562265931207" TEXT="YRDEP:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SPCOMP:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="WG:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="YRORG:">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="HT:">
<icon BUILTIN="button_ok"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="STKG:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SC:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ECOSRC:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ECOSITE1:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ECOPCT1:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ECOSITE2:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ECOPCT2:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ACCESS1:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ACCESS2:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="MGMTCON1:">
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="MGMTCON2:">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="MGMTCON3:">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="AGS_POLE:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="AGS_SML:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="AGS_MED:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="AGS_LGE:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="UGS_POLE:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="UGS_SML:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="UGS_MED:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="UGS_LGE:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="USPCOMP:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="UWG:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="UYRORG:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="UHT:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="USTKG:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="USC:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="VERDATE:">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931207" FOLDED="true" ID="ID_1920806435" MODIFIED="1562265931207" TEXT="SENSITIV:">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="geometry_type">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Polygon"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="alias">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Base Model Inventory (BMI)"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="path">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Plan"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Validation:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="english">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="python"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" POSITION="right" TEXT="ForecastDepletions:">
<icon BUILTIN="prepare"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ForecastDepletions"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="spatial_reference">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="26916"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="fields">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FSOURCE:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="alias">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Source of Forecast"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="length">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="8"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="type">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="String"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FSOURCE"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Validation:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="english">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: A blank or null value is not a valid code"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="python"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="sql"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="test:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="{}">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="values">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="BASECOVR:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="BASECOVR"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="DIGITALA:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="DIGITALA"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="DIGITALP:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="DIGITALP"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ESTIMATE:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ESTIMATE"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FOC:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FOC"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FORECAST:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FORECAST"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FRICNVRT:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FRICNVRT"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="INFRARED:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="INFRARED"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="MARKING:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="MARKING"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="OCULARA:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="OCULARA"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="OCULARG:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="OCULARG"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="OPC:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="OPC"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PHOTO:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PHOTO"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PHOTOLS:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PHOTOLS"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PHOTOSS:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PHOTOSS"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PLOTFIXD:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PLOTFIXD"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PLOTVAR:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="PLOTVAR"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="RADAR:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="RADAR"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="REGENASS:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="REGENASS"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SEMEXTEN:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SEMEXTEN"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SEMINTEN:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SEMINTEN"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SPECTRAL:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="SPECTRAL"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="option"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="mandatory">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FYRDEP:">
<icon BUILTIN="flag"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="alias">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Forecast Year of Disturbance"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="length">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="4"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="type">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Integer"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FYRDEP"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Validation:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="english">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: A zero or null value is not a valid code"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: The attribute population must follow the correct format"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="ST1: The value must not be less than the plan period start year minus 4 and it must not be greater than or equal to the plan term start year"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="python">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="[ &apos;ST1: &lt;b&gt;&lt;mark&gt;${name}&lt;/mark&gt;&lt;/b&gt; must be between %s and %s&apos;%(str(${planyear}-4),str(${planyear})) ${:} row[&apos;${name}&apos;] &lt; (${planyear}-4) or row[&apos;${name}&apos;]&gt;${planyear}]"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="sql"/>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="test:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="{}">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="mandatory">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="FDEVSTAGE:">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="alias">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="Forecast Development Stage"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="length">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="8"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="type">
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="String"/>
</node>
<node CREATED="1562265931207" FOLDED="true" MODIFIED="1562265931207" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FDEVSTAGE"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Validation:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="english">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="ST1: A blank or null value is not a valid code"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="python"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="sql"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="test:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="{}">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="values">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="DEPHARV:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="DEPHARV"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="recently disturbed by harvest and has received no regeneration/renewal treatment"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="DEPNAT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="DEPNAT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="recently disturbed by natural causes and has received no regeneration / renewal treatment"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="LOWMGMT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="LOWMGMT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Below regeneration standards due to past management"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="LOWNAT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="LOWNAT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Below regeneration standards due to natural causes / succession"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="NEWPLANT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="NEWPLANT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="recently renewed : mainly planted"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="NEWSEED:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="NEWSEED"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="recently renewed : mainly seeded"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="NEWNAT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="NEWNAT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="recently renewed : mainly natural regeneration"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FTGPLANT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FTGPLANT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="free-to-grow mainly planted"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FTGSEED:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FTGSEED"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="free-to-grow mainly seeded"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FTGNAT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FTGNAT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="free-to-grow mainly natural regeneration"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="THINPRE:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="THINPRE"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="received pre-commercial thinning/spacing treatment"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="THINCOM:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="THINCOM"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="received commercial thinning/spacing treatment"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="BLKSTRIP:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="BLKSTRIP"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="modified cut: block or strip"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FRSTPASS:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FRSTPASS"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="modified cut: first pass"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="PREPCUT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="PREPCUT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="received a preparatory cut"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SEEDCUT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SEEDCUT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="received a seed cut"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FIRSTCUT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FIRSTCUT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="received a first removal harvest"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="IMPROVE:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="IMPROVE"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="received an improvement cut"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SELECT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SELECT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="received a selection harvest"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SNGLTREE:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SNGLTREE"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Selection:  single-tree"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="GROUPSE:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="GROUPSE"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Selection: group"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="mandatory">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="geometry_type">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Polygon"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="alias">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Forecast Depletions (FDP)"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="path">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Plan"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Definition">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT=" "/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="shortname">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="FDP"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Validation:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="english">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="python"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" POSITION="right" TEXT="PlannedHarvest:">
<icon BUILTIN="go"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="PlannedHarvest"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="shortname">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="PHR"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="spatial_reference">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="26916"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="fields">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SILVSYS:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="alias">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Silvicultural System"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="length">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="2"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="type">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="String"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SILVSYS"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="domain">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SILVSYS"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="definition">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="The silviculture system attribute indicates the process by which a productive forest stand will be managed for timber production purposes.  The process/system is classified according to the method of harvesting that will be used. "/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="description">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="There are three basic silvicultural systems: clear-cut, shelterwood, and selection. Licensees must identify the applicable silvicultural system for those forest stands that have been identified as available for timber production in the availability indicator attribute.  The stage of development attribute provides an indication of the most recent silvicultural system that was applied to each forest stand. Therefore, the silvicultural system management decision attribute should normally correspond to the silvicultural system that is associated with the stage of development attribute. However, Licensees may identify a more appropriate silvicultural system based on the forest unit for the purposes of the new forest management plan. Therefore, the silvicultural system attribute represents the silvicultural system that will be applied to a forest stand. In some cases, this may differ from the silvicultural system that was employed in the past. The silvicultural system must be identified for each stand, although it is normally assigned on a forest unit basis. "/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Validation:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="english">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT=" ST1: The population of this attribute is mandatory "/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT=" ST1: The attribute population must follow the correct coding scheme "/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="python"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="sql"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="test:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="{}">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="values">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="CC:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="CC"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="A silvicultural system of regenerating an even-aged forest stand in which new seedlings become established in fully exposed micro-environments after most or all of the existing trees have been removed. Regeneration can originate naturally or can be applied artificially. Clearcutting may be done in blocks, strips or patches."/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Clearcut"/>
</node>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SE:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SE"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="An uneven-aged silvicultural system where mature and/or undesirable trees are removed individually or in small groups over the whole area, usually in the course of a cutting cycle. Regeneration is generally natural."/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Selection"/>
</node>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SH:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="SH"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="desctext">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="An even-aged silvicultural system where mature trees are harvested in a series of two or more cuts (preparatory, seed, first removal, final removal) for the purpose of obtaining natural regeneration under shelter of the residual trees, either by cutting uniformly over the entire stand area or in narrow strips. Regeneration is natural or artificial. The regeneration interval determines the degree of even-aged uniformity."/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="option">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Shelterwood"/>
</node>
</node>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="mandatory">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="HARVCAT:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="alias">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Harvest Category"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="length">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="8"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="type">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="String"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="name">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="HARVCAT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="domain">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="HARVCAT"/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="definition">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT=" The harvest disturbance category attribute indicates the planned type of harvest that  was completed."/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Description">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Road Right-of-Way"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="The attribute coding scheme for harvest disturbances contains an option for recording spatial  information on road right-of-ways, aggregate pits and landings. The code is ROADROW. The  primary purpose of this code is to identify harvest area for newly constructed road right-of-ways for  primary and branch roads, aggregate pits and landings that are outside of an area of operations,  where the depleted area is not intended for future regeneration purposes and will be maintained for  its intended use. The area associated with road right-of-way, aggregate pit and landing harvests are  not included in the balancing of available harvest area as these areas are usually accounted for in  forest modelling using the estimated roads and landings percentage allowance in SFMM. There  may be exceptions, which should be discussed locally between Districts and Licensees.   "/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="The road right-of-way code (ROADROW) can be used for roads (primary and branch), aggregate  pits or landings delineated within an area of operations provided this consideration was included in  the strategic modeling. Normally harvest polygons within area of operations do not include the road  right-of-way area for primary and branch roads, aggregate pits and landings as part of the harvested  area. Primary and branch road areas are identified by delineating the boundary of the harvest area  up to the road edge or by buffering out the approximate road width using the road centre-line to  remove the area of road. The District and licensee may agree on other ways to identify or remove  roads, aggregate pits and landings in harvest areas.   "/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Operational roads within a harvest polygon will be reported as part of the harvest area. Operational  roads outside of a harvest polygon but within an area of operations are not normally reported on the  harvest disturbance layer."/>
</node>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="Validation:">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="english">
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931208" FOLDED="true" MODIFIED="1562265931208" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="python"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="sql"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="test:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="{}">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="values">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="BRIDGING:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="BRIDGING"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="bridging harvest areas"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="This category identifies the harvested areas that were approved as bridging under the FMP. This code is only valid in the first AR under a new FMP as these areas can only be harvested within the first AWS. "/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="CONTNGNT:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="CONTNGNT"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="REGULAR1:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="REGULAR1"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="regular harvest"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="These harvest areas were categorized as regular under the FMP."/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="REGULAR2:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="REGULAR2"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="regular harvest"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="These harvest areas were categorized as regular under the FMP."/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="SALVAGE:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="SALVAGE"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="salvage harvest"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Salvage is not considered to be the initial form of disturbance  since most areas which require a salvage operation have been  previously disturbed by natural causes, such as fire, insect,  disease, blowdown, etc. All salvage operations are considered to  be harvesting which is often the first silviculture treatment needed  to bring a forest stand back to a more productive state, more  quickly than if left for natural succession. As such, salvage is also  considered to be a form of protection from further loss of  merchantable volume due to insects or fire (e.g., bark  beetles/borers, or fire after insect damage). Salvage operations  also reduce the risk of further natural disturbances, or volume  loss. "/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="REDIRECT:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="REDIRECT"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="redirected harvest"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="These areas are harvested under a pest management plan and count against the available harvest area of the FMP."/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ACCELER:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ACCELER"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="accelerated harvest"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="These areas are harvested under a pest management plan and  are areas in addition to the available harvest area of the FMP. "/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="FRSTPASS:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="FRSTPASS"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="modified cut: first pass"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="For areas managed using the clearcut silvicultural system,  harvest may be planned in two passes. This is normally when  species within the stand are harvested and utilized by different  logger/contractor/forest resource Licensee in different years (e.g.,  first pass is conifer and second pass is hardwood). The first pass  should be recorded if merchantable tree species remain in the  forest stands which have been allocated for harvest, but not yet  harvested."/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="SCNDPASS:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="SCNDPASS"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Second-pass harvest"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="For areas managed using the clearcut silvicultural system,  harvest may be planned in two passes. This is normally when  species within the stand are harvested and utilized by different  logger/contractor/forest resource licensee in different years (e.g.,  first pass is conifer and second pass is hardwood). The second  pass should be recorded when merchantable tree species have  been harvested from forest stands which have been previously  reported as harvested. "/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ROADROW:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ROADROW"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="mandatory">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="FUELWOOD:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="alias">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="fuelwood area"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="length">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="1"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="type">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="String"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="FUELWOOD"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="domain">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="YESNO"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="values">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Y:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Y"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="yes"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="N:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="N"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="No"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Validation:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="english">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="python"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="sql"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="test:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="{}">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="DomainValidation:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="english">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="mandatory">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="geometry_type">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Polygon"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="alias">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Planned Harvest (PHR)"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="path">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Plan"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Validation:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="english">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="python"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" POSITION="right" TEXT="AreaOfConcern:">
<icon BUILTIN="go"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AreaOfConcern"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="spatial_reference">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="26916"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="fields">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AOCID:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="alias">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AOC Identifier"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="length">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="15"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="type">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="String"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AOCID"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Definition">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The AOC identifier attribute is the label assigned to a specific AOC prescription which must correspond to the label on FMP and AWS Areas Selected for Operations maps and the area of concern prescriptions in table FMP-10. The prescription can represent either a group of areas of concern with a common prescription or an individual area of concern with a unique prescription."/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Validation:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="english">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: A blank or null value is not a valid code"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Must be a code from FMP-10 - Manual Check"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="python"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="sql"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="test:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="{}">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="mandatory">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AOCTYPE:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="alias">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AOC Type"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="length">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="1"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="type">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="String"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AOCTYPE"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="domain">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AOCTYPE"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Definition">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The AOC type attribute indicates the type of AOC prescription as either modified or reserved."/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="values">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="M:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="M"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="modified"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Areas which are scheduled for operations but have specific conditions or restrictions on operations as required by an AOC prescription."/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="stroke color">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="#777777"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="fill color">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="#4c7cc9"/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="R:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="R"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="option">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="reserved"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="desctext">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Areas which are reserved (prohibited) from operations as required by an AOC prescription."/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="stroke color">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="#777777"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="fill color">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="#a14cc9"/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Validation:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="english">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: A blank or null value is not a valid code"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="python"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="sql"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="test:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="{}">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="DomainValidation:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="english">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="mandatory">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="geometry_type">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Polygon"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="alias">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Area Of Concern (AOC)"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="path">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Plan"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Validation:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="english">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="python"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="sql"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="shortname">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AOC"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="symbology:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="type">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="categorized"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="field">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="AOCTYPE"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="stroke color">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="#777777"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="fill color">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="#FFFFFF"/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" POSITION="right" TEXT="PlannedResidualPatches:">
<icon BUILTIN="prepare"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="PlannedResidualPatches"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="shortname">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="PRP"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="spatial_reference">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="26916"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="fields">
<icon BUILTIN="help"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="RESID:">
<icon BUILTIN="help"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="alias">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Redisual Patch ID"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="length">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="10"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="type">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="String"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="RESID"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Definition:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The residual patch identifier attribute is a number, label or name assigned to a residual patch(es) as defined in the FMP text. "/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="Validation:">
<icon BUILTIN="help"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="english">
<icon BUILTIN="help"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The presence of this attribute in the file structure or layer is mandatory"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="ST1: A blank or null value is not a valid code"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The value must be defined in the FMP">
<icon BUILTIN="help"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The value must be consistent with the Areas Selected for Operations Maps">
<icon BUILTIN="help"/>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="python"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="sql"/>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="test:">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="{}">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="name">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="RESID"/>
</node>
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="mandatory">
<node CREATED="1562265931209" FOLDED="true" MODIFIED="1562265931209" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="geometry_type">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Polygon"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="alias">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Planned Residual Patches (PRP)"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="path">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Plan"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Validation:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="english">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="python"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" POSITION="right" TEXT="PlannedRoadCorridors:">
<icon BUILTIN="prepare"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="PlannedRoadCorridors"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="shortname">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="PRC"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="spatial_reference">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="26916"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="fields">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="TERM:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="alias">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Plan Term"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="length">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="1"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="type">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Integer"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="TERM"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="domain">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="TERM"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Definition:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The plan term attribute indicates the five-year operational plan term or the next tenyear plan period in which the planned road activity and/or use strategy will occur. "/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Validation:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="english">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="A zero value is a valid code"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="python"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="sql"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="test:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="{}">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="mandatory">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="values">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="1:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="1"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="desctext">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="First 5-year Operational Plan Term (1-5)"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="option">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="1-5"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="2:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="2"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="desctext">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Second 5-year Operational Plan Term (6-10)"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="option">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="6 -10"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="0:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="0"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="desctext">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The next ten year plan period (11-20)"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="option">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="11-20"/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ROADID:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="alias">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Road Identifier"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="length">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="30"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="type">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="String"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ROADID"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Definition">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of."/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Validation:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="english">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: A blank or null value is not a valid code."/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="python"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="test:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="{}">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="mandatory">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ROADCLAS:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="alias">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Road Class"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="length">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="1"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="type">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="String"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ROADCLAS"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="domain">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ROADCLAS"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Definition">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The road class attribute identifies the class of the proposed forest access road in terms of the road use management strategy in the FMP. "/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Validation:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="english">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: A blank or null value is not a valid code."/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="If TERM is zero, then ROADCLAS must be P"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="python">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="[ &apos;ST1: ${name} must be P when TERM is 0 &apos; ${:} row[&apos;${name}&apos;] not in [ &apos;P&apos;  ] and row[&apos;TERM&apos;] == 0 ]   "/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="sql"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="test:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="{}">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="values">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="B:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="B"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="option">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="branch"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="desctext"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="P:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="P"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="option">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Primary"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="mandatory">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="AOCXID:">
<icon BUILTIN="button_ok"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="alias">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="AOC Crossing Identifier"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="length">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="15"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="type">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="String"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="AOCXID"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Definition">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The area of concern crossing identifier attribute indicates the unique value assigned to the preferred 100 meter area where road crossings are permitted within an area of concern. "/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Validation:">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="english">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="A blank or null value IS a valid code"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The AOCXID attribute must contain a unique value within and across layers if one or more Planned Road Corridors layer is submitted.">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="python"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="sql"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="test:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="{}">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="NOXING:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="alias">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Crossing Prohibited"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="length">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="15"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="type">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="String"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="NOXING"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Definition">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The crossing prohibited attribute indicates the area where road crossings are prohibited within the road corridor. These areas may coincide with area of concern (AOC) boundaries where the AOC prescription identifies that the crossing is prohibited. These areas may represent other constraints that prohibit the road from crossing a water body which do not relate to an AOC prescription. "/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Validation:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="english">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT=""/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="python"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="sql"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="test:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="{}">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="TRANS:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="alias">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Road Transfer Year"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="length">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="4"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="type">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Integer"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="TRANS"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Definition">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The road transfer year attribute indicates a four-digit number representing the first year of the 5-year operational term that the transfer of responsibility to the MNR is anticipated to take effect. If there is no intent to transfer responsibility to MNR during the future 20-year period there is no need to specify a year. "/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Validation:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="english">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="A blank or zero value is a valid code"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="If TRANS value is populated, then INTENT must be populated"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The value must be greater than or equal to the plan term start year">
<icon BUILTIN="full-1"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="python">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="[ &apos;ST1: ${name} must be &gt;=  ${planyear} &apos; ${:N} row[&apos;${name}&apos;]  &lt; ${planyear} ]   ">
<icon BUILTIN="full-1"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="sql"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="test:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="{}">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ACYEAR:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="alias">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Access Control Year"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="length">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="4"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="type">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Integer"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ACYear"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Definition">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The access control year attribute indicates a four-digit number representing the expected fiscal year (April 1 to March 31) that the access control is anticipated to take effect. "/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Validation:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="english">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="A zero value is a valid code"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The value must be greater than or equal to the plan term start year"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: If access control year does not equal zero (ACYEAR NOT 0) then access control must be yes"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="python">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="[ &apos;ST1: If ${name} = 0 then ACCESS must be N&apos; ${:} ${NIN} and row[&apos;ACCESS&apos;] &lt;&gt; &apos;Y&apos;  ] "/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="sql"/>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="test:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="{}">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="mandatory">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ACCESS:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="alias">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Access Control"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="length">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="1"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="type">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="String"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ACCESS"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="domain">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="YESNO"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Definition">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="The road access control attribute is a field used to identify where access control activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. "/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="values">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Y:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Y"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="option">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="yes"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="desctext"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="N:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="name">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="N"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="option">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="No"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="Validation:">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="english">
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The population if this attribute is mandatory">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: The attribute population must follow the correct coding scheme">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931210" FOLDED="true" MODIFIED="1562265931210" TEXT="ST1: A blank or null value is not a valid code">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) when plan term is not zero (TERM != 0) "/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="python">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="[ &apos;ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})&apos; ${:} &apos;Y&apos; not in [ row[fff] for fff in [ &apos;ACCESS&apos;,&apos;DECOM&apos;,&apos;MAINTAIN&apos;,&apos;MONITOR&apos;] ]  and row[&apos;TERM&apos;] in [1,2] ]"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="sql"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="test:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="{}">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="mandatory">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="DECOM:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="alias">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Decommissioning"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="length">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="1"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="type">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="String"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="DECOM"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="domain">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="YESNO"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Definition">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Def"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="values">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Y:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Y"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="yes"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="N:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="N"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="No"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Validation:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="english">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) when plan term is not zero (TERM != 0) "/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="python">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="[ &apos;ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})&apos; ${:} &apos;Y&apos; not in [ row[fff] for fff in [ &apos;ACCESS&apos;,&apos;DECOM&apos;,&apos;MAINTAIN&apos;,&apos;MONITOR&apos;] ]  and row[&apos;TERM&apos;] in [1,2] ]"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="sql"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="test:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="{}">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="mandatory">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="INTENT:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="alias">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="MNR Intent"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="length">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="30"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="type">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="String"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="INTENT"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Definition">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="The decommissioning attribute is a field used to identify where decommissioning activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. "/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Validation:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="english">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: If TRANS value is populated, then INTENT must be populated"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="python">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="[ &apos;ST1: ${name} must not be null when TRANS is populated&apos; ${:} ${NIN} and row[&apos;TRANS&apos;] not in ${list_NULL} ]"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="sql"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="test:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="{}">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="mandatory">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="MAINTAIN:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="alias">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Road Maintenance"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="length">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="1"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="type">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="String"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="MAINTAIN"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="domain">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="YESNO"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="values">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Y:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Y"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Road maintenance planned to occur during plan term"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Yes"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="N:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="N"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Road maintenance NOT planned to occur during plan term"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="No"/>
</node>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Validation:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="english">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) when plan term is not zero (TERM != 0) "/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The population of this attribute is mandatory "/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The attribute population must follow the correct coding scheme "/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: A blank or null value is not a valid code "/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="python">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="[ &apos;ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})&apos; ${:} &apos;Y&apos; not in [ row[fff] for fff in [ &apos;ACCESS&apos;,&apos;DECOM&apos;,&apos;MAINTAIN&apos;,&apos;MONITOR&apos;] ]  and row[&apos;TERM&apos;] in [1,2] ]"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="sql"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="test:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="{}">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="mandatory">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="MONITOR:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="alias">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Road Monitoring"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="length">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="1"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="type">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="String"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="MONITOR"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="domain">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="YESNO"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="values">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Y:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Y"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Road maintenance planned to occur during plan term"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Yes"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="N:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="N"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Road maintenance NOT planned to occur during plan term"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="No"/>
</node>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Validation:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="english">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y) when plan term is not zero (TERM != 0) "/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The population of this attribute is mandatory "/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The attribute population must follow the correct coding scheme "/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: A blank or null value is not a valid code "/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="python">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="[ &apos;ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})&apos; ${:} &apos;Y&apos; not in [ row[fff] for fff in [ &apos;ACCESS&apos;,&apos;DECOM&apos;,&apos;MAINTAIN&apos;,&apos;MONITOR&apos;] ]  and row[&apos;TERM&apos;] in [1,2] ]"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="sql"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="test:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="{}">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="mandatory">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="CONTROL1:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="alias">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Access Control / Decom Type"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="length">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="4"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="type">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="String"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="CONTROL1"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="domain">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="CONTROL"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Definition">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="The access control or decommissioning type attributes indicate the method of access control to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute may also be used to identify the type of decommissioning activities scheduled on primary or branch roads that will be constructed during the year."/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="DomainValidation:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="english">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT=""/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="mandatory">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The population of this attribute is mandatory"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Validation:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="english">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The presence and population of CONTROL1 is mandatory where ACCESS = Y"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="python">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="[ &apos;ST1: ${name} must not be null when ACCESS is Y&apos; ${:N} row[&apos;ACCESS&apos;] in [ &apos;Y&apos; ] ]"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="[ &apos;ST1: ${name} must not be null when CONTROL2 is not null&apos; ${:} ${NIN} and row[&apos;CONTROL2&apos;] not in ${list_NULL} ]"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="sql"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="test:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="{}">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="values">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="BERM:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="BERM"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="berm and/or ditch"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="GATE:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="GATE"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="gated / physical barrier"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="PRIV:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="PRIV"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="private land"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="SCAR:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="SCAR"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="scarify and/or plant road"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="SIGN:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="SIGN"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="signed"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="SLSH:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="SLSH"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="pile slash"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="WATX:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="WATX"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="option">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="water crossing"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="mandatory">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="CONTROL2:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="alias">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Access Control / Decom Type 2"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="length">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="4"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="type">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="String"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="name">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="CONTROL2"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="domain">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="CONTROL"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Definition">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Def"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="DomainValidation:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="english">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT=""/>
</node>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="Validation:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="english">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="ST1: The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="python">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="[ &apos;ST1: ${name} must be null or blank when CONTROL1 is blank or null&apos; ${:N} row[&apos;CONTROL1&apos;]  in ${list_NULL}  ]"/>
</node>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="sql"/>
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="test:">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT="{}">
<node CREATED="1562265931211" FOLDED="true" MODIFIED="1562265931211" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="values">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="BERM:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="BERM"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="berm and/or ditch"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="GATE:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="GATE"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="gated / physical barrier"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="PRIV:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="PRIV"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="private land"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="SCAR:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="SCAR"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="scarify and/or plant road"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="SIGN:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="SIGN"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="signed"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="SLSH:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="SLSH"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="pile slash"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="WATX:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="WATX"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="water crossing"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="mandatory">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="geometry_type">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Polygon"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="alias">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Planned Road Corridors (PRC)"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="path">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Plan"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Validation:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="english">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="python"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" POSITION="right" TEXT="OperationalRoadBoundaries:">
<icon BUILTIN="prepare"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="OperationalRoadBoundaries"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="spatial_reference">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="26916"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="fields">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ORBID:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="alias">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Operational Road Boundaries ID"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="length">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="20"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="type">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="String"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ORBID"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Definition:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The operational road boundaries identifier attribute indicates the user defined unique number, label or name assigned to the operational road boundaries. The operational road boundary is the perimeter of, the planned harvest area plus the area from an existing road or planned road corridor to the harvest area within which an operational road is planned to be constructed."/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Validation:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="english">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The population of this attribute is mandatory"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="python"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="sql"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="test:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="{}">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="mandatory">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="geometry_type">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Polygon"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="alias">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Operational Road Boundaries (ORB)"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="shortname">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" STYLE="fork" TEXT="ORB"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="path">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Plan"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Validation:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="english">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="python"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" POSITION="right" TEXT="ExistingRoadUseManagementStrategy:">
<icon BUILTIN="prepare"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ExistingRoadUseManagementStrategy"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="shortname">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ERU"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="spatial_reference">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="26916"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="fields">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ROADID:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="alias">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Road Identifier"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="length">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="30"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="type">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="String"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ROADID"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Definition">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The road identifier attribute is the unique number, label or name assigned to the forest access road that this polygon is a part of."/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Validation:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="english">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: A blank or null value is not a valid code."/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="python"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="test:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="{}">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="mandatory">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ROADCLAS:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="alias">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Road Class"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="length">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="1"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="type">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="String"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ROADCLAS"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="domain">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ROADCLAS"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Definition">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The road class attribute identifies the class of the proposed forest access road in terms of the road use management strategy in the FMP. "/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Validation:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="english">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: A blank or null value is not a valid code."/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="python"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="sql"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="test:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="{}">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="values">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="P:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="P"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Primary"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Primary roads are roads that provide principal access for the management  unit, and are constructed, maintained and used as part of the main road  system on the management unit.  Primary roads are normally permanent  roads. "/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="B:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="B"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="branch"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="A branch road is a road, other than a primary road, that branches off an  existing or new primary or branch road, providing access to, through or  between areas of operations on a management unit."/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="O:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="O"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Operational"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Operational roads are roads within operational road boundaries, other than  primary or branch roads, that provide short-term access for harvest,  renewal and tending operations. Operational roads are normally not  maintained after they are no longer required for forest management  purposes, and are often site prepared and regenerated."/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="mandatory">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="TRANS:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="alias">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Road Transfer Year"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="length">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="4"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="type">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Integer"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="TRANS"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Definition">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The road transfer year attribute indicates a four-digit number representing the first year of the 5-year operational term that the transfer of responsibility to the MNR is anticipated to take effect. If there is no intent to transfer responsibility to MNR during the future 20-year period there is no need to specify a year. "/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Validation:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="english">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="A blank or zero value is a valid code"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="If TRANS value is populated, then INTENT must be populated"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The value must be greater than or equal to the plan term start year">
<icon BUILTIN="full-1"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="python">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="[ &apos;ST1: ${name} must be &gt;=  ${planyear} &apos; ${:N} row[&apos;${name}&apos;]  &lt; ${planyear} ]   ">
<icon BUILTIN="full-1"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="sql"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="test:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="{}">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="mandatory">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ACYEAR:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="alias">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Access Control Year"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="length">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="4"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="type">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Integer"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ACYear"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Definition">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The access control year attribute indicates a four-digit number representing the expected fiscal year (April 1 to March 31) that the access control is anticipated to take effect. "/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Validation:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="english">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="A zero value is a valid code"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The value must be greater than or equal to the plan term start year"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: If access control year does not equal zero (ACYEAR NOT 0) then access control must be yes"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="python">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="[ &apos;ST1: If ${name} = 0 then ACCESS must be N&apos; ${:} ${NIN} and row[&apos;ACCESS&apos;] &lt;&gt; &apos;Y&apos;  ] "/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="sql"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="test:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="{}">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="mandatory">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ACCESS:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="alias">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Access Control"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="length">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="1"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="type">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="String"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ACCESS"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="domain">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="YESNO"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Definition">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The road access control attribute is a field used to identify where access control activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. "/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="values">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Y:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Y"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="yes"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="N:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="N"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="No"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Validation:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="english">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y)"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The population of this attribute is mandatory "/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: The attribute population must follow the correct coding scheme "/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="ST1: A blank or null value is not a valid code "/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="python">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="[ &apos;ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})&apos; ${:} &apos;Y&apos; not in [ row[fff] for fff in [ &apos;ACCESS&apos;,&apos;DECOM&apos;,&apos;MAINTAIN&apos;,&apos;MONITOR&apos;] ] ]"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="sql"/>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="test:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="{}">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="mandatory">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="DECOM:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="alias">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Decommissioning"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="length">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="1"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="type">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="String"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="DECOM"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="domain">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="YESNO"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Definition">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Def"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="values">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Y:">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="name">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="Y"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="option">
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="yes"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="desctext"/>
</node>
<node CREATED="1562265931212" FOLDED="true" MODIFIED="1562265931212" TEXT="N:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="N"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="No"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Validation:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="english">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y)"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The population of this attribute is mandatory "/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The attribute population must follow the correct coding scheme "/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: A blank or null value is not a valid code "/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: If decommissioning is yes (DECOM = Y) then access control must be no (ACCESS =N)"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="python">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="[ &apos;ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})&apos; ${:} &apos;Y&apos; not in [ row[fff] for fff in [ &apos;ACCESS&apos;,&apos;DECOM&apos;,&apos;MAINTAIN&apos;,&apos;MONITOR&apos;] ] ]"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="[&apos;ST1: If decommissioning is yes (DECOM = Y) then access control must be no (ACCESS =N)&apos; ${:N} row[&apos;${name}&apos;] == &apos;Y&apos; and row[&apos;ACCESS&apos;] == &apos;Y&apos; ]"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="sql"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="test:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="{}">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="mandatory">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="INTENT:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="alias">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="MNR Intent"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="length">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="30"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="type">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="String"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="INTENT"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Definition">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="The decommissioning attribute is a field used to identify where decommissioning activities are planned to occur during the 10-year planning period on primary or branch roads that will be constructed during the 10-year planning period. "/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Validation:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="english">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: If TRANS value is populated, then INTENT must be populated"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="python">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="[ &apos;ST1: ${name} must not be null when TRANS is populated&apos; ${:} ${NIN} and row[&apos;TRANS&apos;] not in ${list_NULL} ]"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="sql"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="test:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="{}">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="mandatory">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="MAINTAIN:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="alias">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Road Maintenance"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="length">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="1"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="type">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="String"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="MAINTAIN"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="domain">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="YESNO"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="values">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Y:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Y"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Road maintenance planned to occur during plan term"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Yes"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="N:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="N"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Road maintenance NOT planned to occur during plan term"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="No"/>
</node>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Validation:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="english">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y)"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The population of this attribute is mandatory "/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The attribute population must follow the correct coding scheme "/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: A blank or null value is not a valid code "/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="python">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="[ &apos;ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})&apos; ${:} &apos;Y&apos; not in [ row[fff] for fff in [ &apos;ACCESS&apos;,&apos;DECOM&apos;,&apos;MAINTAIN&apos;,&apos;MONITOR&apos;] ] ]"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="sql"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="test:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="{}">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="mandatory">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="MONITOR:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="alias">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Road Monitoring"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="length">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="1"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="type">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="String"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="MONITOR"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="domain">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="YESNO"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="values">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Y:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Y"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Road maintenance planned to occur during plan term"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Yes"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="N:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="N"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Road maintenance NOT planned to occur during plan term"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="No"/>
</node>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Validation:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="english">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record (DECOM = Y or MAINTAIN = Y or MONITOR = Y or ACCESS = Y)"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The population of this attribute is mandatory "/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The attribute population must follow the correct coding scheme "/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: A blank or null value is not a valid code "/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="python">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="[ &apos;ST1: At a minimum, one of Decommissioning, Maintenance, Monitoring or Access Control must occur for each record. (${name})&apos; ${:} &apos;Y&apos; not in [ row[fff] for fff in [ &apos;ACCESS&apos;,&apos;DECOM&apos;,&apos;MAINTAIN&apos;,&apos;MONITOR&apos;] ] ]"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="sql"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="test:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="{}">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="mandatory">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="RESPONS:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="alias">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Road Responsibility"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="length">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="3"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="type">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="String"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="RESPONS"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="domain">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="RESPONS"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="values">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SFL:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SFL"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Sustainable Forest Licensee"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="MNR:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="MNR"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Ministry of Natural Resources and Forestry"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="mandatory">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Validation:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="english">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: Attribute population must follow the correct coding scheme for roads that are the responsibility of the SFL or MNR. Roads that are the responsibility of other parties are user defined (e.g., OTH for other, MTO for Ministry of Transportation, PRV for private, JNT for joint)."/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: A blank or null value is not a valid code"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="At a minimum, one record should equal SFL"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="python"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="sql"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="test:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="{}">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="CONTROL1:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="alias">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Access Control / Decom Type"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="length">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="4"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="type">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="String"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="CONTROL1"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="domain">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="CONTROL"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Definition">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="The access control or decommissioning type attributes indicate the method of access control to be implemented during the year on primary or branch roads that will be constructed during the year. This attribute may also be used to identify the type of decommissioning activities scheduled on primary or branch roads that will be constructed during the year."/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="DomainValidation:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="english">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT=""/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="values">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="BERM:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="BERM"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="berm and/or ditch"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="GATE:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="GATE"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="gated / physical barrier"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="PRIV:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="PRIV"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="private land"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SCAR:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SCAR"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="scarify and/or plant road"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SIGN:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SIGN"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="signed"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SLSH:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SLSH"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="pile slash"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="WATX:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="WATX"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="water crossing"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Validation:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="english">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The presence and population of CONTROL1 is mandatory where ACCESS = Y"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="python">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="[ &apos;ST1: ${name} must not be null when ACCESS is Y&apos; ${:N} row[&apos;ACCESS&apos;] in [ &apos;Y&apos; ] ]"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="[ &apos;ST1: ${name} must not be null when CONTROL2 is not null&apos; ${:} ${NIN} and row[&apos;CONTROL2&apos;] not in ${list_NULL} ]"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="sql"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="test:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="{}">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="mandatory">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="CONTROL2:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="alias">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Access Control / Decom Type 2"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="length">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="4"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="type">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="String"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="CONTROL2"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="domain">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="CONTROL"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Definition">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Def"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="DomainValidation:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="english">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT=""/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="Validation:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="english">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="ST1: The second field must only have a control type when the first field has a control type (error when CONTROL1 = null and CONTROL2 != null)"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="python">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="[ &apos;ST1: ${name} must be null or blank when CONTROL1 is blank or null&apos; ${:N} row[&apos;CONTROL1&apos;]  in ${list_NULL}  ]"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="sql"/>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="test:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="{}">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="values">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="BERM:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="BERM"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="berm and/or ditch"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="GATE:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="GATE"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="gated / physical barrier"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="PRIV:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="PRIV"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="private land"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SCAR:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SCAR"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="scarify and/or plant road"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SIGN:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SIGN"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="signed"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SLSH:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="SLSH"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="pile slash"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="WATX:">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="name">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="WATX"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="option">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="water crossing"/>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="mandatory">
<node CREATED="1562265931213" FOLDED="true" MODIFIED="1562265931213" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="geometry_type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Polyline"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Existing Road Use Management Strategy (ERU)"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="path">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Plan"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" POSITION="right" TEXT="PlannedAggregateExtractionAreas:">
<icon BUILTIN="prepare"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="PlannedAggregateExtractionAreas"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="spatial_reference">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="26916"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="fields">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="AGAREAID:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Aggregate Extraction Area ID"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="length">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="15"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="String"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="AGAREAID"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Definition:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The aggregate extraction area identifier attribute indicates the unique identifier for the area where forestry aggregate pits may be established."/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="test:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="{}">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="mandatory">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="geometry_type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Polygon"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Planned Aggregate Extraction Areas (PAG)"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="path">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Plan"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="mandatory">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="shortname">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" STYLE="fork" TEXT="PAG"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" POSITION="right" TEXT="RenewalAndTending:">
<icon BUILTIN="prepare"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="RenewalAndTending"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="shortname">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="PRT"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="spatial_reference">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="26916"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="fields">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="TERM:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Plan Term"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="length">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="1"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Integer"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="TERM"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="domain">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="TERM"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Definition:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The plan term attribute indicates the five-year operational plan term or the next tenyear plan period in which the planned road activity and/or use strategy will occur. "/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: A zero or null value is not a valid code"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="test:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="{}">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="mandatory">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="values">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="1:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="1"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="First 5-year Operational Plan Term (1-5)"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="1-5"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="2:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="2"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Second 5-year Operational Plan Term (6-10)"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="6 -10"/>
</node>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="HERB:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Herbicide Application"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="length">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="1"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="String"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="HERB"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="domain">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="YESNO"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Definition:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The herbicide application attribute indicates the area as proposed for the aerial application of herbicide and identifies it as an area of special public interest. "/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="values">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="yes"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="No"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="test:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="{}">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="mandatory">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="INSECT:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Insecticide Application"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="length">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="1"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="String"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="INSECT"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="domain">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="YESNO"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Definition:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The insecticide application attribute indicates the area as proposed for the aerial application of insecticide, as a result of the application of the planning procedure for insect pest management programs, and identifies it as an area of special public interest."/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="values">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="yes"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="No"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="test:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="{}">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="mandatory">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="PB:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Prescribed Burn"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="length">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="1"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="String"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="PB"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="domain">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="YESNO"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Definition:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The prescribed burn attribute indicates the area as a candidate for a high complexity burn and identifies it as an area of special public interest. "/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="values">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="yes"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="No"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="test:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="{}">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="mandatory">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="PEST:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Insect Pest Management"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="length">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="1"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="String"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="PEST"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="domain">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="YESNO"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Definition:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The insect pest management attribute indicates the area as eligible for insect pest management and identifies it as an area of special public interest. "/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="values">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="yes"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="No"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="test:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="{}">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="mandatory">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="IMPROVE:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Tree Improvement Activities"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="length">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="1"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="String"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="IMPROVE"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="domain">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="YESNO"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Definition:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The tree improvement activities attribute indicates the area to support the production of improved seed. "/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="values">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Y"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="yes"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="N"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="option">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="No"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="desctext"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The population of this attribute is mandatory."/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: The attribute population must follow the correct coding scheme"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="test:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="{}">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="mandatory">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="geometry_type">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Polygon"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="alias">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Renewal And Tending (PRT)"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="path">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Plan"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="Validation:">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="english">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="python"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="sql"/>
</node>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" POSITION="right" TEXT="PlannedClearcuts:">
<icon BUILTIN="go"/>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="name">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="PlannedClearcuts"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="spatial_reference">
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="26916"/>
</node>
<node CREATED="1562265931214" FOLDED="true" MODIFIED="1562265931214" TEXT="fields">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="PCCID:">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="alias">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Clearcut ID"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="length">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="20"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="type">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="String"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="name">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="PCCID"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Validation:">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="english">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="ST1: The presence of this attribute in the file structure of the layer is mandatory"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="ST1: A blank or null value is not a valid code"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="python"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="sql"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="test:">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="{}">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="mandatory">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="The presence of this attribute in the file structure of the layer is mandatory"/>
</node>
</node>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="geometry_type">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Polygon"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="alias">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Planned Clearcuts (PCC)"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="path">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Plan"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Definition">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT=" "/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="shortname">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="PCC"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Validation:">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="english">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="This table must contain all mandatory fields"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="python"/>
</node>
</node>
<node CREATED="1562265931215" ID="ID_971078158" MODIFIED="1562265931215" POSITION="left" STYLE="fork" TEXT="metadata:">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="submission_type">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="FMP_2009"/>
</node>
<node CREATED="1562265931215" ID="ID_1471103421" MODIFIED="1562265931215" TEXT="submission_min_year">
<node CREATED="1562265931215" FOLDED="true" ID="ID_329625999" MODIFIED="1562265995037" TEXT="2007"/>
</node>
<node CREATED="1562265931215" ID="ID_1321484869" MODIFIED="1562265931215" TEXT="submission_max_year">
<node CREATED="1562265931215" FOLDED="true" ID="ID_663173767" MODIFIED="1562266001363" TEXT="2008"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="id">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="fi_portal_id"/>
</node>
<node CREATED="1562265931215" ID="ID_10226275" MODIFIED="1562265931215" TEXT="path">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="{fmu}\{product}\{year}"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="DeferredLayerGDB">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.atx"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.spx"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.gdbindexes"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.gdbtablx"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.gdbtable"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.freelist"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" STYLE="fork" TEXT="*/gdb"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*/timestamps"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="DeferredLayerGPKG">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.gpkg"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Document">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" STYLE="fork" TEXT="*_TXT_PlanText.pdf"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*_TBL_Tables.pdf"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*_TXT_Sum.pdf"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*_TXT_SumFR.pdf"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" STYLE="fork" TEXT="*_TXT_SuppDoc.pdf"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Document_Directory">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT=""/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Internal_zipfile">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" STYLE="fork" TEXT="*.zip"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Layer">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.e00"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.shp"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.dbf"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.shx"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.prj"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.shp.xml"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Model">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*MODEL*"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Maps">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="MU{fmu_id}_{planyear}_{product}_P{phase}_MAP*.eps"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="*.eps"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Maps_Directory">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" STYLE="fork" TEXT="Maps"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Model_Directory_not_really...">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="MU{fmu_id}_{planyear}_{product}_P{phase}_MODEL"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Catchall">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT=""/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Catchall_Directory"/>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Trash">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Thumbs.db"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Layer_Filename">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="mu{fmu_id}_{year}fmpdp"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="DeferredLayerGPKG_Directory">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="LAYERS"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="DeferredLayerGDB_Directory">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="LAYERS/mu{fmu_id}_{year}_{product}.gdb"/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Internal_zipfile_Directory">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT=""/>
</node>
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT="Model_Directory">
<node CREATED="1562265931215" FOLDED="true" MODIFIED="1562265931215" TEXT=""/>
</node>
</node>
</node>
</map>
