
#filechecker.py
# This forms the basis of rolling through all rows, running a list of 
# procedures on each one to test validity.
#
# It ought to work on arcpy or ogr, subject to appropriate data format
#

import sys
import time
import json
import os



from spcomp import test_spcomp, test_SpeciesInList
from fimdate import fimdate

from pg_db import pg_db

class jsonloader():
	json = None
	
	def __init__(self,jsonfilename):
		self.getjson(jsonfilename)

#	def __getattr__(self,attr):
#		return self.json[attr]
		
	def getjson(self,jsonfilename):
		#print "jsonfilename: %s"%(jsonfilename)
		json_data=open(jsonfilename)
		self.json = json.load(json_data)
		json_data.close()
		return self.json

	def findTable(self,tabletype):
		for n in self.json:
			try:
				if n['name'] == tabletype: return n
			except KeyError:
				pass
			try:
				if n['shortname'] == tabletype: return n
			except KeyError:
				pass

	def getTableList(self):
		return [n['name'] for n in self.json ]

	#def getTableListShort(self):
		#try:
			#return [n['shortname'] for n in self.json ]
		#except KeyError as e:
			#print "missing a shortname! \n%s"%(e)
			#return []

	def getTableListShort(self):
		retlist = []
		for n in self.json :
			try:
				retlist.append(n['shortname'])
			except KeyError as e:
				print "missing a shortname in %s! \n%s"%(n['name'],e)
				print self.dumpkey(n)
		return retlist

	def dumpkey(self,key):
		return str(key)
		

	def close(self):
		pass

	def __exit__(self, *err):
		pass
		
	def __enter__(self, *err):
		return self
	
	def __del__(self):
		pass

		
class tablechecker( jsonloader ):
	json = ''
	tablejson = ''
	rowvalidation = dict()
	shapefile = None
	
	def __init__(self,jsonfilename,tablefilename,tabletype,year=0):
		self.json = self.getjson(jsonfilename)
		
		#print "getTableList(self): %s\n\n"%(self.getTableList())
		#print "getTableListShort(self): %s\n\n"%(self.getTableListShort())
		
		self.tablejson = self.findTable(tabletype)
		
		self.shapefile = fileloader(tablefilename,layer=str(tabletype),geometry_type=self.tablejson['geometry_type'])
		#if self.tablejson['Validation:']:
		#	print self.tablejson['Validation:']['python']
		self.year = year
		
		self.LoadRowValidations()

	def __exit__(self, *err):
		self.shapefile.close()


		
	def validateTable_HTMLfragment(self):
		msg = ''
		msg+= "<h2>Table Validation</h2>\n<ul>\n"
		msg += "<pre>\n" #+ str(self.tablejson['name'])+"\n"
		for v in self.LoadTableValidations():
			msg+= (v+"\n")
		msg += "</pre>"
		
		try:
			fails = self.TableValidations()
			for f in fails:
				if isinstance(f,str) or isinstance(f,unicode):
					msg+= "\t<li>%s</li>\n"%(f)
				else:
					msg+= "\t<ul>\n"
					for ff in f:
						msg+= "\t\t<li>%s</li>\n"%(ff)
					msg+= "\t</ul>\n"
			msg+= "</ul>\n"
			
		except KeyError as e:
			pass
		return msg
		
		
	def LoadTableValidations(self):
		if isinstance(self.tablejson['Validation:']['python'] , str) or isinstance(self.tablejson['Validation:']['python'] , unicode) :
			valstatements = [ self.tablejson['Validation:']['python'] ]
		else:
			valstatements = self.tablejson['Validation:']['python']
		self.tablevalidation = valstatements
		return self.tablevalidation

	def TableValidations (self):
		valstatements = self.LoadTableValidations()
		#valstatements.append("[ 'POLYID Field values must be unique' for f in ['POLYID' ] if not self.uniqueValueTest(f) ] ")
		
		fields=self.shapefile.fields
		#print fields 
		
		retlist = list()
		for f in valstatements:
			try:
				result = eval(''+f+'\n')
				retlist.append(result)
			except KeyError as e:
				pass
				#result = str(e) + " Field Not Found"
				#print result
				#retlist.append(result)
		return retlist
		
		#return [eval(x) for x in [''+f+'\n' for f in valstatements ] ]
	
	def LoadRowValidations(self):
		self.rowvalidation = dict()
		
		fields=self.shapefile.fields
		try:
			fieldsjson=self.tablejson['fields']
		except KeyError as e:
			#This table has no fields - ignore
			return [ ]
		
		#DONE - when one field, comes back as the field entry, not a list (AR SGR Update table)
		if not isinstance(fieldsjson,list):
			fieldsjson = [ fieldsjson ]
			
		for field in fieldsjson:
			try:
				if isinstance(field['Validation:']['python'] , str) or isinstance(field['Validation:']['python'] , unicode) :
					valstatements = [ field['Validation:']['python'] ]
				else:
					valstatements = field['Validation:']['python']				
			except KeyError as e:
				#No Validations worth bothering with
				return [ ]
				
			#if field['name'] == 'PRI_ECO': 	valstatements.append( "[ '%s not a valid Geographic Range'%(row[${name}[0:1] ) for s in    ")
			self.rowvalidation[field['name']] = [''+f.replace('${name}',field['name']).replace('${year}',str(self.year))+'\n' for f in valstatements ]
		#print self.rowvalidation
			
	def validateRows_HTMLfragment(self,limit=None):
		msg = ''
		
		msg += "<pre>" + str(self.tablejson['name'])+"\n"
		for k,v in self.rowvalidation.iteritems():
			msg+= (k+":\n")
			for vv in v:
				msg+= ("\t"+vv)
		msg += "</pre>"
		#msg += ("<pre>"+str(self.tablejson['name'])+'\n'+str(self.rowvalidation)+"</pre><br>" ).replace(r'\n",',r'\n",'+"\n").replace(r'\n"],',r'\n"],'+"\n")
		i=0
		errorcount = 0
		
		nulldetectorlist = set(self.rowvalidation.keys()).intersection(set(self.shapefile.fields))
		nulldetectorstatement = ' and '.join( [ "(str(row['%s']).strip() in ['0','','None','0.0','0.00'])"%(f)  for f in nulldetectorlist ])
		
		while True:
			try:
				row = self.shapefile.row()
			except StopIteration:
				break			
			except KeyError as e:
				pass	
			#if (row['POLYTYPE'] == 'FOR'): 	print row

			#ignore rows with all None or strip() == '' or 0 values
			try:
				if  eval( nulldetectorstatement ) :
					continue
			except KeyError as e:
				pass

			i+=1
			if 'POLYID' in self.shapefile.fields :
				polyid = row['POLYID']
			else:
				polyid = str(i)
			
			#print polyid
			
			#fails =  [eval(x) for x in [''+f+'\n' for f in self.rowvalidation ] ]		
			for fieldname,listOfValidations in self.rowvalidation.iteritems()	:
				#print fieldname,listOfValidations
				if fieldname.upper() in self.shapefile.fields:
					#Only testing fields that exist, mostly...
					#fails = [eval(x) for x in listOfValidations ]
					 
					for test in listOfValidations :
						
						if test in [u'python\n'] : continue #Ignore blanks - this is how they look...
						#print test
						try:
							results = eval(test)
						except TypeError as e:
							print "POLYID %s has TypeError: %s in %s "%(polyid,e,test)  
							results = [ "POLYID %s has TypeError: %s "%(polyid,e) ] 
						except ZeroDivisionError as e:
							print "POLYID %s has ZeroDivisionError: %s in %s "%(polyid,e,test) 
							results = [ "POLYID %s has ZeroDivisionError: %s "%(polyid,e) ] 
						except NameError as e:
							print "POLYID %s has KeyError: %s in %s "%(polyid,e,test) 
							results = [ "POLYID %s has NameError: %s "%(polyid,e) ] 
						except KeyError as e:
							#print "POLYID %s has error: %s in %s "%(polyid,e,test) 
							results = [  ] 														
						except SyntaxError as e:
							print "ID  %s has SyntaxError: %s in %s "%(polyid,e,test) 
						except ValueError as e:
							print "ID  %s has ValueError: %s in %s "%(polyid,e,test) 

						for f in results:
							msg+= "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n"%(polyid,str(row[fieldname]),f,test)
							errorcount += 1
					
			if limit is not None:
				if i > limit : break			


		msgheader = "<h3>Row Validations</h3>\n" 
		if errorcount > 0:
			msgheader += "<table>\n<tr>"
			if 'POLYID' in self.shapefile.fields :
				msgheader+= "<td>POLYID</td>"
			else:
				msgheader+= "<td>Row #</td>"
			msgheader+= "<td>Value</td>"
			msgheader+= "<td>Failure Statement</td></tr>\n"
			msg = msgheader + msg + "</table>\n"
		else:
			msg = msgheader + msg
		msg += "<p>Count of Rows Checked: %d</p>"%(i)
		msg +=  "<p>Count of Row-level Errors: %d</p>"%(errorcount)
		return msg


	def uniqueValueTest	(self,fieldname):
		return self.shapefile.uniqueValueTest(fieldname)

	def contextTest(self):
		"""This function runs SQL test from the sql validation section
			Use the ${WKT} variable in the query to include the shape in the query as WKT.
			Use the ${PLAN_ID} variable to indicate the most recent FMP to which this AR/AWS relates.
			Use the ${PLAN_AWS_ID} variable to indicate a list of AWS and Plan submission id's.
			
			We should sanitize the inputs, but for now... #FIXME Bobby Tables
		"""
		pass
				
class fileloader():
	fields = [ ]
	dataSource = None
	def __init__(self,filename,layer=0,sql_statement=None,whereclause=None,geometry_type='Polygon'):
		self.usingOGR = False
		self.usingArcpy = False
		try:
			import arcpy
			print "using arcpy"
			self.usingArcpy = True
		except ImportError as e:
			pass
			
			
		try:
			# Import OGR for reading all those pesky shapes:
			try:
				from osgeo import ogr
			except:
				import ogr
			ogr.UseExceptions()
			self.dataSource = ogr.Open(filename)
			if self.dataSource is not None:
				#This is where ogr and arcpy differ... None means the file is not openable...
				self.dataSource.Destroy()
				self.dataSource = None
			self.usingOGR = True
		except ImportError, NameError:
			pass
			
		if self.usingArcpy :
			tmp = arcpy.da.SearchCursor(filename,field_names=['*'])
			tmpfields = tmp.fields
			newfields = [ f  for f in tmp.fields if f != u'Shape' ]
			newfields.append("SHAPE@")
			
			self.arcpycursor = arcpy.da.SearchCursor(filename,field_names=newfields)
			#self.layerDefinition = arcpy.Describe(filename)
			#print [ "%s: %s"%(c.name,c.dataType) for c in self.layerDefinition ]
			self.fields = [ f.upper() for f in self.arcpycursor.fields ]
			
		elif self.usingOGR:
			if (os.path.exists(filename)):
				#If the filename exists and is not a file GDB then use layer 0 (e.g. Shapefile)			
				#If the filename exists and is a File GDB, then we need a layer name. It defaults to 0, which is the first layer. Hopefully that's what they meant...
				self.dataSource = ogr.Open(filename)
				#print "File exists. Opened by Driver %s"%(self.dataSource.GetDriver().GetName() )
				#print "layers: %s"%(self.listLayers())
				if self.dataSource.GetDriver().GetName() == 'AVCBin' :
					topology_type = {'Polygon':'PAL','Point':'LAB','Polyline':'ARC'}[geometry_type]
					#print 'Using Topolygy type %s'%(topology_type)
					self.ogrDataLayer = self.dataSource.GetLayer(topology_type)
					#print "Using layer %s"%(str(layer))
				else:
					self.ogrDataLayer = self.dataSource.GetLayer(layer)		
			elif filename[0:3] == 'PG:' :
				#Handle this as a postgis layer...
				#If the filename is a PG string, then we need a layer name, but layer 0 is also valid, although nonsense...
				
				## PostgreSQL available?
				driverName = "PostgreSQL"
				drv = ogr.GetDriverByName( driverName )
				if drv is None:
					print "%s driver not available.\n" % driverName
				self.dataSource = drv.Open(filename)
				#print "Using layer %s"%(str(layer))
				self.ogrDataLayer = self.dataSource.GetLayer(layer)
			elif ( ogr.GetDriverByName( "OpenFileGDB" ) is not None ) and ( os.path.exists( os.path.split(filename)[0] ) or os.path.exists( os.path.split(os.path.split(filename)[0])[0]) ) :
				parent,child = os.path.split(filename)
				grandparent,grandchild = os.path.split(parent)[0], os.path.join(os.path.split(parent)[1],child) 
				#If the filename does not exist, look in parent and grandparent for extant files to treat as File GDB and then use the child as a layername
				driverName = "OpenFileGDB"
				drv = ogr.GetDriverByName( driverName )

				if drv is None:
					print "%s driver not available.\n" % driverName
				else:
					#Try opening the parent as an OpenFileGDB...
					if os.path.exists(parent):
						self.dataSource = drv.Open(parent)
					elif os.path.exists(grandparent):
						self.dataSource = drv.Open(grandparent)
					
					if self.dataSource is not None :
						layer = child
						self.ogrDataLayer = self.dataSource.GetLayer(child)
						if self.ogrDataLayer is None:
							layer = grandchild
							self.ogrDataLayer = dataSource.GetLayer(grandchild)
						#print "Using layer %s"%(str(layer))
			
			
			self.layerDefinition = self.ogrDataLayer.GetLayerDefn()
			self.fields = [ self.layerDefinition.GetFieldDefn(i).GetName().upper() for i in range(self.layerDefinition.GetFieldCount()) ]

	def close(self):
		pass
		
	def row(self):
		if self.usingOGR:
			try:
				inFeature = self.ogrDataLayer.GetNextFeature()
				values = [ inFeature.GetField(i) for i in range(len(self.fields)) ]
				theRow =  dict(zip(self.fields,values))
				theRow['shape'] = inFeature.GetGeometryRef()
				self.currentRow = theRow
				return theRow
			except AttributeError as e:
				raise StopIteration
		elif self.usingArcpy:
			inFeature = self.arcpycursor.next()
			theRow =  dict(zip(self.fields,inFeature))
			self.currentRow = theRow
			return theRow
			
		else:
			raise StopIteration
	
	def reset(self):
		"""Reset to first row in table"""
		if self.usingArcpy :
			self.arcpycursor.reset()
		elif self.usingOGR :	
			self.ogrDataLayer.ResetReading()
			
	def uniqueValueTest(self,fieldname):
		"""Count Rows in Table. Warning - resets place in table..."""
		self.reset()
		u = []
		try:
			while True:
				u.append( self.row()[fieldname])
		except StopIteration :
			pass
		self.reset()
		su = set(u)
		return ( len(u) == len(su) ) #True means they are the same...
		

	def numRows(self):
		"""Count Rows in Table. Warning - resets place in table..."""
		if self.usingArcpy :
			#SearchCursor doesn't tell us how many... so go count...
			i=0
			self.reset()
			try:
				while True:
					self.arcpycursor.next()
					i += 1
			except StopIteration :
				pass
			self.reset()
			
		elif self.usingOGR :	
			self.ogrDataLayer.GetFeatureCount(force = 1)
			self.reset()
			
	def numLayers(self):
		return len(self.dataSource)
		
	def listLayers(self):
		return [ l.GetName() for l in self.dataSource ]
#	def 

	
if __name__ == "__main__":
#	c = get_iterable_rows(filename=r'Z:\GIS\DonNixon\onaman_addition_04042006.shp')
#	print get_next_row(c)
	verbose = True
	starttime = time.clock()
	if verbose: print "Starttime:",starttime
	###############################################################
	# START OF TESTING CODE
	###############################################################

#	c = fileloader(filename=r'Z:\GIS\DonNixon\onaman_addition_04042006.shp')
#	while True:
#		try:
#			r = c.row()
#			#if r is None: break 
#			print r
#		except StopIteration:
#			break
	jsonfilename=r'w:\epcm\roundtrip.json'
	jsonfilename=r'C:\Users\carran\Documents\src\ePCM\roundtrip.json'
	jsonfilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\Projects\ePCM\roundtrip.json'

	tablefilename=r'Z:\GIS\EPC\EPCtest.shp'
	tablefilename=r'C:/Users/carran/Documents/GIS/EPCTest.shp'
	tablefilename=r'C:/Users/carran/Documents/GIS/wab_efri.shp'
	tablefilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Wabigoon\FMP\2018\Wabigoon_PCM2018_shp\mu130_18pcm00.shp'
	tablefilename=r'Z:\GIS\Wabigoon_2018\Wabigoon_PCM2018_shp\mu130_18pcm00.shp'
	tablefilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Kenora\AR\2015\E00\mu644_15hrv00'
	#tabletype = r'Enhanced Planning Composite'
	tabletype='HRV'
	jsonfilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\Projects\ePCM\json_to_mindmap\AR_test.json'
	jsonfilename = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0],'AR.json')
	#print "jsonfilename: %s"%(jsonfilename)
	
	#tablefilename=r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Kenora\AR\2015\_data\FMP_Schema.gdb\AnnualReports\SilviculturalGroundRuleUpdate'
	#tabletype='SilviculturalGroundRuleUpdate'
		
		
	t = tablechecker(jsonfilename=jsonfilename,
		tablefilename=tablefilename,
		tabletype=tabletype)	
		
		
	#print "POLYID UNIQUE? %s"%(t.shapefile.uniqueValueTest('POLYID'))

	tablevalidation_output = t.validateTable_HTMLfragment()
	rowvalidation_output = t.validateRows_HTMLfragment(limit=None)
	
	f = open('tablereport.html','w')
	f.write('<html><head>\n')
	f.write('<link rel="stylesheet" type="text/css" href="FMPstyle.css">')
	f.write('<title>%s test for %s</title>\n'%(tabletype,tablefilename))
	f.write('<META HTTP-EQUIV="refresh" CONTENT="100">\n')
	f.write('</head><body>\n')
	f.write('<h1>%s test for %s</title>\n'%(tabletype,tablefilename))
	f.write( tablevalidation_output )
	f.write( rowvalidation_output )
	f.write('</body></html>\n')
	
	###############################################################
	# END OF TESTING CODE
	###############################################################
	endtime = time.clock()
	if verbose: print "Endtime:",endtime
	if verbose: print "Seconds Elapsed:", endtime - starttime
	if verbose: print "Minutes Elapsed:", round(( endtime - starttime ) /60,0)
	


