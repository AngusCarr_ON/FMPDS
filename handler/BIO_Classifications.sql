CREATE VIEW BioClass AS
SELECT b.fid,
    ecopri,

    -- Caribou Capable Winter ( Boolean, so 1 / 0 )
    ecopri in ('B012', 'B024', 'B026', 'B034', 'B035', 'B037', 'B038', 'B049', 'B050', 'B064', 'B065', 'B114', 'B126', 'B127', 'B128', 'B136', 'B137', 'B138', 'B163', 'B164', 'B165', 'B179', 'B180', 'B181', 'B222', 'B223') AS capwint,

    --Caribou Capable Refuge
    ecopri in ('B012', 'B024', 'B026', 'B034', 'B035', 'B036', 'B037', 'B038', 'B049', 'B050', 'B052', 'B053', 'B064', 'B065', 'B067', 'B068', 'B082', 'B083', 'B097', 'B098', 'B099', 'B114', 'B126', 'B127', 'B128', 'B136', 'B137', 'B138', 'B139', 'B140', 'B141', 'B163', 'B164', 'B165', 'B179', 'B180', 'B181', 'B222', 'B223') AS capref,

     -- Winter Suitability
    CASE
        WHEN b.plan_start_age >= c.cwu_on AND b.plan_start_age <= c.cwu_off THEN 'cwu'
        WHEN b.plan_start_age >= c.cwp_on AND b.plan_start_age <= c.cwp_off THEN 'cwp'
        WHEN ecopri IN ('B164', 'B165') THEN 'cwp'
        WHEN ecopri IN ('B136', 'B137', 'B138') THEN 'cwu'
        ELSE NULL
    END AS wintsuit,


    -- Caribou Refuge Habitat
    CASE
        WHEN b.plan_start_age >= c.cru_on AND b.plan_start_age <= c.cru_off THEN 'cru'
        WHEN b.plan_start_age >= c.crp_on AND b.plan_start_age <= c.crp_off THEN 'crp'
        WHEN ecopri IN ('B012', 'B024', 'B026', 'B034', 'B035', 'B036', 'B037', 'B038', 'B049', 'B050', 'B052', 'B053', 'B064', 'B065', 'B067', 'B068', 'B082', 'B083', 'B097', 'B098', 'B099', 'B114', 'B126', 'B127', 'B128', 'B136', 'B137', 'B138', 'B139', 'B140', 'B141', 'B163', 'B164', 'B165', 'B179', 'B180', 'B181', 'B222', 'B223') THEN 'crp'
        ELSE NULL
    END AS refuge,

    -- Moose Productivity
    g.mooseprod AS mooprod,

    CASE
        WHEN b.plan_start_age < 35 AND gr.plan_start_ht < 10.0 THEN 'browse'
        WHEN sf.nwsfu IN ('PoSha', 'PoDee', 'BwSha', 'BwDee', 'OthHd', 'HrDom', 'HrdMw', 'ConMx') AND b.plan_start_age >= 35 OR gr.plan_start_ht >= 10.0 THEN 'hrdmix'
        WHEN sf.nwsfu IN ('UplCe', 'OCLow', 'SbLow', 'SbSha', 'SbDee', 'PjSha', 'PjDee', 'SbMx1', 'PjMx1') AND b.plan_start_age >= 70 THEN 'matcon'
        WHEN sf.nwsfu IN ('BfPur', 'BfMx1') AND b.plan_start_age >= 60 THEN 'matcon'
        WHEN sf.nwsfu IN ('PwDom', 'PrDom', 'PrwMx') AND b.plan_start_age >= 80 THEN 'matcon'
        WHEN sf.nwsfu IN ('UplCe', 'OCLow', 'SbLow', 'SbSha', 'SbDee', 'PjSha', 'PjDee', 'SbMx1', 'PjMx1') AND b.plan_start_age >= 35 AND b.plan_start_age < 70 THEN 'youngcon'
        WHEN sf.nwsfu IN ('BfPur', 'BfMx1') AND b.plan_start_age >= 35 AND b.plan_start_age < 60 THEN 'youngcon'
        WHEN sf.nwsfu IN ('PwDom', 'PrDom', 'PrwMx') AND b.plan_start_age >= 35 AND b.plan_start_age < 80 THEN 'youngcon'
        ELSE NULL
    END AS moopst,

    CASE
        WHEN (b.plan_start_age + 10) < 35 THEN 'browse'
        WHEN sf.nwsfu IN ('PoSha', 'PoDee', 'BwSha', 'BwDee', 'OthHd', 'HrDom', 'HrdMw', 'ConMx') AND (b.plan_start_age + 10) >= 35 THEN 'hrdmix'
        WHEN sf.nwsfu IN ('UplCe', 'OCLow', 'SbLow', 'SbSha', 'SbDee', 'PjSha', 'PjDee', 'SbMx1', 'PjMx1') AND (b.plan_start_age + 10) >= 70 THEN 'matcon'
        WHEN sf.nwsfu IN ('BfPur', 'BfMx1') AND (b.plan_start_age + 10) >= 60 THEN 'matcon'
        WHEN sf.nwsfu IN ('PwDom', 'PrDom', 'PrwMx') AND (b.plan_start_age + 10) >= 80 THEN 'matcon'
        WHEN sf.nwsfu IN ('UplCe', 'OCLow', 'SbLow', 'SbSha', 'SbDee', 'PjSha', 'PjDee', 'SbMx1', 'PjMx1') AND (b.plan_start_age + 10) >= 35 AND (b.plan_start_age + 10) < 70 THEN 'youngcon'
        WHEN sf.nwsfu IN ('BfPur', 'BfMx1') AND (b.plan_start_age + 10) >= 35 AND (b.plan_start_age + 10) < 60 THEN 'youngcon'
        WHEN sf.nwsfu IN ('PwDom', 'PrDom', 'PrwMx') AND (b.plan_start_age + 10) >= 35 AND (b.plan_start_age + 10) < 80 THEN 'youngcon'
        ELSE NULL
    END AS moopend

   FROM --planningcompositeinventory f LEFT JOIN
     baseclass b -- ON f.ogc_fid = b.fid
     LEFT JOIN HT_Grow gr ON b.fid = gr.fid
     LEFT JOIN nwsfu sf ON b.fid = sf.fid
     LEFT JOIN lut_sfu s ON s.sfu = sf.nwsfu
     LEFT JOIN lut_elceco_caribou2 c ON b.ecopri = c.elceco
     LEFT JOIN lut_ecogrp g ON b.ecopri = g.ecosite
;
