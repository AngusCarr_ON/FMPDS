#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  toGDBLYR.py
#
#  Copyright 2019 CARRAN <CARRAN@ON34C02585177>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


# This pesky thing just takes a GPKG and puts out an identically named
# GDB. If it thinks it is a FMP|AR|AWS (based on layer names), it will
# add a layer file from the archive

# This requires arcpy... so warn about slowness...

#from tape_handler import tape_handler
import os
import sys
#import datetime

#import shutil, os, sys, datetime
#import glob
#from os import path

print "This may cause a pause while we load arcpy...."

#import socket
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import stack.fs_context

from arcpy_support.make_rel_lyr import copyFeatureClassesFromGPKG #(src,dst,overwrite=False)
from arcpy_support.make_rel_lyr import MakeRelativeLYR #(fmu, product, year, FMUnumb, AOIyrBase, PS, outpath, lyrpath, gdbpath, lyr_src=None, IsDraft=None):
from arcpy_support.make_rel_lyr import validate_lyr_src # (lyr_src=None,year=None,product=None,submission_type=None):

#print "This may cause a pause while we load arcpy...."


class toGDBLyr( ):
	i = None
	o = None
	def __init__ (self, input=None, output=None):
		if input is None:
			#return 1
			raise(IOError,"No input file specified")
		elif os.path.exists(input):
			self.i = input
		#else:
		#	return 1

		if output is None:
			self.o = input.lower().replace('.gpkg','.gdb')
		else:
			self.o = output

		return None

	def go(self):
		print ("Starting things {0} => {1}".format(self.i,self.o))
		try:
			copyFeatureClassesFromGPKG(src=self.i,dst=self.o,overwrite=True)
			pass
		except RuntimeError as e:
			if "NotInitialized" in str(e):
				print "This is Where I get to laugh at you for using an ESRI "
				print "\tproduct... The license server is likely down..."
			else:
				print e
			return None

		print "Getting FS Context"
		c = stack.fs_context.fs_context(self.i).context
		print ("Context: \n===========\n\t{0}\n=============\n".format( "\n\t".join( ["{0}: {1}".format(k.rjust(20),v) for k,v in  c.iteritems() ] ) )  )

		if 'product' in c.keys():
			#print ("MakeRelativeLYR(fmu,product,year,FMUNumb,AOIYRBase,PS,lyrpath,gdbpath,lyr_src,IsDraft")
	#		fmu,
	#		product,
	#		year,
			c['FMUnumb'] = c["fmu_id"]
			c['AOIyrBase'] = c["planyear"]
			c['PS'] = c['id']
			#c['outpath'] =
			c['lyrsrc'] = validate_lyr_src( **c )
			c['lyrpath'] = self.o.lower().replace('.gdb','.lyr')
			c['gdbpath'] = self.o
			MakeRelativeLYR(**c)
			#print c

"""
    "fmu": "Kenora",
    "fmu_id": "644",
    "id": 24743,
    "name": "metadata",
    "path": "{fmu}\\{product}\\{year}",
    "phase": "",
    "planyear": "2012",
    "product": "AR",
    "submission_max_year": 2030,
    "submission_min_year": 2017,
    "submission_type": "AR_2018",
    "year": "2017"
"""



def main(args):
	#print args

	#location of -i
	try:
		i = [ n for n in range(1,len(args)) if args[n] == '-i' ][0]
	except IndexError:
		i = 0
	try:
		inputfile = args[i+1]
	except IndexError:
		return 1

	outputfile = None

	try:
		o = [ n for n in range(1,len(args)) if args[n] == '-o' ][0]
		#[ n for n in args[1:] if n == '-o' ][0]
		outputfile = args[o + 1]
	except IndexError:
		outputfile = None
	#print "inputfile  {0}".format(inputfile )
	#print "outputfile {0}".format(outputfile)

	toGDBLyr( input=inputfile, output=outputfile).go()

	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
