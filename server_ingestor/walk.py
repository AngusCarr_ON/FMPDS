# Import the os module, for the os.walk function
import os,sys
from ingestor import ingestor
from username import run_sql_onevalue
import logging

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from multiprocessing import Pool


def walk_to_ingest(rootDir=r'f:\\',DropSubmissionSchema = True):
	logging.basicConfig(filename=os.path.join(rootDir,'logfile.txt'),format='%(asctime)s %(message)s',level=logging.DEBUG)

	# Set the directory you want to start from
	#rootDir = r'z:\FMP\\'
	#rootDir = r'f:\\'

	# We may be handed an fi_submission file, in which case, split it off
	#print "walk_to_ingest got (%s)"%(rootDir)
	if rootDir.endswith(r'.txt'):
		rootDir = os.path.split(rootDir)[0]
	#else:
	#	print "but it doesn't end in .txt"


	#Error-check - is the path a directory that ends in '\' or '/'
	if not rootDir.endswith(('/','\\')):
		rootDir += '\\'

	dirdict = dict()
	pathdict = dict()
	for dirName, subdirList, fileList in os.walk(rootDir):
		#print('Found directory: %s' % dirName)
		#remove initial part


		subtxt = None # Submission text file contents
		try:
			for fname in fileList:
				if fname.lower().startswith('fi_submission_') and fname.lower().endswith('.txt'):
					#print "FOUND %s in %s"%(fname,dirName)
					subtxtF = fname
					#subtxtF = ([ fname for fname in fileList if fname.lower().startswith('fi_submission_') and fname.lower().endswith('.txt') ])[0]
					with open(os.path.join(dirName,fname)) as text:
						subtxt = [ l for l in [ (line.split('#')[0]).strip() for line in text ] if len(l) > 0]


					#Grab the fmu product year
					sdsplit = os.path.join(dirName,subtxtF).split("\\")# dirName.split("\\") #subdirname.split("\\")
					fmu = ''
					product = ''
					year = ''
					#print sdsplit
					#try:
					fmu,product,year = sdsplit[-4:-1]
					print "%s :  %s"%( [ fmu,product,year ] , dirName )

					#Add submission number to the file...
					subtxt.append('submission='+fname.lower().replace('fi_submission_','').replace('.txt',''))
					pathdict [ fmu,product,year ] = os.path.join(dirName,subtxtF)
					dirdict  [ fmu,product,year ] = subtxt

					del subdirList[:]
		except IndexError as e:
			pass



		#del subdirList[:] #Don't descend further if the elements are correct

		#if fmu=='Whitefeather': print "Found %s, %s, %s: %s"%(fmu,product,year,subtxt)
#	for k,v in dirdict.iteritems():
#		submission_number = [ tag.split('=')[1] for tag in v if tag.split('=')[0].lower() == 'submission' ][0]
#		print "Evaluating: %s ==> %s from %s"%(str(k),submission_number,pathdict[k])


	for k,v in dirdict.iteritems():
		#print "%s : %s"%(k,v)
		#print  "%s"%(str(k))
		if v is None: continue
		if len(v) == 0: continue

		#Report on submission number... we need it anyway... This is a duplication. It is in the subtxt anyway
		try:
			submission_number = int([ tag.split('=')[1] for tag in v if tag.split('=')[0].lower() == 'submission' ][0])
		except ValueError as e:
			print "Skipping %s\n"%(str(k))
			continue
		#print "Ready to Check: %s ==> %s"%(str(k),submission_number)
		#print "Ready to Ingest %s"%(str(k))

		ingest = False
		#Test to see if this entry has been entered in the consolidated DB - if not, ingest
		# k is ( fmu_product_year )
		#sql = "SELECT date_part('epoch',consolidation_time) FROM fmp.submissions WHERE fmu = '%s' AND product = '%s' AND year = %s "%( k[0], k[1].lower(), k[2] )
		sql = "SELECT date_part('epoch',consolidation_time) FROM fmp.submissions WHERE fi_portal_id = %s"%(submission_number)
		try:
			consolidation_time = run_sql_onevalue(sql)
		except TypeError: #A TypeError means no record is being returned, so... ingest away!
			print "\nNo Record returned - assume no previous ingestion of this submission ==> %s"%(submission_number)
			consolidation_time = None

		if consolidation_time is None:
			print "\nConsolidation time is None - assume no previous ingestion of this submission ==> %s"%(submission_number)
			ingest = True
		elif consolidation_time < os.path.getmtime(pathdict[k])  : # Is the metadata file newer than the DB time?
			print "\nMetadata file newer than database record for submission %s"%(submission_number)
			ingest = True

		# Is the consolidation_date in the submission metadata? If not, ingest
		# Is the consolidation_date older that the submission metadata file date? If so, ingest
		# Is the consolidation_date newer than the DB version? - If so, ingest
		# Else skip this one.
		#Does it already report success?
		success_code = run_sql_onevalue("SELECT consolidation_success FROM fmp.submissions WHERE fi_portal_id = %s"%(submission_number))
		if success_code != 'success':
			print "\nFound a failed record in the submissions table (%s)... Correcting..."%(submission_number)
			ingest = True


		#Debugging restriction...
		#if  k[0] != 'Dryden' :   ingest = False

		#if k[1].lower() != 'fmp' : ingest = False
		#if k[0].lower() != 'kenora' : ingest=False


		if ingest:
			#(fmu_product_year,metadata,path_to_metadata)
			try:
				print "\n".join(["" for n in range(1,10)])
				print "=".join(["" for n in range(1,40)])
				i = ingestor(k,v,pathdict[k],DropSubmissionSchema=DropSubmissionSchema)
				i.go()
				run_sql_onevalue("UPDATE fmp.submissions SET consolidation_success = 'success' WHERE fi_portal_id = %s"%(submission_number))
				print "=".join(["" for n in range(1,40)])
			except Exception as e:
				print e
				run_sql_onevalue("UPDATE fmp.submissions SET consolidation_success = '%s' WHERE fi_portal_id = %s"%(str.replace(str(e),"'"," "),submission_number))
				print "!".join(["" for n in range(1,40)])


#print "================="
#print dirdict['Whitefeather','AWS','2013']
#    for fname in fileList:
#        pass
		#print('\t%s' % fname)



if __name__ == "__main__":
	try:
		logging.basicConfig(filename=r"d:\NWR_Admin\src\FMPDS\consolidator.log",format='%(asctime)s %(message)s',level=logging.DEBUG)
	except IOError:
		logging.basicConfig(filename=r"c:\src\FMPDS\server_ingestor\consolidator.log",format='%(asctime)s %(message)s',level=logging.DEBUG)

	# Rename paths as same as "basepaths" used by checker...
	from stack.paths import basepaths as rootDirList

	# Or here are examples to test too:
	rootDirList = []
	#rootDirList.append(r"\\lrcpthbafp00002\NWSI-Extended\Extended_Data\Forest\FMP\Wabigoon\AR\2014")
	#rootDirList.append(r"\\lrcpsoprfp00001\GIS-DATA\WORK-DATA\FMPDS\Martel\FMP\2011")
	rootDirList.append(r"\\cihs.ad.gov.on.ca\mnrf\Groups\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP\Black_Spruce\AR\2011")

	use_multiprocess =  False
	multirun = False

	if use_multiprocess and multirun :
		while True:
			from multiprocessing import Pool
			procs = Pool(len(rootDirList))
			procs.map(walk_to_ingest,rootDirList)
	elif use_multiprocess :
		from multiprocessing import Pool
		procs = Pool(len(rootDirList))
		procs.map(walk_to_ingest,rootDirList)
	else:
		#single-process version:
		for p in rootDirList:
			walk_to_ingest(rootDir = p,DropSubmissionSchema = True)

