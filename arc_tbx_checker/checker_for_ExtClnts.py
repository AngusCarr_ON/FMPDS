import sys
import os

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import stack.paths
from checker.initialize_checker import submission_checker
from stack.static_db import static_db
import arcpy

productDict = { 'Annual Report':'ar',
                'Annual Work Schedule':'aws',
                'Base Model Inventory':'bmi',
                'Enhanced Forest Resource Inventory':'efri',
                'Forest Management Plan':'fmp',
                'Operational Planning Inventory':'opi',
                'Planning Composite Inventory':'pci'
                }
reportDetailList = [ "None", "Short", "Long" ]


inputgdb = arcpy.GetParameterAsText(0) # eg. C:\GIS\FMP\Lakehead\AWS_2018.gdb

product = productDict[str(arcpy.GetParameterAsText(1))] # 'AR', 'AWS', 'BMI', etc.

# forest = str(arcpy.GetParameterAsText(1))
# if forest in ['None','']: forest = static_db.keys()[0]  #'Algonquin_Park'

# layer = arcpy.GetParameterAsText(2) # optional - if specified, it will only check that specific table. eg. 'pci'   If None, it will check all the layers.

year = int(arcpy.GetParameterAsText(2)) # eg. 2018.  This is required to find the correct tech spec version.

fmpstartyear = arcpy.GetParameterAsText(3)

# fmpmVersion = arcpy.GetParameterAsText(5) ## Mandatory. either "2017 (Current)" or "2009 (Old)"

reportDetail = arcpy.GetParameterAsText(4) ## Mandatory. 3 options: "None", "Short", "Long"



if len(fmpstartyear) > 0: ## if fmpstartyear is filled out
    fmpstartyear = int(fmpstartyear)
else:
    fmpstartyear = None


arcpy.AddMessage('- Running the validation check on %s %s on %s...'%(year, product, inputgdb))

s = submission_checker(reportDetail=reportDetail)
jsonfilename= s.get_jsonfilename(product,year)

s.go(jsonfilename,tablefilename=inputgdb,year=year,planyear=fmpstartyear)


import webbrowser
output = os.path.join(s.outputFilePath,s.outputFileName)
webbrowser.open(output)






