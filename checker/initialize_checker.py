#ar_checker.py

import glob
import time
import os
import sys
import datetime
import json

from inspect import currentframe, getframeinfo

from filechecker import tablechecker
from filechecker import fileloader
from filechecker import jsonloader

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from stack.log_functions import createLogFile, createRowValidationsFile, write2LogFile
from stack.log_functions import logfile
from stack.log_functions import pyprint

from stack.fi_portal import findSubID
import stack.css
from stack.pg_db import pg_db
from stack.choose_a_tech_spec import choose_a_tech_spec

class submission_checker ():
	product = None
	forest = None
	year = None
	fmpmVersion = None
	reportDetail = None
	today = datetime.date.today()
	sumTableDict = {}
	projectPathDirname = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	outputFileName = ""
	outputFilePath = ""

	fmuID = None
	subID = None

	SRName = None

	def __init__(self,reportDetail=None,errors_only=False,product=None):
		self.log = logfile()
		#pyprint("Location of the log file is %s."%self.log.filename)
		self.reportDetail = reportDetail
		self.errors_only = errors_only

		if product is not None :
			self.product = product


	def do_all(self,basepaths,forests,products,years,debug=False,doOnlyTable=None):
#		for b in basepaths:
#			#Grab the list of forests
		#if True: # debug: #	print ("def do_all({b},{f},{p},{y},debug={d},doOnlyTable={t})".format(b=basepaths,f=forests,p=products,y=years,d=debug,t=doOnlyTable) )

		list_of_outputs = [ ]
		for f in forests:
			for p in products:
				for y in years:
					# Nullify autodetected things
					self.fmuID = None
					self.subID = None

					#Run each forest/product/year...
					pyprint("Attempting to check %s, %s, %s"%(f,p,y))
					self.do_one (basepaths,forest=f,product=p,year=y,fmpstartyear=None,fmpmVersion=None,debug=debug,doOnlyTable=doOnlyTable)

					list_of_outputs.append(self.outputFilePath)
		return list_of_outputs

	def get_jsonfilename(self,product=None,year=None,user_spec=None):
		if user_spec is not None:
			year_rep = '2009'
			if user_spec.lower() == "2017 (current)" :
				year_rep = '2017'
			elif user_spec.lower() == "2009 (old)" :
				year_rep = '2009'
			elif str(user_spec)[0:4].isdigit(): # in [ '2009', '2018' ]:
				year_rep = str(user_spec)[0:4]
			elif str(user_spec)[-4:].isdigit():
				year_rep = str(user_spec)[-4:]

			if product is None:
				product = user_spec.replace(year_rep,'').strip('_')
			return choose_a_tech_spec(product,year_rep)
		elif product is not None and year is not None :
			return choose_a_tech_spec(product,year)
		else:
			return choose_a_tech_spec(product,year)
		#p = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0],'tech_spec',year_rep,product+'.json')
		#return p if (os.path.exists(p) or os.path.exists(p.replace('.json','.mm')) ) else None

	def do_one(self,basepaths,forest,product,year,fmpstartyear,fmpmVersion, reportDetail=None, doOnlyTable=None,debug=False):
		""" This version used to be called do_all in the tbx version.
		It has different inputs, and so has been renamed.
		It also shows the evolution to work as an arc Toolbox thingummy,
		so it bears study...
		"""
		self.product = product
		self.forest = forest
		self.year = year
		self.fmpmVersion = fmpmVersion

		self.today = datetime.date.today()
		self.sumTableDict = {}
		projectPathDirname = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		#Harold added member variables
		self.outputFileName = ""
		self.outputFilePath = ""

		#initialize log file and delete old one if it exists
		self.log = createLogFile()
		#pyprint("Location of the log file is %s."%self.log)
		self.rowvalidationmsg = createLogFile("rowValidationsMsgs.txt")

		# looking for the json file for the particular product:
		jsonfilename = self.get_jsonfilename(self.product,self.year,user_spec=self.fmpmVersion)
		j = jsonloader(jsonfilename)

		#if j.get_filename():
		#	pyprint("- Using JSON file %s"%(j.get_filename()))

		# defining the plan start year in this priority order - 1. user input, 2. live pg_db, 3. offline pg_db
		if fmpstartyear is not None:
			planyear = fmpstartyear
		#	pyprint("- User specified plan start year = " + str(planyear))
		elif pg_db().get_plan_year(forest,year) :
			planyear = pg_db().get_plan_year(forest,year)
		#	pyprint("- " + str(planyear) + " is the plan start year used for the current validation.")
		else:
			planyear = 0 # So that subtraction works...
		#	pyprint("- " + str(planyear) + " is the plan start year used for the current validation.")

		for b in basepaths:
			# searching through the list of basepaths to find the matching forest, product and year...
			product_path = { 'AR':'AR', 'AWS':'AWS', 'FMP':'FMP', 'BMI':'FMP' , 'OPI':'FMP', 'PCI':'FMP'}[product.upper()]

			self.parentFolder = os.path.join(b,forest,product_path,str(year))

			if not self.fmuID: self.fmuID = pg_db().FMUCodeConverter(self.forest)
			if not self.subID: self.subID = findSubID(self.parentFolder)

			filename = "MU%s_%s_%s_%s_%s.html"%(self.fmuID,product,year,self.subID,self.today)

			outputfilename = os.path.join(b,forest,product_path ,str(year),filename)
			outFilePath = os.path.join(b,forest,product_path ,str(year))
			self.outputFilePath = outFilePath
			self.outputFileName = filename

			# Detect file type and name by what exists:
			listOfPossibleDataFiles = [ os.path.join(self.parentFolder,'_data','FMP_Schema.gdb'), os.path.join(self.parentFolder,'data.gpkg'), os.path.join(self.parentFolder,'e00'), os.path.join(self.parentFolder,'shp') ]
			try:
				dataname = [ fn for fn in listOfPossibleDataFiles if os.path.exists(fn) ][0]
				#pyprint("- Checking data files: " + dataname)
				self.go(jsonfilename,dataname,outputfilename=outputfilename,year=year, planyear = planyear, doOnlyTable=doOnlyTable)
			except IndexError:
				pyprint("Could not find a data file for {f}".format(f=outputfilename))
				pass

	def go(self,jsonfilename,tablefilename,outputfilename=None,spatialdataformat=None,year=0, planyear=None, doOnlyTable=None, layer=None, debug=False,geojson_outputfile=None, raw_json_outputfile=None, csv_outputfile=None):
		if spatialdataformat is not None:
			raise DeprecationWarning("Use of spatialdataformat parameter is deprecated.")

		user_specified_layer = layer
#		if layer is not None:
#			user_specified_layer = layer

		print "\tsubmissionchecker.go Operating on %s==>%s, %s test"%(tablefilename, user_specified_layer if user_specified_layer else "All Layers Specified", doOnlyTable)
		verbose = False
		starttime = time.clock()
		#if verbose: pyprint ("Starttime: %s"%(starttime) )

		j = jsonloader(jsonfilename)

		if outputfilename is None:
			outputfilename = os.path.join(os.path.split(os.path.split(tablefilename)[0])[0],'tablereport.html')

		self.outputFileName = outputfilename
		# pyprint("Output filename = %s"%(self.outputFileName) )
		save_spatial = False

		if geojson_outputfile is not None :
			if os.path.exists(geojson_outputfile):
				os.remove(geojson_outputfile)
			shp_error = list()
			save_spatial = True

		if raw_json_outputfile is not None :
			if os.path.exists(raw_json_outputfile):
				os.remove(raw_json_outputfile)
			shp_error = list()
			save_spatial = True

		if csv_outputfile is not None :
			if os.path.exists(csv_outputfile):
				os.remove(csv_outputfile)
			shp_error = list()
			save_spatial = True


		########
		# Open header text file as a log file
		validation = logfile(self.outputFileName,use_datestamps=False)
		validation.write(stack.css.htmlStyle)
		validation.write('<title>Testing %s</title>\n'%(tablefilename))
		#f.write('<META HTTP-EQUIV="refresh" CONTENT="100">\n')
		validation.write('</head><body>\n')

		validation.write('<h1>%s Report on %s</h1>\n'%(self.product,tablefilename))

		validation.write('''<h2>Report Summary</h2>
			<table id="t01">
			  <tr><td>Submission Type:</td> <td>%s</td>	</tr>
			  <tr><td>Submission Year:</td> <td>%s</td>	</tr>
			  <tr><td>Plan Start Year:</td> <td>%s</td>	</tr>
			  <tr><td>MU Number:</td>	   <td>%s</td>	</tr>
			  <tr><td>MU Name:</td>		 <td>%s</td>	</tr>
			  <tr><td>Date Reviewed:</td>   <td>%s</td>	</tr>
			  <tr><td>Submission ID:</td>   <td>%s</td>	</tr>
			</table> <br>
			'''%(self.product,year,planyear, self.fmuID, self.forest,self.today,self.subID)  )

		# Gather the list of layers from the input container and compare to the layer name passed in - limit search if necessary
		f = fileloader(filename = tablefilename,  openTable=False )
		#print "============================================"
		#print f.listLayers(filename = tablefilename)
		listoflayernames = f.listLayers(filename = tablefilename)

		if user_specified_layer is not None and str(user_specified_layer).upper() in [ l.upper() for l in listoflayernames ]:
			listoflayernames = [ user_specified_layer ]

		f.close()

		if listoflayernames is None:
			listoflayernames = []

		put_summary_header = False

		#print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		#print ( "listoflayernames: {0}".format(listoflayernames) )

		status_number = 0

		# Loop therough the tables in the json and produce output...
		for tabletype in j.getTableList():
			#pyprint(j.getTableNameList(tabletype))
			#pyprint("Should we do a %s test? ==> %s"%(tabletype, j.getTableNameList(tabletype) ) )
			if doOnlyTable is not None and doOnlyTable.upper() not in j.getTableNameList(tabletype):
				#pyprint("Nope...")
				continue
			#else:
			#	pyprint("Yup")

			for layer in listoflayernames:
				#Test if tablefilename contains long name of table... If not try the shortname, then the FI Portal name
				# The shortname may be present in the name of another table so if the layer name is the names list but the shortname is in the layer name, then we should assume not (???)
				# This is not a FIM spec name, so it should not come up...
				if layer.upper() in j.getTableNameList(tabletype) \
					or j.findTable(tabletype)['shortname'].upper() == (layer[8:11]).upper() \
					or ( j.findTable(tabletype)['shortname'].upper() in layer.upper() and layer.upper() not in [ n.upper() for n in j.getTableList() ]   )\
					or layer == user_specified_layer :

					pyprint("Performing %s test on %s, layer %s"%(tabletype,tablefilename, layer ) )

					try:
						with tablechecker(jsonfilename=jsonfilename,
							tablefilename=tablefilename,
							tabletype=tabletype,year=year, layer=layer , planyear=planyear) as t:

							tablevalidation_output = t.validateTable_HTMLfragment(debug=(self.reportDetail=='Debug'))
							rowvalidation_rulesummary = t.validateRows_HTMLruleSummary(limit=self.reportDetail,debug=(self.reportDetail=='Debug'))

							rowvalidation_output = t.validateRows_HTMLfragment(limit=self.reportDetail,debug=(self.reportDetail=='Debug'), errors_only = self.errors_only, save_spatial=save_spatial )

							###########
							# Perform summary, write to file
							if self.reportDetail in [ "Long", None, "None", "Short", "Full", "Debug","Context" ]:
								if not put_summary_header :
									validation.write(t.validate_summary_HTML_Header())
									validation.accumulate(t.validate_summary_HTML_Footer() )
									put_summary_header = True
								validation.write( t.validate_summary_HTMLfragment() )

							##########
							# Write table validations to file h
							if self.reportDetail in [ "Long", None, "None", "Full", "Debug","Context" ] :
								validation.accumulatelines(['<h1>%s test for %s</title>\n'%(tabletype,tablefilename),
									tablevalidation_output,
									"\n"
									])

							###########
							# Perform Row validations, Get list of parts, write to file
							if self.reportDetail in [   "Debug"] :  # "Full" ,
								validation.accumulatelines( rowvalidation_rulesummary )
							if self.reportDetail in [ "Short","Long", "Full" ,"Debug","Context"] :
								validation.accumulatelines( rowvalidation_output )

							if t.status == 2 and status_number == 0:
								status_number = 2
							elif t.status == 1:
								status_number = 1

							############
							# Save Shapes to list for file
							if save_spatial:
								for s,c,e in t.shplist:
									f = { "type":"Feature"}
									f['geometry'] = dict()
									f['geometry']['type'] = 'Point'
									x_str,y_str = s.strip(')').split('(')[1].split(' ')
									f['geometry']['coordinates'] = ( float(x_str) , float(y_str) )
									f['properties'] = dict()
									f['properties']['table']=tabletype
									f['properties']['stage']=c
									# f['properties']['error']=e.strip('\n')
									polyid,val,msg = e.strip('\n').replace('<tr><td>','').replace('</td></tr>','').split('</td><td>')
									f['properties']['polyid'] = polyid
									f['properties']['value']  = val
									f['properties']['msg']    = msg
									f['properties']['wkt']    = s
									f['properties']['x']      = x_str
									f['properties']['y']      = y_str
									shp_error.append(f)
									#del f
								if self.SRName is None:
									print ("Assigning all Shapes to {0}".format(t.SRName))
									self.SRName = t.SRName
									self.SRCode = t.SRCode
									self.SRAuth = t.SRAuth
							#
							############

							#pyprint ("\t>>>>> t.status_number is {0} -> submission status_number is {1}".format(t.status, status_number ))

					except AttributeError as e:
						# This is likely a failure to load the layer - E00 Layer not built...
						print e
						pass
					except RuntimeError as e:
						# This is a failure to load the file, methinks
						# fileloader will produce this error if the table cannot be loaded
						print e
						pass


		if geojson_outputfile is not None:
			if shp_error :
				shp_error_contents = { "type": "FeatureCollection"}
				if self.SRName is None:
					shp_error_contents['crs'] = { "type": "name","properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}}
				else:
					shp_error_contents['crs'] = { "type": "name","properties": {"name": "urn:ogc:def:crs:{0}::{1}".format(self.SRAuth, self.SRCode)}}

				shp_error_contents['features']= shp_error

				with  open( geojson_outputfile, 'w' ) as a :
					json.dump( shp_error_contents, a ,sort_keys=False, indent=2)

		if raw_json_outputfile is not None:
			if shp_error :
				print("doing some fun excitement with raw_json_outputfile now")
				json_errors = [ f['properties'] for f in shp_error ]
				with  open( raw_json_outputfile, 'w' ) as a :
					json.dump( json_errors, fp=a ,sort_keys=False, indent=2)

		if csv_outputfile is not None:
			if shp_error :
				print("doing some fun excitement with csv_outputfile now")
				json_errors = [ f['properties'] for f in shp_error ]
				with  open( csv_outputfile, 'w' ) as a :
					a.write("OBJECTID,polyid,table,stage,value,msg,x,y,wkt\n")
					i=1
					for l in json_errors:
						l['msg'] = l['msg'].replace(',',' ')
						l['OBJECTID'] = i
						a.write('{OBJECTID},"{polyid}","{table}",{stage},"{value}","{msg}",{x},{y},"{wkt}"\n'.format(**l))
						i += 1

					#json.dump( json_errors, fp=a ,sort_keys=False, indent=2)


		# Now finish off the summary table and dump the rest of the data
		validation.writeaccumulate()
		validation.write('\n\n</body></html>\n')


		###############################################################
		# END OF TESTING CODE
		###############################################################
		endtime = time.clock()
		if verbose: pyprint ("Endtime: %s"%(endtime))
		if verbose: pyprint ("Seconds Elapsed: %s"%(endtime - starttime))
		if verbose: pyprint ("Minutes Elapsed: %s"%(round(( endtime - starttime ) /60,0)))

		print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
		print ("\n\n\tChecker is complete, with status {0}\n\n".format( [ "Valid - Green","Invalid - Red","Valid with Concerns - Amber"][status_number] ))
		print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
		return status_number






def bob():
	test()

def test():
	products = [ 'AR' ]
	years = [ 2015 ]
	forests = [ 'Hearst','Algonquin_Park', 'Kenora' , 'Ottawa_Valley', 'Bancroft_Minden' , 'Black_Spruce', 'Sapawe', 'Crossroute', 'Dog_River_Matawin', 'Algoma','Northshore','Sudbury','Nipissing','Nagagami','Timiskaming','Martel','Gordon_Cosens','Romeo_Malette','Pineland','Spanish','Abitibi_River','Temagami','White_River']
	forests = pg_db().get_list_of_fmus()
	# Spanish, Pineland, Romeo, Gordon Cosens, Temagami, White River
	#forests = ['Dog_River_Matawin']
	#forests = ['Temagami']
	#forests = ['Hearst']
	#forests = [ 'Timiskaming' ]
	# \\lrcpsoprfp00001\GIS-DATA\WORK-DATA\FMPDS
	#forests = 'Nipissing' ] #  SIP has a missing field (TRTCAT1)
	basepaths = [r'S:\ROD\Northwestregion\RIAU\Data\Extended_Data\Forest\FMP',r'\\lrcpptboshfs001\MNR_SR_External\FOREST_MGMT\FMPDS', r'\\lrcpsoprfp00001\GIS-DATA\WORK-DATA\FMPDS']
	#basepaths = [r'c:\GIS\FMP']

	s = submission_checker()
	#s.do_all(basepaths,forests,products,years)
	s.do_one(basepaths,
#		forest='Lakehead', product='AWS', year=2018, fmpstartyear=2011,fmpmVersion='2017 (current)')
		forest='Algonquin_Park', product='AR', year=2016, fmpstartyear=2011,fmpmVersion='2009 (old)')
	#ReportDetail could be "None", "Short", "Long"

#For Testing in ArcPy:
"""
c:\Python27\ArcGIS10.3\python.exe
import sys
sys.path.insert(0, '..')
import initialize_checker
reload( initialize_checker )
initialize_checker.test()
"""

if __name__ == "__main__":
	test()
