__author__ = 'doranha'
#this module contains all the code to parse out and assemble spp comp string

import arcpy
from itertools import groupby
import gc
import operator

from spcomp import spcomp_ignore_whitespace
from spcomp import combine_two_stories, spcomp_as_text

def processSPP(occlo,ucclo,ospp,uspp):
	""" Reads through OSPCOMP and then USPCOMP to blend them by occlo and ucclo
	Returns a compstring floated to 100% cover value
	"""
	return spcomp_as_text( combine_two_stories(spcomp_ignore_whitespace(ospp),spcomp_ignore_whitespace(uspp),occlo,ucclo) )


def main():
    #do nothing
    overStory = 'PW 80PR 20'
    underStory = 'SB 30BW 20PW 20Mr 10Bf 10Mh 10'
    #print str(blendSPPAmount(80,0.3,20,0.7))
    #print str(blendSPPAmount(0,0.3,30,0.7))
    result = processSPP(0.3,0.7,overStory,underStory)
    print(result)

if __name__ == '__main__':
    try:
        main()
    except Exception, e:
        import gc
        gc.collect()
        import traceback
        arcpy.AddError(traceback.format_exc())

