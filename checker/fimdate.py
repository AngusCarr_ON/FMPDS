
import datetime
import time

class fimdate(datetime.date):
	"""Generates and compares dates"""
	date = None
	fimdateformat = "%Y%b%d"
	def __new__(cls,fimdatecode):
		try:
			v= time.strptime(fimdatecode,cls.fimdateformat)
			return datetime.date.__new__(cls,v[0],v[1],v[2])
		except ValueError :
			return None
		except TypeError :
			return None


if __name__ == "__main__":
	v =  time.strptime("2010MAR01","%Y%b%d")[0:3]
	print v
	print datetime.date(v[0],v[1],v[2])
	print fimdate("2010MAR01")
	print fimdate("2010MAR01") < fimdate("2015AUG23")
	print fimdate("2010MAR01") > fimdate("2015AUG23")

	row = { 'PITCLOSE':'2015AUG01' }
	stmt = """ [ 'PITCLOSE must be in fiscal year 2015' for row in [row] if row['PITCLOSE'] is not None and row['PITCLOSE'].strip() <> ''  and  ( fimdate( row['PITCLOSE'] )   <  fimdate(str(2015      )  + "MAR01" ) ) and ( fimdate( row['PITCLOSE'] )   >  fimdate(str(2015 + 1) + "MAR31" ) ) ] """

	print eval ( stmt )


	print ( fimdate ("A Decidedly Invlasi Code") )
