#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  checker.py
#

########################################################################
# This is the command-line version of the checker.
# It can be run in a bat file, or straight-up.
#
###############################
# Running under system python, or from the OSGEO4W prompt:
# python checker.py

###############################
# Using the python installed in QGIS:
# ps. It uses short paths because of command line path handling.
# C:\PROGRA~1\QGIS2~1.18\OSGeo4W.bat C:\PROGRA~1\QGIS2~1.18\bin\python c:\src\FMPDS\checker\checker.py

###############################
# Running under OSGeo4W python, but from a command prompt that normally uses a different python, or no python:
# c:\OSGeo4W\OSGeo4W.bat c:\OSGEO4W\bin\python c:\src\FMPDS\checker\checker.py

###############################
# As you would use it in geany:
# cmd /c "c:\OSGeo4W\OSGeo4W.bat c:\OSGEO4W\bin\python %f"
########################################################################

###############################
# Or from Arc's py
# c:\python27\ArcGIS10.4\python cmd_checker.py -j ..\tech_spec\2017\aws.json -i G:\FOREST_MGMT\FMPDS\Ottawa_Valley\AWS\2018\_data\FMP_Schema.gdb -y 2018 -f Ottawa_Valley -p AWS12:30 PM

########################################################################
# Examples of parameters:
#  python cmd_checker.py -j ..\tech_spec\2009\fmp.json -i C:\GIS\Black_Sturgeon_FMP_2011\E00\mu815_11aoc01.e00 -y 2011
#  python cmd_checker.py --forest=Lake_Nipigon --product=FMP --year=2011
########################################################################

import os
import sys
import getopt

import datetime

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import initialize_checker

from stack.pg_db import pg_db
from stack.static_db import static_db
import stack.paths   #.basepaths
from stack.fi_portal import findSubID

def main(argv):
	#for arg in argv: print arg

	try:
		opts, args = getopt.getopt(argv, "do:i:t:b:f:p:y:j:l:n:rws:e",
								   ["help", "debug","outputfile=",
									"input_data_file=","tablename=",
									"basepath=","forest=","product=",
									"year=", "jsonfilename=", "layer=",
									"planyear=", "reportDetail=",
									"web-browser","submission=","errors-only"
									])
	except getopt.GetoptError:
		pass
		usage()
		sys.exit(2)

	#Set defaults for conductor:
	#cond.debug = False
	#cond.inputfile = r'..\tech_spec\2009\FMP.mm'
	#cond.outputfile = 'clean.mm'
	#cond.overwrite = False
	#cond.verbose = True

	# Local options for convenience
	debug = False
	outputfile = "clean.html"
	tablename = None
	basepath = None
	forest = None
	product = None
	product_star = [ 'fmp','ar','aws' ]
	year = None
	year_star = [ str(y) for y in range(2009,2030) ]
	inputfile = None
	layer=None
	planyear = None
	jsonfilename = None
	reportDetail = "Short"
	web_browser = None
	subID = None
	errors_only = False

	for opt, arg in opts:
		if opt in ["--help"]:
			usage()
			sys.exit()
		elif opt in ["-d", "--debug"]:
			debug = True
		elif opt in ["-o", "--outputfile"]:
			outputfile = arg
		elif opt in ["-i", "--input_data_file"]:
			print 'Using -i %s'%(arg)
			inputfile = arg
		elif opt in ["-t", "--tablename"]:
			tablename = arg
		elif opt in ["-b", "--basepath" ]:
			basepath = arg
		elif opt in ["-f", "--forest"]:
			if arg in [ "NW" , "NE", "S" ]:
				forest = ",".join([ k for k,v  in static_db.iteritems() if v[2] == 'NW' ])
			forest = arg
		elif opt in ["-p", "--product"]:
			product = arg
		elif opt in ["-y", "--year"]:
			year = arg
		elif opt in ["-j", "--jsonfilename"]:
			jsonfilename = arg
		elif opt in ["-l", "--layer"]:
			layer = arg
		elif opt in ["-n", "--planyear"]:
			planyear = arg
		elif opt in ["-r"]:
			if reportDetail == "Long":
				reportDetail = "Full"
			else :
				reportDetail = "Long"
		elif opt in ["--reportDetail"]:
			reportDetail = arg
		elif opt in ["-w", "--web-browser"]:
			web_browser = True
		elif opt in ["-s", "--submission"]:
			subID = arg
		elif opt in ["-e","--errors-only"]:
			errors_only = True

	# Check to make sure we have forest-product-year triplet or inputfile
#	if input_data_file is None and None in [ forest, product, year ] :
#		print "Could not find input_data_file or forest-product-year=%s"%([ forest, product, year ])
#		usage()
#		return -1

	# Now run the checker...
	s = initialize_checker.submission_checker(reportDetail=reportDetail,errors_only=errors_only)

	if jsonfilename is None and year is not None and product is not None :
		jsonfilename= s.get_jsonfilename(product.split(',')[0],year.split(',')[0])

	if inputfile and jsonfilename and year:
		# Use the input_data_file, etc to run a test on this one container...
		#def go(self,jsonfilename,tablefilename,outputfilename=None,spatialdataformat=None,year=0)

		if forest :
			f = forest.split(',')[0]
			s.forest = f.replace("_"," ")

			fmuID = pg_db().FMUCodeConverter(forest)
			if not fmuID: fmuID = "UNK"

			s.fmuID = fmuID

			if year is not None and planyear is None:
				planyear = pg_db().get_plan_year(f,year.split(',')[0])

		if product :
			s.product = product.split(",")[0].upper()
		else:
			product="UNK"

		parentFolder = os.path.dirname(inputfile)
		if subID is None :
			subID = "UNK"
			if (findSubID(parentFolder)):
				subID = findSubID(parentFolder)
			elif (findSubID(os.path.dirname(parentFolder))):
				parentFolder = os.path.dirname(parentFolder)
				subID = findSubID(parentFolder)
		s.subID = subID

		# If -o not spec'd, use -i ..\*.html
		if outputfile == 'clean.html' :
			outputfile = os.path.join(parentFolder,"MU%s_%s_%s_%s_%s.html"%(fmuID,product,year,str(subID),datetime.date.today()))

		s.go(jsonfilename,tablefilename=inputfile,outputfilename=outputfile,year=year,planyear=planyear,doOnlyTable=tablename,layer=layer)
		if web_browser:
			import webbrowser
			#webbrowser.open_new_tab("file://"+s.outputfile.replace("\\","/"))
			webbrowser.open(s.outputfile)

	#elif not ( None in [ forest, product, year ] ):
	else:
		if basepath is None:
			basepaths = stack.paths.basepaths
		else:
			basepaths = basepath.split(',')

		if product :
			products = product.split(',')
		else:
			products = product_star

		if year :
			years = year.split(',')
		else:
			years = year_star

		if forest is not None:
			forests = forest.split(',')
		else:
			forests = pg_db().get_list_of_fmus()

		print "s.do_all(\n\t%s,\n\t%s,\n\t%s,\n\t%s)"%(basepaths,forests,products,years)
		html_list = s.do_all(basepaths,forests,products,years,debug=debug,doOnlyTable=tablename)

		import webbrowser
		if web_browser:
			for h in html_list:
				webbrowser.open(h)

	return 0

def usage():
	print """
python checker.py
	[ basepath=Base_path_for_fmp_tree ]
	[ forest=fmu_name ]
	[ product=AWS|AR|FMP ]
	[ year=4_DIGIT_YEAR ]
	[ planyear=4_DIGIT_YEAR ]
	[ input_data_file=full_data_path_and_filename ]
	[ tablename=specific_table_to_process ]
	[ outputfile=filename ]
	[ reportDetail=Short|Long|Full ]
	[


All options are optional, but the checker will not run without either a
	forest-product-year set (using default MNRF base_path )
or
	basepath-forest-product-year set
or
	input_data_file


Basepath is the root of a filesystem set up as described in the checker
user manual.

forest is the fmu name, with underscores instead of spaces. These are
as used in the MNRF filesystem and FMPDS. If your structure is different
then you will need to use the input_data_file.

product is one of AWS, AR, and FMP.

year is the 4-digit year of the submission, specifying the beginning of
government fiscal year in which the report is submitted.

input_data_file is the full path to the data container.
	* If it is a multi-featureClass container ( GDB, geopackage ), tables
		will be found within
	* If it is a directory, spatial files ( SHP, Coverages ) will be
		found within
	* If it is a single featureClass file ( SHP ), it will be checked
		directly

outputfile is used to place the output. It may default to the same
	location as the data file, or to the directory you are in.

tablename, if specified, restricts checking to only that table from the json

layer, if specified, will override the name of the layer used for the test

reportDetail, if specified will use a long or full version of the report,
	as requested. Defaults to short.

Initially, this is only tested within the OSGEO framework. It ought to
work in the arcpy framework, but that is a secondary test, as there is
already an interface for that framework.


"""

if __name__ == '__main__':
	import sys
	print sys.argv
	if len(sys.argv) == 1:
		#Let's assume we are running this from the IDE, shall we?
		testpath = r'\GIS\Black_Sturgeon_FMP_2011\MU815_2011_FMP_P1.gpkg'
		#testpath = r'C:\GIS\FMP\Lake_Nipigon\EFRI\2021\Draft_LakeNipigon_silvatech_submission_07_11_2016.gdb'
		if os.path.exists(testpath):
			sys.exit(main(['-i',testpath,'-y','2011','-p','FMP','-o','test.html','-j',r'..\tech_spec\2009\fmp.json', '-l' , 'pcm', '-t', 'PlanningComposite' ] ))
#			sys.exit(main(['-i',testpath,'-y','2021','-p','PCI','-o','test.html','-j',r'..\tech_spec\2017\PCI.json', '-l' , 'Forest_Polygons','-t','pci'] ))
	sys.exit(main(sys.argv[1:]))
