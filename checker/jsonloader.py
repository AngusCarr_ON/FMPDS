#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  jsonloader.py
#
import json

class jsonloader():
	json = None
	filename = None
	includes_circularity_checker = list()

	def __init__(self,jsonfilename=None,ignore_non_tables=True):
		if jsonfilename: self.getjson(jsonfilename,ignore_non_tables=ignore_non_tables)

#	def __getattr__(self,attr):
#		return self.json[attr]

	def getjson(self,jsonfilename,ignore_non_tables=True, do_includes=True):
		# print "jsonfilename: %s"%(jsonfilename)
		try:
			json_data=open(jsonfilename)
			self.json = json.load(json_data)
			json_data.close()
			self.filename = jsonfilename
		except IOError:
			self.json = self.cheat_and_get_json_from_mindmap(jsonfilename)
		except ValueError:
			self.json = self.cheat_and_get_json_from_mindmap(jsonfilename)

		if ignore_non_tables:
			listToRemove = [ i for i,n in enumerate(self.json)  if not  isinstance(n,dict) ]
			listToRemove.extend([   i for i,n in enumerate(self.json) if isinstance(n,dict)  if 'shortname' not in n.keys() ] )
			for n in sorted(listToRemove,reverse=True):
				del self.json[n]

		if do_includes:	self.do_fields_include()

		return self.json



	def get_filename(self):
		"""
		Gets the filename actually used...
		"""
		return self.filename

	def findTable(self,tabletype):
		"""Finds a table in the top level by name or shortname """
		for n in self.json:
			try:
				if n['name'].upper() == tabletype.upper(): return n
			except KeyError:
				pass
			try:
				if n['shortname'].upper() == tabletype.upper(): return n
			except KeyError:
				pass
		return None

	def getTableList(self):
		"""Returns a list of table names
			- if they are not named, will raise KeyError
		"""
		try:
			return [n['name'] for n in self.json ]
		except KeyError as e:
			retlist = []
			for n in self.json :
				try:
					retlist.append(n['name'])
				except KeyError as e:
					pass
			return retlist

	def getTableListShort(self):
		try:  #The quick method when there are no issues...
			return [n['shortname'] for n in self.json ]
		except KeyError as e:
			retlist = []
			for n in self.json :
				try:
					retlist.append(n['shortname'])
				except KeyError as e:
					#print "missing a shortname in %s! \n%s"%(n['name'],e)
					#print str(n) #self.dumpkey(n)
					pass
			return retlist

	def getTableListMatchingString(self,layername):
		"""Finds a table in the top level by name or shortname that is
		a substring of the layername

		This is anticipated to help figure out the table name for a layer
		"""
		return [ t for t in self.getTableList() + self.getTableListShort() if t in layername.upper() ]


	def getTableNameList(self,tabletype):
		t = self.findTable(tabletype)

		retlist = [ ]
		if 'name' in t.keys():
			retlist.append(t['name'].upper())
		if 'shortname' in t.keys():
			retlist.append(t['shortname'].upper())
		return retlist

#	def dumpkey(self,key):
#		return str(key)

	def cheat_and_get_json_from_mindmap(self,jsonfilename):
		"""
		When the json is not available, is the same file with a .mm extension?

		If so, use some magic to extract the json from the mindmap
		"""

		import sys,os
		sys.path.insert(0, os.path.abspath('..') )
		from mm_processor.conductor import conductor
		c = conductor()

		c.inputfile = os.path.splitext( jsonfilename  )[0] + '.mm'
		c.debug = False
		c.getmm()
		c.convert_mm2json()

		if c.json:
			self.filename = c.inputfile

		return c.json

	def do_fields_include(self,table_name=None):
		"""Does the fields inclusion from the mindmap twice - once for the
		fields_include tag and once for the fields_include_no_validation tag
		"""

		if table_name is not None:
			raise(DeprecationWarning,'\n=====================================\njsonloader.do_fields_include() table_name parameter deprecated\n=====================================')

		self.do_fields_include_action(include_validation=True)
		self.do_fields_include_action(include_validation=False)


	def do_fields_include_action(self,include_validation=True):
		"""
		The fields_include tag is intended to read the json through
		and duplicate any tests from one table into another.

		The fields_include: attribute is a list of dictionaries:
			fields_include:
				table name
					table fields required
		The fields_include attribute is a list of tables with implied '*' for table fields required
			fields_include
				table name

		If you pass in the parameter table_name, we are only going to use
		that table. The notion is to allow recursion. This function uses
		the presence of the 'fields_include' tag to indicte whether it
		should use the table as a src. If not, it calls itself to make
		the src read includes first.

		This requires some kind of
		cross-check that we are not too deep in recursion...

		This is the active component of the fields inclusion, renamed from
		the original name so that the original name could be re-used to
		run it twice with and without the validation part.
		"""
		src_tbl_spec = None


		######################
		# Adapt to allow "no validation" lists
		######################
		if include_validation:
			field_to_include_list_name = 'fields_include'
		else:
			field_to_include_list_name = 'fields_include_no_validation'
		field_to_include_dict_name = field_to_include_list_name+':'

		for t in self.getTableList():
			dst_tab = self.findTable(t)
#			if table_name is not None and t != table_name: continue

			#print "Doing the includes for {table_name}".format(table_name = t )
			src_tbl_spec = dict()

			############################################################
			# Include fields including validation rules
			##############
			#Make sure we have fields to include
			if field_to_include_list_name in dst_tab.keys() :
				# we think we have a list of tables...
				#Make sure the mm2json function got the fields_include as
				# list
				if isinstance(dst_tab[field_to_include_list_name],str) or isinstance(dst_tab[field_to_include_list_name],unicode):
					dst_tab[field_to_include_list_name] = [ dst_tab[field_to_include_list_name] ]


				# Load the dictionary for the include (src_tbl_spec) with '*'
					for k in dst_tab[field_to_include_list_name]:
						if isinstance(k,str) or isinstance(k,unicode):
							src_tbl_spec[k] = '*'
						elif isinstance(k,list):
							for kk in k:
								src_tbl_spec[kk] = '*'
			elif field_to_include_dict_name in dst_tab.keys() :
				for k,v in dst_tab[field_to_include_dict_name].iteritems() :
					#print "\t",k
					if k == 'name': continue

					if isinstance(v,str) or isinstance(v,unicode):
						src_tbl_spec[k] = [ v ]
					else :
						src_tbl_spec[k] = v
			else:
				continue


			#Ensure there is a place to put the fields
			if 'fields' not in dst_tab.keys() :
				dst_tab['fields'] = list()


			for src,src_f in src_tbl_spec.iteritems() :
				# Has the src_t got includes? if so, recurse...
				src_t =  self.findTable(src)
				#if 'fields_include' in src_t.keys() or 'fields_include:' in src_t.keys() :
				#	do_fields_include(src)

				if src_f == ['*']:
					fields_to_add = [ f.copy() for f in src_t['fields'] ]
				else:
					fields_to_add = [ f.copy() for f in src_t['fields'] if f['name'] in src_f ]

				if not include_validation:
					for f in fields_to_add:
						del(f['Validation:'])

				dst_tab['fields'].extend(fields_to_add)

			# now remove the fields_include from this table so we can all move on...
			if field_to_include_list_name in dst_tab.keys():
				dst_tab[field_to_include_list_name] = dst_tab[field_to_include_list_name]
				del dst_tab[field_to_include_list_name]
			elif field_to_include_dict_name in dst_tab.keys() :
				dst_tab[field_to_include_dict_name] = dst_tab[field_to_include_dict_name]
				del dst_tab[field_to_include_dict_name]

	def getMandatoryFieldList (self,tablejson=None):
		"""
		Detects the mandatory tag on a field and uses it to identify fields that match.
		Paste This into the field:
mandatory
    The presence of this attribute in the file structure of the layer is mandatory.
		"""
		mandatoryFieldList = [] #list()

		if tablejson is None:
			try:
				tablejson = self.tablejson
			except AttributeError:
				tablejson = self.findTable(self.getTableListShort()[0])

		try:
			fieldsjson=tablejson['fields']
		except KeyError as e:
			#This table has no fields - ignore
			return []

		#DONE - when one field, comes back as the field entry, not a list (AR SGR Update table)
		if not isinstance(fieldsjson,list):
			fieldsjson = [ fieldsjson ]

		for field in fieldsjson:
			try:
				if 'mandatory' in [n.lower() for n in field.keys() ]:
					mlist = field['mandatory']
					if mlist is None:
						mlist = []
					elif not isinstance (mlist, list):
						mlist = [ mlist ]
					for m in mlist:
						if 'The presence of this attribute in the file structure of the layer is mandatory' in m:
							mandatoryFieldList.append(field['name'])
			except KeyError as e:
				#No Validations worth bothering with
				return mandatoryFieldList
		return mandatoryFieldList

	def getFieldType (self, tablejson, fieldname):
		try:
			fieldsjson=tablejson['fields']
		except KeyError as e:
			#This table has no fields - ignore
			return []

		#DONE - when one field, comes back as the field entry, not a list (AR SGR Update table)
		if not isinstance(fieldsjson,list):
			fieldsjson = [ fieldsjson ]

		try:
			field = fieldsjson[fieldname]
		except KeyError as e:
			return None

		return field['type'].lower()

	def close(self):
		pass

	def __exit__(self, *err):
		pass

	def __enter__(self, *err):
		return self

	def __del__(self):
		pass

def main(args):
	j = jsonloader(None)
	j.json=[{'name':'BigTable', 'shortname':'BIG'}]
	print (j.findTable('BIG')) # Should work
	print (j.findTable('BAD')) # Should fail quietly, having returned None

	print (j.getTableList())
	print (j.getTableListShort())

	# Now test for missing .json, but has .mm...
	j=jsonloader(r"U:\src\FMPDS\tech_spec\2009\ar.json")
	print (j.getTableList())


	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
