

def enum(**enums):
    return type('Enum', (), enums)

class FMPError(Exception):
    e = enum(

        #SPCOMP Error codes
        SPCompNot6='SPCOMP not multiples of 6 characters',
        SPCompGT10SPC='SPCOMP cannot have more than 10 species',
        SPCompHasDuplicateSPC='SPCOMP cannot have duplicate species occurrences',
        SPCompSumNot100='SPCOMP does not add to 100',
        WGNotFirstInSPComp='LEADSPC/WG must be first in SPCOMP',
        WARN_SPCompValNotAscending='WARNING SPCOMP values are not in Descending Order',

        # Testing attributes are in alist - typetester.py
        FieldValueNotAcceptable='Not an acceptable FIM att ==> ',

        #Angus
        SFU_not_calculated='Cannot calculate sfu with this row',
        SPCompHasNonNumericComposition='SPComp must have numeric codes for species occurrence',

        )
    def __init__(self,value,msg):
        self.value = value
        self.msg = msg
    def __str__(self):
        try:
            return self.value + ": " + str(self.msg)
        except IndexError as e:
            pass

if __name__ == "__main__":
    print FMPError
    print 'Garnet, does not make sense to run FMPerror.py in this manner'
    print 'Run checks instead...'
    #print FMPError.e
    #print FMPError.e.SPCompNot6
    try:
        raise FMPError(FMPError.e.SPCompNot6,"Just a test...")
    except FMPError as e:
        print e
